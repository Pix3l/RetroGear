#include "rg.h"
#include "graphic.h"
#include "font.h"

void RG_input(void)
{
	while (SDL_PollEvent(&input_event))
	{
        //Let's quit!
        if (input_event.key.keysym.sym == SDLK_ESCAPE || input_event.type == SDL_QUIT)
        {
            quit = 1;
        }

        /***********************************************************
         * Virtual gamepad handling
         **********************************************************/

        if(input_event.type == SDL_KEYDOWN  || input_event.type == SDL_KEYUP)
        {
            SDLKey key_button = input_event.key.keysym.sym;    //Keyboard key symbol

            if(key_button)
            {
                return key_button;
            }
        }
        else if( input_event.type == SDL_MOUSEMOTION || input_event.type == SDL_MOUSEBUTTONUP  || input_event.type == SDL_MOUSEBUTTONDOWN )
        {
            /**
             * Virtual mouse handling
             **/
            handle_mouse(&input_event);
        }
    }
}

void RG_app(void)
{
    Uint32 key = SDL_MapRGB( screen->format, 255, 0, 255 );
    RG_icons = loadImage("data/rg_icons.bmp", key);
    RG_font_init();

    static Uint32 then = 0;
    static unsigned int dtime = 0;
    int t=0;

    //main game loop
    while(!quit)
    {
        unsigned int maxl = 256;
        Uint32 now = SDL_GetTicks();
        dtime += now - then;
        t = now;
        then = now;

        while (--maxl && dtime >= (1000/60))
        {
            RG_input();
            RG_updateWidgets();

            now = SDL_GetTicks();
            dtime += now - then - (1000/60);
            then = now;
        }

        RG_Render();

        SDL_Delay(1);
    }
    
    cleanWidgets();
}

/**
 * Check intersection between two rectangular areas
 * Return 0 if there's a collision, 1 otherwise.
 * 
 * @param int x1, y1, w1, h1
 * 		Coordinates and size of the first area
 * 
 * @param int x2, y2, w2, h2
 * 		TCoordinates and size of the second area
 * 
 * @return 0
 * 		No collision
 * 		   1
 * 		Collision detected
 **/
int rectCollision(int x1, int y1, int w1, int h1, 
                  int x2, int y2, int w2, int h2)
{
    if (y1+h1 <= y2) return 0;
    if (y1 >= y2+h2) return 0;
    if (x1+w1 <= x2) return 0;
    if (x1 >= x2+w2) return 0;

    return 1;
}
