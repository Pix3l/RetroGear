#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "rg.h"
#include "font.h"
#include "bit.h"
#include "graphic.h"

//Private functions
char *RG_substring(char *string, int position, int length);
void RG_appendChar(char *str, char ch, int length);

void RG_font_init()
{
	//Init the built-in font
	/*
	RG_font=SDL_CreateRGBSurface(SDL_HWSURFACE,FONT_ARRAY_W,FONT_ARRAY_H,32,0,0,0,0);
	
	int x, y;
	for (y = 0; y < FONT_ARRAY_H; y ++)
	{
		for (x = 0; x < FONT_ARRAY_W; x ++)
		{
			if(fontArray[y][x]=='x')
			{
				put_pixel(RG_font, x, y, 0xff0000);
			}
			
			//~ printf("%c",fontArray[y][x]);
		}
		
		//~ printf("\n");
	}
	*/

    // Load bmp font on a surface
    Uint32 key = SDL_MapRGB( screen->format, 255, 0, 255 );

    //Default transparency colour: Acid green 0x00FF00
    if(RG_font == NULL)
    {
        RG_font=loadImage("data/nesfont.bmp", key);
    }

    // Create a temporary surface for single bmp font character
    RG_tmpfont=SDL_CreateRGBSurface(SDL_HWSURFACE,RG_CHAR_W,RG_CHAR_H,32,0,0,0,0);
    if(RG_tmpfont == NULL)
    {
        fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
        exit(1);
    }
}

char *RG_substring(char *string, int position, int length)
{
   char *pointer;
   int c;
 
   pointer = malloc(length+1);
 
   if (pointer == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(EXIT_FAILURE);
   }
 
   for (c = 0 ; c < position -1 ; c++) 
      string++; 
 
   for (c = 0 ; c < length ; c++)
   {
      *(pointer+c) = *string;      
      string++;   
   }
 
   *(pointer+c) = '\0';
 
   return pointer;
}

void RG_appendChar(char *str, char ch, int length)
{
    size_t len = strlen(str);
    
    if((len+1) <= length)
    {
        str[len] = ch;
        str[len+1] = '\0';
    }

	#ifdef DEBUG
		printf("debug %s\n", str);
	#endif

}

void RG_textInput(char *string, int length)
{
	SDL_EnableUNICODE( SDL_ENABLE );

	//~ if( event.type == SDL_KEYDOWN )
	//~ {
		//Append a character if available
		if( ( input_event.key.keysym.unicode >= (Uint16)' ' ) && ( input_event.key.keysym.unicode <= (Uint16)'~' ) )
		{
			//Append the character
			RG_appendChar(string, (char)input_event.key.keysym.unicode, length);
		}

		RG_eraseInput(string);

		//Stop key repeating!
		input_event.key.keysym.unicode = 0;
	//~ }
}

void RG_putChar(int dest_x, int dest_y, int asciicode, int color)
{
	//Start read the font matrix from the correct ascii character
	int start_x = ((asciicode-32)*8)-1;
	
	int x, y;
	for (y = 0; y < 8; y++)
	{
		for (x = 0; x < 8; x++)
		{
			if(fontArray[y][start_x+x]=='x')
			{
				put_pixel(screen, dest_x+x, dest_y+y, color);
			}
			//This put a background color...
			//~ else
			//~ {
				//~ put_pixel(dest, dest_x+x, dest_y+y, 0x00ff00);
			//~ }
		}
	}
}

void RG_putString(int dest_x, int dest_y, char *text, int color)
{
	//For the entire string
	int i=0;
	while(text[i] != '\0')
	{
		RG_putChar(dest_x, dest_y, text[i], color);
		dest_x+=RG_CHAR_W;
		i++;
	}
}

void RG_eraseInput(char *string)
{
	//If backspace was pressed and the string isn't blank
	if( ( input_event.key.keysym.sym == SDLK_BACKSPACE ) && ( strlen(string) != 0 ) )
	{
		//Remove a character from the end
		//~ str.erase( str.length() - 1 );
		char *tmp;
		
		tmp = RG_substring(string, 0, strlen(string)-1);
		strcpy(string, tmp);
		
		input_event.key.keysym.sym = 0;
	}
}
