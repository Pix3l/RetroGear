/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  bit.c - Bitwise operation functions               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _BIT_H
#define _BIT_H

int hasFlag(int flag_store, unsigned char flags);
void setFlag(int *flag_store, unsigned char flags);
void unsetFlag(int *flag_store, unsigned char flags);
void toggleFlag(int *flag_store, unsigned char flags);


#endif
