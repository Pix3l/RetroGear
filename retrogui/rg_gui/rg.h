#ifndef _RG_H
#define _RG_H

#include "widget.h"
#include "mouse.h"
#include "palette.h"

int quit;

SDL_Event input_event;

struct _RG
{
    RG_widget *ptr;	//Current Widget pointed
} RG;

void RG_input(void);
void RG_app(void);
int rectCollision(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2);

#endif
