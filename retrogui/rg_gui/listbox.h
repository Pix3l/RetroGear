#ifndef _RG_LISTBOX_H
#define _RG_LISTBOX_H

#include <SDL/SDL.h>
#include "widget.h"
#include "listitem.h"

#define DEFAULT_LISTBOX_W 100

typedef struct RG_listbox
{
	int x, y;
	char flag_clicked;
	char label[20];
	int num_items;
	int curr_item;	//selected item
	RG_listitem *items;
} RG_listbox;

RG_widget *RG_createListBox (short x, short y, RG_widget *par, char *label);
void RG_addListBoxItem(RG_widget *pwig, char *text);
void RG_drawListBox(RG_widget *pwig);

#endif
