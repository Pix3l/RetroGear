#ifndef _RG_WIDGET_H
#define _RG_WIDGET_H

#include <SDL/SDL.h>

SDL_Surface *RG_icons;
#define RG_ICON_W 8
#define RG_ICON_H 8

typedef enum
{
    WINDOW, BUTTON, TEXFIELD, NUMFIELD, LIST, COMBO, LISTITEM
} WIDGET_TYPE;

typedef enum
{
    MINIMIZE, MAXIMIZE, CLOSE, DRAG
} WIDGET_FLAGS;

typedef struct _RG_widget
{
    short x, y, w, h;
    short id;
    short flags;
    unsigned short depth;
    void (*update)(struct _RG_widget *pwig);	//Update widget status
    void (*callback)();		//Let the widget do something
    void (*draw)(struct _RG_widget *pwig);
    //~ void (*destroy)(struct _RG_widget *pwig);
    short type;
    void *data;	/// Pointer to more object data...
   	struct _RG_widget *parent;
   	struct _RG_widget *next;
   	struct _RG_widget *children;
} RG_widget;

RG_widget *headWidget, *tailWidget, *previousWidget;

RG_widget *RG_createWidget (short x, short y, short w, short h, void *data, void(*callback)(), void(*draw)());
RG_widget *RG_createChildWidget (short x, short y, short w, short h, void *data, void(*callback)(), void(*draw)(), RG_widget *parent);
void RG_updateWidgets();
void swapWidget(RG_widget *widget);
void cleanWidgets();
void RG_drawWidget(int x, int y, int w, int h);
void RG_drawField(int x, int y, int w, int h);
void RG_Render(void);

#endif
