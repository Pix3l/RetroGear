#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "rg.h"
#include "widget.h"
#include "canvas.h"
#include "mouse.h"
#include "palette.h"
#include "bit.h"
#include "graphic.h"

//Private functions
void RG_doCanvas(RG_widget *pwig);
void RG_drawCanvas(RG_widget *pwig);

/**
 * Create a canvas element.
 * This element allows to realize drawings based on a grid pattern.
 * 
 * @param short x, short y
 * 		The canvas widget coordinate
 * 
 * @param short w, short h
 * 		The canvas widget width and height
 * 
 * @param int *cell_size
 * 		The size of cell on the grid for drawing.
 * 		Minimum size is 1 = 1 pixel
 * 
 * @param void(*draw)()
 * 		Overwrite default canvas drawing function.
 * 		If is set to NULL, default will be used.
 * 
 * @param RG_widget *par
 * 		The parent widget
 **/
//~ RG_widget *RG_createCanvas(short x, short y, short w, short h, unsigned int cell_size, int **field, int field_w, int field_h, void(*draw)(), RG_widget *par)
RG_widget *RG_createCanvas(short x, short y, short w, short h, unsigned int cell_size, int field_w, int field_h, void(*draw)(), RG_widget *par)
{
    RG_widget *widget;
    RG_canvas *data = (RG_canvas*)calloc(1, sizeof(RG_canvas));

	//Store button coordinates from parent Widget
	data->x = x;
	data->y = y;
	//~ data->field = field;

	//Initilize the field with white
	int i, j;
	for(i=0; i<10; i++)
	{
		for(j=0; j<10; j++)
		{
			data->field[i][j] = 0xffffff;
		}
	}	

	data->field_w = field_w;
	data->field_h = field_h;
	data->cell_size = cell_size;
	data->flag_showgrid = 1;	//Default is TRUE

	//~ int i, j;
	//~ for(i=0; i<field_h; i++)
	//~ {
		//~ for(j=0; j<field_w; j++)
		//~ {
			//~ printf("%d,", *(data->field+(i*field_w)+j) );
			//printf("%d,", *data->field);
		//~ }
		//~ printf("\n");
	//~ }

	//If draw is overwrittable
	void (*function) = (draw)? draw : RG_drawCanvas;

    //If has parent, create as a child of
    if(par)
    {
		widget = RG_createChildWidget(x, y, w, h, data, RG_doCanvas, RG_drawCanvas, par);
		widget->parent = par;
	}
	else
	{
		widget = RG_createWidget(x, y, w, h, data, RG_doCanvas, RG_drawCanvas);
		widget->parent = NULL;
	}
	
	widget->update = RG_doCanvas;

    return widget;
}

void RG_doCanvas(RG_widget *pwig)
{
	RG_canvas *data = (RG_canvas*)pwig->data;

	if(input_event.key.keysym.sym == SDLK_LEFT)
	{
		printf("left\n");
		data->offsetX-=data->cell_size;
		input_event.key.keysym.sym = 0;

		if(data->offsetX<0)
		{
			data->offsetX=0;
		}
	}
	
	int firstTileX = (data->offsetX / data->cell_size);
	int lastTileX = firstTileX + (pwig->w/data->cell_size);

	if(lastTileX<data->field_w)
	{
		if(input_event.key.keysym.sym == SDLK_RIGHT)
		{
			input_event.key.keysym.sym = 0;
			data->offsetX+=data->cell_size;
		}
	}

	//Up and down scroll
	if(input_event.key.keysym.sym == SDLK_UP)
	{
		data->offsetY-=data->cell_size;
		input_event.key.keysym.sym = 0;

		if(data->offsetY<0)
		{
			data->offsetY=0;
		}
	}

	int firstTileY = (data->offsetY / data->cell_size);
	int lastTileY = firstTileY + (pwig->h/data->cell_size);
	
	if(lastTileY<data->field_h)
	{
		if(input_event.key.keysym.sym == SDLK_DOWN)
		{
			input_event.key.keysym.sym = 0;
			data->offsetY+=data->cell_size;
		}
	}

	//Handling mouse clicks events
	if(rectCollision(RG_Mouse.x, RG_Mouse.y, 1, 1, pwig->x, pwig->y, pwig->w, pwig->h) && !hasFlag(RG_Mouse.flags, mouse_hook) )
	{
		int mouse_x = ((RG_Mouse.x-pwig->x+data->offsetX) / data->cell_size );
		int mouse_y = (RG_Mouse.y-pwig->y+data->offsetY) / data->cell_size;
		
		if(RG_Mouse.leftButton)
		{
			//~ printf("mouse pressed %d %d\n", mouse_x, mouse_y);
			data->field[mouse_y]
					   [mouse_x] = 0x000000;
		} 
		
		if(RG_Mouse.rightButton)
		{
			//~ printf("mouse pressed %d %d\n", mouse_x, mouse_y);
			data->field[mouse_y]
					   [mouse_x] = 0xffffff;
		}
	}

	//Align widget to their parent
	if(pwig->parent)
	{
		pwig->x = pwig->parent->x + data->x;
		pwig->y = pwig->parent->y + data->y;
	}
}

void RG_drawCanvas(RG_widget *pwig)
{
	//Retrive object data
	RG_canvas *data = (RG_canvas*)pwig->data;
	
	SDL_Rect rect;

	//Canvas body
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = pwig->w;
	rect.h = pwig->h;

    SDL_FillRect(screen, &rect, 0xAAAAAA);

	//White borders
	//Horizontal
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = pwig->w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Vertical
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = 1;
	rect.h = pwig->h-1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Gray borders
	//Horizontal
	rect.x = pwig->x+1;
	rect.y = pwig->y+pwig->h-1;
	rect.w = pwig->w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, WHITE);

	//Vertical
	rect.x = pwig->x+pwig->w-1;
	rect.y = pwig->y;
	rect.w = 1;
	rect.h = pwig->h-1;
	SDL_FillRect(screen, &rect, WHITE);

	//Draw the field content
	//~ int i, j;
	//~ for(i=0; i<data->field_h; i+=data->cell_size)
	//~ {
		//~ for(j=0; j<data->field_w; j+=data->cell_size)
		//~ {
			//~ if(*data->field!=0)
			//~ {
				//~ rect.x = (pwig->x+1)+j;
				//~ rect.y = (pwig->y+1)+i;
				//~ rect.w = data->cell_size;
				//~ rect.h = data->cell_size;
				//~ SDL_FillRect(screen, &rect, WHITE);
			//~ }
		//~ }
	//~ }

	/*
	int row = data->offsetY / data->cell_size;
	int col = data->offsetX / data->cell_size;
	
	int difference_x = data->offsetX % data->cell_size;
	int difference_y = data->offsetY % data->cell_size;

	int width = (pwig->w / data->cell_size)+1;
	int height = pwig->h / data->cell_size;

	int x, y;
	for (y = row; y < height; y++)
	{
		for (x = col; x < width; x++)
		{
			//~ if( *(data->field+((row+y)*data->field_w)+col+x) !=4 )
			if( (data->field+((row+y)*data->field_w)+col+x) !=4 )
			{
				rect.x = (pwig->x+1)+ (x * data->cell_size);
				rect.y = (pwig->y+1)+ (y * data->cell_size);
				rect.w = data->cell_size;
				rect.h = data->cell_size;

				SDL_FillRect(screen, &rect, WHITE);		
			}
		}
	}
	*/

    //Check if we need to enable scrolling
    int canvas_w = pwig->w;
    if(data->cell_size*data->field_w < pwig->w)
    {
		canvas_w = data->cell_size*data->field_w;
	}

    int canvas_h = pwig->h;
    if(data->cell_size*data->field_h < pwig->h)
    {
		canvas_h = data->cell_size*data->field_h;
	} 

    int firstTileX = (data->offsetX / data->cell_size);
    int lastTileX = firstTileX + (canvas_w/data->cell_size);
    int firstTileY = (data->offsetY / data->cell_size);
    int lastTileY = firstTileY + (canvas_h/data->cell_size);
    
	//~ printf("%d %d %d\n", lastTileX, data->field_w, firstTileX);    

    if(lastTileY>data->field_h)
    {
		lastTileY=data->field_h;
	}
    
	int x, y;
    for (y = firstTileY; y < lastTileY; y++)
    {
        for (x = firstTileX; x < lastTileX; x++)
        {
			int cell_x = (pwig->x+1)+(x * data->cell_size) - (firstTileX*data->cell_size);
			int cell_y = (pwig->y+1)+(y * data->cell_size) - (firstTileY*data->cell_size);
			
			//~ if(data->field[y][x]!=4)
            	drawFillRect(cell_x, cell_y , data->cell_size, data->cell_size, data->field[y][x]);
            	
            //~ printf("%d,", data->field[y][x]);
		}
		//~ printf("\n");
    }
    //~ printf("\n");

	//Show the grid
	if(data->flag_showgrid && data->cell_size>2)
	{
		SDL_Rect line;

		int i;
		for(i = 0; i < (pwig->w-data->cell_size); i+=data->cell_size)
		{
			
			line.x = (pwig->x+data->cell_size)+i;
			line.y = pwig->y+1;
			line.w = 1;
			line.h = pwig->h-2;
			
			SDL_FillRect(screen, &line, 0x6d6d6d);
		}
		
		for(i = 0; i < (pwig->h-data->cell_size); i+=data->cell_size)
		{
			
			line.x = pwig->x+1;
			line.y = (pwig->y+data->cell_size)+i;
			line.w = pwig->w-2;
			line.h = 1;

			SDL_FillRect(screen, &line, 0x6d6d6d);
		}
	}
}
