#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "widget.h"
#include "listitem.h"
#include "listbox.h"
#include "font.h"

/**
 * Align a ListItem to their parent widget
 * 
 * @param RG_widget *pwig
 * 		The parent widget
 * 
 * @param WIDGET_TYPE type
 * 		The type of the widget
 **/
void alignListItems(RG_widget *pwig, WIDGET_TYPE type)
{
	void *data = NULL;
	
	//Retrive object data by casting
    switch(type)
    {
        case LIST:
            *data = (RG_listbox*)pwig->data;
            break;
        case COMBO:
            //~ *data = (RG_combobox*)pwig->data;
            break;
    }

	int i;
	for(i = 0; i<data->num_items; i++)
	{
		data->items[i].x = 1+pwig->x;
		data->items[i].y = 1+pwig->y+(i*RG_CHAR_H);
	}
}
