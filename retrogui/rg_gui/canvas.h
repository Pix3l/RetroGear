#ifndef _RG_CANVAS_H
#define _RG_CANVAS_H

#include "widget.h"

typedef struct RG_canvas
{
	int x, y;			//This is necessary for align the child to their parent window
	int cell_size;		//Grid size 
	int field[100][100];
	int field_w, field_h;
	int offsetX, offsetY;
	int flag_showgrid;
	int flag_scrollable;
} RG_canvas;

//~ RG_widget *RG_createGrid (short x, short y, short w, short h, void(*callback)(), RG_widget *par, char *text);
//~ RG_widget *RG_createCanvas(short x, short y, short w, short h, unsigned int cell_size, int **field, int field_w, int field_h, void(*draw)(), RG_widget *par);
RG_widget *RG_createCanvas(short x, short y, short w, short h, unsigned int cell_size, int field_w, int field_h, void(*draw)(), RG_widget *par);

#endif
