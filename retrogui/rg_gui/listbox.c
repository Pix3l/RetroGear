#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "rg.h"
#include "widget.h"
#include "listbox.h"
#include "listitem.h"
#include "font.h"
#include "bit.h"
#include "graphic.h"

void RG_updateListBox(RG_widget *pwig);
void alignListItems(RG_widget *pwig);

/**
 * Create a simple Listbox
 * 
 * @param int x, int y
 * 		The button coordinate
 * 
 * @param RG_widget *par
 * 		The parent widget
 * 
 * @param char *text
 * 		String to put over the button.
 * 		Set NULL if you want to use an image
 **/
RG_widget *RG_createListBox (short x, short y, RG_widget *par, char *label)
{
    RG_listbox *data = (RG_listbox*)calloc(1, sizeof(RG_listbox));

	//Store listbox coordinates from parent Widget
	data->x = x;
	data->y = y;

    if(label)
    {
		strcpy(data->label, label);
    }

	RG_widget *widget;    
    
    //If has parent, create as a child of
    if(par)
    {
		widget = RG_createChildWidget (x, y, DEFAULT_LISTBOX_W, RG_CHAR_H, data , NULL, RG_drawListBox, par);
		widget->parent = par;
	}
	else
	{
		widget = RG_createWidget(x, y, DEFAULT_LISTBOX_W, RG_CHAR_H, data , NULL, RG_drawListBox);
		widget->parent = NULL;
		
	}

    widget->type = LIST;
    widget->update = RG_updateListBox;

    return widget;
}

void RG_updateListBox(RG_widget *pwig)
{
	RG_listbox *data = (RG_listbox*)pwig->data;

	if(RG_Mouse.leftButton)
	{
		int i;
		for(i = 0; i < data->num_items; ++i)
		{
			if(rectCollision(RG_Mouse.x, RG_Mouse.y, 1, 1, data->items[i].x, data->items[i].y, pwig->w, RG_CHAR_H) && !hasFlag(RG_Mouse.flags, mouse_hook) )
			{
				if(RG_Mouse.leftButton)
				{
					data->curr_item=i;
					setFlag(&RG_Mouse.flags, mouse_hook);
					//~ printf("%d %d\n", RG_Mouse.x, RG_Mouse.y);
					//~ printf("%d %d %s\n", data->items[i].x, data->items[i].y, data->items[i].text);
				}
			}
		}
	}

	if(!RG_Mouse.leftButton)
	{
		unsetFlag(&RG_Mouse.flags, mouse_hook);
	}

	pwig->x = pwig->parent->x + data->x;
	pwig->y = pwig->parent->y + data->y;

	alignListItems(pwig);

	if(pwig->callback)
	{
		pwig->callback();
	}
}

/**
 * Create an item for Listboxes, Comboboxes
 * 
 * @param char *text
 * 		String to put over the button.
 * 		Set NULL if you want to use an image
 **/
void RG_addListBoxItem(RG_widget *pwig, char *text)
{
	//Retrive object data
	RG_listbox *data = (RG_listbox*)pwig->data;
	
	RG_listitem *item = (RG_listitem*)calloc(1, sizeof(RG_listitem));
	strcpy(item->text,text);
	
	//Realloc Listbox items
	data->num_items++;
	data->items = (RG_listitem*) realloc(data->items, sizeof(RG_listitem) * data->num_items);
	memcpy(&data->items[data->num_items-1], item, sizeof(RG_listitem));
	
	free(item);
	
	alignListItems(pwig);
	
	//~ printf("%d\n", data->num_items);
	pwig->h = data->num_items*RG_CHAR_H;

}

void alignListItems(RG_widget *pwig)
{
	//Retrive object data
	RG_listbox *data = (RG_listbox*)pwig->data;

	int i;
	for(i = 0; i<data->num_items; i++)
	{
		data->items[i].x = 1+pwig->x;
		data->items[i].y = 1+pwig->y+(i*RG_CHAR_H);
	}
}

void RG_drawListBox(RG_widget *pwig)
{
	//Retrive object data
	RG_listbox *data = (RG_listbox*)pwig->data;

	//Draw the entire box
	RG_drawField(pwig->x, pwig->y, pwig->w, pwig->h+(data->num_items*RG_CHAR_H));
	
	//~ printf("%d %d\n", pwig->x, pwig->y);

	//Selection background
	SDL_Rect rect;
	rect.x = pwig->x+1;
	rect.y = pwig->y+1;
	rect.w = pwig->w-1;
	rect.h = RG_CHAR_H-1;

	int i;
	for(i = 0; i < data->num_items; i++)
	{
            if(data->curr_item == i)
            {
                    rect.y = pwig->y+(i*RG_CHAR_H);
                    SDL_FillRect(screen, &rect, 0x6d6d6d);
            }

            RG_putString(data->items[i].x, data->items[i].y, data->items[i].text, BLACK);
            //~ printf("%d %d %s\n", data->items[i].x, data->items[i].y, data->items[i].text);
	}

	//Draw label if set
	if(data->label)
	{
            RG_putString(pwig->x, pwig->y-(RG_CHAR_H/2), data->label, BLACK);
            rect.x = pwig->x+(strlen(data->label)*8);
	}
}
