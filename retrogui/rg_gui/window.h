#ifndef _RG_WINDOW_H
#define _RG_WINDOW_H

#include "widget.h"

#define TITLE_BG 0x525573
#define WINDOW_BG 0x2120AD

#define TITLE_SIZE 12

typedef struct RG_window
{
	int diff_x, diff_y;
	char flag_maximized;
	short _x, _y, _w, _h; // For backup use on maximize.
	char name[20];
} RG_window;

RG_widget *RG_createWindow (short x, short y, short w, short h, void(*callback) (), RG_widget *par, char *name);
void RG_doWindow(RG_widget *pwig);
void RG_drawWindow(RG_widget *pwig);

#endif
