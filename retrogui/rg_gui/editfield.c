#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "rg.h"
#include "widget.h"
#include "editfield.h"
#include "font.h"
#include "bit.h"
#include "graphic.h"

RG_widget *RG_createEditField (short x, short y, void(*callback)(), RG_widget *par, char *label, int type)
{
    RG_widget *widget;
    RG_editfield *data = (RG_editfield*)calloc(1, sizeof(RG_editfield));

	//Store button coordinates from parent Widget
	data->x = x;
	data->y = y;

    if(label)
    {
		strcpy(data->label, label);
    }
    else
    {
    	strcpy(data->label, "");
	}
	
	strcpy(data->store, "");
    
    //If has parent, create as a child of
    if(par)
    {
		widget = RG_createChildWidget (x, y, RG_CHAR_W*20, DEFAULT_EDITFIELD_H, data , callback, RG_drawEditField, par);
		widget->parent = par;
	}
	else
	{
		widget = RG_createWidget(x, y, RG_CHAR_W*20, DEFAULT_EDITFIELD_H, data , callback, RG_drawEditField);
		widget->parent = NULL;
	}
	
	widget->type = type;
	widget->update = RG_doEditField;

    return widget;
}

void RG_doEditField(RG_widget *pwig)
{
	RG_editfield *data = (RG_editfield*)pwig->data;

	//~ pwig->x += pwig->parent->x;
	//~ pwig->y += pwig->parent->y;

	if(rectCollision(RG_Mouse.x, RG_Mouse.y, 1, 1, pwig->x, pwig->y, pwig->w, pwig->h) && !hasFlag(RG_Mouse.flags, mouse_hook) )
	{
		if(RG_Mouse.leftButton)
		{
			RG_Mouse.leftButton=0;
			data->flag_active=1;
			//~ printf("field clicked!\n");
		}
	}
	else 	//Lost focus
	{
		if(RG_Mouse.leftButton && data->flag_active)
		{
			data->flag_active=0;
			unsetFlag(&RG_Mouse.flags, mouse_hook);

			if(pwig->callback)
			{
				pwig->callback();
			}
		}
	}

	if(data->flag_active==1)
	{
		RG_textInput(data->store, 100);
	}

	pwig->x = pwig->parent->x + data->x;
	pwig->y = pwig->parent->y + data->y;

}

void RG_drawEditField(RG_widget *pwig)
{
    //Retrive object data
    RG_editfield *data = (RG_editfield*)pwig->data;

    SDL_Rect rect;

    //Draw label if set
    if(data->label)
    {
        RG_putString(pwig->x, pwig->y-(RG_CHAR_H/2), data->label, BLACK);
        rect.x = pwig->x+(strlen(data->label)*8);
    }

    RG_drawField(pwig->x, pwig->y, pwig->w, pwig->h);

    //Draw text
    RG_putString(pwig->x+RG_CHAR_W, pwig->y+4, data->store, BLACK);

    if(data->flag_active==1)
    {
        int xpos = (pwig->x + RG_CHAR_W) + (strlen(data->store)*RG_CHAR_W);

        //Blink!
        //~ if((int)pobj->frame_index%2)
        //~ {
                //~ return;
        //~ }

        drawFillRect(2+xpos, pwig->y+2, RG_CHAR_W, RG_CHAR_H, BLACK);
    }
}
