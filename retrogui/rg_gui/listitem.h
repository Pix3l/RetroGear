#ifndef _RG_LISTITEM_H
#define _RG_LISTITEM_H

#include <SDL/SDL.h>
#include "widget.h"

#define LIST_ITEM_W 80
#define LIST_ITEM_H 20

typedef struct _RG_listitem
{
	int x, y, w, h;
	char text[20];
	struct _RG_listitem *next;
} RG_listitem;

#endif
