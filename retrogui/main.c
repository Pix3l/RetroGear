/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                      main.c - Main game source                     *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <stdio.h>
#include "rg_gui/rg.h"
#include "rg_gui/graphic.h"
#include "rg_gui/window.h"
#include "rg_gui/button.h"

#define SCREEN_WIDTH 320
#define SCREEN_HEIGHT 240

void testAlert(void)
{
    RG_createMessagebox(50, 50, NULL, NULL, "Alert", "This is a test alert", 0);
}

/**
 * Prima di liberare le risorse dal sistema bisogna terminare il ciclo
 * principale del programma
 **/
int main(int argc, char *argv[])
{
    // init video stuff
    if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) != 0)
    {
            fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
            exit(-1);
    }
    atexit(SDL_Quit);

    screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, SDL_HWSURFACE);
    if(screen == NULL)
    {
            fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
            exit(-1);
    }

    SDL_WM_SetCaption("RetroGui", "RetroGui");

    RG_widget *winz, *winz2, *list1;

    winz = RG_createWindow(10, 20, 170, 90, NULL, NULL, "winz");
    //~ RG_createButton(5, 65, NULL, winz, "Button");
    //~ RG_createButton(75, 65, NULL, winz, "x");
    //~ RG_createButton(105, 65, NULL, winz, "x");
    RG_createImageButton(5, 35, testAlert, winz, 3);

    //~ winz2 = RG_createWindow(20, 120, 170, 90, NULL, NULL, "winz2");
    //~ list1 = RG_createListBox (5, 20, winz2, "");
    //~ RG_addListBoxItem(list1, "rpg");
    //~ RG_addListBoxItem(list1, "shmup");

    //~ int i, j;
    //~ for(i=0; i<10; i++)
    //~ {
            //~ for(j=0; j<10; j++)
            //~ {
                    //~ field_test[i][j] = 0xff0000;
            //~ }
    //~ }
    //~ 
    //~ field_test[1][1] = 0xffffff;
    //~ field_test[2][6] = 0xffffff;
    //~ field_test[2][9] = 0xffffff;

    //~ RG_createCanvas(5, 20, 120, 60, 16, field_test, 10, 10, NULL, winz2);
    //~ RG_createCanvas(5, 20, 120, 60, 16, 10, 10, NULL, winz2);

    //Main game loop
    RG_app();

    //Free the memory from allocated resources
    //~ cleanUp();

    return 0;
}
