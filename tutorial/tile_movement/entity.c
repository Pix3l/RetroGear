/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    entity.c - Game objects type                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "entity.h"
#include "camera.h"
#include "game.h"
#include "tile.h"
#include "gfx.h"
#include "util.h"
#include "player.h"

/**
 * Crea un oggetto di tipo Entity e lo aggiunge all'ultimo creato, nel
 * caso non vi siano gia' delle entity, l'oggetto diventa la testa della
 * lista
 * 
 * @param int id
 * 		L'id della della entity, +1 rispetto ad una entity precedente se
 * 		esistente
 * 
 * @param int solid
 * 		Si specifica se la entity e' di tipo solido o meno
 * 
 * @param int x, y, w, h
 * 		Rispettivamente coordinate e dimensioni della entity
 * 
 * @param int lives
 * 		Numero di vite associate alla entity
 *
 * @param SDL_Surface *sprite
 * 		Puntatore ad una struttura SDL_Surface pre-esistente contentente
 * 		lo sprite della entity
 * 
 * @param float animation_speed
 * 		Velocita' d'animazione della entity
 * 
 * @param float speed
 * 		Velocita' della entity nel campo di gioco
 * 
 * @param float gravity
 * 		Fattore di gravita' per la entity nel campo di gioco
 * 
 * @param void (*update)()
 * 		Funzione di gestione della entity e dei suoi comportamenti
 * 
 **/
void createEntity(int type, int solid, int x, int y, int w, int h, 
				  int lives, SDL_Surface *sprite, float animation_speed,
				  float speed, float gravity, void (*update)())
{
	//calloc alloc and initialize the memory
	Entity *pnew = (Entity*) calloc(1, sizeof(Entity));

	if(headList == NULL)
	{
		pnew->id = 1;
		headList = pnew;
		tailList = headList;
		//~ printf("New entity HEAD %d\n", tailList->id);
	}
	else
	{
		pnew->id = tailList->id+1;
		tailList->next = pnew;
		tailList = pnew;
		//~ printf("New entity TAIL %d\n", tailList->id);
	}
	
	tailList->next = NULL;
	
	tailList->type = type;
	//~ tailList->solid = solid;
	tailList->x = x;
	tailList->y = y;
	tailList->w = w;
	tailList->h = h;
	tailList->lives = lives;
	tailList->sprite = sprite;
	tailList->animation_speed = animation_speed;
	tailList->speed = speed;
	tailList->gravity = gravity;

	if(update==NULL)
	{
	 quit=1;
	 printf("Function pointer not initialized!\n");
	}
	
	tailList->update = update;

	//Valori di default
	tailList->flag_active = 1;
	tailList->xstart = x;
	tailList->ystart = y;
	tailList->direction_x = -1;
	tailList->direction_y = 0;
	tailList->visible = 1;
}

/**
 * Set an entity coordinates to given two and set their facing direction
 * 
 * @param Entity *pobj
 * 		Pointer to entity object
 * @param int x
 * 		X coordinate for the entity
 * @param int y
 * 		Y coordinate for the entity
 * @param int dirx
 * 		Entity facing direction on horizontal axis
 * @param int diry
 * 		Entity facing direction on vertical axis
 **/
void setEntityPosition(Entity *pobj, int x, int y, int dirx, int diry)
{
	pobj->x = x;
	pobj->y = y;
	pobj->direction_x = dirx;
	pobj->direction_y = diry;
}

/**
 * Handles the life cycle of the entity by calling their update function
 * and delete those marked for destruction
 **/
void doEntities()
{
	Entity *entity = headList, *previous=NULL;
	while(entity!=NULL)
	{
		//Free memory from old entities
		if(entity->status==DESTROY)
		{
			if(headList==entity && tailList==entity)
			{
				free(entity);
				headList=tailList=NULL;
				return;
			}

			if(headList==entity)
			{
				#ifdef DESTROY_DBG
					printf("head!\n");
				#endif

				headList = entity->next;
				free(entity);
				entity = headList;
			}
			else if(tailList==entity)
			{

				#ifdef DESTROY_DBG
					printf("tail!\n");
				#endif

				previous->next=NULL;
				tailList=previous;
				free(entity);
				entity = tailList;
			}
			else
			{
				#ifdef DESTROY_DBG
					printf("Body! Entity is now ID %d\n", entity->id);
				#endif

				previous->next = entity->next;
				
				#ifdef DESTROY_DBG
					printf("Body! Previous next will be ID %d\n", entity->next->id);
				#endif
				
				free(entity);
				entity = previous->next;
			}
		}
		else 	//Just update their lifecycle
		{
			if(entity->flag_active==1)
			{
				entity->update(entity);
			}
		}

		previous = entity;
		entity = entity->next;
	}
}

void resetEntities()
{
	//We reset alives entities position to their start value before
	//restart the game
	if(Game.status==PREGAME)
	{
		Entity *pobj = headList;
		while(pobj!=NULL)
		{
			setEntityPosition(pobj, pobj->xstart, pobj->ystart, -1, 0);
			pobj=pobj->next;
		}
	}
}

/**
* Crea una nuova entity presente da mappa assegnandogli le coordinate
* X e Y rispettive e valori di default per i rimanenti
* 
* @param int id
* 		Numero identificativo della entity nella mappa
* 
* @param int x, int y
* 		Coordinate della entity nel livello di gioco.
*/
void createEntityFromMap(int id, int x, int y)
{
	switch(id)
	{
		case 2:	//Player
			setPlayerPosition(x, y);
		break;
	}
}

/**
 * Conta il numero di entity presenti in lista
 * 
 * @return int
 * 		Il numero di entity presenti
 **/
int countEntities()
{
	int i = 0;
	Entity *current = headList;
	while(current!=NULL)
	{
		i++;
		current=current->next;
	}
	
	return i;
}

/**
 * Count the number of entity of the specified type
 * 
 * @return int
 * 		The number of counted entities
 **/
int countTypeEntity(int type)
{
	unsigned int i = 0;
	
	Entity *current = headList;
	while(current!=NULL)
	{
		if(current->type==type)
		{
			i++;
		}
		
		current=current->next;
	}
	
	//Since entity creation starts by 0
	return i-1;
}

/**
 * Conta il numero di entity presenti attualmente sullo schermo in base 
 * al loro tipo
 * 
 * @param int type
 * 		Il tipo di entity
 * 
 * @return int
 * 		Il numero di entity presenti
 **/
int countEntityOnCamera(int type)
{
	int i = 0;
	Entity *current = headList;
	while(current!=NULL)
	{
		if(isInCamera(current->x, current->y, current->w, current->h) && type==current->type)
		{
			i++;
		}

		current=current->next;
	}
	
	printf("%d\n",i);
	return i;
}

/**
 * void moveEntity_X(Entity *pobj)
 * 
 * Gestisce il movimento di un oggetto Entity orizzontalmente, 
 * utilizzando i valori impostati nell'oggetto stesso.
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto entity da gestire
 **/
void moveEntity_X(Entity *pobj)
{	
	if (pobj->direction_x)
	{
		pobj->hspeed += pobj->speed * pobj->direction_x;
		pobj->status = MOVE;
	}
	else
	{
		if(pobj->hspeed > 0.0f)
		{
			pobj->hspeed -= 0.02f;

			if(pobj->hspeed < 0.0f)
				pobj->hspeed = 0.0f;
		}
		else if(pobj->hspeed < 0.0f)
		{
			pobj->hspeed += 0.02f;

			if(pobj->hspeed > 0.0f)
				pobj->hspeed = 0.0f;
		}
	}

	//Limitiamo la velocita' massima
	if (pobj->hspeed > 0.5f) pobj->hspeed = 0.5f;
	if (pobj->hspeed < -0.5f) pobj->hspeed = -0.5f;

	if(!tileCollision(pobj->x+pobj->hspeed, pobj->y, pobj->w, pobj->h, SOLID, 0))
		pobj->x += pobj->hspeed;
	else
	{
		pobj->direction_x *=-1;
	}
}

/**
 * Gestisce la gravita' per una Entity specifica in base al valore di vspeed
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto entity da gestire
 **/
void doEntityGravity(Entity *pobj)
{
	//Gestiamo le collisioni in verticale
	if(!tileCollision(pobj->x, pobj->y+pobj->vspeed, pobj->w, pobj->h, SOLID, 0))
	{
		pobj->y+= pobj->vspeed;
		pobj->vspeed+=0.1f;
	}

	if(pobj->vspeed >= TILESIZE) //Limitiamo la velocita' alla dimensione di un tile
		pobj->vspeed = TILESIZE;

	if(tileCollision(pobj->x, pobj->y+pobj->vspeed, pobj->w, pobj->h, SOLID, 0))	//Caduta
	{
		//Avviciniamo il giocatore al tile decrementando gradualmente la sua velocita' verticale
		pobj->vspeed -=0.1f;

		if(pobj->vspeed < 0)
			pobj->vspeed +=0.5f;
		else
		{
			pobj->vspeed=1.0f;	//Testiamo il terreno
		}
	}
}

/**
 * void jumpEntity(Entity *pobj)
 * 
 * Esegue un movimento di tipo salto statico su di un'oggetto
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto entity da gestire
 **/
void jumpEntity(Entity *pobj)
{
	//Incrementiamo la velocità verticale della entity con la gravità
	pobj->vspeed+=pobj->gravity;
	
	//Limitiamo la velocita' alla dimensione di un tile
	if(pobj->vspeed >= TILESIZE)
		pobj->vspeed = TILESIZE;

	//Collisione con un'ostacolo del livello
	if(tileCollision(pobj->x, pobj->y+pobj->vspeed, pobj->w, pobj->h, SOLID, 0))
	{	
		int ty = (pobj->y+pobj->vspeed) / TILESIZE;
		
		if(pobj->vspeed < 0)	//Salto
		 pobj->y = (ty+1)*TILESIZE;
		else					//Caduta
		 pobj->y = (ty-1)*TILESIZE+(pobj->h > TILESIZE ? TILESIZE+16 : 16);	//+(TILESIZE+Entity Height)

		//Testiamo il terreno
		pobj->vspeed=1.0f;
	}
}

/**
 * Gestisce l'animazione complessa di una entity, animazione composta
 * dalle azioni basilari, come camminare, saltare, abbassarsi, 
 * arrampicarsi, rimbalzo e attacco.
 * Le azioni sono specificate nell'apposita struttura enum.
 * 
 * @param Entity *pobj
 * 		La entity da animare
 * 
 * @param int loop
 *      Flag per l'impostazione d'animazione infinita.
 *      Se impostato a 0, la entità sarà animata senza sosta, con valore
 *      diverso la entità assumerà la corretta animazione di stop per
 *      la relativa direzione.
 **/
void animateEntity(Entity *pobj, int loop)
{
    /**
     * L'indice dei frame dell'animazione è incrementato in base 
     * alla velocità d'animazione scelta
     **/
    pobj->frame_index += pobj->animation_speed;
    
    ///Di default impostiamo 4 fotogrammi d'animazione per la camminata
    if (pobj->frame_index >= 3)
    {
        pobj->frame_index = 0;
    }
    
    if(pobj->direction_x != 0)
    {
        pobj->sprite_index = (pobj->direction_x < 0 ? WALKLEFT1 : WALKRIGHT1)+ abs(pobj->frame_index);
    }

    //TODO: Se direction_y fosse diverso da 0 durante il movimento sull'asse X? Caduta ad esempio.

    if(pobj->direction_y != 0)
    {
        pobj->sprite_index = (pobj->direction_y < 0 ? WALKUP1 : WALKDOWN1)+ abs(pobj->frame_index);
    }
    
    //Se l'animazione non è continuativa anche da fermi
    if(!loop)
    {
        //Se la velocità orizzontale è 0 e la direzione Y 0, vuol dire che il personaggio si stava muovendo sull'asse X
        if (pobj->hspeed == 0 && pobj->direction_y == 0) 
        {
            pobj->frame_index = 0;
            
            /**
             * In base alla direzione del personaggio, selezioniamo la riga
             * dello sprite impostando l'indice di partenza nel foglio
             **/
            pobj->sprite_index = (pobj->direction_x < 0 ? STANDLEFT : STANDRIGHT);
        }
        
        if (pobj->vspeed == 0 && pobj->direction_x == 0) 
        {
            pobj->frame_index = 0;
            
            /**
             * In base alla direzione del personaggio, selezioniamo la riga
             * dello sprite impostando l'indice di partenza nel foglio
             **/
            pobj->sprite_index = (pobj->direction_y < 0 ? STANDUP : STANDDOWN);
        }
    }

    /**
     * Animazioni per la derapata
     **/
    if(Player.direction_x == -1 && Player.hspeed > 0.0f)
    {
        pobj->sprite_index = SIDESLIP1;
    }
    
    if(Player.direction_x == 1 && Player.hspeed < 0.0f)
    {
        pobj->sprite_index = SIDESLIP2;
    }
}

/**
 * 
 * void drawEntity(Entity *pobj)
 * 
 * Disegna un Entity sullo schermo e ne anima la grafica.
 * Un meccanismo interno decidera' se aggiornare solamente la posizione
 * occupata dall'oggetto oppure ridisegnare lo schermo intero.
 * TODO: correggere descrizione
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto entity da disegnare
 * 
 * @param Camera *pcam
 * 		Puntatore all'oggetto Camera, necessario per
 * 		una corretta visualizzazione su schermo durante fasi di 
 * 		scrolling del livello.
 **/
void drawEntity(Entity *pobj)
{
	if(pobj->visible)
	{
		//Differenza in pixel tra sprite e bounding box
		int diff_w = (pobj->w<TILESIZE)? (pobj->w - TILESIZE)/2 : 0;
		int diff_h = (pobj->h<TILESIZE)? (pobj->h - TILESIZE)+1 : 0;

		SDL_Rect dest;
		dest.x = ((int)pobj->x - camera.offsetX)+diff_w;
		dest.y = ((int)pobj->y - camera.offsetY)+diff_h; //-pobj->h;
		dest.w = (int)dest.x+pobj->w;
		dest.h = (int)dest.y+pobj->h;

		//Entity in attesa di distruzione
		if(pobj->status==KILL)
		{
			//Stomped entity
			dest.y-=diff_h;
			getSprite(pobj->sprite, pobj->sprite_index, TILESIZE, TILESIZE, &dest);

			return;
		}
		
		//Blinking
		if(pobj->status==BLINK)
		{
			if((int)pobj->frame_index%2)
			{
				return;
			}
		}
		getSprite(pobj->sprite, pobj->sprite_index, TILESIZE, TILESIZE, &dest);

		#ifdef DEBUG_GFX
			drawFillRect(((int)pobj->x - camera.offsetX), ((int)pobj->y - camera.offsetY), pobj->w, pobj->h, 0xff0000);
		#endif
	}
}

/**
 * Clean allocated entities
 **/
void cleanEntities()
{
	#ifdef DESTROY_DBG
		printf("Clear entities ");
	#endif

	Entity *current = headList, *next=NULL;
	while(current!=NULL)
	{
		//~ printf("Destroy entity ID %d\n", current->id);
		next=current->next;
		free(current);
		current=next;
		
		#ifdef DESTROY_DBG
			printf(".");
		#endif
	}

    headList=NULL;
    tailList=NULL;
}
