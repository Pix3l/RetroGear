/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               typewriter.h - Virtual typewriter for text           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "typewriter.h"
#include "font.h"
#include "gfx.h"
#include "controls.h"

void initTypewriter(Typewriter *typewriter, int x, int y, int width)
{
	typewriter->x = x;
	typewriter->y = y;
	typewriter->max_width = width;
	typewriter->flag_paused = 0;

	curr_typewriter = typewriter;
}

void setTypewriterText(Typewriter *typewriter, char *text)
{
	typewriter->text = text;
}

void doTypewriter(Typewriter *typewriter)
{
	//If not empty string
	if(!typewriter->text)
	{
		return;
	}

	if(typewriter->index==strlen(typewriter->text))
	{
		return;
	}
	
	static int delay = 0;
	delay++;
	
	//Speed up
	if(curr_gamepad->button_A)
	{
		delay=7;
	}
	
	if(delay % 7 == 0)
	{
		typewriter->index++;
		delay=0;
	}
	
	//End of text
	if(typewriter->index==strlen(typewriter->text))
	{
		typewriter->flag_paused=1;
		curr_gamepad->button_A=0;
	}
}

/**
 * Check if the current typewriter is "paused".
 * This can mean the dialog is at a break point, or at it's end.
 **/
int typewriter_isPaused(Typewriter *typewriter)
{
	return typewriter->flag_paused;
}

/**
 * Reset typewriter object
 * 
 * @param Typewriter *typewriter
 * 		Typewriter object pointer
 **/
void typewriterReset(Typewriter *typewriter)
{
	typewriter->index=0;
	typewriter->text=NULL;
	typewriter->flag_paused=0;
	curr_typewriter = typewriter;
	
}

void drawTypewriter(Typewriter *typewriter)
{
	//If not empty string
	if(!typewriter->text)
	{
		return;
	}
	
	char *text = typewriter->text;
	
	int x = typewriter->x;
	int y = typewriter->y;
	int char_pos = 0;
	
	int row_limit = 240;
	
	int i;
	for(i=0; i<typewriter->index; i++)
	{
		int x_coord = x+FONT_W*char_pos;
		
		if(text[i]!='\n')
		{
			drawChar(screen, bmpfont, x_coord, y, FONT_W, FONT_H, text[i], white, 0);
			char_pos++;
		}
		
		if(text[i]=='\n' || x_coord >= row_limit)
		{
			x_coord = 241;
			x=typewriter->x;
			char_pos=0;
			y+=FONT_H;	
		}
	}
}
