/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   game.h - Game handling function                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _GAME_H
#define _GAME_H

struct _Game
{
	int hiscore;
	int status; //Stato di gioco
	int score;
	int flag_continue;	//Flag true/false activate continue function
	int flag_konami;
	int flag_pause;
	//~ void (*doStatus)();		//Puntatore al gestore di stato
	//~ void (*drawStatus)();	//Puntatore al disegnatore di stato
} Game;

//Possibili stati di gioco
enum States
{
	GAMEOVER, MENU, PREGAME, GAME, LOST, WIN, EDITOR
};

void doTitleScreen();
void doPreGame();
void doGame();
void doWin();
void doGameOver();
void doLogic();
void setGameState(int status);
void mainLoop();

#endif
