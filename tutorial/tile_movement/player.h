/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  player.h - Generic game main player               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _PLAYER_H
#define _PLAYER_H

#define PLAYER_W 12
#define PLAYER_H 16

#define MIN_H_SPEED -1.0f
#define MAX_H_SPEED 1.0f

#define MIN_V_SPEED -1.0f
#define MAX_V_SPEED 1.0f

#include "entity.h"

Entity Player; //[2]
Entity *curr_player;

void initPlayer(Entity *player);
void setPlayerPosition(int x, int y);
void playerExtraLife();
void updatePlayer();
void drawPlayer();

#include "sfx.h"
//Extra variables
Mix_Chunk *player_snd;

#endif
