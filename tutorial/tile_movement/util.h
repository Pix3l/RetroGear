/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  util.h - Generic purpose functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _UTIL_H
#define _UTIL_H

#include <SDL/SDL.h>

//Global variables
int repeat, quit;

//~ SDL_Surface *enemy_spr;

char sys_message[50];	//Messaggi di sistema

void setNotification(int curr_time, char *notification);
void appendChar(char* str, char ch);
char *substr(char *string, int position, int length);

int rectCollision(int x1, int y1, int w1, int h1, 
                  int x2, int y2, int w2, int h2);
int round_int(float value);
void init();
void reset();
void nextLevel();
void cleanUp();

#endif
