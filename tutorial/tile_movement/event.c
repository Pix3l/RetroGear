/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *              event.c - Level events handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "event.h"
#include "level.h"

void debugEvents()
{
	Event *current = evt_headList, *next=NULL;
	while(current!=NULL)
	{
		next=current->next;
		printf("[DEBUG] %s %d,%d,%s %d\n", current->mapname, current->evt_x, current->evt_y, current->parameters, current->flag_save);
		current=next;
	}
}

void loadEvents(char *event_file)
{
	/* Load event file: */
	FILE *fp;
	char filename[1024];

	snprintf(filename, 1024, "data/%s.evt", event_file);

	fp = fopen(filename, "r");
	if (fp == NULL)
	{
		#ifdef DEBUG_FILE_READ
			perror(filename);
		#endif
		return;
	}
	
	/* Read event file: */
	int evt_x, evt_y;
	char parameters[100];
    
    while (fscanf (fp, "%02d%*c%02d%*c%s", &evt_x, &evt_y, parameters) != EOF)
    {
    	//~ printf("%d %d %s\n", evt_x, evt_y, parameters);
    	addEvent(curr_level->name, evt_x, evt_y, parameters, 0);
    }
    
    fclose(fp);
    
    #ifdef DEBUG_EVENT
    	debugEvents();
    #endif
}

/**
 * Add an event to the game internal events list
 * 
 * @param char *mapname
 * 		The current level name
 * 
 * @param int evt_x, evt_y
 * 		The coordinates of the current event (in tile)
 * 
 * @param void (*evt_action)()
 * 		The function to execute on event call
 * 
 * @param char *parameters
 * 		Event's function parameters
 * 
 * @param int flag_save
 * 		Flag for save the event on the game save state file
 **/
void addEvent(char *mapname, unsigned int evt_x, unsigned int evt_y, char *parameters, int flag_save)
{
	//Check if the event already exist
	Event *tmp = getEvent(mapname, evt_x, evt_y);

	if(tmp)
	{
		return;
	}
	
	//calloc alloc and initialize the memory
	Event *pnew = (Event*) calloc(1, sizeof(Event));

	if(evt_headList == NULL)
	{
		evt_headList = pnew;
		evt_tailList = evt_headList;
		evt_tailList->next = NULL;
	}
	else
	{
		evt_tailList->next = pnew;
		evt_tailList = pnew;
	}

	//~ evt_tailList->mapname = (char*) malloc(sizeof(char) * (strlen(mapname) + 1));
	strcpy(evt_tailList->mapname, mapname);
	evt_tailList->evt_x = evt_x;
	evt_tailList->evt_y = evt_y;
	strcpy(evt_tailList->parameters, parameters);
	evt_tailList->flag_save = flag_save;
	
	#ifdef DEBUG_EVENT
		printf("Created event: %d %d %s\n", evt_tailList->evt_x, evt_tailList->evt_y, evt_tailList->parameters);
	#endif
}

Event *getEvent(char *mapname, unsigned int evt_x, unsigned int evt_y)
{
	Event *current = evt_headList, *next=NULL;
	while(current!=NULL)
	{
		next=current->next;
		
		//Check for the corresponding event
		if(strcmp (current->mapname, mapname)== 0 &&
		   current->evt_x == evt_x && current->evt_y == evt_y)
		{
			#ifdef DEBUG_EVENT
				printf("Event %s %d %d found: %s\n", current->mapname, current->evt_x, current->evt_y, current->parameters);
			#endif
			return current;
		}
		
		current=next;
	}
	
	return NULL;
}

/**
 * Return event parameters string
 * 
 * @param char *mapname
 * 		The level map name
 * 
 * @param unsigned int evt_x
 * 		Event x coordinate
 * 
 * @param unsigned int evt_y
 * 		Event y coordinate
 * 
 * @param char *parameters
 * 		String to copy the parameters
 * 
 * @return char *parameters
 * 		The event parameters string
 **/
char *getEventParameters(char *mapname, unsigned int evt_x, unsigned int evt_y)
{
	Event *current = evt_headList, *next=NULL;
	while(current!=NULL)
	{
		next=current->next;
		
		//Check for the corresponding event
		if(strcmp (current->mapname, mapname)== 0 &&
		   current->evt_x == evt_x && current->evt_y == evt_y)
		{
			#ifdef DEBUG_EVENT
				printf("Event %s %d %d found: %s\n", current->mapname, current->evt_x, current->evt_y, current->parameters);
			#endif
			
			return current->parameters;
		}
		
		current=next;
	}
	
	return NULL;
}

/**
 * Delete an event from the game internal events list
 * 
 * @param char *mapname
 * 		The current level name
 * 
 * @param int evt_x, evt_y
 * 		The coordinates of the current event (in tile)
 **/
void deleteEvent(char *mapname, unsigned int evt_x, unsigned int evt_y)
{
	Event *event = evt_headList, *previous=NULL;
	while(event!=NULL)
	{
		//Check for the corresponding event
		if(strcmp (event->mapname, mapname)== 0 &&
		   event->evt_x == evt_x && event->evt_y == evt_y)
		{
			//Delete it and setup the list
			if(evt_headList==event && evt_tailList==event)
			{
				free(event);
				evt_headList=evt_tailList=NULL;
				return;
			}

			if(evt_headList==event)
			{
				evt_headList = event->next;
				free(event);
				event = evt_headList;
			}
			else if(evt_tailList==event)
			{
				previous->next=NULL;
				evt_tailList=previous;
				free(event);
				event = evt_tailList;
			}
			else
			{
				previous->next = event->next;
				free(event);
				event = previous->next;
			}
		}

		previous = event;
		event = event->next;
	}
}

void cleanEvents()
{
	Event *current = evt_headList, *next=NULL;
	while(current!=NULL)
	{
		next=current->next;
		free(current);
		current=next;
	}

    evt_headList=NULL;
    evt_tailList=NULL;
}
