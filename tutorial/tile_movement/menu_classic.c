/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             		menu_classic.c - Classic game menu				  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "menu.h"
#include "menu_classic.h"
#include "font.h"
#include "util.h"
#include "controls.h"
#include "timer.h"
#include "fps.h"
#include "gfx.h"

//Private functions
//~ void drawMenuBorder(Menu *pmenu);	//@deprecated

void initClassicMenu(Menu* pmenu, Item *pitem, int x, int y, char *name, int max_items, SDL_Color color, Menu *parent, Menu *child)
{
	initMenu(pmenu, pitem, x, y, name, max_items, color, parent, child, &menuInput, &drawMenuClassic);
}

/**
 * Align given menu items as a table with passed rows and cols, and
 * apply the new menu width, height, rows and cols as well.
 * 
 * @param Menu *pmenu
 * 		The Menu object
 * 
 * @param int rows
 * 		The number of rows for the table
 * 
 * @param int cols
 * 		The number of cols for the table
 * 
 * @param int col_width
 * 		The column width in pixels.
 * 
 * @param int row_spacing
 * 		The row spacing in pixels.
 **/
void setMenuTable(Menu *pmenu, int rows, int cols, int col_width, int row_spacing)
{
	int i=0, row=0, col=0;
	for(row=0; row< rows; row++)
	{
		for(col=0; col < cols; col++)
		{
			if(i > pmenu->num_items-1)
			{
				break;
			}
			
			pmenu->items[i].x = pmenu->x + ( (col*FONT_W)+(col*col_width)+FONT_W );
			pmenu->items[i].y = pmenu->y + ( row * (FONT_H + row_spacing) );

			//~ printf("[DEBUG] i = %d ; num_items = %d\n", i, pmenu->num_items);

			i++;
		}
	}
	
	pmenu->rows = rows;
	pmenu->cols = cols;
	
	pmenu->w = (col_width*cols)+(FONT_W*(cols-1));
	pmenu->h = rows * (FONT_H + row_spacing);
}

void drawMenuClassic(Menu *pmenu)
{
	if(!pmenu->flag_active)
	return;
	
	if(pmenu->flag_border)
	{
		//~ drawMenuBorder(pmenu);
		//0x000000 is the background color
		drawGui(pmenu->x+FONT_W, pmenu->y+FONT_H, pmenu->w, pmenu->h, 0x000000, white);
	}

	if(pmenu->flag_title)
	{
		drawString(screen, pmenu->x+(FONT_W*2), pmenu->y-10, pmenu->name, pmenu->color, 0);
	}	

	int i=0;
	for(i = 0; i < pmenu->num_items; ++i)
	{
		drawString(screen, pmenu->items[i].x+FONT_W, pmenu->items[i].y+FONT_H, pmenu->items[i].name, pmenu->items[i].color, 0);
	}
	
	drawChar(screen, bmpfont, pmenu->items[pmenu->curr_item].x, pmenu->items[pmenu->curr_item].y+FONT_H, FONT_W, FONT_H, pmenu->cursor, pmenu->color, 0);
}

/*
void drawMenuBorder(Menu *pmenu)
{
	//Draw background under menu
	drawFillRect(pmenu->x, pmenu->y, pmenu->w*FONT_W, pmenu->h*FONT_H, 0x000000);

	//Up-left corner
	drawChar(screen, bmpfont, pmenu->x-FONT_W, pmenu->y-FONT_H, FONT_W, FONT_H, 0, cyan, 0);
	//Up-right corner
	drawChar(screen, bmpfont, pmenu->x+pmenu->w*8+8, pmenu->y-8, FONT_W, FONT_H, 6, cyan, 0);
	
	//Linee orizzontali
	int i;
	for(i=-1; i < pmenu->w; i++)
	{
		drawChar(screen, bmpfont, (pmenu->x+8)+8*i, pmenu->y-8, FONT_W, FONT_H, 2, cyan, 0);
		drawChar(screen, bmpfont, (pmenu->x+FONT_W)+8*i, pmenu->items[pmenu->num_items-1].y+8, FONT_W, FONT_H, 3, cyan, 0);
	}

	//Linee verticali
	for(i=0; i < pmenu->h; i++)
	{
		drawChar(screen, bmpfont, pmenu->x-8, pmenu->y+8*i, FONT_W, FONT_H, 4, cyan, 0);
		drawChar(screen, bmpfont, pmenu->x+pmenu->w*8+8, pmenu->y+8*i, FONT_W, FONT_H, 5, cyan, 0);
	}

	//Down-left corner
	drawChar(screen, bmpfont, pmenu->x-8, pmenu->items[pmenu->num_items-1].y+8, FONT_W, FONT_H, 1, cyan, 0);

	//Down-right corner
	drawChar(screen, bmpfont, pmenu->x+pmenu->w*8+8, pmenu->items[pmenu->num_items-1].y+8, FONT_W, FONT_H, 7, cyan, 0);
}
*/

