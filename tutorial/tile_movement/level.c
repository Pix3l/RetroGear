/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    level.c - Level file handling                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "level.h"
#include "tile.h"
#include "entity.h"

/**
 * Crea una struttura di livello vuota, idonea per la creazione di un
 * nuovo livello di gioco
 *
 * @param struct _Level* plevel
 *      Puntatore ad una struttura di livello
**/
//TODO: rendere interattivo il processo richiedendo i dati di livello
int emptyLevel(Level* plevel)
{
	int i,j;

	plevel->number = 0;		//I nuovi livelli di default avranno 1

	//Load header info
	//~ strcpy(plevel->name, "New level");
	memset(plevel->name,0,sizeof(plevel->name));

	/* (Song file for this level) */
	//~ strcpy(plevel->song_title, "song file name here");
	memset(plevel->song_title,0,sizeof(plevel->song_title));
	
	plevel->bkgd_red = 0;
	plevel->bkgd_green = 0;
	plevel->bkgd_blue = 0;

	//New level will have default cols and rows size
	plevel->cols = COLS;
	plevel->rows = ROWS;

	//Layer 1: Solid tiles
	for(i=0; i<plevel->rows; i++)// && !feof (fi))
		for(j=0; j<plevel->cols; j++)
			plevel->map[0][i][j]=0;

	//Layer 2: Tiles
	for(i=0; i<plevel->rows; i++)// && !feof (fi))
		for(j=0; j<plevel->cols; j++)
			plevel->map[1][i][j]=0;

  return 0;
}

/**
 * Load a level along with its parameters
 * 
 * @param struct _Level* plevel
 *      Pointer to the level holding structure
 * 
 * @param int level
 * 		Actual level number (Part of the level name)
 * 
 * @return
 *      -1 In case of fail
 * 
 **/
int loadLevel(Level* plevel, int level)
{
	int i,j;
	FILE * fi;
	char str[80];
	char filename[1024];

	/* Load data file: */
	snprintf(filename, 1024, "data/level%d.dat", level);

	fi = fopen(filename, "r");
	if (fi == NULL)
	{
		perror(filename);
		return -1;
	}

	plevel->number = level;

	//Load header info
	/* (Level title) */
	fgets(str, 20, fi);
	strcpy(plevel->name, str);
	plevel->name[strlen(plevel->name)-1] = '\0';

	/* (Song file for this level) */
	fgets(str, sizeof(plevel->song_title), fi);
	strcpy(plevel->song_title, str);
	plevel->song_title[strlen(plevel->song_title)-1] = '\0';
	
	//Background color
	//~ fscanf (fi, "%6x", &plevel->background_color);
	fscanf (fi, "%d%*c%d%*c%d", &plevel->bkgd_red, &plevel->bkgd_green, &plevel->bkgd_blue);

	//~ printf("%d\n", plevel->bkgd_red);
	//~ printf("%d\n", plevel->bkgd_green);
	//~ printf("%d\n", plevel->bkgd_blue);

	//Layers
	fscanf (fi, "%d", &plevel->num_layers);

	//Level rows and cols
	fscanf (fi, "%02d%*c%02d", &plevel->cols, &plevel->rows);

	//Layer 1: Solid tiles
	for(i=0; i<plevel->rows; i++)// && !feof (fi))
		for(j=0; j<plevel->cols; j++)
		{
			fscanf (fi, "%02d%*c", &plevel->map[0][i][j]);
			createEntityFromMap(plevel->map[0][i][j], j*TILESIZE, i*TILESIZE);
		}
	
	//Layer 2: Tiles
	for(i=0; i<plevel->rows; i++)// && !feof (fi))
		for(j=0; j<plevel->cols; j++)
			fscanf (fi, "%02d%*c", &plevel->map[1][i][j]);

	plevel->flag_complete=0;

	fclose(fi);
	return 0;
}

void saveMap()
{
	int i,j;
	FILE * fp;

	fp = fopen("test.txt", "wb");
	if (fp == NULL)
	{
	  return;
	}

	/* (Level title) */
	fprintf(fp, "%s\n", curr_level->name);
	
	/* (Song file for this level) */
	fprintf(fp, "%s\n", curr_level->song_title);

	/* (Level width) & (Level height) */
	fprintf(fp, "%02d,%02d", curr_level->cols, curr_level->rows);
	
	fprintf(fp, "\n");

	//Layer 1: Solid tiles / Entities
	for(i=0; i< curr_level->rows; i++)// && !feof (fi))
	{
		for(j=0; j< curr_level->cols; j++)
		 	fprintf(fp, "%d,", curr_level->map[0][i][j]);
		
		fprintf(fp, "\n");
	}
	
	fprintf(fp, "\n");
	
	//Layer 2: Tiles
	for(i=0; i< curr_level->rows; i++)// && !feof (fi))
	{
		for(j=0; j< curr_level->cols; j++)
			fprintf(fp, "%d,", curr_level->map[1][i][j]);
		
		fprintf(fp, "\n");
	}

	fclose(fp);
}

void debugLevel(Level* plevel)
{
	int i,j;
	
	printf("[Debug] Level\n");
	printf("Name: %s\n", plevel->name);
	printf("song_title: %s\n", plevel->song_title);
	
	printf("Map:\n");
	for(i=0; i< plevel->rows; i++)
	{
		for(j=0; j< plevel->cols; j++)
		{
			printf ("%d,", plevel->map[1][i][j]);
		}
		printf("\n");
	}

	printf("Solid:\n");
	for(i=0; i< plevel->rows; i++)
	{
		for(j=0; j< plevel->cols; j++)
		{
			printf ("%d,", plevel->map[0][i][j]);
		}
		printf("\n");
	}
	
	printf("rows: %d\n", plevel->rows);
	printf("cols: %d\n", plevel->cols);
}
