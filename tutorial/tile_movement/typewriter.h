/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               typewriter.h - Virtual typewriter for text           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _TYPEWRITER_H
#define _TYPEWRITER_H

typedef struct _Typewriter {
	int x, y;		//Origins
	int max_width;	//Max width
	int index;
	char *text;		//Pointer to text
	int delay;
	int flag_paused;
} Typewriter;

Typewriter typewriter, *curr_typewriter;

#include <SDL/SDL.h>

#define TYPEWRITER_TIMER 800

void initTypewriter(Typewriter *tpwr, int x, int y, int width);
void doTypewriter(Typewriter *typewriter);
void setTypewriterText(Typewriter *typewriter, char *text);
int typewriter_isPaused(Typewriter *typewriter);
void typewriterReset(Typewriter *typewriter);
void drawTypewriter(Typewriter *typewriter);

#endif
