/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   gfx.c - Generic drawing functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "gfx.h"
#include "font.h"

Uint32 get_pixel(SDL_Surface *surface, int x, int y)
{
	int bpp = surface->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)surface->pixels + y * surface->pitch + x * bpp;
	
	switch (bpp)
	{
		case 1:
		return *p;

		case 2:
		return *(Uint16 *)p;

		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
				return p[0] << 16 | p[1] << 8 | p[2];
			else
				return p[0] | p[1] << 8 | p[2] << 16;
		
		case 4:
			return *(Uint32 *)p;
		
		default:
			return 0;
	}
}

void put_pixel(SDL_Surface *_ima, int x, int y, Uint32 pixel)
{
	int bpp = _ima->format->BytesPerPixel;
	Uint8 *p = (Uint8 *)_ima->pixels + y * _ima->pitch + x*bpp;

	switch (bpp)
	{
		case 1:
			*p = pixel;
			break;
			
		case 2:
			*(Uint16 *)p = pixel;
			break;
			
		case 3:
			if (SDL_BYTEORDER == SDL_BIG_ENDIAN)
			{
				p[0]=(pixel >> 16) & 0xff;
				p[1]=(pixel >> 8) & 0xff;
				p[2]=pixel & 0xff;
			}
			else
			{
				p[0]=pixel & 0xff;
				p[1]=(pixel >> 8) & 0xff;
				p[2]=(pixel >> 16) & 0xff;
			}
			break;
			
		case 4:
			*(Uint32 *) p = pixel;
			break;
	}
}

/**
 * Rimpiazza un colore specifico (target) con uno un rimpiazzo (replecement)
 * all'interno di una determinata superficie
 * 
 * @param SDL_Surface *src
 *      Puntatore alla superficie su cui agire
 * @param Uint32 target
 *      Colore RGB da sostituire
 * @param Uint32 replacement
 *      Colore RGB di rimpiazzo
 **/
void replaceColor (SDL_Surface * src, Uint32 target, Uint32 replacement)
{
	int x;
	int y;
	
	if (SDL_MUSTLOCK (src))
	{
		if (SDL_LockSurface (src) < 0)
			return;
	}
	
	for (x = 0; x < src->w; x ++)
	{
	    //~ printf("X %d\n", x);
		for (y = 0; y < src->h; y ++)
		{
		    //~ printf("Y %d\n", y);
			if (get_pixel (src, x, y) == target)
             put_pixel (src, x, y, replacement);
		}
	}

	if (SDL_MUSTLOCK (src))
		SDL_UnlockSurface (src);
}

/**
 * Disegna un rettangolo sullo schermo alle coordinate e
 * dimensioni specificate, riempiendone il contenuto, sfruttando la 
 * funzione SDL_FillRect().
 *
 * @param int x, int y
 * 		Le coordinate X e Y da cui iniziare a disegnare
 * @param int w, int h
 * 		Larghezza e altezza del rettangolo
 * @param int color
 * 		Colore di disegno
 * 
 **/
void drawFillRect(int x, int y, int w, int h, int color)
{
	SDL_Rect rect = {x,y,w,h};
    SDL_FillRect(screen, &rect, color);
}

/**
 * Disegna un semplice rettangolo senza riempimento.
 * 
 * @param int x, int y
 * 		Le coordinate X e Y da cui iniziare a disegnare
 * @param int w, int h
 * 		Larghezza e altezza del rettangolo
 * @param int color
 * 		Colore di disegno
 **/
void drawRect(int x, int y, unsigned int w, unsigned int h, int color)
{
	int i,j;

	for (j = 0; j < w; j++)
	{
		for (i = 0; i < h; i++)
		{
			if(j==0 || j==w-1 || i==0 || i==h-1)
			{
				put_pixel (screen, x+j, y+i, color);
			}
		}
	}
}

/**
 * Draw a gui usign font system by drawing specific characters.
 * Each border is spaced by the defined font width and height of 
 * characters.
 * 
 * Max gui width = (SCREEN_WIDTH / FONT_W)
 * Max gui height = (SCREEN_HEIGHT / FONT_H)
 * 
 * @param int x, y
 * 		Coordinate to start draw the gui
 * @param unsigned int w, h
 * 		Width and height of the gui
 * @param int bg
 * 		Hexadecimal color value of gui's background
 * @param SDL_Color color
 * 		The border color (Character color)
 **/
void drawGui(int x, int y, unsigned int w, unsigned int h, int bg, SDL_Color color)
{
	w = w/FONT_W;
	h = h/FONT_H;

	//Draw background under menu
	drawFillRect(x, y, (w*FONT_W)+FONT_W, h*FONT_H, bg);

	//Up-left corner
	drawChar(screen, bmpfont, x-FONT_W, y-FONT_H, FONT_W, FONT_H, 0, color, 0);
	//Up-right corner
	drawChar(screen, bmpfont, x+(w*FONT_W)+FONT_W, y-FONT_H, FONT_W, FONT_H, 6, color, 0);

	//Linee orizzontali
	int i;
	for(i=0; i <= w; i++)
	{
		drawChar(screen, bmpfont, x+(FONT_W*i), y-FONT_H, FONT_W, FONT_H, 2, color, 0);
		drawChar(screen, bmpfont, x+(FONT_W*i), y+(h*FONT_H), FONT_W, FONT_H, 3, color, 0);
	}

	//Linee verticali
	for(i=0; i < h; i++)
	{
		drawChar(screen, bmpfont, x-FONT_W, y+(FONT_H*i), FONT_W, FONT_H, 4, color, 0);
		drawChar(screen, bmpfont, x+(w*FONT_W)+FONT_W, y+(FONT_H*i), FONT_W, FONT_H, 5, color, 0);
	}

	//Down-left corner
	drawChar(screen, bmpfont, x-FONT_W, y+(h*FONT_H), FONT_W, FONT_H, 1, color, 0);
	//Down-right corner
	drawChar(screen, bmpfont, x+(w*FONT_W)+FONT_W, y+(h*FONT_H), FONT_W, FONT_H, 7, color, 0);
}

/**
 * Carica un'immagine BMP specifica in una superficie e ne converte il 
 * formato in quello attuale dello schermo per un blitting piu' rapido
 * 
 * @param Char *file
 *      Percorso e nome del file immagine BMP da caricare
 * 
 * @param Uint32 key
 * 		Valore del colore di trasparenza
 * 
 * @return SDL_Surface *surface
 *      Superficie contentente l'immagine caricata e convertita nel
 * 		formato attuale dello schermo
 **/
SDL_Surface* loadImage(char *file, Uint32 key)
{
	SDL_Surface *temp1, *temp2;
	temp1 = SDL_LoadBMP(file);
	
	if (temp1 == NULL) {
		printf("Can't load file: %s\n", SDL_GetError());
		exit(1);
	}

	temp2 = SDL_DisplayFormat(temp1);
	SDL_SetColorKey(temp2, SDL_SRCCOLORKEY, key);
	SDL_FreeSurface(temp1);
	return temp2;
}

/**
 * Prende una porzione di dimensioni specificate da una superficie e la mostra alle coordinate volute
 * 
 * @param SDL_Surface *src
 *      Superficie da cui prelevare il tile
 * @param SDL_Surface *dest
 *      Superficie di destinazione su cui applicare il tile
 * @param int xspr, int yspr
 *      Coordinate da cui si iniziera' a prelevare il tile
 * @param int w, int h
 *      Larghezza e grandezza del tile
 * @param int x, int y
 *      Coordinate a cui verra' disegnato il tile sulla superficie
 *      di destinazione
 **/
void putImage(SDL_Surface *src, SDL_Surface *dest, int x_orig, int y_orig, int w, int h, int x, int y)
{
    SDL_Rect pick;
    pick.x=x_orig;
    pick.y=y_orig;
    pick.w=w;
    pick.h=h;
    
    SDL_Rect area;
    area.x=x;
    area.y=y;
    area.w=w;
    area.h=h;
    
    SDL_BlitSurface(src,&pick,screen,&area);
}

/**
 * Preleva un fotogramma da uno sprite sheet all'indice indicato, 
 * di dimensioni specifiche e lo mostra alle coordinate volute
 * 
 * @param SDL_Surface *src
 *      Superficie da cui prelevare lo sprite
 * @param int index
 *      Indice dello sprite nell'immagine
 * @param int width, int height
 *      Dimensioni dello sprite
 * @param SDL_Rect dest
 *      Cordinate a cui disegnare lo sprite
 **/
void getSprite(SDL_Surface *sprite_sheet, int index, int width, int height, SDL_Rect *dest)
{
     SDL_Rect pick;
     //Di default ci si aspetta 6 colonne e 6 righe
     pick.x=(index % 6)*width;
     pick.y=(index / 6)*height;
	//TODO: Se il numero delle righe non corrisponde a quello delle colonne l'esito e' errato

     pick.w=width;
     pick.h=height;

     SDL_BlitSurface(sprite_sheet,&pick,screen, dest);
}

/*
 * Veloce ma non performante !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
 */
void drawPixel(SDL_Surface *_ima, int x, int y, Uint32 pixel)
{
 Uint32 bpp, ofs;

 bpp = _ima->format->BytesPerPixel;
 ofs = _ima->pitch*y + x*bpp;

 SDL_LockSurface(_ima);
 memcpy(_ima->pixels + ofs, &pixel, bpp);
 SDL_UnlockSurface(_ima);
}
