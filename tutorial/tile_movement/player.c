/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  player.c - Generic game main player               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "player.h"
#include "controls.h"
#include "tile.h"
#include "camera.h"
#include "gfx.h"
#include "sfx.h"
#include "game.h"
#include "font.h"
#include "fps.h"
#include "timer.h"
#include "entity.h"

//Private functions
void playerAction();
void movePlayer();
static void animatePlayer();

/**
 * Reset the player's default values
 * 
 * @param Entity *player
 * 		Pointer to a Player objects
 **/
void initPlayer(Entity *player)
{
    player->visible = 1;
    player->flag_active = 1;
	player->w= PLAYER_W;
	player->h= PLAYER_H;
	player->lives = 3;
	player->sprite_index = 0;

	if(!player->sprite)
	{
		player->sprite = loadImage("data/player.bmp", COLORKEY);
	}

	player->animation_speed = 0.09f;
	player->speed = 1.0f;
	player->hspeed = 0;
	player->vspeed = 0;
    
	player->direction_x = 0;
	player->direction_y = 1;
}

void setPlayerPosition(int x, int y)
{
	curr_player->x = x;
	curr_player->y = y;
	curr_player->xstart = x;
	curr_player->ystart = y;
}

void playerExtraLife()
{
	curr_player->lives++;
	playSound(extralife_snd);
}

void playerAction()
{
	curr_player->sprite_index = 0;	//Action sprite index
	//Action time
	//Action function	
}

/**
 * Handle keyboard keys and update player moves and status 
 **/
void updatePlayer()
{
	//~ scrollCameraX(&camera, curr_player->x);
	//~ scrollCameraY(&camera, curr_player->y);
	
    //~ animatePlayer();
    animateEntity(curr_player, 0);
	movePlayer();

	//Real time status check here!
}

void movePlayer()
{
    Player.x += Player.hspeed;
    Player.y += Player.vspeed;
    
    //If the player is not aligned to grid, we ignore the input
	if(!isInTile(Player.x,Player.y))
	 return;
    
	//horizontal
	if (curr_gamepad->button_Left)
	{
		Player.direction_x = -1;
        Player.direction_y = 0;
        
        Player.hspeed = -1.0f;
	}
	
	if (curr_gamepad->button_Right)
	{
		Player.direction_x = +1;
        Player.direction_y = 0;
        
        Player.hspeed = +1.0f;
	}

	if (!curr_gamepad->button_Left && !curr_gamepad->button_Right)
	{
        Player.hspeed = 0;
	}
	
	//vertical
	if (curr_gamepad->button_Up)
	{
		Player.direction_y = -1;
        Player.direction_x = 0;
        
        Player.vspeed = -1.0f;
	}
	
	if (curr_gamepad->button_Down)
	{
		Player.direction_y = +1;
        Player.direction_x = 0;
        
        Player.vspeed = +1.0f;
	}
    
	if (!curr_gamepad->button_Up && !curr_gamepad->button_Down)
	{
        Player.vspeed = 0;
	}
}

void playerDie()
{
	curr_player->sprite_index = DIE;

	//Stop any music from the game
	//~ if(isMusicPlaying())
	//~ {
		//~ pauseMusic();
	//~ }

	if(curr_player->timer[0]==0)
	{
		curr_player->timer[0]=fps.t;
		return;
	}

	if(getSeconds(curr_player->timer[0]) >= 3)
	{
		curr_player->lives--;
		curr_player->status=STAND;
		curr_player->hspeed=curr_player->vspeed=0;
		setPlayerPosition(curr_player->xstart, curr_player->ystart);
		timer.start_time = fps.t;
		setGameState(PREGAME);
		curr_player->timer[0] = 0;	//Reset timer interno
		return;
	}
}

void animatePlayer()
{
	/**
	 * L'indice dei frame dell'animazione è incrementato in base 
	 * alla velocità d'animazione scelta
	 **/
	Player.frame_index += Player.animation_speed;
	/**
	 * Di default impostiamo 4 fotogrammi d'animazione per la 
	 * camminata
	 **/
	if (Player.frame_index >= 3) Player.frame_index = 0;

	if(Player.direction_x < 0)
	{
		Player.sprite_index = WALKLEFT1+ abs(Player.frame_index);	
	}
	else if(Player.direction_x > 0)
	{
		Player.sprite_index = WALKRIGHT1 + abs(Player.frame_index);
	}

	if(Player.direction_y < 0)
	{
		Player.sprite_index = WALKUP1+ abs(Player.frame_index);	
	}
	else if(Player.direction_y > 0)
	{
		Player.sprite_index = WALKDOWN1 + abs(Player.frame_index);
	}
}

void drawPlayer()
{
	if(Game.status < GAME) return;

	// Destinazione del tile
	SDL_Rect dest;

	dest.x = (int)curr_player->x-2 - camera.offsetX;
	dest.y = (int)curr_player->y+1 - camera.offsetY;
	dest.w = (int)dest.x+curr_player->w;
	dest.h = (int)dest.y+curr_player->h;

	getSprite(curr_player->sprite, curr_player->sprite_index, 16, 16, &dest);
	
	#ifdef DEBUG_GFX
		drawFillRect(curr_player->x, curr_player->y, curr_player->w, curr_player->h, 0xff0000);
	#endif
}
