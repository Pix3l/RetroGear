/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               typewriter.h - Virtual typewriter for text           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "typewriter.h"
#include "font.h"
#include "gfx.h"
#include "controls.h"
#include "util.h"
#include "timer.h"
#include "bit.h"

void initTypewriter(Typewriter *typewriter, int x, int y, int width, int(*callback)())
{
    typewriter->x = x;
    typewriter->y = y;
    typewriter->max_width = width;
    typewriter->callback = callback;

    if(callback!=NULL)
    {
        setFlag(&typewriter->flags, typewriter_waitCallback);
    }
    
    resetTypewriter(typewriter);
}

/**
 * Reset typewriter object
 * 
 * @param Typewriter *typewriter
 * 		Typewriter object pointer
 **/
void resetTypewriter(Typewriter *typewriter)
{
    typewriter->text=NULL;
    //~ typewriter->flags = 0;
    typewriter->curPos = 0;
    typewriter->delay = 1;  //This make the typewriter be able to start!

    unsetFlag(&typewriter->flags, typewriter_active);
    setFlag(&typewriter->flags, typewriter_inactive);
    
    curr_typewriter = typewriter;
}

void setTypewriterText(Typewriter *typewriter, char *text)
{
    if(hasFlag(typewriter->flags, typewriter_inactive))
    {
        typewriter->text = text;
        setFlag(&typewriter->flags, typewriter_active);
    }
}

void doTypewriter(Typewriter *typewriter)
{
    //If not empty string
    if(!typewriter->text)
    {
        return;
    }

    //Check if the text is finished
    if( isTypewriterEnd(typewriter) )
    {
        setFlag(&typewriter->flags, typewriter_paused);

        //Wait for callback to procede
        if(hasFlag(typewriter->flags, typewriter_waitCallback))
        {
            #ifdef DEBUG_TYPEWRITER
                printf("[Debug] Typewriter is waiting callback\n");
            #endif
            if(typewriter->callback && typewriter->callback()!=1)
            {
                return;
            }
        }
    
        if(curr_gamepad->button_A)
        {
            curr_gamepad->button_A=0;
            resetTypewriter(typewriter);
            return;
        }
        
        return;
    }

    //Decrease the delay...
    typewriter->delay--;

    //Speed up
    if(curr_gamepad->button_A)
    {
        if(hasFlag(typewriter->flags, typewriter_waitPage))
        {
            #ifdef DEBUG_TYPEWRITER
                printf("[Debug] Typewriter is waiting new page\n");
            #endif
            
            unsetFlag(&typewriter->flags, typewriter_waitPage|typewriter_paused);
            typewriter->text += typewriter->curPos+1;
            typewriter->curPos = 0;
            //~ curr_gamepad->button_A=0;
            return;
        }
        
        typewriter->delay=0;
    }

    //Se una nuova pagina inizia, la stringa da mostrare deve iniziare dal primo carattere della pagina da mostrare
    if(typewriter->text[typewriter->curPos]=='\f')
    {
        setFlag(&typewriter->flags, typewriter_paused|typewriter_waitPage);
        return;
    }

    if(typewriter->delay == 0)
    {
        typewriter->curPos++;
        typewriter->delay=7;    //Reset delay
    }
}

int isTypewriterEnd(Typewriter *typewriter)
{
    if( !typewriter->text || typewriter->text[typewriter->curPos] == '\0' )
    {
        return 1;
    }
    return 0;
}

void drawTypewriter(Typewriter *typewriter)
{
    //Make sure we have text to draw
    if(!typewriter->text)
    {
        return;
    }

    char c = typewriter->text[typewriter->curPos];    //Backup current character

    typewriter->text[typewriter->curPos] = '\0';    //Replace current character with a string end

    //Newline or end of the line, we bring the text to wrap
    //TODO: Inserire un carattere LINEFEED richiederebbe un memmove probabilmente
    //~ if(typewriter->curPos*FONT_W > typewriter->max_width)
    //~ {
        //~ typewriter->text[typewriter->curPos] = '\n';
    //~ }

    //Print the truncated string
    drawString(typewriter->x, typewriter->y, typewriter->text, white, 0);

    typewriter->text[typewriter->curPos] = (char)c; //Restore back the old character into the string

    //If there are other pages, we draw the flashing arrow
    //~ if(typewriter->flag_status == PAUSED)
    if(hasFlag(typewriter->flags, typewriter_paused))
    {
        if(get_time_elapsed(5)%2)
        {
            return;
        }
        drawChar(typewriter->x+(typewriter->max_width-FONT_W), typewriter->y+FONT_H*7, 31, white, 0);
    }
}
