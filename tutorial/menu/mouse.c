/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  mouse.c - Mouse handling functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "mouse.h"
#include "tile.h"
#include "gfx.h"

/**
 * Gestisce gli input e le posizioni del Mouse
 **/
void getMousePosition()
{
	//Assegnamo le posizioni del mouse in un'apposita struttura
	SDL_GetMouseState(&Mouse.x, &Mouse.y);
}

/**
 * Allinea il puntatore del mouse alla griglia
 **/
void setMouseOnGrid()
{
	Mouse.x /= TILESIZE;
	Mouse.y /= TILESIZE;
	
	Mouse.x *= TILESIZE;
	Mouse.y *= TILESIZE;
}

void drawMousePointer()
{
	drawRect(Mouse.x, Mouse.y, TILESIZE, TILESIZE, 0xff0000);
}
