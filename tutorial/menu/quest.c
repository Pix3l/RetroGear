/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   quest.c - RPG like example menu                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <string.h>

#include "quest.h"
#include "controls.h"
#include "gfx.h"
#include "font.h"
#include "dialogue.h"
#include "game.h"
#include "menu.h"
#include "menu_classic.h"
#include "menu_paginator.h"
#include "bit.h"
#include "potion.h"
#include "diary.h"
#include "player.h"
#include "tile.h"

//~ void quest_Ask()
//~ {
    //~ printf("curr_menu %p, battle_menu %p\n", curr_menu, &battle_menu);
    //~ curr_menu = &quest_menu;
    //~ setFlag(&quest_menu.flags, menu_active);
//~ }

void generic()
{
    setFlag(&quest_menu.flags, menu_active);
    
    snprintf(dlg_holder, MAX_PARAM_LENGTH, "element %s", curr_menu->items[curr_menu->curr_item].name);
    initDialogue(&defaultTypewriter, dlg_holder, NULL);
}

/**
 * Players actions
 **/
char swordtext[] = "This is your sword.";
void dummy2()
{
    initDialogue(&defaultTypewriter, swordtext, NULL);
}

void showItemsMenu()
{
    setFlag(&quest_menu_items.flags, menu_show_border|menu_active);
    curr_menu = &quest_menu_items;
}

//~ char ask[] = "Are you sure?";
//~ void noise()
//~ {
    //~ setFlag(&question_menu.flags, menu_active);
    //~ question_menu.items[0].func = NULL; //Yes
    //~ question_menu.items[1].func = &useDiary; //No
    //~ initDialog(ask, &question_menu);
//~ }

char save[] = "Game has been saved!";
void savegame()
{
    initDialogue(&defaultTypewriter, save, NULL);
    deactiveMenues(quest_menu.next);
    deactiveMenues(&question_menu);
    quest_menu.curr_item = 0;
    curr_menu = &quest_menu;
}

char talkText[] = "There's nothing to say...";
void talk()
{
    #ifdef DEBUG_DLG
        printf("[Dialogue] Talk action x: %d y: %d:\n", (int)curr_player->x, (int)curr_player->y);
    #endif

    //~ if(initDialog_from_file(curr_player->x+curr_player->direction_x, curr_player->y+curr_player->direction_y, curr_level->name)<=0)
    //~ {
        //~ initDialog(talkText, NULL);
    //~ }

    //~ unsetFlag(&quest_menu.flags, menu_active);	//Hide menu (Close)
    initDialogue(&defaultTypewriter, talkText, NULL);
}

void init_quest_menu()
{
    initClassicMenu(&quest_menu, NULL, 0, 0, "Command", 6, white, NULL, &quest_menu_items);

    addMenuItem(&quest_menu, "Talk", NULL, white, talk);
    addMenuItem(&quest_menu, "Mag ", NULL, white, generic);
    addMenuItem(&quest_menu, "Stat", NULL, white, generic);
    addMenuItem(&quest_menu, "Look", NULL, white, generic);
    addMenuItem(&quest_menu, "Item", NULL, white, showItemsMenu);
    addMenuItem(&quest_menu, "????", NULL, white, generic);

    setMenuTable(&quest_menu, 3, 2, 4*FONT_W, 1);
    setFlag(&quest_menu.flags, menu_persistent|menu_show_border);
    //unsetFlag(&quest_menu.flags, menu_active);

    //Init items and menu
    initClassicMenu(&quest_menu_items, quest_items, 0, quest_menu.y+quest_menu.h+(FONT_H*2), "itemz", QUEST_ITEMS, white, &quest_menu, NULL);
    initStaticMenuItems(quest_items, QUEST_ITEMS);	//Clean the structures before use

    addStaticMenuItem(&quest_menu_items, "Diary", 0, NULL, green, &useDiary);
    addStaticMenuItem(&quest_menu_items, "Sword", 0, NULL, red, &dummy2);
    addStaticMenuItem(&quest_menu_items, "potion", 0, NULL, white, &usePotion);
    addStaticMenuItem(&quest_menu_items, "elixir", 0, NULL, white, &generic);
    addStaticMenuItem(&quest_menu_items, "bomb", 0, NULL, white, &generic);
    //5
    addStaticMenuItem(&quest_menu_items, "armor", 0, NULL, red, &generic);
    addStaticMenuItem(&quest_menu_items, "shield", 0, NULL, red, &generic);

    setMenuPaginator(&quest_menu_items, 5);
    unsetFlag(&quest_menu_items.flags, menu_active);

    //This menu has parent the main menu, as return point for dialogue end
    initClassicMenu(&question_menu, question_items, 208, 136, "Save", 2, white, &quest_menu, NULL);
    initStaticMenuItems(question_items, 2);
    addStaticMenuItem(&question_menu, "Yes", 1, NULL, white, &savegame);
    addStaticMenuItem(&question_menu, "No" , 0, NULL, white, NULL);
//    question_menu.update = &doDialogueMenu; //Custom handler
    unsetFlag(&question_menu.flags, menu_active);
    setFlag(&question_menu.flags, menu_persistent|menu_show_border);
}

void doQuest()
{
    if(!hasFlag(defaultTypewriter.flags, typewriter_active))
    {
        doMenu(curr_menu);
    }

    if(hasFlag(defaultTypewriter.flags, typewriter_waitCallback))
    {
        doMenu(&question_menu);
    }

    //~ doDialogue(&defaultDialogue);
    doTypewriter(&defaultTypewriter);
}

void drawQuest()
{
    drawMenu(&quest_menu);
    drawMenu(&question_menu);
    drawDialogue(&defaultTypewriter);
}
