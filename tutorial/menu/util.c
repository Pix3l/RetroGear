/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  util.c - Generic purpose functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "util.h"
#include "entity.h"
#include "font.h"
#include "timer.h"
#include "fps.h"
#include "gfx.h"
#include "game.h"
#include "level.h"
#include "sfx.h"
#include "tile.h"
#include "controls.h"
#include "typewriter.h"
#include "camera.h"
#include "player.h"
#include "score.h"
#include "menu.h"
#include "transition.h"
#include "event.h"
#include "draw.h"

/**
 * Inizializza i parametri interni del motore di gioco
 **/
void init()
{
    //Game frequency
    fps.frequency= 1000 / 100;

    quit = 0;
    Game.status = MENU;
    curr_menu = &main_menu;
	
    //Init game engine subsystems
    initFont();
    initAudio();	//Quit the program on error
    initScore();
    initController();

    initTypewriter(&defaultTypewriter, FONT_W, (SCREEN_HEIGHT/2)+56, SCREEN_WIDTH-(FONT_W*3), NULL);
    initTransition(TILESIZE, transition_lines);

    setCurrentPlayer(&Player);
    initPlayer(curr_player);

    //Level initialization
    initLevel();
    loadLevel(&level, "main");

    #ifdef DEBUG_LEVEL
        debugLevel(&level);
    #endif

    initCamera(&camera);
}

void reset()
{
    initLevel();

    //Clean all entities
    cleanEntities();
    clearScoreList();

    initPlayer(curr_player);

    initCamera(&camera);

    Game.score = 0;
    Game.flags = 0;
}

void setNotification(int curr_time, char *notification)
{
	sys_timer.start_time = curr_time;
	sprintf(sys_message,notification);
}

int round_int(float value)
{
    return (value > 0.0) ? (value + 0.5) : (value - 0.5);
	//~ return ( (x - floor(x)) >= 0.5 ) ? ceil(x) : floor(x);    
}

/**
 * void cleanUp()
 * 
 * Libera le risorse precedentemente allocate in memoria e termina
 * correttamente le librerie.
 *
 **/
void cleanUp()
{
	quit = 1;

    SDL_FreeSurface(screen);
    SDL_FreeSurface(font_sheet);
    SDL_FreeSurface(tmpfont);
    SDL_FreeSurface(tile_sheet);
    SDL_FreeSurface(alpha_sheet);
    SDL_FreeSurface(fade);  //Transition

    cleanInput();
    cleanEntities();
    //~ clearScoreList();
    cleanEvents(&eventsGeneric, 1);
    cleanEvents(&eventsWarp, 1);
    destroyMenu(&main_menu);

    destroyMusic(title_music);
    destroyMusic(pregame_music);
    destroyMusic(game_music);
    destroyMusic(goal_music);

    //Free default sounds
    destroySound(collectable_snd);
    destroySound(stomp_snd);
    destroySound(bounce_snd);
    destroySound(jump_snd);
    destroySound(action_snd);

    Mix_CloseAudio();
    SDL_Quit();
}
