/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *              event.c - Level events handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "event.h"
#include "level.h"

#define DEBUG_EVENT

char* paramterCopy(char* destination, const char* source);
Event *createEvent(char *mapname, unsigned int evt_x, unsigned int evt_y, char *parameters, int flag_save);

/**
 * Copy a "source" string into destination string "store"
 * and set control characters
 *
 * @param char *store
 *      A pointer to a string for store
 *
 * @param char *source
 *      A pointer to a string for store
 *
 * @return char *
 *      The store pointer
 **/
char* paramterCopy(char *store, const char* source)
{
	// return if no memory is allocated to the destination
	if (store == NULL)
    {
		return NULL;
    }

    #ifdef DEBUG_EVENT
        printf("paramterCopy] Source %s\n", source);
    #endif

    unsigned char c;
    size_t n = 0;

    //~ while ( (c = source[n]) != '\n')
    while ((c = *source++) != '\0')
    {
        #ifdef DEBUG_EVENT
            printf("[paramterCopy] curr char: %c\n", c);
        #endif

        if(c=='\\')
        {
            c = *source++;
            //~ c = source[n++];

            #ifdef DEBUG_EVENT
                printf("[paramterCopy] next char: %c\n", c);
            #endif

            if(c=='n')
            {
                c = '\n';
            }

            if(c=='f')
            {
                c = '\f';
            }
        }

        store[n++] = c;
    }
    store[n++] = '\0';

    #ifdef DEBUG_EVENT
        printf("[paramterCopy] Destination: %s\n", store);
    #endif

	return store;
}

/**
 * Private function that create a new Event and return a pointer
 *
 * @param char *mapname
 * 		Pertinent level name
 * 
 * @param int evt_x, evt_y
 * 		The coordinates of the current event (in tile values)
 * 
 * @param char *parameters
 * 		Event's function parameters
 * 
 * @param int flag_save
 * 		Flag for persistence in the list
 * 
 * @return
 *      Pointer to the newly created Event object
 **/
Event *createEvent(char *mapname, unsigned int evt_x, unsigned int evt_y, char *parameters, int flag_save)
{
    Event *event;
    event = (Event*) calloc(1, sizeof(Event));

    if(event == NULL)
    {
        #ifdef DEBUG_EVENT
            printf("Error creating event: %d %d %s\n", evt_x, evt_y, parameters);
        #endif
        return NULL;
    }

    strcpy(event->mapname, mapname);

    paramterCopy(event->parameters, parameters);

    event->evt_x = evt_x;
    event->evt_y = evt_y;
    event->flag_save = flag_save;
    event->next = NULL;

	return event;
}

void debugEvents(Event *pointer)
{
    if(pointer==NULL)
    {
        printf("Debug Event: list is empty or end\n");
        return;
    }

    printf("Event %s %d %d %s\n", pointer->mapname, pointer->evt_x, pointer->evt_y, pointer->parameters);
    debugEvents(pointer->next);
}

/**
 * Load and create events from file .evt
 *
 * @param char *event_file
 *      Events file name without extension
 *
 * @param EVENT_TYPE evt_type
 **/
void loadEvents(char *event_file, EVENT_TYPE evt_type)
{
    Event **pList = NULL;
    char filename[80];

    switch(evt_type)
    {
        case WARP_EVT:
            #ifdef DEBUG_EVENT
                printf("[loadEvents] Load warp events\n");
            #endif
                
            cleanEvents(&eventsWarp, 1);
            
            pList = &eventsWarp;
            snprintf(filename, 80, "maps/%s.wrp", event_file);
        break;

        case GENERIC_EVT:
            #ifdef DEBUG_EVENT
                printf("[loadEvents] Load generic events\n");
            #endif

            //Some generic events persist during the entire game, don't remove them
            cleanEvents(&eventsWarp, 0);
        
            pList = &eventsGeneric;
            snprintf(filename, 80, "maps/%s.evt", event_file);
        break;

        case OBJECT_EVT:
        break;

        case DIALOGUE_EVT:

            #ifdef DEBUG_EVENT
                printf("[loadEvents] Load dialogue events\n");
            #endif

            cleanEvents(&eventsWarp, 1);
            
            pList = &eventsGeneric;
            snprintf(filename, 80, "dlg/%s.dlg", event_file);
        break;
    }

	FILE *fp = fopen(filename, "r");
	if (fp == NULL)
	{
	    #ifdef DEBUG_FILE_READ
		    perror(filename);
	    #endif
	    return;
	}
	
	/* Read event file: */
	int evt_x, evt_y;
	char parameters[MAX_PARAM_LENGTH];
  
    while (fscanf (fp, "%03d%*c%03d%*c%[^\n]", &evt_x, &evt_y, parameters) != EOF)
    {
        printf("[loadEvents] Paramters: %s\n", parameters);

    	if( addEvent(pList, curr_level->name, evt_x, evt_y, parameters, 0) )
        {
            #ifdef DEBUG_EVENT
                printf("[loadEvents] Loaded event %d %d %s\n", evt_x, evt_y, parameters);
            #endif 
        }
    }
    
    fclose(fp);
    
    #ifdef DEBUG_EVENT
    	debugEvents(eventsGeneric);
    	debugEvents(eventsWarp);
    #endif
}

/**
 * Add an event to the game internal events list
 *
 * @param EventList *pList
 *      Pointer to the events list
 * 
 * @param char *mapname
 * 		Pertinent level name
 * 
 * @param int evt_x, evt_y
 * 		The coordinates of the current event (in tile values)
 * 
 * @param char *parameters
 * 		Event's function parameters
 * 
 * @param int flag_save
 * 		Flag for persistence in the list
 **/
int addEvent(Event **pEvent, char *mapname, unsigned int evt_x, unsigned int evt_y, char *parameters, int flag_save)
{
    Event *curr = NULL;
    //Inserimento head
    if(*pEvent == NULL)
    {
        *pEvent = createEvent(mapname, evt_x, evt_y, parameters, flag_save);

        if(pEvent == NULL)
        {
            return -1;
        }

        curr = *pEvent;

        #ifdef DEBUG_EVENT
            printf("Created head event: %d %d %s\n", curr->evt_x, curr->evt_y, curr->parameters);
        #endif

        return 1;
    }

    curr = *pEvent;
    while (curr!=NULL)
    {
        //Check if the event already exist
        if( curr->evt_x == evt_x && curr->evt_y == evt_y )
        {
            if( strcmp(curr->mapname, mapname) == 0 )
            {
                #ifdef DEBUG_EVENT
                    printf("Event %d %d for map %s already exists\n", evt_x, evt_y, mapname);
                #endif
                return -1;
            }
        }

        if(curr->next==NULL)
        {
            break;  //Last node
        }
        else
        {
            curr = curr->next;
        }
    }

    /* Allocate memory for the new node and put data in it.*/
    curr->next = createEvent(mapname, evt_x, evt_y, parameters, flag_save);
    curr = curr->next;
    if(curr == NULL)
    {
        return -1;
    }
    
    #ifdef DEBUG_EVENT
        printf("Created new event: %d %d %s\n", curr->evt_x, curr->evt_y, curr->parameters);
    #endif

    return 0;
}

/**
 * Search and return a specific event by their arguments for the specified map
 *
 * @param EventList *pList
 *      Pointer to the events list
 * 
 * @param char *mapname
 * 		Pertinent level name
 * 
 * @param int evt_x, evt_y
 * 		The coordinates of the current event (in tile values)
 * 
 * @param char *parameters
 * 		Event's function parameters
 * 
 * @param int flag_save
 * 		Flag for persistence in the list
 *
 * @return Event*
 *      Pointer to the event object, NULL if not found
 **/
Event *getEvent(Event *pList, char *mapname, unsigned int evt_x, unsigned int evt_y)
{
    Event *current = pList;
    while (current!=NULL)
    {
        //Check if the event exist
        if( current->evt_x == evt_x && current->evt_y == evt_y )
        {
            if( strcmp(current->mapname, mapname) == 0 )
            {
                #ifdef DEBUG_EVENT
                    printf("Event found: %d,%d,%s for map %s\n", current->evt_x, current->evt_y, current->parameters, current->mapname);
                #endif
                return current;
            }
        }

        if(current->next==NULL)
        {
            break;  //Last node
        }
        else
        {
            current = current->next;
        }
    }

    #ifdef DEBUG_EVENT
        printf("Event %s %d %d not found\n", mapname, evt_x, evt_y);
    #endif
	
	return NULL;
}

/**
 * Delete an event from the game internal events list
 * 
 * @param char *mapname
 * 		The current level name
 * 
 * @param int evt_x, evt_y
 * 		The coordinates of the current event (in tile)
 **/
void deleteEvent(Event **pList, char *mapname, unsigned int evt_x, unsigned int evt_y)
{
    Event *curr = *pList, *prev = *pList, *next=NULL;
    while (curr!=NULL)
    {
        //Check if the event already exist
        if( curr->evt_x == evt_x && curr->evt_y == evt_y )
        {
            if( strcmp(curr->mapname, mapname) == 0 )
            {
                //Head is tail (one node list)
                if(curr == *pList && curr->next == NULL)
                {
                    #ifdef DEBUG_EVENT
                        printf("Deleted event head/tail: %d,%d,%s for map %s\n", curr->evt_x, curr->evt_y, curr->parameters, curr->mapname);
                    #endif
                    free(curr);
                    *pList=NULL;
                    return;
                }
                
                //head
                if(curr == *pList)
                {
                    next = curr->next;      //The second node
                    #ifdef DEBUG_EVENT
                        printf("Deleted event head: %d,%d,%s for map %s\n", curr->evt_x, curr->evt_y, curr->parameters, curr->mapname);
                    #endif
                    free(curr);             //Delete the head
                    *pList = next;    //The new head is the second node

                    printf("head event is: %d,%d,%s for map %s\n", next->evt_x, next->evt_y, next->parameters, next->mapname);

                }
                //Tail
                else if( curr->next == NULL )
                {
                    prev->next = NULL;
                    free(curr);

                    #ifdef DEBUG_EVENT
                        printf("Deleted event tail %d,%d for map %s\n", evt_x, evt_y, mapname);
                    #endif
                }
                //Body
                else
                {
                    prev->next = curr->next;
                    free(curr);

                    #ifdef DEBUG_EVENT
                        printf("Deleted event body %d,%d for map %s\n", evt_x, evt_y, mapname);
                    #endif
                }
            }
        }

        if(curr->next==NULL)
        {
            break;  //Last node
        }
        else
        {
            //If not tail, save this node as previous
            if( curr->next != NULL )
            {
                prev = curr;
            }

            curr = curr->next;
        }
    }
}

//~ void handleEvent(Event *pevent, EVENT_TYPE type)
//~ {
    //~ switch(type)
    //~ {
        //~ case WARP:
//~ 
            //~ unsigned int dest_x, dest_y;
            //~ char dest_map[10];
        //~ 
            //~ //house;1;3
            //~ sscanf(evt->parameters, "%s;%03d;%03d", arguments);
//~ 
            //~ doWarp(dest_map, dest_x, dest_y);
        //~ break;
//~ 
        //~ case DIALOGUE:
//~ 
            //~ //Hello, this is a dialogue.\n Enjoy it
            //~ doDialog(pevent->parameters);
        //~ break;
    //~ }
//~ }

/**
 * Cleans the event list, saving the only with flag_save setted to 1.
 * With parameter purge setted to 1, completely cleans list freeing up memory.
 *
 * @param EventList *pList
 *      Pointer to the events list
 * 
 * @param int purge
 *      Flag for purging also persistent events from the list
 **/
void cleanEvents(Event **pList, int purge)
{
    Event *current = *pList,
          *tail = *pList,
          *next = NULL;

	while(current!=NULL)
	{
        //Skip this event if flag_save is 1 and purge is not requested
        if(!purge && current->flag_save)
        {
            #ifdef DEBUG_EVENT
                printf("Skip event %s %d %d %s because persistent\n", current->mapname, current->evt_x, current->evt_y, current->parameters);
            #endif
            tail = current;    //Ultimo evento in lista ad essere skippato
            current=current->next;
        }
        else
        {
            #ifdef DEBUG_EVENT
                printf("Purge event: %d,%d,%s for map %s\n", current->evt_x, current->evt_y, current->parameters, current->mapname);
            #endif
            next=current->next;
            tail->next = next;  //Last saved next will be current next
            free(current);
            current=next;
        }
    }

    if(purge)
    {
        *pList = NULL;    //Clean head pointer
    }
}
