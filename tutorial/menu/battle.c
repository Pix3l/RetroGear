/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             		quest.c - RPG like example menu					  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <string.h>

#include "battle.h"
#include "controls.h"
#include "gfx.h"
#include "font.h"
#include "dialogue.h"
#include "menu.h"
#include "menu_classic.h"
#include "menu_paginator.h"
#include "bit.h"
#include "quest.h"
#include "game.h"

int player_energy = 100;
int player_magic = 100;
int player_level = 5;

int battle_Ask()
{
    //~ printf("curr_menu %p, battle_menu %p\n", curr_menu, &battle_menu);
    curr_menu = &battle_menu;
    setFlag(&battle_menu.flags, menu_active);

    return 1;
}

static void generic()
{
    unsetFlag(&battle_menu.flags, menu_active);
    unsetFlag(&quest_menu_items.flags, menu_active);

    battle_menu.curr_item = 0;

    snprintf(dlg_holder, MAX_PARAM_LENGTH,"element %s", curr_menu->items[curr_menu->curr_item].name);
    initDialogue(&defaultTypewriter, dlg_holder, battle_Ask);
}

void attackEnemy()
{
    setFlag(&battle_menu.flags, menu_active);
    unsetFlag(&enemy_menu.flags, menu_active);
    
    snprintf(dlg_holder, MAX_PARAM_LENGTH, "Attacking %s enemy!\fInflicts %s\nX points of damage!", enemy_menu.items[curr_menu->curr_item].name, enemy_menu.items[curr_menu->curr_item].name);
    initDialogue(&defaultTypewriter, dlg_holder, battle_Ask);
}

/**
 * Battle actions
 **/
void battle_ActionAttack()
{
    unsetFlag(&enemy_menu.flags, menu_active);
    unsetFlag(&quest_menu_items.flags, menu_active);
    
    curr_menu = &enemy_menu;
    setFlag(&enemy_menu.flags, menu_active);
}

char runtext[] = "Can\'t escape!";
void battle_ActionRun()
{
    unsetFlag(&battle_menu.flags, menu_active);
    initDialogue(&defaultTypewriter, runtext, battle_Ask);
}

void battle_ActionItems()
{
    curr_menu = &quest_menu_items;
    setFlag(&quest_menu_items.flags, menu_active);
}

char battletext[] = "Monsters appear!";
void init_battle()
{
    curr_menu = &battle_menu;
	initSprite(&monster, (SCREEN_WIDTH/2)-16, (SCREEN_HEIGHT/2)-16, 32, 32, 0, "data/quest_ichigo.bmp");
    
    //Controls
    initClassicMenu(&battle_menu, battle_actions, 0, (SCREEN_HEIGHT/2)+73, "actions", 4, white, NULL, NULL);
    initStaticMenuItems(battle_actions, 4);	//Clean the structures before use

    addStaticMenuItem(&battle_menu, "Attack", 0, NULL, white, &battle_ActionAttack);
    addStaticMenuItem(&battle_menu, "Items", 0, NULL, white, &battle_ActionItems);
    addStaticMenuItem(&battle_menu, "Defense", 0, NULL, white, &generic);
    addStaticMenuItem(&battle_menu, "Run", 0, NULL, white, &battle_ActionRun);
    setFlag(&battle_menu.flags, menu_persistent|menu_show_border);
    unsetFlag(&battle_menu.flags, menu_active);

    //Enemies
    initClassicMenu(&enemy_menu, enemy_list, FONT_W*10, (SCREEN_HEIGHT/2)+73, "enemies", 4, white, &battle_menu, NULL);
    initStaticMenuItems(enemy_list, 4);	//Clean the structures before use
    addStaticMenuItem(&enemy_menu, "Ichigo 1", 0, NULL, white, &attackEnemy);
    addStaticMenuItem(&enemy_menu, "Ichigo 2", 0, NULL, white, &attackEnemy);
    unsetFlag(&enemy_menu.flags, menu_active);

    //Items
    initClassicMenu(&quest_menu_items, quest_items, FONT_W*10, (SCREEN_HEIGHT/2)+73, "items", 4, white, &battle_menu, NULL);
    initStaticMenuItems(quest_items, 4);	//Clean the structures before use

    addStaticMenuItem(&quest_menu_items, "potion", 0, NULL, white, &generic);
    addStaticMenuItem(&quest_menu_items, "elixir", 0, NULL, white, &generic);
    addStaticMenuItem(&quest_menu_items, "bomb", 0, NULL, white, &generic);
    unsetFlag(&quest_menu_items.flags, menu_active);

    //Start a dialogue
    initDialogue(&defaultTypewriter, battletext, &battle_Ask);
}

void doBattle()
{
    if(!hasFlag(defaultTypewriter.flags, typewriter_active))
    {
        doMenu(curr_menu);
    }

    doTypewriter(&defaultTypewriter);
}

/**
 * Custom rendering for enemies and relatives menues
 **/
void drawEnemies()
{
}

void drawBattle()
{
    drawSprite(&monster, monster.x-32, monster.y);
    drawSprite(&monster, monster.x+32, monster.y);
    
    drawDialogue(&defaultTypewriter);

    /**
     * Party info
     **/
    drawGui(FONT_W, FONT_H, SCREEN_WIDTH-(FONT_W*3), 48, 0x000000, white);

    drawString(FONT_W*2, 0, "Pix3l", white, 0);
    sprintf(message,"HP: %d", player_energy);
    drawString(FONT_W*2, FONT_H*6, message, white, 0);
    sprintf(message,"MP: %d", player_magic);
    drawString(FONT_W*2, FONT_H*4, message, white, 0);
    sprintf(message,"LV: %d", player_level);
    drawString(FONT_W*2, FONT_H*2, message, white, 0);

    drawString(FONT_W*12, 0, "Serpo", white, 0);
    sprintf(message,"HP: %d", player_energy);
    drawString(FONT_W*12, FONT_H*6, message, white, 0);
    sprintf(message,"MP: %d", player_magic);
    drawString(FONT_W*12, FONT_H*4, message, white, 0);
    sprintf(message,"LV: %d", player_level);
    drawString(FONT_W*12, FONT_H*2, message, white, 0);

    drawString(FONT_W*22, 0, "Gaia", white, 0);
    sprintf(message,"HP: %d", player_energy);
    drawString(FONT_W*22, FONT_H*6, message, white, 0);
    sprintf(message,"MP: %d", player_magic);
    drawString(FONT_W*22, FONT_H*4, message, white, 0);
    sprintf(message,"LV: %d", player_level);
    drawString(FONT_W*22, FONT_H*2, message, white, 0);
    /***/

    if(!hasFlag(defaultTypewriter.flags, typewriter_active))
    {
        drawGui(FONT_W*11, SCREEN_HEIGHT-(FONT_W*5), FONT_W*19, FONT_W*4, 0x000000, white);
        drawMenu(&battle_menu);
        drawMenu(&enemy_menu);
        drawMenu(&quest_menu_items);
    }
}
