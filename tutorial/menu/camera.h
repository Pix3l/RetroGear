/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   camera.h - Scrolling functions                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _CAMERA_H
#define _CAMERA_H

#include "rgtypes.h"

#define CENTER_X ((SCREEN_WIDTH - TILESIZE) / 2)
#define CENTER_Y ((SCREEN_HEIGHT - TILESIZE) / 2)

RG_rect camera;

void initCamera(RG_rect *pcam);
void scrollCameraX(RG_rect *pcam, int x);
void scrollCameraY(RG_rect *pcam, int y);
int is_onCamera(RG_rect *pcam, RG_rect area);

#endif
