/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  drmario.c - Dr. Mario example menu                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 			Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "drmario.h"
#include "menu_classic.h"
#include "menu_selector.h"
#include "font.h"
#include "gfx.h"
#include "game.h"
#include "bit.h"

void init_drMario_menu()
{
    //Virus level menu
    initSelector(&virus_level, 10, 40, "Virus level", white, 20, NULL, &speed);
    setFlag(&virus_level.flags, menu_show_title|menu_show_border);
    virus_level.cursor=31;
    virus_level.draw = &drawVirusLevel; //Custom draw

    //Speed menu
    initClassicMenu(&speed, NULL, 10, 100, "Speed", 3, white, &virus_level, &music);
    setFlag(&speed.flags, menu_show_title);
    unsetFlag(&speed.flags, menu_active);
    speed.draw = &drawSpeedMenu; //Custom draw
    addMenuItem(&speed, "Low", "", white, doPreGame);
    addMenuItem(&speed, "Mid", "", white, doPreGame);
    addMenuItem(&speed, "High", "", white, doPreGame);
    //~ setMenuTable(&speed, 1, 1);
//    menuTable(&speed, 1, 3, 4, 0);

    //Music menu
    initClassicMenu(&music, NULL, 10, 180, "Music", 3, white, &speed, NULL);
    setFlag(&music.flags, menu_show_title);
    unsetFlag(&music.flags, menu_active);
    music.draw = &drawMusicMenu; //Custom draw
    addMenuItem(&music, "Fever", "", white, doPreGame);
    addMenuItem(&music, "Chill", "", white, doPreGame);
    addMenuItem(&music, "Off", "", white, doPreGame);
    //~ setMenuTable(&music, 1, 1);
//    menuTable(&music, 1, 3, 5, 0);
}

void drawVirusLevel()
{
    if(hasFlag(virus_level.flags, menu_active))
    {
        //~ drawGui(virus_level.x, virus_level.y, virus_level.w, virus_level.h, 0x000000);
        drawGui(virus_level.x, virus_level.y-(FONT_H*3)-1, FONT_W*(strlen(virus_level.name)), FONT_H, BLACK, white);
    }

    if(hasFlag(virus_level.flags, menu_show_title))
    {
        drawString(virus_level.x, virus_level.y-(FONT_H*3), virus_level.name, virus_level.color, 0);
    }

    drawChar(virus_level.x-(FONT_W/2)+(virus_level.curr_item*SELECTOR_SPACE), virus_level.y-FONT_H-2, virus_level.cursor, white, 0);
    
    char message[10];
    sprintf(message,"%02d", virus_level.curr_item);
    int x = virus_level.x+(virus_level.num_items*SELECTOR_SPACE)+20;
    //~ drawGui(x, virus_level.y, FONT_W*2, FONT_H, 0x000000, red);
    drawString(virus_level.x+(virus_level.num_items*SELECTOR_SPACE)+20, virus_level.y, message, white, 0);

    drawFillRect(virus_level.x, virus_level.y+FONT_H, virus_level.num_items*SELECTOR_SPACE, 2, 0xffffff);

    int i;
    for(i=0; i<virus_level.num_items/2; i++)
    {
        if(i%2)
        {
            drawFillRect(virus_level.x*i, virus_level.y+6, 1, 6, 0xffffff);
        }

        //~ if(i%virus_level.num_items)
        //~ {
                //~ drawFillRect(virus_level.x*i, virus_level.y-2, 1, 6, 0xff0000);
        //~ }
    }
}

//TODO: accorpare le due funzioni in una?

void drawSpeedMenu()
{
    if(hasFlag(speed.flags, menu_active))
    {
        drawGui(speed.x, speed.y-(FONT_H*3)-1, FONT_W*(strlen(speed.name)), FONT_H, BLACK, red);
    }

    drawString(speed.x, speed.y-(FONT_H*3), speed.name, speed.color, 0);

    int i;
    for(i = 0; i < speed.num_items; ++i)
    {
        drawString(speed.items[i].x, speed.items[i].y, speed.items[i].name, speed.items[i].color, 0);
    }

    drawChar(speed.items[speed.curr_item].x-FONT_W, speed.items[speed.curr_item].y, speed.cursor, speed.color, 0);
}

void drawMusicMenu()
{
    if(hasFlag(music.flags, menu_active))
    {
        drawGui(music.x, music.y-(FONT_H*3)-1, FONT_W*(strlen(music.name)), FONT_H, BLACK, red);
    }

    drawString(music.x, music.y-(FONT_H*3), music.name, music.color, 0);

    int i;
    for(i = 0; i < music.num_items; ++i)
    {
        if(i==music.curr_item)
        {
            drawGui(music.items[music.curr_item].x, music.items[music.curr_item].y+(i*FONT_H), FONT_W*(strlen(music.items[music.curr_item].name))-4, FONT_H, 0x000000, red);
        }

        drawString(music.items[i].x, music.items[i].y+(i*FONT_H), music.items[i].name, music.items[i].color, 0);
    }
	
    //~ drawChar(screen, bmpfont, music.items[music.curr_item].x-FONT_W, music.items[music.curr_item].y, FONT_W, FONT_H, music.cursor, music.color, 0);
}
