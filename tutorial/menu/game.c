/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   game.c - Game handling function                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "game.h"
#include "typewriter.h"
#include "fps.h"
#include "draw.h"
#include "controls.h"
#include "menu.h"
#include "player.h"
#include "entity.h"
#include "sfx.h"
#include "util.h"
#include "drmario.h"
#include "quest.h"
#include "ouijaui.h"
#include "dialogue.h"
#include "battle.h"

/**
 * void doTitleScreen()
 * 
 * Questa funzione gestisce la schermata dei titoli e relativo menu.
 * Permette al giocatore di scegliere varie opzioni di gioco.
 **/ 
void doTitleScreen(void)
{
    /**
     * Since its possibile to have multiple menu on title screen we use
     * a generic pointer
     **/
    doMenu(curr_menu);

	//If music is not playining
    if(!isMusicPlaying())
    {
    	//~ playMusic(title_music, 0);
    }
}

/**
 * void doPreGame()
 * 
 * Questa funzione gestisce uno screen di pre-game.
 * Permette al giocatore di prendersi una manciata di secondi per
 * prepararsi prima di iniziare il gioco vero e proprio.
 **/ 
void doPreGame(void)
{
    if(Game.status!=PREGAME)
    {
        setGameState(PREGAME);
        init_drMario_menu();
        curr_menu = &virus_level;
    }
    
    doMenu(curr_menu);
}

void doGame(void)
{
    if(Game.status!=GAME)
    {
        setGameState(GAME);
        init_quest_menu();
        curr_menu = &quest_menu;
    }

    doQuest();
}

/**
 * Gestisce gli eventi dello stato di vittoria
 **/
void doWin(void)
{
    if(Game.status!=WIN)
    {
        setGameState(WIN);
        curr_menu = &ouija;
        initOuija();
    }

    doOuija();
}

void doGameOver(void)
{
    if(Game.status!=GAMEOVER)
    {
        setGameState(GAMEOVER);
        init_battle();
    }

    doBattle();
    
    //~ doDialog();

    //~ if(isTypewriterEnd(curr_typewriter))
    //~ {
        //~ curr_menu = &battle_menu;
    //~ }

	//~ if (curr_gamepad->button_Start || curr_gamepad->button_A)
    //~ {
    	//~ curr_gamepad->button_Start = 0;
    	//~ curr_gamepad->button_A = 0;
    	//~ reset();
        //~ setGameState(MENU);
    //~ }
}

void doLogic(void)
{
	//We reset alives entities position to their start value before
	//restart the game
	if(Game.status==PREGAME)
	{
		resetEntities();
	}
}

void setGameState(int state)
{
	Game.status = state;
}

/**
 * Pause or unpause the game and play sound
 **/
void pauseGame()
{
	if (curr_gamepad->button_Start)
	{
		playSound(pause_snd);
		curr_gamepad->button_Start=0;

		if(!hasFlag(Game.flags, on_pause))
		{
            setFlag(&Game.flags, on_pause);
			pauseMusic();
			return;
		}
		
		if(hasFlag(Game.flags, on_pause))
		{
			unsetFlag(&Game.flags, on_pause);
			resumeMusic();
			return;
		} 
	}
}

void mainLoop(void)
{
    static Uint32 then = 0;
    static unsigned int dtime = 0;

    //main game loop
    while(!quit)
    {
        unsigned int maxl = 256;
        Uint32 now = SDL_GetTicks();
        dtime += now - then;
        fps.t = now;
        then = now;

        while (--maxl && dtime >= LOGICMS)
        {
            inputHandler();

            switch(Game.status)
            {
                case MENU:
                        doTitleScreen();
                        break;
                case PREGAME:
                        doPreGame();
                        break;
                case GAME:
                        doGame();
                        break;
                case LOST:
                        doLogic();
                        break;
                case WIN:
                        doWin();
                        break;
                case GAMEOVER:
                        doGameOver();
                        break;
            }

            now = SDL_GetTicks();
            dtime += now - then - LOGICMS;
            then = now;
        }

        render_time();

        draw();

        SDL_Delay(1);
    }
}
