/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    level.c - Level file handling                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _LEVEL_H
#define _LEVEL_H

#include <SDL/SDL.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "event.h"

#define COLS 300
#define ROWS 300
#define LAYERS 3

/**
 * Current map layer
 * 0 - Entities / Solid blocks
 * 1 - Background
**/
#define SOLID_LAYER 0
#define BACKG_LAYER 1
#define ALPHA_LAYER 2

/**
 * Current map layer
 * 0 - Entities / Solid blocks
 * 1 - Background
**/
unsigned int level_index;   //Current level index

typedef struct _Level
{
	char name[20];      //level filename without extension
	char description[20];
	char theme[5];
	char song_title[10];
	//~ char bkgd_image[100];
	int bkgd_red, bkgd_green, bkgd_blue;
	int map[LAYERS][ROWS][COLS];
	unsigned int cols, rows;
	unsigned int curr_layer;
	unsigned int num_layers;
	int flag_complete;	//Is the level completed?
    unsigned char flags;
} Level;

Level level, *curr_level;

//Level sprite sheets
SDL_Surface *tile_sheet, *alpha_sheet;

void initLevel();
int loadLevel(Level* plevel, char *filename);
void resetLevel(Level* plevel);
void debugLevel(Level* plevel);
void saveLevel(void);
void drawTileMap(Level *plevel);
void doWarp(char *mapname, unsigned int col, unsigned int row);
#endif
