/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 menu.h - Game menu handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _MENU_H
#define _MENU_H

typedef enum _Menu_flags {
    menu_active         = 1 << 0,  // 000001  1
    menu_show_title     = 1 << 1,  // 000010  2
    menu_show_border    = 1 << 2,  // 000100  4
    menu_persistent     = 1 << 3,  // 000100
    //~ flag     = 8     // 001000  8
} Menu_flags;

#include <SDL/SDL.h>

/**
 * Generic game menu structure
 * 
 * @param int flag_active
 * 		Active/deactive menu. Default 1.
 * 		-1: Persistent
 * 		 0: Invisible/Inactive
 * 		 1: Visible/Active
 * 
 * @param int flag_title
 * 		Show/Hide menu title. Default 0.
 * 
 * @param int flag_border
 * 		Show/Hide menu border. Default 1.
 *
 * @param int x,y
 * 		Menu coordinate
 * 
 * @param unsigned int w,h
 * 		Menu dimensions (Used for drawing borders)
 * 
 * @param char name
 * 		Menu name
 * 
 * @param char cursor
 * 		Menu cursors character. Default '>'.
 *
 * @param int num_items
 * 		Number of items stored in the menu
 * 
 * @param int max_items
 * 		Number of maximum items can be stored in the menu
 * 		-1 Infinite
 * 		0 None
 * 
 * @param int curr_item
 * 		Item actually selected
 * 
 * @param SDL_Color color
 * 		Menu color, affect border and cursor
 * 
 * @param _Item *items
 * 		Pointer to Item structure containing menu contents
 * 
 * @param _Menu *parent, *child
 * 		Parent Menu pointer, Child Menu Pointer.
 **/
typedef struct _Menu {
	unsigned char flags;
	int x, y;
	unsigned int w, h;
	char name[20];
	char cursor;
	int rows, cols;
	unsigned int page_start; //Supporto alla paginazione nei menù, elemento da cui inizia la pagina da mostrare
	int num_items, max_items;
	int curr_item;
	SDL_Color color;
	struct _Item *items;
	struct _Menu *previous, *next;
	void (*update)();
	void (*draw)();
} Menu;

/**
 * Menu items structure
 * 
 * @param int id
 * 		Item ID
 * 
 * @param int x, y
 * 		Item coordinate
 * 
 * @param char *name
 * 		Item name	//TODO: Limit to 8 chars?
 * 
 * @param int value
 * 		Item value (Ex: price or quantity)
 * 
 * @param SDL_Color color
 * 		Item's text color
 * 
 * @param void (*func)()
 * 		Function to be called on item selected
 **/
typedef struct _Item
{
	int flag_used;		//Item setted or not
	int x, y;
	char name[15];
	char *description;	//Optional content for some type of menu (invetory)
	int	value;
	SDL_Color color;
	void (*func)();
	//Icon stuff
	//~ SDL_Surface *icon;
	//~ float frame_index, animation_speed;
} Item;

Menu main_menu, *curr_menu;

void initMenu(Menu* pmenu, Item *pitem, int x, int y, char *name, int max_items, SDL_Color color, Menu *parent, Menu *child, void(*update)(), void(*draw)());
//~ Item* createItem(char *name, char* desc, SDL_Color color, void (*func)()); //, Menu *pmenu)
int addMenuItem(Menu *pmenu, char *name, char* desc, SDL_Color color, void (*func)());
void initStaticMenuItem(Item pitem);
void initStaticMenuItems(Item pitem[], int max_items);
int addStaticMenuItem(Menu *pmenu, char *name, int value, char *description, SDL_Color color, void (*call)());
void doMenu(Menu *pmenu);
void drawMenu(Menu *pmenu);
int isMenuPersistent(Menu *pmenu);
void previousMenu(Menu *pmenu);
void nextMenu(Menu *pmenu);
void destroyMenu(Menu *pmenu);
void alignMenuCenter(Menu* pmenu);
void alignMenuBottom(Menu* pmenu);
void alignItemRow(Menu* pmenu, int rows, int flag_packed);
void setMenuCursor(Menu* pmenu, char cursor);
void setItemPosition(Item* pitem, int x, int y);
void deactiveMenues(Menu *pmenu);
void menuInput(Menu *pmenu);

#endif
