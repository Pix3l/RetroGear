/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                     entity.h - Game object type                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _ENTITY_H
#define _ENTITY_H

#include <SDL/SDL.h>
#include "sprite.h"

#define ANIM_SPEED 10		//Default animation speed

//Sprite index in the standard sprite sheet
enum
{
	//Left
	STANDLEFT = 0,
	WALKLEFT1 = 1,
	WALKLEFT2 = 2,
	WALKLEFT3 = 3,
	SIDESLIP1 = 4,
	JUMPLEFT = 5,

	//Right
	STANDRIGHT = 6,
	WALKRIGHT1 = 7,
	WALKRIGHT2 = 8,
	WALKRIGHT3 = 9,
	SIDESLIP2 = 10,
	JUMPRIGHT = 11,

	//Down
	STANDDOWN = 12,
	WALKDOWN1 = 13,
	WALKDOWN2 = 14,
	WALKDOWN3 = 15,
	SIDESLIP3 = 16,

	//Up
	STANDUP = 18,
	WALKUP1 = 19,
	WALKUP2 = 20,
	WALKUP3 = 21,
	SIDESLIP4 = 22,
    
	CLIMB1 = 22,
	CLIMB2 = 23,
	
	DIE = 17

} spriteSheet;

//Flag di status delle entity
typedef enum
{
    MOVE, ACTION, JUMP, FALL, CLIMB, STAND, BLINK, KILL, DESTROY
} ENTITY_STATUS;

typedef enum
{
	UP=0, RIGHT=90, DOWN=180, LEFT=270
} ENTITY_DIRECTION;

typedef enum
{
    PLAYER, COLLECTABLE, ENEMY, BULLET, WALL, OBSTACLE
} ENTITY_TYPE;

typedef struct _Entity {
	int id;
	int type;		    //Entity type
	int visible;	    //Flag visibile/invisibile
	int flag_active;	//Flag active/inactive (Ex: Inactive when outside of the screen)
	//~ int solid;
	float x, y;
	int w, h;
	float xstart, ystart;
	float previous_x, previous_y;	
	int lives;		//HINT: Use it as energy if you don't need it as lives!
	//~ int score;		//Personal score (actual score / given points)	//Definito globalmente...
	int direction_x, direction_y;
	int timer[3];	//Timer timer;

	//Entity sprite
	Sprite sprite;
	
	//Fisica
	float speed;	// Its current speed (pixels per step).
	float gravity;	
	float hspeed, vspeed;

	int status;				//Stato attuale della entity (bump, attacco, sconfitta, da distruggere,...)
	void (*update)();       //Entity update function
	struct _Entity *next;	//Next entity in list
} Entity;

Entity *EntityList_Head, *EntityList_Tail;

//~ gravity_direction Direction of gravity (270 is downwards).
//~ motion_set(dir,speed) Sets the motion with the given speed in direction dir.
//~ motion_add(dir,speed) Adds the motion to the current motion (as a vector addition).
//~ path_index Index of the current path the instance follows. Set to -1 to have no path.
//~ path_position Position in the current path. 0 is the beginning of the path. 1 is the end of the path.
//~ path_orientation Orientation (counter-clockwise) into which the path is performed. 0 is the normal orientation of the path.
//~ path_scale Scale of the path. Increase to make the path larger. 1 is the default value.

void createEntity(int type, int x, int y, int w, int h, int lives, float speed, float gravity, void (*update)());
void initEntity(Entity *pobj, int type, int x, int y, int w, int h, int lives, float speed, float gravity, void (*update)());
void setEntityPosition(Entity *pobj, int x, int y, int dirx, int diry);
void createEntityFromMap(int id, int x, int y);
void doEntities();
void resetEntities();
int countEntities();
int countTypeEntity(int type);
int countEntityOnCamera(int type);
void moveEntity_X(Entity *pobj);
void doEntityGravity(Entity *pobj);
int isEntityOnFloor(Entity *pobj);
void jumpEntity(Entity *pobj);
void animateEntity(Entity *pobj, int loop);
void drawEntity(Entity *pobj);
void cleanEntities();

#endif
