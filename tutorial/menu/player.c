/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  player.c - Generic game main player               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <math.h>

#include "player.h"
#include "controls.h"
#include "tile.h"
#include "camera.h"
#include "gfx.h"
#include "sfx.h"
#include "game.h"
#include "font.h"
#include "fps.h"
#include "timer.h"
#include "entity.h"
#include "rgtypes.h"
#include "score.h"
#include "collision.h"
#include "transition.h"

//Private functions
void playerAction();
void movePlayerStatic();
void movePlayerDynamic();
void playerThink();

int think;
float speed_limit;

/**
 * Reset the player's default values
 * 
 * @param Entity *player
 * 		Pointer to a Player objects
 **/
void initPlayer(Entity *player)
{
    player->type = PLAYER;
    player->visible = 1;
    player->flag_active = 1;
	player->w= PLAYER_W;
	player->h= PLAYER_H;
	player->lives = 3;
	player->speed = 0.05f;

    //Difference between sprite and player size
    int sprite_diff_w = (PLAYER_SPRITE_W - PLAYER_W) / 2;
    int sprite_diff_h = (PLAYER_SPRITE_H - PLAYER_H) -1;

	if( !player->sprite.surface )
	{
		initSprite(&player->sprite, player->x-sprite_diff_w, player->y-sprite_diff_h, PLAYER_SPRITE_W, PLAYER_SPRITE_H, 0.09f, "data/player.bmp");
	}

	player->hspeed = 0;
	player->vspeed = 0;

    player->gravity=0.1f;
    
	player->direction_x = 1;
	player->direction_y = 0;

    speed_limit = MAX_H_SPEED;
}

void setCurrentPlayer(Entity *player)
{
    curr_player = player;
}

void setPlayerPosition(int x, int y)
{
	curr_player->x = x;
	curr_player->y = y;
	curr_player->xstart = x;
	curr_player->ystart = y;
}

void playerExtraLife()
{
	curr_player->lives++;
	playSound(extralife_snd);
}

void playerAction()
{
	curr_player->sprite.index = 0;	//Action sprite index
	//Action time
	//Action function	
}

/**
 * Handle keyboard keys and update player moves and status 
 **/
void updatePlayer()
{
    //~ scrollCameraX(&camera, curr_player->x);
    //~ scrollCameraY(&camera, curr_player->y);
	
    //~ animatePlayer();
    animateEntity(curr_player, 0);
    movePlayerStatic();
}

/**
 * Move the player with dynamic speed
 **/
void movePlayerDynamic()
{
	/**
     * Horizontal movement
     **/

	if (curr_gamepad->button_Left)
	{
		curr_player->direction_x = -1;
        curr_player->status = MOVE;

        curr_player->hspeed += curr_player->speed * curr_player->direction_x;
	}
	
	if (curr_gamepad->button_Right)
	{
		curr_player->direction_x = +1;
        curr_player->status = MOVE;

        curr_player->hspeed += curr_player->speed * curr_player->direction_x;
	}

    //Decrease velocity only when touching the floor
    if ( isEntityOnFloor(&Player) )
    {
        points_index = 0;   //Reset points indexer
        
        if (!curr_gamepad->button_Left && !curr_gamepad->button_Right)
        {
            float friction = curr_player->speed/2;
            if(curr_player->hspeed > 0.0f)
            {
                curr_player->hspeed -= friction;

                if(curr_player->hspeed < 0.0f)
                {
                    curr_player->hspeed = 0.0f;
                    curr_gamepad->button_Left = 0;
                }
            }
            else if(curr_player->hspeed < 0.0f)
            {
                curr_player->hspeed += friction;

                if(curr_player->hspeed > 0.0f)
                {
                    curr_player->hspeed = 0.0f;
                    curr_gamepad->button_Right = 0;
                }
            }
        }

        //Set the maximum speed for walk and run
        speed_limit = ( curr_gamepad->button_B )? MAX_RUN_SPEED : MAX_H_SPEED;
    }

    //Limit maximum speed
    if (curr_player->hspeed > speed_limit) curr_player->hspeed = speed_limit;
    if (curr_player->hspeed < -speed_limit) curr_player->hspeed = -speed_limit;

    RG_Point point = {-1, -1};

    //Checking collision on the left
    if(curr_player->hspeed<0)
    {
        //Has collide on left?
        point = tileCollision(&Player, floorf(curr_player->x+curr_player->hspeed), curr_player->y);
        if( point.x != -1 )
        {
            curr_player->x= point.x+TILESIZE;
            curr_player->hspeed = 0;  //No need to slide if we touch an obstacle
        }
        else
        {
            curr_player->x += curr_player->hspeed;         //Update its position
        }
    }
    else if(curr_player->hspeed>0)
    {
        point = tileCollision(&Player, ceilf(curr_player->x+curr_player->hspeed), curr_player->y);
        if( point.x != -1 )
        {
            curr_player->x= point.x-curr_player->w;
            curr_player->hspeed = 0;  //No need to slide if we touch an obstacle
        }
        else
        {
            curr_player->x += curr_player->hspeed;         //Update its position
        }
    }

	/**
     * vertical movement
     **/
	if (curr_gamepad->button_A==PRESSED)
	{
        curr_gamepad->button_A = LOCKED;
        if(!isEntityOnFloor(&Player))
        {
            return;
        }

        playSound(jump_snd);

		//if the player isn't jumping already
		curr_player->vspeed = -5.0f;		//jump!
	}

	if (!curr_gamepad->button_A)
	{	
		//if the player isn't jumping already
		curr_player->vspeed+=curr_player->gravity;
	}

    if(curr_player->vspeed>0)
    {
        //Has collide on bottom?
        if(isEntityOnFloor(&Player))
        {
            if(!curr_gamepad->button_A)	//player only may jump again if the jump key is released while on ground
                curr_gamepad->button_A = 0;
        }
    }

    doEntityGravity(&Player);
}

/**
 * Move the player with dynamic speed
 **/
void movePlayerStatic()
{
    RG_Point point = {-1, -1};

    //Checking collision on the left and right
    if(curr_player->hspeed>0 || curr_player->hspeed<0)
    {
        //Move only if there's no obstacles
        point = tileCollision(&Player, floorf(curr_player->x+curr_player->hspeed), floorf(curr_player->y+curr_player->vspeed));
        if( point.x != -1 )
        {
            think = 0;
            curr_player->x += curr_player->hspeed;
            curr_player->status = MOVE;
        }
    }

    //Checking collision on up and down
    if(curr_player->vspeed>0 || curr_player->vspeed<0)
    {
        //Move only if there's no obstacles
        point = tileCollision(&Player, floorf(curr_player->x+curr_player->hspeed), floorf(curr_player->y+curr_player->vspeed));
        if( point.y != -1 )
        {
            think = 0;
            curr_player->y += curr_player->vspeed;
            curr_player->status = MOVE;
        }
    }

    //If the player is not aligned to grid, we ignore the input
	if(!isInTile(curr_player->x,curr_player->y))
    {
        return;
    }
    else
    {
        if(think==0)
        {
            playerThink();
        }
    }

	/**
     * Horizontal movement
     **/
	if (curr_gamepad->button_Left && curr_player->status == STAND)
	{
		curr_player->direction_x = -1;
        curr_player->direction_y = 0;
        
        curr_player->hspeed = -1.0f;
	}

    if (curr_gamepad->button_Right && curr_player->status == STAND)
	{
		curr_player->direction_x = 1;
        curr_player->direction_y = 0;
        
        curr_player->hspeed = +1.0f;
	}

	if (!curr_gamepad->button_Left && !curr_gamepad->button_Right)
	{
        curr_player->hspeed = 0;
        curr_player->status = STAND;
	}
	
	/**
     * vertical movement
     **/
	if (curr_gamepad->button_Up && curr_player->status == STAND)
	{
		curr_player->direction_y = -1;
        curr_player->direction_x = 0;
        
        curr_player->vspeed = -1.0f;
	}
	
	if (curr_gamepad->button_Down && curr_player->status == STAND)
	{
		curr_player->direction_y = 1;
        curr_player->direction_x = 0;
        
        curr_player->vspeed = +1.0f;
	}
    
	if (!curr_gamepad->button_Up && !curr_gamepad->button_Down)
	{
        curr_player->vspeed = 0;
        curr_player->status = STAND;
	}
}

void playerThink()
{
    int col, row;
    //Clean previous position
    col = pixelsToTiles( Player.x );
    row = pixelsToTiles( Player.y );

    //Check for stairs
    if(curr_level->map[SOLID_LAYER][row][col] == WARP)
    {
        //Do the warp!
        Event *warp = getEvent(eventsWarp, curr_level->name, col, row);
        
        if(warp)
        {
            printf("Warp event\n");
            //~ playSound(stairs_snd);
            
            unsigned int dest_x, dest_y;
            char dest_map[10];
            
            //%[^,] means read until a comma is encountered
            sscanf (warp->parameters, "%[^','],%d,%d", dest_map, &dest_x, &dest_y);
            
            printf("%s %d,%d\n", dest_map, dest_x, dest_y);
            
            initTransition(TILESIZE, transition_fadein);
            transition.flag_active=1;
            //~ nextLevel(dest_map, dest_x, dest_y);

            cleanEntities();
            loadLevel(curr_level, dest_map);
            initCamera(&camera);
            setPlayerPosition(dest_x, dest_y);
        }
    }

    think = 1;
}
