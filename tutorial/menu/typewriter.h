/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               typewriter.h - Virtual typewriter for text           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _TYPEWRITER_H
#define _TYPEWRITER_H

typedef enum _Typewriter_flags {
    typewriter_inactive     = 1 << 0,  // 000001  1
    typewriter_active       = 1 << 1,  // 000010  2
    typewriter_paused       = 1 << 2,  // 000100  4
    typewriter_stopped      = 1 << 3,  // 001000
    typewriter_waitCallback = 1 << 4,  // 010000
    typewriter_waitPage     = 1 << 5,  // 010000
    //~ flag     = 8     // 001000  8
} Typewriter_flags;

typedef struct _Typewriter {
    int x, y;		//Origins
    int max_width;	//Max width
    int curPos;
    char *text;		//Pointer to text
    int delay;
    int (*callback)();
    unsigned char flags;
} Typewriter;

Typewriter typewriter, *curr_typewriter;
Typewriter defaultTypewriter;

#include <SDL/SDL.h>

#define TYPEWRITER_TIMER 800
#define MAX_CHAR_PER_LINE 30
#define LINE_SPACING 1

static int MAX_CHAR_PER_PAGE = MAX_CHAR_PER_LINE * 6;    //30 chars per row * 6 lines

void initTypewriter(Typewriter *tpwr, int x, int y, int width, int(*callback)());
void resetTypewriter(Typewriter *typewriter);
void doTypewriter(Typewriter *typewriter);
void setTypewriterText(Typewriter *typewriter, char *text);
int isTypewriterEnd(Typewriter *typewriter);
void drawTypewriter(Typewriter *typewriter);

#endif
