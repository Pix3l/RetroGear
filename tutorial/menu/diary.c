/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 	 diary.c - RPG quest save object                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "diary.h"
#include "font.h"
#include "gfx.h"
#include "dialogue.h"
#include "controls.h"
#include "typewriter.h"
#include "bit.h"
#include "quest.h"

char ask[] = "Are you sure?";
int noise()
{
//    question_menu.items[0].func = NULL; //Yes
//    question_menu.items[1].func = &useDiary; //No
    
    setFlag(&question_menu.flags, menu_active);
    doMenu(&question_menu);
//    menuInput(dialog_menu);

    if(curr_gamepad->button_A)
    {
        curr_gamepad->button_A = 0;

        if(question_menu.items[question_menu.curr_item].func || question_menu.items[question_menu.curr_item].value!=0)
        {
            question_menu.items[question_menu.curr_item].func();
        }
        else
        {
            resetTypewriter(&typewriter);
            unsetFlag(&question_menu.flags, menu_active);
        }
    }

    return 0;
}

//~ char save[] = "Game has been saved!";
//~ void savegame()
//~ {
    //~ initDialogue(&defaultDialogue, &defaultTypewriter, save, NULL);
    //~ deactiveMenues(quest_menu.next);
    //~ quest_menu.curr_item = 0;
//~ }

char diarytext[] = "You can save your progress\nwith this.\fDo you want to save your game\nnow?";
void useDiary()
{
    setFlag(&defaultDialogue.flags, dialogue_waitConfirm);
    initDialogue(&defaultTypewriter, diarytext, &noise);
    setFlag(&defaultTypewriter.flags, typewriter_waitCallback);
}
