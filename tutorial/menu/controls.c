/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   controls.c - User input handling                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <SDL/SDL.h>

#include "controls.h"
#include "game.h"
#include "util.h"
#include "mouse.h"
#include "bit.h"

SDL_Event input_event;
Uint8 *keystate; // keyboard state
SDL_Joystick *joystick_ptr; // Joystick device pointer

void handle_keyboard(SDL_Event *input_event);
void handle_joystick(SDL_Event *input_event);
void handle_mouse(SDL_Event *input_event);

//~ #define DEBUG_VGAMEPAD
//~ #define DEBUG_JOYSTICK
//~ #define DEBUG_VMOUSE

void initController(int verbose)
{
	//Initialize current gamepad pointer
	curr_gamepad = &gamepad; //[0];

    //Initialize and setup the joysticks
    joystick_ptr = SDL_JoystickOpen(0);

    //Debug informations
    #ifdef DEBUG_JOYSTICK

        int joystick_count = SDL_NumJoysticks();

        printf("There are %d joysticks connected\n", joystick_count);

        int i=0;
        for(i=1; i <= joystick_count ; i++)
        {
            printf("Joystick %d have %d buttons and %d axes\n", i, SDL_JoystickNumButtons(joystick_ptr), SDL_JoystickNumAxes(joystick_ptr) );  
        }

    #endif
}

void inputHandler()
{
	while (SDL_PollEvent(&input_event))
	{
        //Let's quit!
        if (input_event.key.keysym.sym == SDLK_ESCAPE || input_event.type == SDL_QUIT)
        {
            quit = 1;
        }

        /***********************************************************
         * Virtual gamepad handling
         **********************************************************/

        if(input_event.type == SDL_KEYDOWN  || input_event.type == SDL_KEYUP)
        {
            /**
             * Handling keyboard
             **/
            handle_keyboard(&input_event);
        }
        else if( input_event.type == SDL_JOYAXISMOTION || input_event.type == SDL_JOYBUTTONUP  || input_event.type == SDL_JOYBUTTONDOWN )
        {
            /**
             * Handling joystick
             **/
            handle_joystick(&input_event);
        }
        else if( input_event.type == SDL_MOUSEMOTION || input_event.type == SDL_MOUSEBUTTONUP  || input_event.type == SDL_MOUSEBUTTONDOWN )
        {
            /**
             * Virtual mouse handling
             **/
            handle_mouse(&input_event);
        }
    }
}

/**
 * Handles keyboard events enhancing the virtual gamepad
 *
 * @param SDL_Event *input_event
 *      Pointer to the SDL_Event structure where events are stored
 **/
void handle_keyboard(SDL_Event *input_event)
{
    SDLKey key_button = input_event->key.keysym.sym;    //Keyboard key symbol
    
    if( key_button == BUTTON_A )
    {
        if(input_event->type == SDL_KEYDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button A pressed\n");
            #endif

            curr_gamepad->button_A = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button A released\n");
            #endif

            curr_gamepad->button_A = 0;
        }
    }

    if( key_button == BUTTON_B )
    {
        if(input_event->type == SDL_KEYDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button B pressed\n");
            #endif
            curr_gamepad->button_B = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button B released\n");
            #endif

            curr_gamepad->button_B = 0;
        }
    }

    if(key_button == BUTTON_START )
    {
        if(input_event->type == SDL_KEYDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Start pressed\n");
            #endif
            curr_gamepad->button_Start = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Start released\n");
            #endif

            curr_gamepad->button_Start = 0;
        }
    }

    if(key_button == BUTTON_SELECT )
    {
        if(input_event->type == SDL_KEYDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Select pressed\n");
            #endif
            curr_gamepad->button_Select = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Select released\n");
            #endif

            curr_gamepad->button_Select = 0;
        }
    }

    /*******************************************************
     * Direction and axis
     ******************************************************/

    /* Horizontal movement */

    if(key_button == SDLK_LEFT )
    {
        if(input_event->type == SDL_KEYDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Left pressed\n");
            #endif
            curr_gamepad->button_Left = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Left released\n");
            #endif

            curr_gamepad->button_Left = 0;
        }
    }

    if(key_button == SDLK_RIGHT )
    {
        if(input_event->type == SDL_KEYDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Right pressed\n");
            #endif
            curr_gamepad->button_Right = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Right released\n");
            #endif

            curr_gamepad->button_Right = 0;
        }
    }

    /* Vertical movement */

    if(key_button == SDLK_UP )
    {
        if(input_event->type == SDL_KEYDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Up pressed\n");
            #endif
            curr_gamepad->button_Up = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Up released\n");
            #endif

            curr_gamepad->button_Up = 0;
        }
    }

    if(key_button == SDLK_DOWN )
    {
        if(input_event->type == SDL_KEYDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Down pressed\n");
            #endif
            curr_gamepad->button_Down = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Down released\n");
            #endif

            curr_gamepad->button_Down = 0;
        }
    }
}

/**
 * Handles joystick/gamepad events enhancing the virtual gamepad
 *
 * @param SDL_Event *input_event
 *      Pointer to the SDL_Event structure where events are stored
 **/
void handle_joystick(SDL_Event *input_event)
{
    Uint8 joy_button = input_event->jbutton.button;   //Joystick key symbol
    
    if( joy_button == 1 )
    {
        if( input_event->type == SDL_JOYBUTTONDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick button A pressed\n");
            #endif
            curr_gamepad->button_A = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick button A released\n");
            #endif

            curr_gamepad->button_A = 0;
        }
    }

    if( joy_button == 2 )
    {
        if( input_event->type == SDL_JOYBUTTONDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick button B pressed\n");
            #endif
            curr_gamepad->button_B = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick button B released\n");
            #endif

            curr_gamepad->button_B = 0;
        }
    }

    if( joy_button == 9)
    {
        if( input_event->type == SDL_JOYBUTTONDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Start pressed\n");
            #endif
            curr_gamepad->button_Start = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Start released\n");
            #endif

            curr_gamepad->button_Start = 0;
        }
    }

    if( joy_button == 8)
    {
        if( input_event->type == SDL_JOYBUTTONDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Select pressed\n");
            #endif
            curr_gamepad->button_Select = 1;
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Select released\n");
            #endif

            curr_gamepad->button_Select = 0;
        }
    }

    if(input_event->jaxis.axis == 0)
    {
        if(input_event->jaxis.value < -DEAD_ZONE)
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick direction Left pressed\n");
            #endif

            curr_gamepad->button_Left = 1;
        }
        else if( input_event->jaxis.value > DEAD_ZONE )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Right pressed\n");
            #endif

            curr_gamepad->button_Right = 1;
        }
        else
        {
            curr_gamepad->button_Left  = 0;
            curr_gamepad->button_Right = 0;

            #ifdef DEBUG_VGAMEPAD
                printf("Joystick horizontal directions released\n");
            #endif
        }
    }

    /* Vertical movement */

    if( input_event->jaxis.axis == 1 )
    {
        if( input_event->jaxis.value < -DEAD_ZONE) 
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick direction Up pressed\n");
            #endif

            curr_gamepad->button_Up = 1;
        }
        else if( input_event->jaxis.value > DEAD_ZONE )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick direction Down pressed\n");
            #endif

            curr_gamepad->button_Down = 1;
        }
        else
        {
            curr_gamepad->button_Up = 0;
            curr_gamepad->button_Down = 0;

            #ifdef DEBUG_VGAMEPAD
                printf("Joystick vertical directions released\n");
            #endif
        }
    }
}

/**
 * Handles mouse events enhancing the virtual mouse
 *
 * @param SDL_Event *input_event
 *      Pointer to the SDL_Event structure where events are stored
 **/
void handle_mouse(SDL_Event *input_event)
{
    if(input_event->type == SDL_MOUSEMOTION)
    {
        // Update mouse position
        Mouse.x = input_event->motion.x;
        Mouse.y = input_event->motion.y;

        #ifdef DEBUG_VMOUSE
            printf("Mouse coordinates %d,%d\n", Mouse.x, Mouse.y);
        #endif
    }

    if(input_event->type == SDL_MOUSEBUTTONDOWN)
    {
        if (input_event->button.button == 1)
        {
            Mouse.leftButton = 1;

            #ifdef DEBUG_VMOUSE
                printf("Mouse left button pressed\n");
            #endif
        }

        if (input_event->button.button == 3)
        {
            Mouse.rightButton = 1;

            #ifdef DEBUG_VMOUSE
                printf("Mouse right button pressed\n");
            #endif
        }
    }
    
    if(input_event->type == SDL_MOUSEBUTTONUP)
    {
        // update button down state if left-clicking
        if (input_event->button.button == 1)
        {
            Mouse.leftButton = 0;

            #ifdef DEBUG_VGAMEPAD
                printf("Mouse left button pressed\n");
            #endif
        }

        if (input_event->button.button == 3)
        {
            Mouse.rightButton = 0;

            #ifdef DEBUG_VGAMEPAD
                printf("Mouse right button pressed\n");
            #endif
        }
    }
}

/**
 * Check if a gamepad button is in status locked
 *
 * @param int *button
 *      Pointer to the button variable
 *
 * @return int
 *      1 if locked
 *      0 if other status
 **/
int isButtonLocked(int button)
{
    if(button==LOCKED)
    {
        return 1;
    }

    return 0;
}

int konamiCode()
{
    //Flag che impedisce l'utilizzo del Konami Code
    if(!hasFlag(Game.flags, konami_code))
    {
        return -1;
    }
    
    //Konami code!
    int konami[] = {curr_gamepad->button_Up, curr_gamepad->button_Up, 
                    curr_gamepad->button_Down, curr_gamepad->button_Down,
                    curr_gamepad->button_Left, curr_gamepad->button_Right,
                    curr_gamepad->button_Left, curr_gamepad->button_Right,
                    curr_gamepad->button_B, curr_gamepad->button_A };

    int size = sizeof(konami)/sizeof(int);
    
    static int index = 0;
    if(konami[index]==1)
    {
            index++;
    }
    
    //Se il giocatore ha inserito la giusta sequenza
    if(index==size)
    {
            printf("Konami code!!!\n");
            //Fai qualcosa...
            index = 0;
    }
    
    return 0;
}

void cleanInput()
{
    SDL_JoystickClose(joystick_ptr);
}
