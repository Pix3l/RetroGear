/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               collision.h - Collision related functions            *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _COLLISION_H
#define _COLLISION_H

#include "entity.h"
#include "rgtypes.h"

int rectCollision(int x1, int y1, int w1, int h1, int x2, int y2, int w2, int h2);
RG_Point tileCollision(Entity *pobj, int new_x, int new_y);

#endif
