/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   battle.h - RPG like battle system                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _BATTLE_H
#define _BATTLE_H

#include "menu.h"
#include "sprite.h"

Sprite monster;

Menu battle_menu, enemy_menu;
Item battle_actions[4], enemy_list[2];

void init_battle();
void doBattle();
void drawBattle();

#endif
