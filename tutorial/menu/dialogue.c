/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               dialogue.c - Dialogue handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "dialogue.h"
#include "font.h"
#include "event.h"
#include "typewriter.h"
#include "controls.h"
#include "gfx.h"
#include "bit.h"
#include "tile.h"

/**
 * Dialogues can be stored in a event parameter or in this string on occasion.
 * The length is the same for both. 
 **/
char dlg_holder[MAX_PARAM_LENGTH];   //Dialogue text holder

void initDialogue(Typewriter *pTypewriter, char text[], int(*callback)())
{
    initTypewriter(pTypewriter, FONT_W, (SCREEN_HEIGHT/2)+56, SCREEN_WIDTH-(FONT_W*3), callback);
    setTypewriterText(pTypewriter, text);
}

/**
 *  Starts a dialogue by loading it from text file for a specific sets
 *  of coordinates of a specific entity, in the current loaded map
 *
 *  If no file or associated coordinates and mapname will be found, 
 *
 *  @param int x, y
 *      X and Y coordinates of the entity associated to the dialogue
 *
 *  @param char *filename
 *      This must be the entity name, an entity_name.dlg file will be
 *      searched
 *
 *  @return int
 *      Returns:
 *          -1 if file has not found
 *          0 if a dialogue has not been found with the specified arguments
 *          1 if dialogue has been found
 **/
int initDialogueFromFile(int x, int y, char *filename)
{
    char str[80];
    sprintf(str, "dlg/%s.dlg", filename);

    FILE *fp = fopen(str, "r");
	if (fp == NULL)
	{
		perror(str);
		return -1;
	}

    x = pixelsToTiles(x);
    y = pixelsToTiles(y);

    int dlg_x, dlg_y;

    unsigned char c;
    size_t n = 0;

    while (fscanf(fp, "%03d%*c%03d%*c", &dlg_x, &dlg_y) > 0)
    {
        #ifdef DEBUG_DLG
            printf("[Dialogue] x: %d y: %d:\n", dlg_x, dlg_y);
        #endif

        if(dlg_x==x && dlg_y == y)
        {
            while ((c = fgetc(fp)) != '\n')
            {
                if(c=='\\')
                {
                    c = fgetc(fp);
                    if(c=='n')
                    {
                        c = '\n';
                    }

                    if(c=='f')
                    {
                        c = '\f';
                    }
                }

                dlg_holder[n++] = c;
            }
            dlg_holder[n++] = '\0';
        
            initDialogue(&defaultTypewriter, dlg_holder, NULL);
            return 1;
        }
        
        fscanf(fp, "%*[^\n]");  //Skip the rest of the line
    }
    fclose(fp);

    return 0;
}

void doDialogue(Typewriter *pTypewriter)
{
    if(!hasFlag(pTypewriter->flags, typewriter_active))
        return;
    
    doTypewriter(pTypewriter);

/*
    if(isTypewriterEnd(pTypewriter))
    {
        //~ if(hasFlag(pDialogue->flags, dialogue_waitConfirm))
        //~ {
            //~ printf("waiting confirmation\n");
            //~ if(doDialogueCallback(pDialogue)==1)
            //~ {
                //~ closeDialogue(pDialogue);
            //~ }
            //~ return;
        //~ }
        
        if(curr_gamepad->button_A)
        {
            curr_gamepad->button_A = 0;
            closeDialogue(pTypewriter);
            //~ doDialogueCallback(pDialogue);
            
        }
    }
*/
}

/**
int doDialogueCallback(Dialogue *pDialogue)
{
    if(pDialogue->callback!=NULL)
    {
        return pDialogue->callback();
    }

    return 0;
}
**/

void closeDialogue(Typewriter *pTypewriter)
{
    resetTypewriter(pTypewriter);
    unsetFlag(&pTypewriter->flags, typewriter_active);
}

void drawDialogue(Typewriter *pTypewriter)
{
    if(hasFlag(pTypewriter->flags, typewriter_active))
    {
        // Common text gui with default values
        drawGui(FONT_W, (SCREEN_HEIGHT/2)+56, SCREEN_WIDTH-(FONT_W*3), 56, BLACK, white);

        drawTypewriter(pTypewriter);
    }
}
