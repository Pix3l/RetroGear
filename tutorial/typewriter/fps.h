/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    fps.c - FPG handling functions                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _FPS_H
#define _FPS_H

#define LOGICHZ 60
#define LOGICMS (1000/LOGICHZ)

#include <SDL/SDL.h>

int i_n, i_d;

struct _fps
{
	int t, tl, frequency, temp;
} fps;

void render_time (void);
int linear_interpolation (int a, int b, int int_factor);

#endif
