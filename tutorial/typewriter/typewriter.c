/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               typewriter.h - Virtual typewriter for text           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "typewriter.h"
#include "font.h"
#include "gfx.h"
#include "controls.h"
#include "util.h"
#include "timer.h"

void initTypewriter(Typewriter *typewriter, int x, int y, int width)
{
	typewriter->x = x;
	typewriter->y = y;
	typewriter->max_width = width;
	typewriter->flag_status = INACTIVE;
	typewriter->curPos = 0;
	typewriter->curPage = 0;
	typewriter->delay = 1;  //This make the typewriter be able to start!

	curr_typewriter = typewriter;
}

/**
 * Reset typewriter object
 * 
 * @param Typewriter *typewriter
 * 		Typewriter object pointer
 **/
void resetTypewriter(Typewriter *typewriter)
{
	typewriter->text=NULL;
	typewriter->flag_status = INACTIVE;
	typewriter->curPos = 0;
	typewriter->curPage = 0;
	typewriter->delay = 1;  //This make the typewriter be able to start!
	curr_typewriter = typewriter;
	
}

void setTypewriterText(Typewriter *typewriter, char *text)
{
    if(typewriter->flag_status==INACTIVE)
    {
        typewriter->text = text;
        typewriter->flag_status=ACTIVE;
    }
}

void doTypewriter(Typewriter *typewriter)
{
	//If there's no more text we exit
	if(!typewriter->text)
	{
        typewriter->flag_status=INACTIVE;
        return;
	}
    
    //Check if the text is finished
    if( isTypewriterEnd(typewriter) )
    {
        typewriter->flag_status = PAUSED;
    }
    
    //Handle the pause (Page end, forced pause, other...)
    if(typewriter->flag_status == PAUSED)
    {
        if(curr_gamepad->button_A)
        {
            //~ //Check if the text is finished
            if( isTypewriterEnd(typewriter) )
            {
                /**
                 * instead of reset the enteir object, we simply switch
                 * back to the start of the text
                 **/
                resetTypewriter(typewriter);
                return;
            }
            
            //If we passed the last line, we are in a new page
            if(typewriter->flag_status==PAUSED)
            {
                //We reset the beginning of the text.
                typewriter->curPage = typewriter->curPos+1;
                typewriter->curPos = 0;
            }
            else
            {
                typewriter->curPos++;
            }
            
            typewriter->flag_status = ACTIVE;
        }
        
        return;
    }
	
    //Decrease the delay...
    typewriter->delay--;

	//Speed up
	if(curr_gamepad->button_A)
	{
		typewriter->delay=0;
	}
	
	if(typewriter->delay == 0)
	{
		typewriter->curPos++;
		typewriter->delay=7;    //Reset delay
        
        char c = typewriter->text[typewriter->curPage + typewriter->curPos];
        
        if(c=='|')
        {
            //Set a pause for next page
            typewriter->flag_status=PAUSED;
        }
	}
}

/**
 * Get the current typewriter status.
 *
 * @param Typewriter *typewriter
 *      Pointer to the typewriter object
 *
 * @return int
 *      The typewriter flag status value
 **/
int getTypewriterStatus(Typewriter *typewriter)
{
	return typewriter->flag_status;
}

int isTypewriterEnd(Typewriter *typewriter)
{
    if( typewriter->curPos+typewriter->curPage >= strlen(typewriter->text) )
    {
        return 1;
    }

    return 0;
}

void drawTypewriter(Typewriter *typewriter)
{
    //Make sure we have text to draw
	if(!typewriter->text)
	{
		return;
	}
	
    /**
     * We make a copy of the actual value of typewriter->y, to prevent the text fall or rise at every new line.
     **/
	int y = typewriter->y;
	int char_pos = 0;
	
	int i=0;
	for(i=0; i < typewriter->curPos; i++)
	{
        if(char_pos>=MAX_CHAR_PER_PAGE)
        {
            i=char_pos;
        }
        
        //We are at the last line
        if( y >= typewriter->y+(FONT_H*6) )
        {
            //Page end, set a pause
            typewriter->flag_status = PAUSED;
        }

        char c = typewriter->text[typewriter->curPage + i];
        
        //Character x destination
        int dest_x = typewriter->x + (FONT_W*char_pos);

        //Newline or end of the line, we bring the text to wrap
		if(c=='\n' || dest_x > typewriter->max_width)
		{
			char_pos=0;
			y+=FONT_H;
		}
        //Draw all text except the special characters
		else if(c!='|')
		{
			drawChar(screen, bmpfont, dest_x, y, FONT_W, FONT_H, c, white, 0);
			char_pos++;
		}
	}
    
    //If there are other pages, we draw the flashing arrow
    if(typewriter->flag_status == PAUSED)
    {
        if(getSeconds(5)%2)
        {
            return;
        }
        drawChar(screen, bmpfont, typewriter->x+(typewriter->max_width-FONT_W), typewriter->y+FONT_H*7, FONT_W, FONT_H, 31, white, 0);
    }
}
