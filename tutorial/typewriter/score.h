/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  score.c - Score handling functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _SCORE_H
#define _SCORE_H

#include <SDL/SDL.h>

typedef struct _scoreType
{
  float x, y;
  float xstart, ystart;
  
  float speed;

  int sprite_index;	//tile index

  short int status;

  struct _scoreType *next;
} scoreType;

scoreType *scoreHead, *scoreTail;

static int __attribute__((__unused__)) points[] = {100, 200, 400, 500, 800, 1000, 2000, 4000, 5000, 8000};
int points_index;
SDL_Surface *score_sheet;

void createScore(int x, int y, float speed, int sprite_index);
void initScore();
void doScore();
int getScore(int index);
void addScore(int score); //, int *pscore);
void drawScore();
void clearScoreList();

#endif
