/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             		menu_classic.h - Classic game menu				  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _MENU_CLASSIC_H
#define _MENU_CLASSIC_H

#include "menu.h"

void initClassicMenu(Menu* pmenu, Item *pitem, int x, int y, char *name, int max_items, SDL_Color color, Menu *parent, Menu *child);
void initStaticClassicMenu(Menu* pmenu, Item *pitem, int x, int y, char *name, int max_items, SDL_Color color, Menu *parent, Menu *child);
void setMenuTable(Menu *pmenu, int rows, int cols, int col_width, int row_spacing);
void setMenuWidth(Menu* pmenu, int strlen);
void setMenuHeight(Menu* pmenu, int height);
void setMenuActive(Menu* pmenu, int flag_active);
void showMenuBorder(Menu* pmenu, int flag_border);
void setMenuCursor(Menu* pmenu, char cursor);
void drawMenuClassic(Menu *pmenu);

#endif
