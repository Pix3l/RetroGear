/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  player.c - Generic game main player               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "player.h"
#include "controls.h"
#include "tile.h"
#include "camera.h"
#include "gfx.h"
#include "sfx.h"
#include "game.h"
#include "font.h"
#include "fps.h"
#include "timer.h"
#include "entity.h"

//Private functions
void playerAction();
void movePlayer();

/**
 * Reset the player's default values
 * 
 * @param Entity *player
 * 		Pointer to a Player objects
 **/
void initPlayer(Entity *player)
{
    player->visible = 1;
    player->flag_active = 1;
	player->w= PLAYER_W;
	player->h= PLAYER_H;
	player->lives = 3;
	player->sprite_index = 0;

	if(!player->sprite)
	{
		player->sprite = loadImage("data/player.bmp", COLORKEY);
	}

	player->animation_speed = 0.1f;
	player->speed = 0.05f;
}

void setPlayerPosition(int x, int y)
{
	curr_player->x = x;
	curr_player->y = y;
	curr_player->xstart = x;
	curr_player->ystart = y;
}

void playerExtraLife()
{
	curr_player->lives++;
	playSound(extralife_snd);
}

void playerAction()
{
	curr_player->sprite_index = 0;	//Action sprite index
	//Action time
	//Action function	
}

/**
 * Handle keyboard keys and update player moves and status 
 **/
void updatePlayer()
{
	scrollCameraX(&camera, curr_player->x);
	scrollCameraY(&camera, curr_player->y);
	
	movePlayer();

	//Real time status check here!
}

void movePlayer()
{
	//Handle input X
	if (curr_gamepad->button_Left)
	{
		curr_player->direction_x = -1;
	}
	
	if (curr_gamepad->button_Right)
	{
		curr_player->direction_x = 1;
	}

	if (!curr_gamepad->button_Left)
	{
		if (curr_player->direction_x == -1)
			curr_player->direction_x = 0;
	}
	
	if (!curr_gamepad->button_Right)
	{
		if (curr_player->direction_x == 1)
			curr_player->direction_x = 0;
	}

	//Handle input Y
	if (curr_gamepad->button_Up)
	{
		curr_player->direction_y = -1;
	}
	
	if (curr_gamepad->button_Down)
	{
		curr_player->direction_y = 1;
	}

	if (!curr_gamepad->button_Up)
	{
		if (curr_player->direction_y == -1)
			curr_player->direction_y = 0;
	}
	
	if (!curr_gamepad->button_Down)
	{
		if (curr_player->direction_y == 1)
			curr_player->direction_y = 0;
	}

	//Movements X
	if (curr_player->direction_x)
	{
		curr_player->hspeed += curr_player->speed * curr_player->direction_x;
	}
	else 
	{
		curr_player->hspeed = 0.0f;
	}

	if (curr_player->hspeed > MAX_H_SPEED) curr_player->hspeed = MAX_H_SPEED;
	if (curr_player->hspeed < MIN_H_SPEED) curr_player->hspeed = MIN_H_SPEED;

	//Movements Y
	if (curr_player->direction_y)
	{
		curr_player->vspeed += curr_player->speed * curr_player->direction_y;
	}
	else 
	{
		curr_player->vspeed = 0.0f;
	}

	if (curr_player->vspeed > MAX_V_SPEED) curr_player->vspeed = MIN_V_SPEED;
	if (curr_player->vspeed < MIN_V_SPEED) curr_player->vspeed = MIN_V_SPEED;
}

void playerDie()
{
	curr_player->sprite_index = DIE;

	//Stop any music from the game
	if(isMusicPlaying())
	{
		pauseMusic();
	}

	if(curr_player->timer[0]==0)
	{
		curr_player->timer[0]=fps.t;
		return;
	}

	if(getSeconds(curr_player->timer[0]) >= 3)
	{
		curr_player->lives--;
		curr_player->status=STAND;
		curr_player->hspeed=curr_player->vspeed=0;
		setPlayerPosition(curr_player->xstart, curr_player->ystart);
		timer.start_time = fps.t;
		setGameState(PREGAME);
		curr_player->timer[0] = 0;	//Reset timer interno
		return;
	}
}

void drawPlayer()
{
	if(Game.status < GAME) return;

	// Destinazione del tile
	SDL_Rect dest;

	dest.x = (int)curr_player->x-2 - camera.offsetX;
	dest.y = (int)curr_player->y+1 - camera.offsetY;
	dest.w = (int)dest.x+curr_player->w;
	dest.h = (int)dest.y+curr_player->h;

	getSprite(curr_player->sprite, curr_player->sprite_index, 16, 16, &dest);
	
	#ifdef DEBUG_GFX
		drawFillRect(curr_player->x, curr_player->y, curr_player->w, curr_player->h, 0xff0000);
	#endif
}
