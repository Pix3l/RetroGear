/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               typewriter.h - Virtual typewriter for text           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _TYPEWRITER_H
#define _TYPEWRITER_H

typedef enum
{
    INACTIVE, ACTIVE, PAUSED, STOPPED
} TYPEWRITER_STATUS;

typedef struct _Typewriter {
	int x, y;		//Origins
	int max_width;	//Max width
	int curPos;
    int curPage;
	char *text;		//Pointer to text
	int delay;
	int flag_status;
} Typewriter;

Typewriter typewriter, *curr_typewriter;

#include <SDL/SDL.h>

#define TYPEWRITER_TIMER 800
#define MAX_CHAR_PER_LINE 30
#define LINE_SPACING 1

static int MAX_CHAR_PER_PAGE = MAX_CHAR_PER_LINE * 6;    //30 chars per row * 6 lines

void initTypewriter(Typewriter *tpwr, int x, int y, int width);
void resetTypewriter(Typewriter *typewriter);
void doTypewriter(Typewriter *typewriter);
void setTypewriterText(Typewriter *typewriter, char *text);
int getTypewriterStatus(Typewriter *typewriter);
int isTypewriterEnd(Typewriter *typewriter);
void drawTypewriter(Typewriter *typewriter);
void drawTypewriterb(Typewriter *typewriter);
void drawTypewriterc(Typewriter *typewriter);

#endif
