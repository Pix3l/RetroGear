/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   camera.c - Scrolling functions                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "camera.h"
#include "tile.h"
#include "gfx.h"
#include "util.h"
#include "config.h"

void initCamera(Camera *pcam)
{
	//Initialize camera at Player position
	pcam->offsetX = 0;
	pcam->offsetY = 0;
    
	//(Lunghezza della mappa - Dimensione della finestra)
	pcam->maxOffsetX = (TILESIZE*curr_level->cols - SCREEN_WIDTH);
	pcam->maxOffsetY = (TILESIZE*curr_level->rows - SCREEN_HEIGHT);
	
	//~ printf("offsets: %f %f\n", pcam->offsetX, pcam->offsetY);
	//~ printf("maxoffsets %d %d\n", pcam->maxOffsetX, pcam->maxOffsetY);
}

/**
 * void scrollCameraX()
 * 
 * Aggiorna lo scrolling sull'asse Y in base ad una coordinata X 
 * specificata in argomento.
 * 
 * @param int x
 * 		La coordinata X sulla quale basare lo scrolling della mappa
 * @param Camera *pcam
 * 		Puntatore alla struttura camera da usare per lo scrolling
 * 
 **/
void scrollCameraX(Camera *pcam, int x)
{
	//Update camera position
	pcam->offsetX += (x - pcam->offsetX - CENTER_X); // /20;

	#ifndef NO_SCROLL_BOUND
		//Limit camera's movement inside the map width
		if (pcam->offsetX < 0)
			pcam->offsetX = 0;
		
		if (pcam->offsetX > pcam->maxOffsetX)
			pcam->offsetX = pcam->maxOffsetX;
	#endif
}

/**
 * Aggiorna lo scrolling sull'asse Y in base ad una coordinata Y 
 * specificata in argomento.
 * 
 * @param int y
 * 		La coordinata Y sulla quale basare lo scrolling della mappa
 * @param Camera *pcam
 * 		Puntatore alla struttura camera da usare per lo scrolling
 * 
 **/
void scrollCameraY(Camera *pcam, int y)
{
	//Update camera position
	pcam->offsetY += (y - pcam->offsetY - CENTER_Y); // /20;
	
	#ifndef NO_SCROLL_BOUND
		//Limit camera's movement inside the map heigth
		if (pcam->offsetY < 0)
			pcam->offsetY = 0;
		
		if (pcam->offsetY > pcam->maxOffsetY)
			pcam->offsetY = pcam->maxOffsetY;
	#endif
}

/**
 * Controlla che un determinato oggetto si trovi in un'area della mappa
 * attualmente visualizzata da camera, quindi disegnata e visible.
 * 
 * @param int x, int y, int w, int h
 *      Coordinate e dimensioni dell'oggetto da controllare
 * 
 * @return 0
 *      In caso l'oggetto in questione non sia inquadrato
 * 
 * @return 1
 *      In caso l'oggetto in questione sia inquadrato
 **/
int isInCamera(int x, int y, int w, int h)
{
	//Camera's rectangle
	SDL_Rect rect;
	rect.x = camera.offsetX / TILESIZE;
	rect.y = camera.offsetY / TILESIZE;;
    rect.w = rect.x + SCREEN_WIDTH / TILESIZE;
    rect.h = rect.y + (SCREEN_HEIGHT / TILESIZE)+2;
	
	if(rectCollision(rect.x, rect.y, SCREEN_WIDTH-1, SCREEN_HEIGHT-1,
					 x, y, w, h))
	{
		return 1;
	}
	
	return 0;
}
