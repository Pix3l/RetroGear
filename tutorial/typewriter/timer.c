/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               timer.c - Game timing handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "timer.h"
#include "fps.h"

/**
 * int getSeconds(int)
 * 
 * Calcola la differenza di tempo tra quello attuale e quello passato
 * come argomento, ritornandone il valore espresso in secondi.
 * 
 * @param int lastTick
 *      Momento dal quale comcinare a contare il tempo trascorso
 * @return int seconds
 *      Tempo passato dall'ultimo tick specificato
 * 
 **/
//TODO: funzioni un po' confuse...
int getSeconds(int lastTick)
{
    int seconds = (fps.t - lastTick)/1000;
    return seconds;
}

int getMilliSeconds(int lastTick)
{
    int milliseconds = (fps.t - lastTick);
    return milliseconds;
}
