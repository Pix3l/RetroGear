/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                transition.c - Transintion effects handler          *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _TRANSITION_H
#define _TRANSITION_H

/**
 * Generic transition object
 * 
 * int flag_active
 * 		Active/Deactive transition effect
 *
 * int progress
 * 		Total progresso of the transition
 * 
 * int size
 * 		Size in pixel of the transition element
 * 
 * void (*effect)
 * 		Function pointer to the desired transition effect
 **/
struct _transition
{
	int flag_active;
	int progress;
	int size;
	void (*effect)();
} transition;

void initTransition(int size, void (*effect)());
void doTransition();
void transition_lines();
void transition_random();

#endif
