/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                      main.c - Main game source                     *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "config.h"
#include "util.h"
#include "gfx.h"
#include "game.h"
#include "menu_classic.h"
#include "typewriter.h"

/**
 * Prima di liberare le risorse dal sistema bisogna terminare il ciclo
 * principale del programma
 **/
void quitGame()
{
	quit = 1;
}

void mainInit()
{
	SDL_WM_SetCaption("RetroGear", "RetroGear");
	
	//Main menu
	initClassicMenu(&main_menu, NULL, 30, 10, "main", -1, white, NULL, NULL);
	setMenuBorder(&main_menu, 1);
	addMenuItem(&main_menu, "New Game", NULL, white, doPreGame);
	addMenuItem(&main_menu, "Quit", NULL, white, quitGame);
	alignMenuCenter(&main_menu);
	alignMenuBottom(&main_menu);
	setMenuActive(&main_menu, -1);
	
	//~ typewriterReset(&typewriter);
	setTypewriterText(curr_typewriter, "This is a typewriter test for\nretrogear engine with\npaginated text support!\n\n\n\n  Have fun with this!");
}

int main(int argc, char *argv[])
{
	// init video stuff
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_JOYSTICK) != 0)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}
	atexit(SDL_Quit);

    // init screen
    #ifdef DOUBLE_SCREEN

		double_screen = SDL_SetVideoMode(SCREEN_WIDTH*2, SCREEN_HEIGHT*2, 0, SDL_HWSURFACE);
		if(double_screen == NULL)
		{
			fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
			exit(-1);
		}
		
		screen = SDL_CreateRGBSurface(0,SCREEN_WIDTH,SCREEN_HEIGHT,32,0,0,0,0);
		
	#else

		screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, SDL_HWSURFACE);
		if(screen == NULL)
		{
			fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
			exit(-1);
		}

	#endif
       
	//Subsystems initialization
	init();
   
	//User defined initialization
	mainInit();
   
	//Main game loop
	mainLoop();
   
	//Free the memory from allocated resources
	cleanUp();

	return 0;
}
