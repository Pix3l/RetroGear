/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   tile.c - Tiles related functions                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "tile.h"
#include "gfx.h"
#include "camera.h"

/**
 * Inizializza la superficie tiles con uno sprite sheet contenente le 
 * immagini.
 * 
 **/
void initTiles()
{
	if(curr_level==NULL)
	{
		printf("Error: Level structure not initialized...\n");
		return;	
	}
	
	//Colore di default: VERDE 0x00FF00
	Uint32 key = SDL_MapRGB(screen->format, 0, 255, 0 ); 
	tiles=loadImage("data/tile_sheet.bmp", key);
}


//~ void level_load_image(texture_type* ptexture, char* theme, char * file, int use_alpha)
//~ {
  //~ char fname[1024];
//~ 
  //~ snprintf(fname, 1024, "%s/themes/%s/%s", st_dir, theme, file);
  //~ if(!faccessible(fname))
    //~ snprintf(fname, 1024, "%s/images/themes/%s/%s", DATA_PREFIX, theme, file);
//~ 
  //~ texture_load(ptexture, fname, use_alpha);
//~ }

/**
 * Controlla se una coordinata rientra o meno in un possibile tile del
 * livello in base alla grandezza di esso definita globalmente.
 * 
 * Utile per controllare l'allineamento di una Entity su un'ipotetica
 * griglia di gioco
 * 
 * @param int x, int y
 * 		Coordinate da controllare sul campo di gioco
 * 
 * @return int
 * 		0 Se non e' allineato con i tile
 *      1 Se e' allineato con i tile
 **/
int isInTile(int x, int y)
{
	int cols, rows;
	// Se non c'e' resto siamo all'interno del tile
	cols = !(x%TILESIZE); //Asse x
	rows = !(y%TILESIZE); //Asse y
	// Ritorniamo 1 se siamo nel tile
	if(cols==1 && rows==1)
	 return 1;

	return 0; // Non e' perfettamente nel tile
}

/**
 * Controlla eventuali collisioni alle coordinate specificate
 * 
 * @param int x, int y
 * 		Coordinate da controllare sul campo di gioco
 * @param int w, int h
 * 		Larghezza e altezza della entity
 * @param int type
 * 		Valore del tile nel file di livello (Tipologia)
 * @param unsigned int layer
 * 		Il layer su cui controllare la collisione del tile
 **/
int tileCollision(int x, int y, int w, int h, int type, unsigned int layer)
{
    int i, j;
    int minx, miny, maxx, maxy;
    
    // Ritorna una collisione se si esce dal campo di gioco
    if (x < 0 || (x + w) > TILESIZE * curr_level->cols ||
        y < 0 || (y + h) > TILESIZE * curr_level->rows)
        return 1;

    // Convertiamo le coordinate da pixel a tiles
    minx = x / TILESIZE;
    miny = y / TILESIZE;
    
    maxx = (x + w - 1) / TILESIZE;
    maxy = (y + h - 1) / TILESIZE;

    // Ritorniamo una collisione all'intersezione del tile
    for (i = minx; i <= maxx ; i++)
    {
        for (j = miny ; j <= maxy ; j++)
        {
            if (curr_level->map[layer][j][i]==type)
            	return 1;
        }
    }
    // Nessuna collisione
    return 0;
}

/**
 * void drawTile(SDL_Surface *tiles, int tile, SDL_Rect *dest)
 * 
 * Disegna un tile specifico in base al valore di indice passato in
 * argomento
 * 
 * @param SDL_Surface *tiles
 * 		Superficie contenente i vari tiles
 * @param int tile
 * 		Valore indicativo del tile da prelevare
 * @param SDL_Rect *dest
 * 		Destinazione del tile sulla superficie principale
 *
 **/
void drawTile(SDL_Surface *tiles, int tile, SDL_Rect *dest)
{
	SDL_Rect pick;

	//Di default ci si aspetta 5 colonne
	pick.x=(tile % 5)*TILESIZE;
	pick.y=(tile / 5)*TILESIZE;
	pick.w=TILESIZE;
	pick.h=TILESIZE;

	if(tile!=0)	//No need to draw empty tile
	 SDL_BlitSurface(tiles,&pick,screen, dest);
}

/**
 * void drawTileMap(SDL_Rect)
 * 
 * Disegna la mappa di gioco, con relativa grafica in tiles.
 * La porzione disegnata e' solamente quella attualmente visibile, 
 * rappresentata dall'apposita struttura SDL_Rect.
 * 
 * @param SDL_Rect rect
 *      Rettangolo rappresentativo della porzione di matrice attualmente
 * 		letta.
 * 
 **/
void drawTileMap(Level *plevel)
{
	int row = camera.offsetY / TILESIZE;
	int col = camera.offsetX / TILESIZE;
	
	//Da 0 a TILESIZE!!!
	int difference_x = (int)camera.offsetX % TILESIZE;
	int difference_y = (int)camera.offsetY % TILESIZE;

	int width = (SCREEN_WIDTH / TILESIZE)+1;
	int height = SCREEN_HEIGHT / TILESIZE;
	
	SDL_Rect dest;
	
	int x, y;
	for (y = 0; y < height; y ++)
	{
		for (x = 0; x < width; x ++)
		{
			dest.x = x * TILESIZE - difference_x;
			dest.y = y * TILESIZE - difference_y;
			dest.w = dest.h = 0;

        	//~plevel->layer
        	drawTile(tiles, plevel->map[1][row + y][col + x], &dest);

		}
	}
}
