/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               timer.h - Game timing handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _TIMER_H
#define _TIMER_H

/**
 * Struttura generica per la gestione del tempo.
 **/
typedef struct _Timer
{
	int start_time, curr_time;	//TODO: curr_time sostituibile con fps.t?
} Timer;

Timer timer, sys_timer;

int getSeconds();
int getMilliSeconds();

#endif
