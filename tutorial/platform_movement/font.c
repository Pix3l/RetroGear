/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                font.c - Image font handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <string.h>

#include "font.h"
#include "gfx.h"
#include "controls.h"
#include "util.h"

/**
 * Inizializza la superficie bmpfont caricando l'immagine con i font
 * e realizzando una superficie temporanea per la manipolazione grafica
 * dei singoli caratteri
 **/
void initFont()
{
	// Load bmp font on a surface
	Uint32 key = SDL_MapRGB( screen->format, 255, 0, 255 ); 
    bmpfont=loadImage("data/nesfont.bmp", key);

    // Create a temporary surface for single bmp font character
    tmpfont=SDL_CreateRGBSurface(SDL_HWSURFACE,FONT_W,FONT_H,32,0,0,0,0);
    if(tmpfont == NULL) {
        fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
        exit(1);
    }
}

/**
 * Gestisce l'input da tastiera per la scrittura di testo
 **/
void textInput()
{
	SDL_EnableUNICODE( SDL_ENABLE );

	if( input_event.type == SDL_KEYDOWN )
	{
		//If the key is a space
		if( input_event.key.keysym.unicode == (Uint16)' ' )
		{
			//Append the character
			appendChar(message, (char)input_event.key.keysym.unicode);
		}

		//If the key is a number
		else if( ( input_event.key.keysym.unicode >= (Uint16)'0' ) && ( input_event.key.keysym.unicode <= (Uint16)'9' ) )
		{
			//Append the character
			appendChar(message, (char)input_event.key.keysym.unicode);
		}
		//If the key is a uppercase letter
		else if( ( input_event.key.keysym.unicode >= (Uint16)'A' ) && ( input_event.key.keysym.unicode <= (Uint16)'Z' ) )
		{
			//Append the character
			appendChar(message, (char)input_event.key.keysym.unicode);
		}
		//If the key is a lowercase letter
		else if( ( input_event.key.keysym.unicode >= (Uint16)'a' ) && ( input_event.key.keysym.unicode <= (Uint16)'z' ) )
		{
			//Append the character
			appendChar(message, (char)input_event.key.keysym.unicode);
		}

		//If backspace was pressed and the string isn't blank
		if( ( input_event.key.keysym.sym == SDLK_BACKSPACE ) && ( strlen(message) != 0 ) )
		{
			//Remove a character from the end
			//~ str.erase( str.length() - 1 );
			//~ message = substr(strlen(message)-1,12);
		}
	}
}

/**
 * Copia una porzione dalla superficie bmpfont e la copia sulla superficie screen
 * 
 * @param SDL_Surface *screen
 *      Puntatore alla superficie di destinazione
 * @param SDL_Surface *bmpfont
 *      Puntatore alla superficie di origine
 * @param int X, int Y
 *      Destinazione in cui si andra' a copiare il contenuto di origine
 * @param int w, int h
 *      Rispettivamente, larghezza e grandezza della superficie da copiare
 * @param int asciicode
 *      Singolo carattere convertito in un equivalente decimale
 * @param SDL_Color color
 *      Struttura contenente i valori RGB di colore da usare per la superficie
**/
void drawChar(SDL_Surface *screen, SDL_Surface *bmpfont, int X, int Y, int w, int h, int asciicode, SDL_Color color, int alpha)
{
     SDL_Rect area, pick;
     
     //Cordinate nella superficie con le lettere
     pick.x=(asciicode % 32)*w;
     pick.y=(asciicode / 32)*h;
     pick.w=w;
     pick.h=h;
     area.x=X;
     area.y=Y;
     area.w=w;
     area.h=h;
     
     SDL_BlitSurface(bmpfont,&pick,tmpfont,NULL);
     
     //Replace white with a chosen color (target)
     int target_color = SDL_MapRGB (tmpfont->format, color.r, color.g, color.b);
	 replaceColor(tmpfont, WHITE, target_color);

     //Set background transaparency
     if(alpha>0)
      SDL_SetColorKey(tmpfont, SDL_SRCCOLORKEY, BLACK);
     else //Disable any background transaparency
      SDL_SetColorKey(tmpfont, 0, 0);
     
     SDL_BlitSurface(tmpfont,NULL,screen,&area);
}

void drawString(SDL_Surface *screen, int x, int y, char *text, SDL_Color color, int alpha)
{
	if(bmpfont==NULL)
	{
		printf("Error: Font surface not initialized!\n");
		quit=1;
		return;
	}
	
	int i=0;
	//~ int asciicode;
	
	SDL_Rect area = {x, y, FONT_W, FONT_H};

	//Per la lunghezza della stringa
	for (i=0;i<strlen(text);i++)
	{
		//~ asciicode=text[i]; //Character decimal value
		//~ printf("%d ascii\n", asciicode);

		drawChar(screen, bmpfont, area.x, area.y, area.w, area.h, text[i], color, alpha);    
		area.x+=FONT_W;
	}
}
