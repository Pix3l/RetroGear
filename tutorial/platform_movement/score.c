/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   score.c - Score handling functions               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "score.h"
#include "tile.h"
#include "entity.h"
#include "game.h"
#include "player.h"
#include "gfx.h"

void createScore(int x, int y, float speed, int sprite_index)
{
	//calloc alloc and initialize the memory
	scoreType *pnew = (scoreType*) calloc(1, sizeof(scoreType));

	if(scoreHead == NULL)
	{
		scoreHead = pnew;
		scoreTail = scoreHead;
	}
	else
	{
		scoreTail->next = pnew;
		scoreTail = pnew;
	}
	
	scoreTail->next = NULL;
	
	scoreTail->x = x;
	scoreTail->y = y;
	//~ scoreTail->w = w;
	//~ scoreTail->h = h;
	scoreTail->speed = speed;

	scoreTail->xstart = x;
	scoreTail->ystart = y;
	
	scoreTail->sprite_index = sprite_index;
}

void doScore()
{
	scoreType *current = scoreHead, *previous=NULL;
	while(current!=NULL)
	{
		if(current->status==DESTROY)
		{			
			if(current==scoreHead && current==scoreTail)
			{
				free(current);
				scoreHead=scoreTail=NULL;
				return;
			}

			if(scoreHead==current)
			{
				scoreHead = current->next;
				free(current);
				current = scoreHead;
			}
			else if(scoreTail==current)
			{
				previous->next=NULL;
				scoreTail=previous;
				free(current);
				current = scoreTail;
			}
			else
			{
				previous->next = current->next;
				free(current);
				current = previous->next;
			}
		}
		else
		{
			if( (current->ystart - current->y) >= 48)
			{
				current->status=DESTROY;
			}
			else
			{
				current->y-=current->speed;
			}
		}
		
		previous = current;
		current=current->next;
	}

}

/**
 * Inizializza il sistema di punteggio del gioco
 **/
void initScore()
{
	Game.score = 0;
	points_index = 1;
	score_sheet=loadImage("data/score_sheet.bmp", 0x00FF00);
}

/**
 * Preleva e ritorna un punteggio dall'array dei punti
 * 
 * @param int index
 * 		L'indice a cui cercare nell'array
 * 
 * @return int
 * 		Il punteggio, 0 in caso di indice non valido
 **/
int getScore(int index)
{
	int size = sizeof(points)/sizeof(int);

	if(index>=size)
	{
		points_index = size-1;

		// 1up...
		playerExtraLife();
	}
	else if(index<=size)
	{
		return points[index];
	}
	
	return 0;
}

/**
 * Somma un punteggio libero al totale di gioco
 * 
 * @param int score
 * 		Il punteggio da aggiungere
 * @todo int *pscore
 * 		Puntatore alla variabile di punteggio totale
 * 		(Si possono avere N variabili per N giocatori)
 **/
void addScore(int score)//, int *pscore)
{
	Game.score +=score;
}

void resetScoreType()
{
	scoreType *pscore = scoreHead;
	while(pscore!=NULL)
	{
		pscore->status=DESTROY;
		pscore = pscore->next;
	}
}

/**
 * Disegna tutti gli oggetti scoreType esistenti sullo schermo
 **/
void drawScore()
{
	//~ drawString(screen, pobj->x+camera.offsetX, pobj->y+camera.offsetY-pobj->h, (char*)points[index], white, 0);
	
	SDL_Rect dest;
	
	scoreType *current = scoreHead;
	while(current!=NULL)
	{
		dest.x = current->x;
		dest.y = current->y;
		dest.w = dest.h = TILESIZE;
		
		getSprite(score_sheet, current->sprite_index, dest.w, dest.h, &dest);

		current = current->next;
	}
}

/**
 * Ripulisce la lista degli oggetti scoreType nel caso ve ne siano
 * (Utile per eventuali chiusure improvvise della finestra da parte
 * dell'utente)
 **/
void clearScoreList()
{
	#ifdef DESTROY_DBG
		printf("Clear scoreType ");
	#endif

	scoreType *current = scoreHead, *next=NULL;
	while(current!=NULL)
	{
		next=current->next;
		free(current);
		current=next;
		
		#ifdef DESTROY_DBG
			printf(".");
		#endif
	}
	
	#ifdef DESTROY_DBG
		printf("Done!\n");
	#endif
}
