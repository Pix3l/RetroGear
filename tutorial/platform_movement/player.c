/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  player.c - Generic game main player               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <math.h>

#include "player.h"
#include "controls.h"
#include "tile.h"
#include "camera.h"
#include "gfx.h"
#include "sfx.h"
#include "game.h"
#include "font.h"
#include "fps.h"
#include "timer.h"
#include "entity.h"
#include "rgtypes.h"

//Private functions
void playerAction();
void movePlayerStatic();
void movePlayerDynamic();

float speed_limit;

/**
 * Reset the player's default values
 * 
 * @param Entity *player
 * 		Pointer to a Player objects
 **/
void initPlayer(Entity *player)
{
    player->type = PLAYER;
    player->visible = 1;
    player->flag_active = 1;
    player->w= PLAYER_W;
    player->h= PLAYER_H;
    player->lives = 3;
    Player.speed = 0.15f;

    //Difference between sprite and player size
    int sprite_diff_w = (PLAYER_SPRITE_W - PLAYER_W) / 2;
    int sprite_diff_h = (PLAYER_SPRITE_H - PLAYER_H) -1;

	if( !player->sprite.surface )
	{
		initSprite(&player->sprite, player->x-sprite_diff_w, player->y-sprite_diff_h, 16, 16, 0.09f, "data/player.bmp");
	}

	player->hspeed = 0;
	player->vspeed = 0;

        Player.gravity=0.2f;
    
	player->direction_x = 1;
	player->direction_y = 0;

        speed_limit = MAX_H_SPEED;
}

void setPlayerPosition(int x, int y)
{
	curr_player->x = x;
	curr_player->y = y;
	curr_player->xstart = x;
	curr_player->ystart = y;
}

void playerExtraLife()
{
	curr_player->lives++;
	playSound(extralife_snd);
}

void playerAction()
{
	curr_player->sprite.index = 0;	//Action sprite index
	//Action time
	//Action function	
}

/**
 * Handle keyboard keys and update player moves and status 
 **/
void updatePlayer()
{
    //~ scrollCameraX(&camera, curr_player->x);
    //~ scrollCameraY(&camera, curr_player->y);
	
    //~ animatePlayer();
    animateEntity(curr_player, 0);
    movePlayerDynamic();
}

/**
 * Move the player with dynamic speed
 **/
void movePlayer_X()
{
	if (curr_gamepad->button_Left)
	{
		Player.direction_x = -1;
        Player.status = MOVE;

        Player.hspeed += Player.speed * Player.direction_x;
	}
	
	if (curr_gamepad->button_Right)
	{
		Player.direction_x = +1;
        Player.status = MOVE;

        Player.hspeed += Player.speed * Player.direction_x;
	}

    //Decrease velocity only when touching the floor
    if ( isEntityOnFloor(&Player) )
    {
        if (!curr_gamepad->button_Left && !curr_gamepad->button_Right)
        {
            if(Player.hspeed > 0.0f)
            {
                Player.hspeed -= Player.speed;

                if(Player.hspeed < 0.0f)
                {
                    Player.hspeed = 0.0f;
                    curr_gamepad->button_Left = 0;
                }
            }
            else if(Player.hspeed < 0.0f)
            {
                Player.hspeed +=  Player.speed;

                if(Player.hspeed > 0.0f)
                {
                    Player.hspeed = 0.0f;
                    curr_gamepad->button_Right = 0;
                }
            }
        }

        //Set the maximum speed for walk and run
        speed_limit = ( curr_gamepad->button_B )? MAX_RUN_SPEED : MAX_H_SPEED;
    }

    //Limit maximum speed
    if (Player.hspeed > speed_limit) Player.hspeed = speed_limit;
    if (Player.hspeed < -speed_limit) Player.hspeed = -speed_limit;

    RG_Point *point = NULL;

    //Checking collision on the left
    if(Player.hspeed<0)
    {
        //Has collide on left?
        point = tileCollision(&Player, floorf(Player.x+Player.hspeed), Player.y);
        if( point != NULL )
        {
            Player.x= point->x+TILESIZE;
            Player.hspeed = 0;  //No need to slide if we touch an obstacle
        }
        else
        {
            Player.x += Player.hspeed;         //Update its position
        }
    }
    else if(Player.hspeed>0)
    {
        point = tileCollision(&Player, ceilf(Player.x+Player.hspeed), Player.y);
        if( point != NULL )
        {
            Player.x= point->x-Player.w;
            Player.hspeed = 0;  //No need to slide if we touch an obstacle
        }
        else
        {
            Player.x += Player.hspeed;         //Update its position
        }
    }
}

/**
 * Move the player with dynamic speed
 **/
void movePlayerDynamic()
{
    movePlayer_X();

	/**
     * vertical movement
     **/
	if (curr_gamepad->button_A==PRESSED)
	{
        curr_gamepad->button_A = LOCKED;
        if(!isEntityOnFloor(&Player))
        {
            return;
        }

		//if the player isn't jumping already
		Player.vspeed = -5.0f;		//jump!
	}

	if (!curr_gamepad->button_A)
	{	
		//if the player isn't jumping already
		Player.vspeed+=Player.gravity;
	}

    if(Player.vspeed>0)
    {
        //Has collide on bottom?
        if(isEntityOnFloor(&Player))
        {
            if(!curr_gamepad->button_A)	//player only may jump again if the jump key is released while on ground
                curr_gamepad->button_A = 0;
        }
    }

    doEntityGravity(&Player);
}

/**
 * Move the player with dynamic speed
 **/
void movePlayerStatic()
{
    Player.x += Player.hspeed;
    Player.y += Player.vspeed;
    
    //If the player is not aligned to grid, we ignore the input
	if(!isInTile(Player.x,Player.y))
	 return;
    
	//horizontal
	if (curr_gamepad->button_Left)
	{
		Player.direction_x = -1;
        Player.direction_y = 0;
        
        Player.hspeed = -1.0f;
	}
	
	if (curr_gamepad->button_Right)
	{
		Player.direction_x = +1;
        Player.direction_y = 0;
        
        Player.hspeed = +1.0f;
	}

	if (!curr_gamepad->button_Left && !curr_gamepad->button_Right)
	{
        Player.hspeed = 0;
	}
	
	//vertical
	if (curr_gamepad->button_Up)
	{
		Player.direction_y = -1;
        Player.direction_x = 0;
        
        Player.vspeed = -1.0f;
	}
	
	if (curr_gamepad->button_Down)
	{
		Player.direction_y = +1;
        Player.direction_x = 0;
        
        Player.vspeed = +1.0f;
	}
    
	if (!curr_gamepad->button_Up && !curr_gamepad->button_Down)
	{
        Player.vspeed = 0;
	}
}

void drawPlayer()
{
    if(Game.status < GAME) return;

    int dest_x = (int)curr_player->x-2 - camera.offsetX;
    int dest_y = (int)curr_player->y+1 - camera.offsetY;

    drawSprite(&curr_player->sprite, dest_x, dest_y);
	
    #ifdef DEBUG_GFX
	    drawFillRect(curr_player->x, curr_player->y, curr_player->w, curr_player->h, 0xff0000);
    #endif
}
