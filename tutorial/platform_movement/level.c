/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    level.c - Level file handling                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "level.h"
#include "tile.h"
#include "entity.h"
#include "util.h"
#include "event.h"

/**
 * Load a level along with its parameters
 * 
 * @param struct _Level* plevel
 *      Pointer to the level holding structure
 * 
 * @param char *filename
 * 		Name of the file map without extension
 * 
 * @return
 *      -1 In case of fail
 * 
 **/
int loadLevel(Level* plevel, char *filename)
{
	int i,j;
	char str[80];

	/* Load data file: */
	sprintf(str, "maps/%s.map", filename);

	FILE *fi = fopen(str, "r");
	if (fi == NULL)
	{
		#ifdef DEBUG_FILE_READ
			perror(str);
		#endif

		return -1;
	}

	//~ plevel->number = level;

	///Load header info
	/* (Level title) */
	fgets(str, 20, fi);
	strcpy(plevel->name, str);
	plevel->name[strlen(plevel->name)-1] = '\0';

	/* (Level description) */
	//~ fgets(str, 20, fi);
	//~ strcpy(plevel->description, str);
	//~ plevel->description[strlen(plevel->description)-1] = '\0';
    
        /* (Tileset theme) */
	fgets(str, 20, fi);
	strcpy(plevel->theme, str);
	plevel->theme[strlen(plevel->theme)-1] = '\0';

	/* (Song file for this level) */
	fgets(str, sizeof(plevel->song_title), fi);
	strcpy(plevel->song_title, str);
	plevel->song_title[strlen(plevel->song_title)-1] = '\0';
	
	//Background color
	//~ fscanf (fi, "%6x", &plevel->background_color);
	fscanf (fi, "%d%*c%d%*c%d", &plevel->bkgd_red, &plevel->bkgd_green, &plevel->bkgd_blue);

	//~ printf("%d\n", plevel->bkgd_red);
	//~ printf("%d\n", plevel->bkgd_green);
	//~ printf("%d\n", plevel->bkgd_blue);

	//Layers
	fscanf (fi, "%d", &plevel->num_layers);

	//Level cols and rows
	fscanf (fi, "%03d%*c%03d", &plevel->cols, &plevel->rows);

	int layer = 0;
	for(layer=0; layer < curr_level->num_layers; layer++)
	{
		for(i=0; i < curr_level->rows; i++)
		{
			for(j=0; j < curr_level->cols; j++)
			{
				fscanf(fi, "%02d%*c", &curr_level->map[layer][i][j]);

                            //Entities are only in SOLID_LAYER, create entity only while loop this layer
                            if(layer==0)
				            {
                                createEntityFromMap(plevel->map[SOLID_LAYER][i][j], j*TILESIZE, i*TILESIZE);
                            }

				//~ printf ("%d,", plevel->map[layer][i][j]);
			}

			//~ printf("\n");
		}
		
		//~ printf("\n");
	}

	fclose(fi);
	
	loadEvents(filename);

    #ifdef DEBUG_MAP
        debugLevel(plevel);
    #endif
	
	return 0;
}

void saveMap()
{
	int i,j;
	FILE * fp;

	fp = fopen("test.txt", "wb");
	if (fp == NULL)
	{
	  return;
	}

	/* (Level title) */
	fprintf(fp, "%s\n", curr_level->name);
	
	/* (Song file for this level) */
	fprintf(fp, "%s\n", curr_level->song_title);

	/* (Level width) & (Level height) */
	fprintf(fp, "%02d,%02d", curr_level->cols, curr_level->rows);
	
	fprintf(fp, "\n");

	//Layer 1: Solid tiles / Entities
	for(i=0; i< curr_level->rows; i++)// && !feof (fi))
	{
		for(j=0; j< curr_level->cols; j++)
		 	fprintf(fp, "%d,", curr_level->map[0][i][j]);
		
		fprintf(fp, "\n");
	}
	
	fprintf(fp, "\n");
	
	//Layer 2: Tiles
	for(i=0; i< curr_level->rows; i++)// && !feof (fi))
	{
		for(j=0; j< curr_level->cols; j++)
			fprintf(fp, "%d,", curr_level->map[1][i][j]);
		
		fprintf(fp, "\n");
	}

	fclose(fp);
}

void debugLevel(Level* plevel)
{
	int i=0,j=0;
	
	printf("[Debug] Level\n");
	printf("Name: %s\n", plevel->name);
	printf("song_title: %s\n", plevel->song_title);
        printf("RGB: %d,%d,%d\n", plevel->bkgd_red, plevel->bkgd_green, plevel->bkgd_blue);

	printf("layers: %d\n", plevel->num_layers);
	printf("rows: %d\n", plevel->rows);
	printf("cols: %d\n", plevel->cols);

	printf("Map:\n");
	int layer=0;
	for(layer=0; layer < plevel->num_layers; layer++)
	{
		printf("layer: %d\n", layer);
		for(i=0; i < plevel->rows; i++)
		{
			for(j=0; j < plevel->cols; j++)
			{
				printf ("%d,", plevel->map[layer][i][j]);
			}
			printf("\n");
		}
		
		printf("\n");
	}
}
