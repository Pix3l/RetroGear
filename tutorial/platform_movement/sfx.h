/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    sfx.c - Common sound functions                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _SFX_H
#define _SFX_H

#include <SDL/SDL_mixer.h>

//Default game sounds effects and musics
Mix_Chunk *collectable_snd, *stomp_snd, *bounce_snd, *jump_snd, *action_snd, *extralife_snd;
Mix_Music *title_music, *pregame_music, *game_music, *gameover_music, *goal_music;

int channel;			//Canale su cui eseguire il suono
int audio_rate;			//Frequenza di trasmissione
Uint16 audio_format; 	//Formato dell'audio da eseguire
int audio_channels;		//2 canali = stereo
int audio_buffers;		//Dimensione del buffer audio in memoria

void initAudio();
Mix_Chunk* loadSound(char *file);
void playSound(Mix_Chunk *sound);
void destroySound(Mix_Chunk *sound);
Mix_Music* loadMusic(char *file);
void playMusic(Mix_Music *music, int repeat);
void pauseMusic();
void resumeMusic();
int isMusicPlaying();
void destroyMusic(Mix_Music *music);

#endif
