/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   tile.c - Tiles related functions                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "tile.h"
#include "gfx.h"
#include "camera.h"
#include "entity.h"
#include "rgtypes.h"
#include "level.h"

/**
 * Inizializza la superficie tiles con uno sprite sheet contenente le 
 * immagini.
 * 
 **/
void initTiles()
{
	if(curr_level==NULL)
	{
		printf("Error: Level structure not initialized...\n");
		return;	
	}
	
	//Colore di default: VERDE 0x00FF00
	Uint32 key = SDL_MapRGB(screen->format, 0, 255, 0 ); 
	tile_sheet=loadImage("data/tile_sheet.bmp", key);
	alpha_sheet=loadImage("data/alpha_sheet.bmp", key);
}

/**
 * Controlla se una coordinata rientra o meno in un possibile tile del
 * livello in base alla grandezza di esso definita globalmente.
 * 
 * Utile per controllare l'allineamento di una Entity su un'ipotetica
 * griglia di gioco
 * 
 * @param int x, int y
 * 		Coordinate da controllare sul campo di gioco
 * 
 * @return int
 * 		0 Se non e' allineato con i tile
 *      1 Se e' allineato con i tile
 **/
int isInTile(int x, int y)
{
	int cols, rows;
	// Se non c'e' resto siamo all'interno del tile
	cols = !(x%TILESIZE); //Asse x
	rows = !(y%TILESIZE); //Asse y
	// Ritorniamo 1 se siamo nel tile
	if(cols==1 && rows==1)
	 return 1;

	return 0; // Non e' perfettamente nel tile
}

/**
 * Converts a pixel position to a tile position.
 */
int pixelsToTiles(int pixels)
{
    return pixels / TILESIZE;
}

/**
 * Converts a tile position to a pixel position.
 */
int tilesToPixels(int numTiles)
{
    // use this if the tile size isn't a power of 2:
    return numTiles * TILESIZE;
}

/**
 * 
 * @param int x, int y
 * 		Coordinate da controllare sul campo di gioco
 * @param int w, int h
 * 		Larghezza e altezza della entity
 * @param int type
 * 		Valore del tile nel file di livello (Tipologia)
 * @param unsigned int layer
 * 		Il layer su cui controllare la collisione del tile
 **/
RG_Point* tileCollision(Entity *pobj, int new_x, int new_y)
{
    int col=0, row=0,
        minx=0, miny=0,
        maxx=0, maxy=0;

    //Resetting the RG_Point structure for good
    RG_Point point;
    point.x = 0;
    point.y = 0;
    
    // From pixels to tiles
    minx = pixelsToTiles( (pobj->x < new_x)? pobj->x : new_x );
    miny = pixelsToTiles( (pobj->y < new_y)? pobj->y : new_y );

    //Left : Right
    maxx = pixelsToTiles( (pobj->x > new_x)? (pobj->x + pobj->w -1) : (new_x + pobj->w-1) );
    //Up : Down
    maxy = pixelsToTiles( (pobj->y > new_y)? (pobj->y + pobj->h - 1) : (new_y + pobj->h - 1) );

    // We return the coordinates of the tile with which the entity collides
    for (col = minx; col <= maxx ; col++)
    {
        for (row = miny ; row <= maxy ; row++)
        {
            if (curr_level->map[SOLID_LAYER][row][col]==SOLID)
            {
                point.x = tilesToPixels(col);
                point.y = tilesToPixels(row);
                
                return &point;
            }
        }
    }

    //No collisions, no point to return
    return NULL;
}

/**
 * void drawTile(SDL_Surface *tiles, int tile, SDL_Rect *dest)
 * 
 * Disegna un tile specifico in base al valore di indice passato in
 * argomento
 * 
 * @param SDL_Surface *tiles
 * 		Superficie contenente i vari tiles
 * @param int tile
 * 		Valore indicativo del tile da prelevare
 * @param SDL_Rect *dest
 * 		Destinazione del tile sulla superficie principale
 *
 **/
void drawTile(SDL_Surface *tiles, int tile, SDL_Rect *dest)
{
	SDL_Rect pick;

	//Di default ci si aspetta 5 colonne
	pick.x=(tile % 6)*TILESIZE;
	pick.y=(tile / 6)*TILESIZE;
	pick.w=TILESIZE;
	pick.h=TILESIZE;

	if(tile!=0)	//No need to draw empty tile
	 SDL_BlitSurface(tiles,&pick,screen, dest);
}

/**
 * void drawTileMap(SDL_Rect)
 * 
 * Disegna la mappa di gioco, con relativa grafica in tiles.
 * La porzione disegnata e' solamente quella attualmente visibile, 
 * rappresentata dall'apposita struttura SDL_Rect.
 * 
 * @param SDL_Rect rect
 *      Rettangolo rappresentativo della porzione di matrice attualmente
 * 		letta.
 * 
 **/
void drawTileMap(Level *plevel)
{
	int row = camera.offsetY / TILESIZE;
	int col = camera.offsetX / TILESIZE;
	
	//Da 0 a TILESIZE!!!
	int difference_x = (int)camera.offsetX % TILESIZE;
	int difference_y = (int)camera.offsetY % TILESIZE;

	//TODO: la larghezza è l'altezza va controllata per evitare di uscire dai bordi
	int width = (SCREEN_WIDTH / TILESIZE)+1;
	int height = (SCREEN_HEIGHT / TILESIZE)+1;

	//out of bounds check
	#ifndef SCROLL_BOUND
		if(col+width>plevel->cols)
		{
			width -= (col+width) - plevel->cols;
		}
		
		if(row+height>plevel->rows)
		{
			height -= (row+height) - plevel->rows;
		}
	#endif
	
	SDL_Rect dest;
	
	int x, y;
    for (y = 0; y < height; y ++)
    {
        for (x = 0; x < width; x ++)
        {
            dest.x = x * TILESIZE - difference_x;
            dest.y = y * TILESIZE - difference_y;
            dest.w = dest.h = 0;

            //~plevel->layer
            //~ drawTile(tiles, plevel->map[1][row + y][col + x], &dest);
            
            if(plevel->map[1][row + y][col + x]!=0)	//No need to draw empty tile
            {
                getSprite(tile_sheet, plevel->map[1][row + y][col + x], TILESIZE, TILESIZE, &dest);
            }

            if(plevel->map[2][row + y][col + x]!=0)	//No need to draw empty tile
            {
                getSprite(alpha_sheet, plevel->map[2][row + y][col + x], TILESIZE, TILESIZE, &dest);
            }
        }
    }
}
