/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  util.c - Generic purpose functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "util.h"
#include "entity.h"
#include "font.h"
#include "timer.h"
#include "fps.h"
#include "gfx.h"
#include "game.h"
#include "level.h"
#include "sfx.h"
#include "tile.h"
#include "controls.h"
#include "typewriter.h"
#include "camera.h"
#include "player.h"
#include "score.h"
#include "menu.h"
#include "transition.h"
#include "event.h"

/**
 * Inizializza i parametri interni del motore di gioco
 **/
void init()
{
    //Game frequency
    fps.frequency= 1000 / 100;

    quit = 0;
    Game.status = MENU;
    curr_level = &level;
    curr_menu = &main_menu;
	
    //Init game engine subsystems
    initFont();
    //~ initAudio();	//Quit the program on error
    initTiles();
    //~ initScore();
    initController();

    //~ initTypewriter(&typewriter, FONT_W, (SCREEN_HEIGHT/2)+56, SCREEN_WIDTH-(FONT_W*3));
    //~ initTransition(TILESIZE, transition_lines);

    curr_player = &Player;
    initPlayer(curr_player);

    loadLevel(&level, "main");
    //~ curr_level->curr_layer=1;
    //~ initCamera(&camera);
}

void reset()
{
    curr_level = &level;

    //Clean all entities
    cleanEntities();
    clearScoreList();

    initPlayer(curr_player);

    loadLevel(&level, "platform");
    curr_level->curr_layer=1;
    initCamera(&camera);

    Game.score = 0;
}

void nextLevel()
{

}

void setNotification(int curr_time, char *notification)
{
	sys_timer.start_time = curr_time;
	sprintf(sys_message,notification);
}

/**
 * Appende un singolo char ad una stringa e la termina correttamente
 *
 * @param char *str
 * 		La stringa a cui appendee il char
 * @param char ch
 * 		Il carattere da appendere alla stringa
 **/
void appendChar(char* str, char ch)
{
	int len = strlen(str);
	str[len] = ch;
	str[len+1] = '\0';
	//~ printf("debug %s\n", s);
}

/**
 * This method will check intersection between two rectangular areas.
 * This will return 0 if there's a collision, 1 otherwise.
 * 
 * @param int x1, y1, w1, h1
 * 		Coordinates and size of the first area
 * 
 * @param int x2, y2, w2, h2
 * 		TCoordinates and size of the second area
 * 
 * @return 0
 * 		No collision
 * 		   1
 * 		Collision detected
 **/
int rectCollision(int x1, int y1, int w1, int h1, 
                  int x2, int y2, int w2, int h2)
{
    if (y1+h1 <= y2) return 0;
    if (y1 >= y2+h2) return 0;
    if (x1+w1 <= x2) return 0;
    if (x1 >= x2+w2) return 0;

    return 1;
}

int round_int(float value)
{
    return (value > 0.0) ? (value + 0.5) : (value - 0.5);
	//~ return ( (x - floor(x)) >= 0.5 ) ? ceil(x) : floor(x);    
}

/**
 * void cleanUp()
 * 
 * Libera le risorse precedentemente allocate in memoria e termina
 * correttamente le librerie.
 *
 **/
void cleanUp()
{
	quit = 1;

    SDL_FreeSurface(screen);
    SDL_FreeSurface(bmpfont);
    SDL_FreeSurface(tmpfont);
    SDL_FreeSurface(tile_sheet);
    SDL_FreeSurface(alpha_sheet);
    SDL_FreeSurface(fade);  //Transition

    cleanEntities();
    clearScoreList();
    cleanEvents();
    destroyMenu(&main_menu);

    destroyMusic(title_music);
    destroyMusic(pregame_music);
    destroyMusic(game_music);
    destroyMusic(goal_music);

    //Free default sounds
    destroySound(collectable_snd);
    destroySound(stomp_snd);
    destroySound(bounce_snd);
    destroySound(jump_snd);
    destroySound(action_snd);

    Mix_CloseAudio();
    SDL_Quit();
}
