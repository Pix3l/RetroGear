/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 sprite.c - Sprite handling functions               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _SPRITE_H
#define _SPRITE_H

#include <SDL/SDL.h>

//animation_speed=-1 - Nessuna animazione (Oggetto statico)
typedef struct _Sprite {
    SDL_Surface *surface;
	int x, y;
	int w, h;
	int index;
	float animation_timer;
	float animation_speed;
} Sprite;

Sprite title;

void initSprite(Sprite *sprite, int x, int y, int w, int h, float animation_speed, char *file);
void destroySprite(Sprite *sprite);
void drawSprite(Sprite *sprite, int x, int y);

#endif
