# RetroGear
## A simple and light 2D game engine

Released under **GPL license**, **RetroGear** aims to be a low resource and memory consuming 2D generic game engine, trying to allow pratically anyone to develope the game of their dream with less efforts, in a simple and easy way!

Some of the features offered by this game engine are:

- Game states management (screen titles, pre-game, game, game over)  
![Title screen](http://retrogear.sourceforge.net/screens/title.png)
![Pregame](http://retrogear.sourceforge.net/screens/pregame.png)  

- 2D maps management along with some useful features like scrolling and multilayering support  
![Map](http://retrogear.sourceforge.net/screens/worldmap.png)
![Level](http://retrogear.sourceforge.net/screens/tom1.png)  

- Menu and dialogues system  
![Menu](http://retrogear.sourceforge.net/screens/menu_dialogue.gif)
![Dialogue](http://retrogear.sourceforge.net/screens/plaoo.png)  

- Graphics, animations, sounds effects, scoring, in-game menus and in-game entities management simplified with dedicated libraries and systems  
![Sprites](http://retrogear.sourceforge.net/screens/sheet1.png)  

- Dedicated level editor! (Along with a custom gui framework)  
![Editor](http://retrogear.sourceforge.net/screens/leveler_beta.png)  

The project is working in progress, you can find it at:

-  [http://retrogear.sourceforge.net/](http://retrogear.sourceforge.net/)  
-  [https://gitlab.com/Pix3l/RetroGear](https://gitlab.com/Pix3l/RetroGear) (Mirror) 

Along with **Italian and English documentations**, sources, demo and tutorials.  

## Getting started

Checkout the **[repository](https://sourceforge.net/p/retrogear/git/ci/master/tree/)** for the latest updates of sources and documentation translations!

Navigate through 
