/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    entity.c - Game objects type                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <math.h>

#include "entity.h"
#include "camera.h"
#include "game.h"
#include "tile.h"
#include "gfx.h"
#include "util.h"
#include "player.h"
#include "sprite.h"
#include "rgtypes.h"
#include "collision.h"

//~ #define DEBUG_GFX

/**
 * Crea un oggetto di tipo Entity e lo aggiunge all'ultimo creato, nel
 * caso non vi siano gia' delle entity, l'oggetto diventa la testa della
 * lista
 * 
 * @param int id
 * 		L'id della della entity, +1 rispetto ad una entity precedente se
 * 		esistente
 * 
 * @param int solid
 * 		Si specifica se la entity e' di tipo solido o meno
 * 
 * @param int x, y, w, h
 * 		Rispettivamente coordinate e dimensioni della entity
 * 
 * @param int lives
 * 		Numero di vite associate alla entity
 *
 * @param SDL_Surface *sprite
 * 		Puntatore ad una struttura SDL_Surface pre-esistente contentente
 * 		lo sprite della entity
 * 
 * @param float animation_speed
 * 		Velocita' d'animazione della entity
 * 
 * @param float speed
 * 		Velocita' della entity nel campo di gioco
 * 
 * @param float gravity
 * 		Fattore di gravita' per la entity nel campo di gioco
 * 
 * @param void (*update)()
 * 		Funzione di gestione della entity e dei suoi comportamenti
 * 
 **/
void createEntity(int type, int x, int y, int w, int h, int lives,
                  float speed, float gravity, void (*update)())
{
	//calloc alloc and initialize the memory
	Entity *pnew = (Entity*) calloc(1, sizeof(Entity));

	if(EntityList_Head == NULL)
	{
		pnew->id = 1;
		EntityList_Head = pnew;
		EntityList_Tail = EntityList_Head;
		//~ printf("New entity HEAD %d\n", EntityList_Tail->id);
	}
	else
	{
		pnew->id = EntityList_Tail->id+1;
		EntityList_Tail->next = pnew;
		EntityList_Tail = pnew;
		//~ printf("New entity TAIL %d\n", EntityList_Tail->id);
	}
	
	EntityList_Tail->next = NULL;

    initEntity(EntityList_Tail, type, x, y, w, h, lives, speed, gravity, update);
}

/**
 * Initialize values for an entity
 * 
 * @param int id
 * 		L'id della della entity, +1 rispetto ad una entity precedente se
 * 		esistente
 * 
 * @param int solid
 * 		Si specifica se la entity e' di tipo solido o meno
 * 
 * @param int x, y, w, h
 * 		Rispettivamente coordinate e dimensioni della entity
 * 
 * @param int lives
 * 		Numero di vite associate alla entity
 *
 * @param SDL_Surface *sprite
 * 		Puntatore ad una struttura SDL_Surface pre-esistente contentente
 * 		lo sprite della entity
 * 
 * @param float animation_speed
 * 		Velocita' d'animazione della entity
 * 
 * @param float speed
 * 		Velocita' della entity nel campo di gioco
 * 
 * @param float gravity
 * 		Fattore di gravita' per la entity nel campo di gioco
 * 
 * @param void (*update)()
 * 		Funzione di gestione della entity e dei suoi comportamenti
 * 
 **/
void initEntity(Entity *pobj, int type, int x, int y, int w, int h, int lives,
                  float speed, float gravity, void (*update)())
{
	pobj->type = type;
	pobj->x = x;
	pobj->y = y;
	pobj->w = w;
	pobj->h = h;
	pobj->lives = lives;

	pobj->speed = speed;
	pobj->gravity = gravity;

	if(update==NULL)
	{
        quit=1;
        printf("Error: Entity update function not setted!\n");
	}
	
	pobj->update = update;

	//Valori di default
	pobj->flag_active = 1;
	pobj->xstart = x;
	pobj->ystart = y;
	pobj->direction_x = -1;
	pobj->direction_y = 0;
	pobj->visible = 1;
    pobj->hspeed = 0;
    pobj->vspeed = 0;

    //Not visible on screen, inactivate the entity
    RG_rect area = {pobj->x, pobj->y, pobj->w, pobj->h};
	if( is_onCamera(&camera, area) ==0 )
    {
        pobj->flag_active = 0;
    }
}

/**
 * Set an entity coordinates to given two and set their facing direction
 * 
 * @param Entity *pobj
 * 		Pointer to entity object
 * @param int x
 * 		X coordinate for the entity
 * @param int y
 * 		Y coordinate for the entity
 * @param int dirx
 * 		Entity facing direction on horizontal axis
 * @param int diry
 * 		Entity facing direction on vertical axis
 **/
void setEntityPosition(Entity *pobj, int x, int y, int dirx, int diry)
{
	pobj->x = x;
	pobj->y = y;
	pobj->direction_x = dirx;
	pobj->direction_y = diry;
}

/**
 * Handles the life cycle of the entity by calling their update function
 * and delete those marked for destruction
 **/
void doEntities()
{
	Entity *entity = EntityList_Head, *previous=NULL;
	while(entity!=NULL)
	{
		//Free memory from old entities
		if(entity->status==DESTROY)
		{
			if(EntityList_Head==entity && EntityList_Tail==entity)
			{
				free(entity);
				EntityList_Head=EntityList_Tail=NULL;
				return;
			}

			if(EntityList_Head==entity)
			{
				#ifdef DESTROY_DBG
					printf("head!\n");
				#endif

				EntityList_Head = entity->next;
				free(entity);
				entity = EntityList_Head;
			}
			else if(EntityList_Tail==entity)
			{

				#ifdef DESTROY_DBG
					printf("tail!\n");
				#endif

				previous->next=NULL;
				EntityList_Tail=previous;
				free(entity);
				entity = EntityList_Tail;
			}
			else
			{
				#ifdef DESTROY_DBG
					printf("Body! Entity is now ID %d\n", entity->id);
				#endif

				previous->next = entity->next;
				
				#ifdef DESTROY_DBG
					printf("Body! Previous next will be ID %d\n", entity->next->id);
				#endif

				free(entity);
				entity = previous->next;
			}
		}
		else 	//Just update their lifecycle
		{
            //Not visible on screen, inactivate the entity
            RG_rect area = {entity->x, entity->y, entity->w, entity->h};
            if( is_onCamera(&camera, area) == 1 )
            {
                entity->flag_active = 1;
            }
            
			if(entity->flag_active==1)
			{
				entity->update(entity);
			}
		}

		previous = entity;
		entity = entity->next;
	}
}

void resetEntities()
{
	//We reset alives entities position to their start value before
	//restart the game
	if(Game.status==PREGAME)
	{
		Entity *pobj = EntityList_Head;
		while(pobj!=NULL)
		{
			setEntityPosition(pobj, pobj->xstart, pobj->ystart, -1, 0);
			pobj=pobj->next;
		}
	}
}

/**
* Crea una nuova entity presente da mappa assegnandogli le coordinate
* X e Y rispettive e valori di default per i rimanenti
* 
* @param int id
* 		Numero identificativo della entity nella mappa
* 
* @param int col, int row
* 		Matrix position of the entity, this will be converted
*/
void createEntityFromMap(int id, int col, int row)
{
	/**
	 * Reserved numbers are:
	 * 1 - Solid tiles
	 * 2 - Player start position
	 * 3 - Warp
	 **/

    col = tilesToPixels(col);
    row = tilesToPixels(row);
	
	switch(id)
	{
		case PLAYER_ID:	//Player
			setPlayerPosition(col, row);
		break;
	}
}

/**
 * Conta il numero di entity presenti in lista
 * 
 * @return int
 * 		Il numero di entity presenti
 **/
int countEntities()
{
	int i = 0;
	Entity *current = EntityList_Head;
	while(current!=NULL)
	{
		i++;
		current=current->next;
	}
	
	return i;
}

/**
 * Count the number of entity of the specified type
 * 
 * @return int
 * 		The number of counted entities
 **/
int countTypeEntity(int type)
{
	unsigned int i = 0;
	
	Entity *current = EntityList_Head;
	while(current!=NULL)
	{
		if(current->type==type)
		{
			i++;
		}
		current=current->next;
	}
	
	//Since entity creation starts by 0
	return i-1;
}

/**
 * Conta il numero di entity presenti attualmente sullo schermo in base 
 * al loro tipo
 * 
 * @param int type
 * 		Il tipo di entity
 * 
 * @return int
 * 		Il numero di entity presenti
 **/
int countEntityOnCamera(int type)
{
	int i = 0;
	Entity *current = EntityList_Head;
	while(current!=NULL)
	{
        RG_rect area = {current->x, current->y, current->w, current->h};
	    if(is_onCamera(&camera, area) && type==current->type)
	    {
		    i++;
	    }
	    current=current->next;
	}
	
	printf("%d\n",i);
	return i;
}

/**
 * void moveEntity_X(Entity *pobj)
 * 
 * Gestisce il movimento di un oggetto Entity orizzontalmente, 
 * utilizzando i valori impostati nell'oggetto stesso.
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto entity da gestire
 **/
void moveEntity_X(Entity *pobj)
{
    RG_Point point = {-1, -1};
    
    //If we have a direction, lets move
	if (pobj->direction_x!=0)
	{
		pobj->hspeed += pobj->speed * pobj->direction_x;
		pobj->status = MOVE;
	}

    //Limit maximum speed
    if (pobj->hspeed > 0.5f) pobj->hspeed = 0.5f;
    if (pobj->hspeed < -0.5f) pobj->hspeed = -0.5f;

    //Checking collision on the left
    if(pobj->hspeed<0)
    {
        //We slow down to a complete stop
		if(!pobj->direction_x)
		{
			pobj->hspeed += 0.02f;

			if(pobj->hspeed > 0.0f)
				pobj->hspeed = 0.0f;
		}
        else
        {
            //Has collide on left?
            point = tileCollision(pobj, floorf(pobj->x+pobj->hspeed), pobj->y);
            if( point.x != -1 )
            {
                pobj->x= point.x+TILESIZE;

                //Reverse direction
                pobj->direction_x *=-1;
            }
            else
            {
                pobj->x += pobj->hspeed;         //Update its position
            }
        }
    }
    else if(pobj->hspeed>0)
    {
        //We slow down to a complete stop
		if(!pobj->direction_x)
		{
			pobj->hspeed -= 0.02f;

			if(pobj->hspeed < 0.0f)
				pobj->hspeed = 0.0f;
		}
        else
        {
            point = tileCollision(pobj, ceilf(pobj->x+pobj->hspeed), pobj->y);
            if( point.x != -1 )
            {
                pobj->x= point.x-pobj->w;

                //Reverse direction
                pobj->direction_x *=-1;
            }
            else
            {
                pobj->x += pobj->hspeed;         //Update its position
            }
        }
    }
}

/**
 * Handles the gravity and jumps for entities
 * 
 * @param Entity *pobj
 * 		Pointer to the entity object
 **/
void doEntityGravity(Entity *pobj)
{
    RG_Point point = {0, -1};
    
    //Checking collision in the up direction
    if(pobj->vspeed<0)
    {
        //Has collide on top?
        point = tileCollision(pobj, pobj->x, floorf(pobj->y+pobj->vspeed));
        if( point.y != -1 )
        {
            pobj->y = point.y + TILESIZE;   //Place the object nearest the top tile
			pobj->vspeed = 0;

            pobj->direction_y = 0;
        }
        else
        {
            pobj->direction_y = -1;
            pobj->status = JUMP;
            pobj->y += pobj->vspeed;         //Update its position
            pobj->vspeed += pobj->gravity;   //Gravity continues to push down
        }
    }
    else
    {
        //Has collide on bottom?
        point = tileCollision(pobj, pobj->x, ceilf(pobj->y+pobj->vspeed));
        if( point.y != -1 )
        {
            pobj->y = ceilf(point.y - pobj->h);   //Place the object on top of the bottom tile
            pobj->vspeed = 1;   //1 so we test against the ground again int the next frame (0 would test against the ground in the next+1 frame)

            if(pobj->status!=MOVE)
            {
                pobj->status = IDLE;
                pobj->direction_y = 0;
            }

            pobj->direction_y = 0;
        }
        else
        {
            pobj->direction_y = 1;
            pobj->status = FALL;
            pobj->y += pobj->vspeed;         //Update its position
            pobj->vspeed += pobj->gravity;   //Gravity continues to push down

            if( pobj->vspeed >= TILESIZE)	
                pobj->vspeed = TILESIZE;
        }
    }
}

/**
 * Check if an Entity is touching the floor or not
 *
 * @param Entity *pobj
 * 		Pointer to the entity object
 *
 * @return int
 *      1 in case of collision
 *      0 if the entity is not touching a solid tile at pobj->y+1
 **/
int isEntityOnFloor(Entity *pobj)
{
    RG_Point point = {0, -1};

    point = tileCollision(pobj, pobj->x, ceilf(pobj->y)+1);
    if( point.y != -1 )
    {
        return 1;
    }

    return 0;
}

/**
 * void jumpEntity(Entity *pobj)
 * 
 * Esegue un movimento di tipo salto statico su di un'oggetto
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto entity da gestire
 **/
void jumpEntity(Entity *pobj)
{
    /**
	//Incrementiamo la velocità verticale della entity con la gravità
	pobj->vspeed+=pobj->gravity;
	
	//Limitiamo la velocita' alla dimensione di un tile
	if(pobj->vspeed >= TILESIZE)
		pobj->vspeed = TILESIZE;

	//Collisione con un'ostacolo del livello
	if(tileCollision(pobj->x, pobj->y+pobj->vspeed, pobj->w, pobj->h, SOLID, 0))
	{	
		int ty = (pobj->y+pobj->vspeed) / TILESIZE;
		
		if(pobj->vspeed < 0)	//Salto
		 pobj->y = (ty+1)*TILESIZE;
		else					//Caduta
		 pobj->y = (ty-1)*TILESIZE+(pobj->h > TILESIZE ? TILESIZE+16 : 16);	//+(TILESIZE+Entity Height)

		//Testiamo il terreno
		pobj->vspeed=1.0f;
	}
    **/
}

/**
 * Gestisce l'animazione complessa di una entity, animazione composta
 * dalle azioni basilari, come camminare, saltare, abbassarsi, 
 * arrampicarsi, rimbalzo e attacco.
 * Le azioni sono specificate nell'apposita struttura enum.
 * 
 * @param Entity *pobj
 * 		La entity da animare
 * 
 * @param int loop
 *      Flag per l'impostazione d'animazione infinita.
 *      Se impostato a 0, la entità sarà animata senza sosta, con valore
 *      diverso la entità assumerà la corretta animazione di stop per
 *      la relativa direzione.
 **/
void animateEntity(Entity *pobj, int loop)
{
    //Entity in attesa di distruzione
    if(pobj->status==KILL)
    {
        pobj->sprite.index = DIE;   //Killed entity
        return;
    }

    /**
     * L'indice dei frame dell'animazione è incrementato in base 
     * alla velocità d'animazione scelta
     **/
    pobj->sprite.animation_timer += pobj->sprite.animation_speed;

    //Climb is a special animation state
	if(pobj->status==CLIMB)
	{
        if(Player.vspeed==0) pobj->sprite.animation_timer = 0; 
        
		if (pobj->sprite.animation_timer >= 2) pobj->sprite.animation_timer = 0;
        
		pobj->sprite.index = CLIMB1 + floorf(pobj->sprite.animation_timer);
		return;
	}

    ///Di default impostiamo 4 fotogrammi d'animazione per la camminata
    if (pobj->sprite.animation_timer >= 3)
    {
        pobj->sprite.animation_timer = 0;
    }
    
    if(pobj->direction_x != 0) 
    {
        pobj->sprite.index = (pobj->direction_x < 0 ? WALKLEFT1 : WALKRIGHT1)+ abs(pobj->sprite.animation_timer);
    }

    //TODO: Se direction_y fosse diverso da 0 durante il movimento sull'asse X? Caduta ad esempio.

    if(pobj->direction_y != 0)// && pobj->status == MOVE)
    {
        pobj->sprite.index = (pobj->direction_y < 0 ? WALKUP1 : WALKDOWN1)+ abs(pobj->sprite.animation_timer);
    }
    
    //Se l'animazione non è continuativa anche da fermi
    if(!loop)
    {
        //Se la velocità orizzontale è 0 e la direzione Y 0, vuol dire che il personaggio si stava muovendo sull'asse X
        if (pobj->hspeed == 0 && pobj->direction_y == 0)
        {
            pobj->sprite.animation_timer = 0;
            
            /**
             * In base alla direzione del personaggio, selezioniamo la riga
             * dello sprite impostando l'indice di partenza nel foglio
             **/
            pobj->sprite.index = (pobj->direction_x < 0 ? IDLELEFT : IDLERIGHT);
        }
        
        if (pobj->vspeed == 0 && pobj->direction_x == 0)
        {
            pobj->sprite.animation_timer = 0;
            
            /**
             * In base alla direzione del personaggio, selezioniamo la riga
             * dello sprite impostando l'indice di partenza nel foglio
             **/
            pobj->sprite.index = (pobj->direction_y < 0 ? IDLEUP : IDLEDOWN);
        }
    }

    /**
     * Animazioni per il salto/caduta
     **/
    if( pobj->status == JUMP || pobj->status == FALL )
    {
        if( pobj->direction_x == -1 ) 
        {
            pobj->sprite.index = JUMPLEFT;
        }

        if( pobj->direction_x == 1 ) 
        {
            pobj->sprite.index = JUMPRIGHT;
        }
    }
    else
    {
        /**
         * Animazioni per la derapata
         **/
        if(pobj->direction_x == -1 && pobj->hspeed > 0.0f)
        {
            pobj->sprite.index = SIDESLIP1;
        }
        
        if(pobj->direction_x == 1 && pobj->hspeed < 0.0f)
        {
            pobj->sprite.index = SIDESLIP2;
        }
    }
}

/**
 * 
 * void drawEntity(Entity *pobj)
 * 
 * Disegna un Entity sullo schermo e ne anima la grafica.
 * Un meccanismo interno decidera' se aggiornare solamente la posizione
 * occupata dall'oggetto oppure ridisegnare lo schermo intero.
 * TODO: correggere descrizione
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto entity da disegnare
 * 
 * @param Camera *pcam
 * 		Puntatore all'oggetto Camera, necessario per
 * 		una corretta visualizzazione su schermo durante fasi di 
 * 		scrolling del livello.
 **/
void drawEntity(Entity *pobj)
{
    //If the entity is visible and a sprite surface exist
	if(pobj->visible && pobj->sprite.surface)
	{
		//Differenza in pixel tra sprite e bounding box
		int diff_w = (pobj->w < TILESIZE)? (pobj->w - TILESIZE) : 0;
		int diff_h = (pobj->h < TILESIZE)? (pobj->h - TILESIZE) : 0;

        int dest_x = ((int)pobj->x - camera.x) + diff_w +1;
        int dest_y = ((int)pobj->y - camera.y) + diff_h;

		//Entity in attesa di distruzione
		if(pobj->status==KILL)
		{
		    //Killed entity
		    dest_y -= diff_h;
            drawSprite(&pobj->sprite, dest_x, dest_y);

		    return;
		}
		
		//Blinking
		if(pobj->status==BLINK)
		{
			if( (int)pobj->sprite.animation_timer % 2 )
			{
				return;
			}
		}

        //Draw the real entity instead of its sprite as red rectangle
		#ifdef DEBUG_GFX
			drawFillRect( (int)(pobj->x-camera.x), (int)(pobj->y - camera.y), pobj->w, pobj->h, 0xff0000);
        #else
            drawSprite(&pobj->sprite, dest_x, dest_y);
		#endif
	}
}

/**
 * Clean allocated entities
 **/
void cleanEntities()
{
	#ifdef DESTROY_DBG
		printf("Clear entities ");
	#endif

	Entity *current = EntityList_Head, *next=NULL;
	while(current!=NULL)
	{
	    //~ printf("Destroy entity ID %d\n", current->id);
	    next=current->next;

        //The sprite surface need a special function
        SDL_FreeSurface(current->sprite.surface);   

        free(current);
	    current=next;
	
	    #ifdef DESTROY_DBG
		    printf(".");
	    #endif
	}

    EntityList_Head=NULL;
    EntityList_Tail=NULL;
}
