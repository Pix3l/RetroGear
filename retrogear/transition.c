/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                transition.c - Transintion effects handler          *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "transition.h"
#include "gfx.h"
#include "tile.h"

/**
 * Initialize the transition effect
 * 
 * @param int size
 * 		Size of the the rects to draw
 * 
 * @param void (*effect)()
 * 		The effect to apply to the transition
 * 
 **/
//TODO: Apply a default size to the transition? TILESIZE!
void initTransition(int size, void (*effect)())
{
	transition.flag_active = 0;
	transition.progress = 0;
	transition.size = size;
	transition.effect = effect;
}

void doTransition()
{
	static int delay = 0;
	delay++;
	
	if(delay % 5 == 0)
	{
		transition.effect();
		delay=0;
	}
}

/**
 * Fill the screen with random black squares
 **/
void transition_lines()
{
	//~ int progress = transition.progress+transition.size;
	
	int i;
	for(i=0; i<SCREEN_HEIGHT; i+=transition.size)
	{
		//To the left
		if((i/2)%TILESIZE)
		{
			int x = (SCREEN_WIDTH-TILESIZE)-(transition.progress*TILESIZE);
			int w = SCREEN_WIDTH-x;
			drawFillRect(x, i, w, transition.size, 0x000000);
		}
		else 	//To the right
		{
			int w = 0+transition.size+(transition.progress*TILESIZE);
			drawFillRect(0, i, w, transition.size, 0x000000);
		}
	}

	transition.progress++;
	 
	//~ printf("%d\n", transition.progress*TILESIZE);

	if(transition.progress*TILESIZE>=SCREEN_WIDTH)
	{
		printf("end transition\n");
		transition.flag_active=0;
	}
}

/**
 * Fills the screen with black stripes, alternating from left to right 
 * and vice versa.
 **/
void transition_random()
{
	
}

/**
 * Fills the screen with black in circular motion
 **/
void transition_pie()
{
	
}

void transition_fadein()
{
	if(fade == NULL)
	{
		printf("create rgb\n");
		fade= SDL_CreateRGBSurface(SDL_HWSURFACE,screen->w,screen->h,32,0,0,0,0);
		//~ SDL_SetAlpha( fade, SDL_SRCALPHA, SDL_ALPHA_TRANSPARENT );
	}
    
    //~ if(fade == NULL) {
        //~ fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
        //~ exit(1);
	//~ }
	
	static int alpha=0;
	
	if(alpha<255)
	{
		alpha+=10;
	}
	
	SDL_SetAlpha( fade, SDL_SRCALPHA, alpha );
	SDL_BlitSurface(fade,NULL,screen,NULL);

	if(alpha>=250)
	{
		alpha=0;
		transition.flag_active=0;
	}
}
