/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 sprite.c - Sprite handling functions               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include <math.h>

#include "sprite.h"
#include "gfx.h"

#define DEBUG_SPRITE

/**
 * Initilize a sprite structure, with dynamic allocation of the surface image
 **/
void initSprite(Sprite *sprite, int x, int y, int w, int h, float animation_speed, char *file)
{
    sprite->x = x;
    sprite->y = y;
    sprite->w = w;
    sprite->h = h;

    sprite->animation_timer = 0;
    sprite->animation_speed = animation_speed;

    sprite->surface = loadImage(file, COLORKEY);

    #ifdef DEBUG_SPRITE
        printf("[initSprite] Source %s, x %d, y %d, w %d, h %d, animation_speed %.1f\n", file, x, y, w, h, animation_speed);
    #endif

    //Use SDL_Surface properties
    //~ sprite->w = surface.w;
    //~ sprite->h = surface.h;
}

/**
 * Initilize a sprite structure with the association of a pre existing surface image
 **/
void setSprite(Sprite *sprite, int x, int y, int w, int h, float animation_speed, SDL_Surface *surface)
{
    sprite->x = x;
    sprite->y = y;
    sprite->w = w;
    sprite->h = h;

    sprite->animation_timer = 0;
    sprite->animation_speed = animation_speed;

    sprite->surface = surface;

    #ifdef DEBUG_SPRITE
        printf("[setSprite] x %d, y %d, w %d, h %d, animation_speed %.1f\n", x, y, w, h, animation_speed);
    #endif
}

void destroySprite(Sprite *sprite)
{
    SDL_FreeSurface(sprite->surface);
}

//Probabilmente questa funzione non è necessaria...
void drawSprite(Sprite *sprite, int x, int y)
{
    //Da controllare la correttezza logica...
    SDL_Rect pick;
    pick.x=floor(sprite->index % SPRITE_FRAMES) * sprite->w;
    pick.y=floor(sprite->index / SPRITE_FRAMES) * sprite->h;
    pick.w = sprite->w;
    pick.h = sprite->h;

    SDL_Rect dest;
    dest.x = x;
    dest.y = y;
    dest.w = sprite->w;
    dest.h = sprite->h;
    
    SDL_BlitSurface( sprite->surface, &pick, screen, &dest );
}
