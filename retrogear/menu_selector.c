/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *              menu_selector.c - Selector type menu item 	          *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "menu_selector.h"
#include "controls.h"
#include "gfx.h"
#include "font.h"
#include "menu.h"
#include "bit.h"

void initSelector(Menu* pmenu, int x, int y, char *name, SDL_Color color, int size, Menu *previous, Menu *next)
{
    initMenu(pmenu, NULL, x, y, name, 0, color, previous, next, &doSelector, &drawSelector);
    pmenu->num_items=size;
    pmenu->cursor=31;
    pmenu->w=size*SELECTOR_SPACE;
    pmenu->h=FONT_H+2;
    setFlag(&pmenu->flags, menu_show_title);
}

void doSelector(Menu *pmenu)
{
    if(isButtonPressed(BUTTON_LEFT))
    {
        waitButtonRelease(BUTTON_LEFT);
        pmenu->curr_item--;
    }

    if(isButtonPressed(BUTTON_RIGHT))
    {
        waitButtonRelease(BUTTON_RIGHT);
        pmenu->curr_item++;
    }

    if(isButtonPressed(BUTTON_UP))
    {
        waitButtonRelease(BUTTON_UP);
        previousMenu(pmenu);
    }

    if(isButtonPressed(BUTTON_DOWN))
    {
        waitButtonRelease(BUTTON_DOWN);
        nextMenu(pmenu);
    }

    //Limit the cursor
    if(pmenu->curr_item<0)
    {
        pmenu->curr_item=0;
    }

    if(pmenu->curr_item>pmenu->num_items)
    {
        pmenu->curr_item=pmenu->num_items;
    }
}

//TODO: inserire un argomento di flag per aggiungere il bordo al valore del selettore?
void drawSelector(Menu *pmenu)
{
    if( hasFlag(pmenu->flags, menu_show_title) )
    {
	    drawString(pmenu->x, pmenu->y-(FONT_H*3), pmenu->name, pmenu->color, 0);
    }

    //Cursor
    drawChar(pmenu->x-(FONT_W/2)+(pmenu->curr_item*SELECTOR_SPACE), pmenu->y-(FONT_H-2), pmenu->cursor, white, 0);

    //Draw selector value
    char message[10];
    sprintf(message,"%02d", pmenu->curr_item);
    drawString(pmenu->x+(pmenu->num_items*SELECTOR_SPACE)+FONT_W, pmenu->y, message, white, 0);

    //Draw selector bar
    drawFillRect(pmenu->x, pmenu->y+(FONT_H-2), pmenu->num_items*SELECTOR_SPACE, 2, 0xffffff);

    int i;
    for(i=0; i<pmenu->num_items/2; i++)
    {
        if(i%2)
        {
	        drawFillRect(pmenu->x*i, pmenu->y+(FONT_H/2), 1, 6, 0xffffff);
        }

        //~ if(i%pmenu->num_items)
        //~ {
	        //~ drawFillRect(pmenu->x*i, pmenu->y-2, 1, 6, 0xff0000);
        //~ }
    }
}
