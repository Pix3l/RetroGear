/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    sfx.c - Common sound functions                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "sfx.h"

/**
 * initAudio()
 * 
 * Inizializza il sottosistema sonoro di SDL_mixer, termina il programma
 * in caso di errore.
 * 
 **/
void initAudio()
{
	audio_rate = 22050;			//Frequency of audio playback
	audio_format = AUDIO_S16; 	//Format of the audio we're playing
	audio_channels = 2;			//2 channels = stereo
	audio_buffers = 4096;		//Size of the audio buffers in memory
	
	//Initialize SDL_mixer with our chosen audio settings
	if(Mix_OpenAudio(audio_rate, audio_format, audio_channels, audio_buffers)) {
		printf("Unable to initialize audio: %s\n", Mix_GetError());
		exit(1);
	}

	//Load defaults sound effects
	collectable_snd = loadSound("snd/collectable.wav");
	stomp_snd = loadSound("snd/stomp.wav");
	//~ bounce_snd = loadSound("snd/bounce.wav");
	jump_snd = loadSound("snd/jump.wav");
	//~ action_snd = loadSound("snd/action.wav");
	extralife_snd = loadSound("snd/extra_life.wav");

	title_music = loadMusic("snd/title_theme.wav");
	pregame_music = loadMusic("snd/pregame_theme.wav");
	game_music = loadMusic("snd/level_theme.wav");
	gameover_music = loadMusic("snd/gameover_theme.wav");
	goal_music = loadMusic("snd/goal.wav");
}

/**
 * Mix_Chunk loadSound(char)
 * 
 * Carica uno specifico effetto sonoro (wav, ogg, etc...) su di uno 
 * specifico canale audio e ne restituisce un puntatore.
 * 
 * @param char *file
 * 		Nome del file da caricare (compreso percorso se necessario)
 * 
 * @return Mix_Chunk *sound[slot]
 **/
Mix_Chunk* loadSound(char *file)
{
	//Carichiamo il file in memoria e teniamo traccia del tutto
	Mix_Chunk *soundptr = Mix_LoadWAV(file);
	if(soundptr == NULL) {
		printf("Unable to load file: %s\n", Mix_GetError());
		return NULL;
	}
	
	return soundptr;
}

/**
 * void playSound(Mix_Chunk *sound)
 * 
 * Esegue l'effetto sonoro puntato in memoria
 * 
 * @param Mix_Chunk *sound
 * 		Puntatore al suono da eseguire
 **/
void playSound(Mix_Chunk *sound)
{	
	//Play our sound file, and capture the channel on which it is played
	Mix_PlayChannel(-1, sound, 0);
	if(channel == -1) {
		printf("Unable to play sound: %s\n", Mix_GetError());
	}
}

/**
 * void destroySound(Mix_Chunk *sound)
 * 
 * Libera la memoria dal sonoro allocato
 * 
 * @param Mix_Chunk *sound
 * 		Puntatore al suono da cancellare
 * 
 **/
void destroySound(Mix_Chunk *sound)
{	
	Mix_FreeChunk(sound);
	sound = NULL;
}

/**
 * Mix_Music* loadMusic(char *file)
 * 
 * Carica uno specifico file audio per la musica (wav, ogg, etc...) su
 * di uno specifico canale audio e ne restituisce un puntatore.
 * 
 * @param char *file
 * 		Nome del file da caricare (compreso percorso se necessario)
 * 
 * @param int slot
 * 		Slot nel quale caricare il suono
 * 
 * @return Mix_Chunk *sound[slot]
 **/
Mix_Music* loadMusic(char *file)
{
	//Carichiamo il file in memoria e teniamo traccia del tutto
	Mix_Music *musicptr = Mix_LoadMUS(file);
	if(musicptr == NULL) {
		printf("Unable to load file: %s\n", Mix_GetError());
		return NULL;
	}
	
	return musicptr;
}

/**
 * Play the specified music
 * 
 * @param Mix_Music *music
 * 		Pointer to executed music
 * 
 * @param int repeat
 * 		Loop flag, 0 is equal to 1 time, -1 is equal to infinite loop
 **/
void playMusic(Mix_Music *music, int repeat)
{	
	//~ //Play our sound file, and capture the channel on which it is played
	//~ Mix_PlayMusic(music, repeat);
	//Controlla la fine della musica ed esegue una funzione di callback
	//~ Mix_HookMusicFinished(funzione);

	if(Mix_PlayMusic(music, repeat)==-1) 
	{
		printf("Mix_PlayMusic: %s\n", Mix_GetError());
	}
	else
	{
		Mix_PlayMusic(music, repeat);
	}

}

void pauseMusic()
{
	Mix_PauseMusic();
}

void resumeMusic()
{
	if(!isMusicPlaying())
    {
    	Mix_ResumeMusic();
    } 
}

/**
 * Check if music is currently in play
 * 
 * @return int
 * 		1 play
 * 		0 no play
 **/
int isMusicPlaying()
{
	return Mix_PlayingMusic();
}

//TODO: Check if sound play
//~ int isSoundPlaying()
//~ {
	//~ return Mix_Playing(-1);
//~ }

/**
 * void destroyMusic(Mix_Music *music)
 * 
 * Libera la memoria dalla musica allocata
 * 
 * @param Mix_Music *music
 * 		Puntatore alla musica da cancellare
 * 
 **/
void destroyMusic(Mix_Music *music)
{	
	Mix_FreeMusic(music);
	music = NULL;
}
