/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             		ouija.c - Ingame virtual keyboard 	              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _OUIJA_H
#define _OUIJA_H

#include "menu.h"

#define OUIJA_STR_MAX_LENGTH 6

Menu ouija;
Item ouija_items[64];

extern char ouija_str_holder[OUIJA_STR_MAX_LENGTH];    //Hold player ouija input

void initOuija();
void doOuija();
void ouija_appendCharacter();
void ouija_deleteCharacter();
void ouija_done();
void drawOuija();

#endif
