/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                font.c - Image font handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <string.h>

#include "font.h"
#include "gfx.h"
#include "controls.h"
#include "util.h"

/**
 * Inizializza la superficie font_sheet caricando l'immagine con i font
 * e realizzando una superficie temporanea per la manipolazione grafica
 * dei singoli caratteri
 **/
void initFont(void)
{
    // Load bmp font on a surface
    Uint32 key = SDL_MapRGB( screen->format, 255, 0, 255 );

    //Default transparency colour: Acid green 0x00FF00
    if(font_sheet == NULL)
    {
        font_sheet=loadImage("data/nesfont.bmp", key);
    }

    // Create a temporary surface for single bmp font character
    tmpfont=SDL_CreateRGBSurface(SDL_HWSURFACE,FONT_W,FONT_H,32,0,0,0,0);
    if(tmpfont == NULL) {
        fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
        exit(1);
    }
}

/**
 * Copia una porzione dalla superficie font_sheet e la copia sulla superficie screen
 * 
 * @param SDL_Surface *font_sheet
 *      Puntatore alla superficie di origine
 * @param int X, int Y
 *      Destinazione in cui si andra' a copiare il contenuto di origine
 * @param int w, int h
 *      Rispettivamente, larghezza e grandezza della superficie da copiare
 * @param int asciicode
 *      Singolo carattere convertito in un equivalente decimale
 * @param SDL_Color color
 *      Struttura contenente i valori RGB di colore da usare per la superficie
**/
void drawChar(int dest_x, int dest_y, int asciicode, SDL_Color color, int alpha)
{
     SDL_Rect area, pick;
     
     //Cordinate nella superficie con le lettere
     pick.x = (asciicode % 32)*FONT_W;
     pick.y = (asciicode / 32)*FONT_H;
     pick.w = FONT_W;
     pick.h = FONT_H;
     area.x = dest_x;
     area.y = dest_y;
     area.w = FONT_W;
     area.h = FONT_H;
     
     SDL_BlitSurface(font_sheet, &pick, tmpfont,NULL);
     
     //Replace white with a chosen color (target)
     int target_color = SDL_MapRGB (tmpfont->format, color.r, color.g, color.b);
	 replaceColor(tmpfont, WHITE, target_color);

     //Set background transaparency
     if(alpha>0)
      SDL_SetColorKey(tmpfont, SDL_SRCCOLORKEY, BLACK);
     else //Disable any background transaparency
      SDL_SetColorKey(tmpfont, 0, 0);
     
     SDL_BlitSurface(tmpfont, NULL, screen,&area);
}

void drawString(int dest_x, int dest_y, char *text, SDL_Color color, int alpha)
{
    if(font_sheet==NULL)
    {
        printf("Error: Font surface not initialized!\n");
        quit=1;
        return;
    }

    //For the entire string
    int i = 0;
    int x = dest_x;
    while(text[i] != '\0')
    {
        //Newline or end of the line, we bring the text to wrap
        if(text[i]=='\n')
        {
            x=dest_x; i++;
            dest_y+=FONT_H;
            continue;
        }
        
        drawChar(x, dest_y, text[i], color, alpha);

        x+=FONT_W;
        i++;
    }
}
