/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                font.c - Image font handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _FONT_H
#define _FONT_H

#include <SDL/SDL.h>

#define FONT_W 8
#define FONT_H 8

SDL_Surface *font_sheet, *tmpfont;  //Font surfaces
char message[35];	//Testo generico

void initFont(void);
void textInput();
void drawChar(int dest_x, int dest_y, int asciicode, SDL_Color color, int alpha);
void drawString(int dest_x, int dest_y, char *text, SDL_Color color, int alpha);

#endif
