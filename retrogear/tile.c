/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   tile.c - Tiles related functions                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "tile.h"
#include "entity.h"
#include "rgtypes.h"
#include "level.h"

/**
 * Controlla se una coordinata rientra o meno in un possibile tile del
 * livello in base alla grandezza di esso definita globalmente.
 * 
 * Utile per controllare l'allineamento di una Entity su un'ipotetica
 * griglia di gioco
 * 
 * @param int x, int y
 * 		Coordinate da controllare sul campo di gioco
 * 
 * @return int
 * 		0 Se non e' allineato con i tile
 *      1 Se e' allineato con i tile
 **/
int isInTile(int x, int y)
{
	int cols, rows;
	// Se non c'e' resto siamo all'interno del tile
	cols = !(x%TILESIZE); //Asse x
	rows = !(y%TILESIZE); //Asse y
	// Ritorniamo 1 se siamo nel tile
	if(cols==1 && rows==1)
	 return 1;

	return 0; // Non e' perfettamente nel tile
}

/**
 * Converts a pixel position to a tile position.
 */
int pixelsToTiles(int pixels)
{
    return pixels / TILESIZE;
}

/**
 * Converts a tile position to a pixel position.
 */
int tilesToPixels(int numTiles)
{
    // use this if the tile size isn't a power of 2:
    return numTiles * TILESIZE;
}

int getTile(int x, int y, int layer)
{
    x = pixelsToTiles(x);
    y = pixelsToTiles(y);
    return curr_level->map[layer][y][x];
}
