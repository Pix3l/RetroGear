/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   game.c - Game handling function                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "game.h"
#include "typewriter.h"
#include "fps.h"
#include "draw.h"
#include "controls.h"
#include "menu.h"
#include "player.h"
#include "entity.h"
#include "sfx.h"
#include "util.h"
#include "dialogue.h"
#include "timer.h"
#include "bit.h"
#include "score.h"
#include "level.h"
#include "controls.h"

/**
 * void doTitleScreen()
 * 
 * Questa funzione gestisce la schermata dei titoli e relativo menu.
 * Permette al giocatore di scegliere varie opzioni di gioco.
 **/ 
void doTitleScreen(void)
{
    menuInput(&main_menu);

	//If music is not playining
    if(!isMusicPlaying())
    {
    	//~ playMusic(title_music, 0);
    }
}

/**
 * void doPreGame()
 * 
 * Questa funzione gestisce uno screen di pre-game.
 * Permette al giocatore di prendersi una manciata di secondi per
 * prepararsi prima di iniziare il gioco vero e proprio.
 **/ 
void doPreGame(void)
{
	//~ if(Game.status!=PREGAME)
	//~ {
		//~ setGameState(PREGAME);
	//~ }

	//Stop any music from the game
	if(isMusicPlaying())
	{
		pauseMusic();
	}

    if(get_time_elapsed(timer.start_time) > 2)
    {
        //Reset generic timer
        timer.start_time = 0;
        //Let's play!
        setGameState(GAME);
        playMusic(game_music, 0);
        return;
    }
    
    if(Player.lives==0)
    {
    	playMusic(gameover_music, 0);
    	setGameState(GAMEOVER);
    	return;
    }
    
    //~ if(!isMusicPlaying())
    //~ {
    	//~ playMusic(music_pregame, 0);
    //~ } 
}

void doGame(void)
{
    pauseGame();
	if(hasFlag(Game.flags, on_pause))
	return;

	//Player handling
	updatePlayer();

	doEntities();
	doScore();

    if(!hasFlag(defaultTypewriter.flags, typewriter_active))
    {
        doMenu(curr_menu);
    }
    doDialogue(&defaultTypewriter);
}

/**
 * Gestisce gli eventi dello stato di vittoria
 **/
void doWin(void)
{
	if(Game.status!=WIN)
	{
		setGameState(WIN);
	}

	//Stop any music from the game
	if(isMusicPlaying())
	{
		pauseMusic();
	}

	if(isButtonPressed(BUTTON_START) || isButtonPressed(BUTTON_A))
    {
    	waitButtonRelease(BUTTON_START);
    	waitButtonRelease(BUTTON_A);

        setGameState(MENU);
        init();
    }
}

/**
 * Gestisce lo stato di game over.
 * A pressione del tasto INVIO reimposta i parametri di gioco allo stato
 * di default.
 **/
void doGameOver(void)
{
	if(Game.status!=GAMEOVER)
	{
		setGameState(GAMEOVER);
	}

	if(isButtonPressed(BUTTON_START) || isButtonPressed(BUTTON_A))
    {
    	waitButtonRelease(BUTTON_START);
    	waitButtonRelease(BUTTON_A);
    	reset();
        setGameState(MENU);
    }
}

void doLogic(void)
{
	//We reset alives entities position to their start value before
	//restart the game
	if(Game.status==PREGAME)
	{
		resetEntities();
	}
}

void setGameState(int state)
{
	Game.status = state;
}

/**
 * Pause or unpause the game and play sound
 **/
void pauseGame()
{
	if (isButtonPressed(BUTTON_START))
	{
        waitButtonRelease(BUTTON_START);
        
		playSound(pause_snd);

		if(!hasFlag(Game.flags, on_pause))
		{
            setFlag(&Game.flags, on_pause);
			pauseMusic();
			return;
		}
		
		if(hasFlag(Game.flags, on_pause))
		{
			unsetFlag(&Game.flags, on_pause);
			resumeMusic();
			return;
		} 
	}
}

void mainLoop()
{
	static Uint32 then = 0;
	static unsigned int dtime = 0;

	//main game loop
	while(!quit)
	{
		unsigned int maxl = 256;
		Uint32 now = SDL_GetTicks();
		dtime += now - then;
		fps.t = now;
		then = now;

		while (--maxl && dtime >= LOGICMS)
		{
			inputHandler();

			switch(Game.status)
			{
				case MENU:
					doTitleScreen();
					break;
				case PREGAME:
					doPreGame();
					break;
				case GAME:
					doGame();
					break;
				case LOST:
					doLogic();
					break;
				case WIN:
					doWin();
					break;
				case GAMEOVER:
					doGameOver();
					break;
			}
			
			now = SDL_GetTicks();
			dtime += now - then - LOGICMS;
			then = now;
		}

		render_time();
		
		draw();
		
		SDL_Delay(1);
	}
}
