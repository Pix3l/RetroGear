/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               timer.c - Game timing handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "timer.h"
#include "fps.h"

/**
 * int get_time_elapsed(int)
 * 
 * Return the time passed by a specific time, expressed in seconds
 * 
 * @param int time
 *      Time from which to start counting the elapsed time
 * 
 * @return int seconds
 *      Time elapsed from the specified time in seconds
 * 
 **/
int get_time_elapsed(int time)
{
    return (fps.t - time)/1000;
}

/**
 * int get_time_elapsed_ms(int)
 * 
 * Return the time passed by a specific time, expressed in milliseconds
 * 
 * @param int time
 *      Time from which to start counting the elapsed time
 * 
 * @return int seconds
 *      Time elapsed from the specified time in milliseconds
 * 
 **/
int get_time_elapsed_ms(int time)
{
    return (fps.t - time);
}
