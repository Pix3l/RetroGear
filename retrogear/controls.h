/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   controls.h - User input handling                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/
#ifndef _CONTROLS_H
#define _CONTROLS_H

#include <SDL/SDL.h>

#define DEAD_ZONE 3200  
//~ #define VGAMEPADS 1
#define VGAMEPAD_USE_EVENTS

//Virtual gamepad single button statuses
typedef enum _GamepadButtons_flags {
    button_released     = 0,
    button_pressed      = 1,
    button_held         = 2,
    button_wait_release = 3,    //Keystate
} GamepadButtons_flags;

//Virtual gamepad 
typedef enum _GAMEPAD_MAP_BUTTONS {
    BUTTON_A, BUTTON_B, BUTTON_C, BUTTON_X, BUTTON_Y,
    BUTTON_L, BUTTON_R, 
    BUTTON_START, BUTTON_SELECT,
    BUTTON_LEFT, BUTTON_RIGHT, BUTTON_DOWN, BUTTON_UP,
    BUTTON_LAST
} GAMEPAD_MAP_BUTTONS;

typedef struct _Gamepad {
	unsigned int state[BUTTON_LAST];
    unsigned int buttons[BUTTON_LAST];
} Gamepad;

//Virtual gamepad structure (Keyboard / Gamepad)
Gamepad gamepad; //[VGAMEPADS];

//Current virtual gamepad (Single player)
Gamepad *curr_gamepad;

void initController();
void inputHandler();
int konamiCode();
void cleanInput();

int isButtonPressed(GAMEPAD_MAP_BUTTONS button);
int isButtonHeld(GAMEPAD_MAP_BUTTONS button);
void waitButtonRelease(GAMEPAD_MAP_BUTTONS button); //Keystate

#endif
