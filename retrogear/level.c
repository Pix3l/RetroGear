/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    level.c - Level file handling                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <math.h>

#include "level.h"
#include "tile.h"
#include "entity.h"
#include "util.h"
#include "event.h"
#include "game.h"
#include "score.h"
#include "fps.h"
#include "timer.h"
#include "camera.h"
#include "gfx.h"
#include "player.h"
#include "transition.h"

/**
 * Initilize level structure and data
 **/
void initLevel()
{
    curr_level = &level;        //Pointer to default level structure
    curr_level->curr_layer=1;   //Current layer is SOLID

    level_index = 1;    //Level counting starts from 1
	
	//Default transparency colour: Acid green 0x00FF00
	Uint32 key = SDL_MapRGB(screen->format, 0, 255, 0 );

    if(tile_sheet==NULL)
    {
	    tile_sheet=loadImage("data/tile_sheet.bmp", key);
    }

    //TODO: load if neeeded
    if(alpha_sheet==NULL)
    {
	    alpha_sheet=loadImage("data/alpha_sheet.bmp", key);
    }
}

/**
 * Reset a Level structure
 * 
 * @param struct _Level* plevel
 *      Pointer to the level holding structure
 **/
void resetLevel(Level* plevel)
{
	strcpy(plevel->name, "");
	strcpy(plevel->song_title, "");
    plevel->bkgd_red = 0;
    plevel->bkgd_green = 0;
    plevel->bkgd_blue = 0;
    
	plevel->num_layers = 0;
	plevel->rows = 0;
	plevel->cols = 0;

	int layer=0, i=0, j=0;
	for(layer=0; layer < plevel->num_layers; layer++)
	{
		for(i=0; i < plevel->rows; i++)
		{
			for(j=0; j < plevel->cols; j++)
			{
				plevel->map[layer][i][j] = 0;
			}
		}
	}
}

/**
 * Load a level along with its parameters
 * 
 * @param struct _Level* plevel
 *      Pointer to the level holding structure
 * 
 * @param char *filename
 * 		Name of the file map without extension
 * 
 * @return
 *      -1 In case of fail
 * 
 **/
int loadLevel(Level* plevel, char *filename)
{
    #ifdef DEBUG_FILE_READ
        printf("size of level %lu\n", sizeof(Level));
    #endif

    resetLevel(plevel);

    int i,j;
    char str[80];

    /* Load data file: */
    sprintf(str, "maps/%s.map", filename);

    FILE *fi = fopen(str, "r");
    if (fi == NULL)
    {
	    #ifdef DEBUG_FILE_READ
		    perror(str);
	    #endif

	    return -1;
    }

    ///Load header info

    /* (Level title) */
    fgets(str, 20, fi);
    strcpy(plevel->name, str);
    plevel->name[strlen(plevel->name)-1] = '\0';

    /* (Level description) */
    fgets(str, 20, fi);
    strcpy(plevel->description, str);
    plevel->description[strlen(plevel->description)-1] = '\0';

    /* (Tileset theme) */
    fgets(str, 20, fi);
    strcpy(plevel->theme, str);
    plevel->theme[strlen(plevel->theme)-1] = '\0';

    /* (Song file for this level) */
    fgets(str, 10, fi);
    strcpy(plevel->song_title, str);
    plevel->song_title[strlen(plevel->song_title)-1] = '\0';

    //Background color
    //~ fscanf (fi, "%6x", &plevel->background_color);
    fscanf (fi, "%hu%*c%hu%*c%hu", &plevel->bkgd_red, &plevel->bkgd_green, &plevel->bkgd_blue);

    //~ printf("%d\n", plevel->bkgd_red);
    //~ printf("%d\n", plevel->bkgd_green);
    //~ printf("%d\n", plevel->bkgd_blue);

    //Layers
    fscanf (fi, "%hu", &plevel->num_layers);

    //Level cols and rows
    fscanf (fi, "%03hu%*c%03hu", &plevel->cols, &plevel->rows);

    int layer = 0;
    for(layer=0; layer < plevel->num_layers; layer++)
    {
	    for(i=0; i < plevel->rows; i++)
	    {
		    for(j=0; j < plevel->cols; j++)
		    {
		        fscanf(fi, "%03hu%*c", &plevel->map[layer][i][j]);

                //Entities are only in SOLID_LAYER, create entity only while loop this layer
                if(layer==SOLID_LAYER)
                {
                    createEntityFromMap(plevel->map[SOLID_LAYER][i][j], j, i);
                }

			    //~ printf ("%d,", plevel->map[layer][i][j]);
		    }

		    //~ printf("\n");
	    }
	
	    //~ printf("\n");
    }

    fclose(fi);

    loadEvents(filename, WARP_EVT);

    #ifdef DEBUG_MAP
        debugLevel(plevel);
    #endif
	
	return 0;
}

void saveLevel(void)
{
	int i,j;
	FILE * fp;

	fp = fopen("out.map", "wb");
	if (fp == NULL)
	{
	  return;
	}

	/* (Level title) */
	fprintf(fp, "%s\n", curr_level->name);
	
	/* (Song file for this level) */
	fprintf(fp, "%s\n", curr_level->song_title);

	/* (Level width) & (Level height) */
	fprintf(fp, "%02d,%02d", curr_level->cols, curr_level->rows);
	
	fprintf(fp, "\n");

	//Layer 1: Solid tiles / Entities
	for(i=0; i< curr_level->rows; i++)// && !feof (fi))
	{
		for(j=0; j< curr_level->cols; j++)
		 	fprintf(fp, "%d,", curr_level->map[0][i][j]);
		
		fprintf(fp, "\n");
	}
	
	fprintf(fp, "\n");
	
	//Layer 2: Tiles
	for(i=0; i< curr_level->rows; i++)// && !feof (fi))
	{
		for(j=0; j< curr_level->cols; j++)
			fprintf(fp, "%d,", curr_level->map[1][i][j]);
		
		fprintf(fp, "\n");
	}

	fclose(fp);
}

void debugLevel(Level* plevel)
{
	int i=0,j=0;

	printf("[Debug] Level\n");
	printf("Name: %s\n", plevel->name);
	printf("Description: %s\n", plevel->description);
	printf("Gfx set: %s\n", plevel->theme);
	printf("Background music: %s\n", plevel->song_title);
    printf("RGB background: %d,%d,%d\n", plevel->bkgd_red, plevel->bkgd_green, plevel->bkgd_blue);

	printf("layers: %d\n", plevel->num_layers);
	printf("rows: %d\n", plevel->rows);
	printf("cols: %d\n", plevel->cols);

	printf("Map:\n");
	int layer=0;
	for(layer=0; layer < plevel->num_layers; layer++)
	{
		printf("layer: %d\n", layer);
		for(i=0; i < plevel->rows; i++)
		{
			for(j=0; j < plevel->cols; j++)
			{
				printf ("%d,", plevel->map[layer][i][j]);
			}
			printf("\n");
		}
		
		printf("\n");
	}
}

/**
 * void drawTileMap(SDL_Rect)
 * 
 * Draw a level based on tiles, only the visible portion will be drawed.
 * 
 * @param SDL_Rect rect
 *      Rettangolo rappresentativo della porzione di matrice attualmente
 * 		letta.
 * 
 **/
void drawTileMap(Level *plevel)
{
    //~ int row = pixelsToTiles(camera.y);
	//~ int col = pixelsToTiles(camera.x);
    int row = pixelsToTiles(camera.y);
	int col = pixelsToTiles(camera.x);
	
	//Da 0 a TILESIZE!!!
	int difference_x = (int)camera.x % TILESIZE;
	int difference_y = (int)camera.y % TILESIZE;

	//TODO: la larghezza è l'altezza va controllata per evitare di uscire dai bordi
	int width = (SCREEN_WIDTH / TILESIZE)+1;
	int height = (SCREEN_HEIGHT / TILESIZE)+1;

	//out of bounds check
	#ifndef NO_SCROLL_BOUND
		if(col+width>plevel->cols)
		{
			width -= (col+width) - plevel->cols;
		}
		
		if(row+height>plevel->rows)
		{
			height -= (row+height) - plevel->rows;
		}
	#endif
	
	SDL_Rect dest;
	
	int x, y;
    for (y = 0; y < height; y ++)
    {
        for (x = 0; x < width; x ++)
        {
            dest.x = x * TILESIZE - difference_x;
            dest.y = y * TILESIZE - difference_y;
            dest.w = dest.h = 0;

            if(plevel->map[BACKG_LAYER][row + y][col + x]!=0)	//No need to draw empty tile
            {
                getSprite(tile_sheet, plevel->map[BACKG_LAYER][row + y][col + x], TILESIZE, TILESIZE, dest);
            }

            if(plevel->map[ALPHA_LAYER][row + y][col + x]!=0)	//No need to draw empty tile
            {
                getSprite(alpha_sheet, plevel->map[ALPHA_LAYER][row + y][col + x], TILESIZE, TILESIZE, dest);
            }
        }
    }
}

/**
 * Do a level warp if an event is found
 *
 * @param char *mapname
 *      Current map name
 *
 * @param unsigned int col
 *      Column position of the event
 *
 * @param unsigned int row
 *      Row position of the event
 **/
void doWarp(char *mapname, unsigned int col, unsigned int row)
{
    //Do the warp!
    Event *warp = getEvent(eventsWarp, curr_level->name, col, row);
    
    if(warp)
    {
        #ifdef _SFX_H
            //~ playSound(stairs_snd);
        #endif

        unsigned int dest_x, dest_y;
        char dest_map[10];
        sscanf (warp->parameters, "%[^','],%d,%d", dest_map, &dest_x, &dest_y);

        #ifdef DEBUG_EVENT
            printf("Warp event: destination %s at %d,%d\n", dest_map, dest_x, dest_y);
        #endif

        #ifdef _TRANSITION_H
            initTransition(TILESIZE, transition_fadein);
            transition.flag_active=1;
        #endif

        cleanEntities();
        loadLevel(curr_level, dest_map);
        initCamera(&camera);

        dest_x = tilesToPixels(dest_x);
        dest_y = tilesToPixels(dest_y);
        setPlayerPosition(dest_x, dest_y);
    }
}
