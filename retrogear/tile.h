/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   tile.h - Tiles function library                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _TILE_H
#define _TILE_H

#define TILESIZE 16
#define SOLID 1		//1 is reserved for solid blocks!
#define WARP 3		//3 is reserved for warps

#include <SDL/SDL.h>
#include "level.h"
#include "entity.h"
#include "rgtypes.h"

int isInTile(int x, int y);
int pixelsToTiles(int pixels);
int tilesToPixels(int numTiles);
int getTile(int x, int y, int layer);

#endif
