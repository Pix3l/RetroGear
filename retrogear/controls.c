/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   controls.c - User input handling                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <SDL/SDL.h>

#include "controls.h"
#include "game.h"
#include "util.h"
#include "mouse.h"
#include "bit.h"

SDL_Event input_event;
Uint8 *keystate; // keyboard state
SDL_Joystick *joystick_ptr; // Joystick device pointer

void setButtonPressed(GAMEPAD_MAP_BUTTONS button);
void setButtonReleased(GAMEPAD_MAP_BUTTONS button);
void handle_keyboard(SDL_Event *input_event);
void handle_joystick(SDL_Event *input_event);
void handle_mouse(SDL_Event *input_event);

//~ #define DEBUG_VGAMEPAD
//~ #define DEBUG_JOYSTICK
//~ #define DEBUG_VMOUSE

void initController(void)
{
	//Initialize current gamepad pointer
	curr_gamepad = &gamepad;
    
    //Init virtual gamepad with defaults
    int button = 0;
    for(button = 0; button < BUTTON_LAST; button++)
    {
        curr_gamepad->state[button] = button_released;
    }

    //Default gamepad mapping
    curr_gamepad->buttons[BUTTON_A] = SDLK_x;
    curr_gamepad->buttons[BUTTON_B] = SDLK_z;
    curr_gamepad->buttons[BUTTON_START] = SDLK_RETURN;
    curr_gamepad->buttons[BUTTON_SELECT] = SDLK_LSHIFT;
    curr_gamepad->buttons[BUTTON_LEFT] = SDLK_LEFT;
    curr_gamepad->buttons[BUTTON_RIGHT] = SDLK_RIGHT;
    curr_gamepad->buttons[BUTTON_DOWN] = SDLK_DOWN;
    curr_gamepad->buttons[BUTTON_UP] = SDLK_UP;

    //Initialize and setup the joysticks
    joystick_ptr = SDL_JoystickOpen(0);

    //Debug informations
    #ifdef DEBUG_JOYSTICK

        int joystick_count = SDL_NumJoysticks();

        printf("There are %d joysticks connected\n", joystick_count);

        int i=0;
        for(i=1; i <= joystick_count ; i++)
        {
            printf("Joystick %d have %d buttons and %d axes\n", i, SDL_JoystickNumButtons(joystick_ptr), SDL_JoystickNumAxes(joystick_ptr) );  
        }

    #endif

    #define VGAMEPAD_INITIALIZED
}

void inputHandler()
{
    #ifndef VGAMEPAD_INITIALIZED
        printf("Controls system not initialized\n");
        quit=1;
    #endif
    
    #ifndef VGAMEPAD_USE_EVENTS
        keystate = SDL_GetKeyState(NULL);
    #endif
    
	while (SDL_PollEvent(&input_event))
	{
        //Let's quit!
        if (input_event.key.keysym.sym == SDLK_ESCAPE || input_event.type == SDL_QUIT)
        {
            quit = 1;
        }

        /***********************************************************
         * Virtual gamepad handling
         **********************************************************/

        if(input_event.type == SDL_KEYDOWN  || input_event.type == SDL_KEYUP)
        {
            #ifdef VGAMEPAD_USE_EVENTS
                /**
                 * Handling keyboard
                 **/
                handle_keyboard(&input_event);
            #endif
        }
        else if( input_event.type == SDL_JOYAXISMOTION || input_event.type == SDL_JOYBUTTONUP  || input_event.type == SDL_JOYBUTTONDOWN )
        {
            /**
             * Handling joystick
             **/
            handle_joystick(&input_event);
        }
        else if( input_event.type == SDL_MOUSEMOTION || input_event.type == SDL_MOUSEBUTTONUP  || input_event.type == SDL_MOUSEBUTTONDOWN )
        {
            /**
             * Virtual mouse handling
             **/
            handle_mouse(&input_event);
        }
    }
}

/**
 * Check if a button is pressed trough a snapshot of the current keyboard state
 *
 * @param GAMEPAD_MAP_BUTTONS button
 *      The virtual gamepad button index
 *
 * @return int
 *  The state, 0 or 1
 **/
int isButtonPressedKeystate(GAMEPAD_MAP_BUTTONS button)
{
    if(curr_gamepad->state[button] != button_wait_release)
    {
        if(keystate[ curr_gamepad->buttons[button] ])
        {
            if(curr_gamepad->state[button] == button_pressed)
            {
                curr_gamepad->state[button] = button_held;
                return button_held;
            }

            curr_gamepad->state[button] = button_pressed;
            return button_pressed;
        }
    }

    curr_gamepad->state[button] = button_released;
    return button_released;
}

int isButtonPressedEvent(GAMEPAD_MAP_BUTTONS button)
{
    int state = curr_gamepad->state[button];
    if( state == button_pressed )
    {
        #ifdef DEBUG_VGAMEPAD
            printf("Button %s is helded\n", SDL_GetKeyName(curr_gamepad->buttons[button]));
        #endif

        state = button_held;
        curr_gamepad->state[button] = state;
    }
    
    return state;
}

int isButtonPressed(GAMEPAD_MAP_BUTTONS button)
{
    #ifdef VGAMEPAD_USE_EVENTS
        return isButtonPressedEvent(button);
    #else
        return isButtonPressedKeystate(button);
    #endif
}

int isButtonHeld(GAMEPAD_MAP_BUTTONS button)
{
    if( curr_gamepad->state[button] == button_held )
    {
        #ifdef DEBUG_VGAMEPAD
            printf("Button %s is helded\n", SDL_GetKeyName(curr_gamepad->buttons[button]));
        #endif
        return 1;
    }

    return 0;
}

/**
 * Private function to set virtual gamepad button state to "pressed"
 *
 * @param GAMEPAD_MAP_BUTTONS button
 *      The virtual gamepad button index
 **/
void setButtonPressed(GAMEPAD_MAP_BUTTONS button)
{
    if( curr_gamepad->state[button] == button_pressed )
    {
        #ifdef DEBUG_VGAMEPAD
            printf("Button %s is helded\n", SDL_GetKeyName(curr_gamepad->buttons[button]));
        #endif
        
        curr_gamepad->state[button] = button_held;
        return;
    }

    #ifdef DEBUG_VGAMEPAD
        printf("Button %s is pressed\n", SDL_GetKeyName(curr_gamepad->buttons[button]));
    #endif
    
    curr_gamepad->state[button] = button_pressed;
}

/**
 * Private function to set virtual gamepad button state to "released"
 *
 * @param GAMEPAD_MAP_BUTTONS button
 *      The virtual gamepad button index
 **/
void setButtonReleased(GAMEPAD_MAP_BUTTONS button)
{
    #ifdef DEBUG_VGAMEPAD
        printf("Button %s is released\n", SDL_GetKeyName(curr_gamepad->buttons[button]));
    #endif

    curr_gamepad->state[button] = button_released;
}

void waitButtonRelease(GAMEPAD_MAP_BUTTONS button)
{
    #ifdef VGAMEPAD_USE_EVENTS
        setButtonReleased(button);
    #else
        curr_gamepad->state[button] = button_wait_release;
    #endif
}

/**
 * Handles keyboard events enhancing the virtual gamepad
 *
 * @param SDL_Event *input_event
 *      Pointer to the SDL_Event structure where events are stored
 **/
void handle_keyboard(SDL_Event *input_event)
{
    SDLKey key_button = input_event->key.keysym.sym;    //Keyboard key symbol

    int button = 0;
    for(button = 0; button < BUTTON_LAST; button++)
    {
        if( key_button == curr_gamepad->buttons[button] )
        {
            if(input_event->type == SDL_KEYDOWN )
            {
                //~ #ifdef DEBUG_VGAMEPAD
                    //~ printf("Button %s is pressed\n", SDL_GetKeyName(curr_gamepad->buttons[button]));
                //~ #endif

                setButtonPressed(button);
                return;
            }
            else// if(input_event->key.state == SDL_RELEASED )
            {
                //~ #ifdef DEBUG_VGAMEPAD
                    //~ printf("Button %s is released\n", SDL_GetKeyName(curr_gamepad->buttons[button]));
                //~ #endif

                setButtonReleased(button);
                return;
            }
        }
    }
}

/**
 * Handles joystick/gamepad events enhancing the virtual gamepad
 *
 * @param SDL_Event *input_event
 *      Pointer to the SDL_Event structure where events are stored
 **/
void handle_joystick(SDL_Event *input_event)
{
    Uint8 joy_button = input_event->jbutton.button;   //Joystick key symbol
    
    if( joy_button == 1 )
    {
        if( input_event->type == SDL_JOYBUTTONDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick button A pressed\n");
            #endif

            setButtonPressed(BUTTON_A);
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick button A released\n");
            #endif

            setButtonReleased(BUTTON_A);
        }
    }

    if( joy_button == 2 )
    {
        if( input_event->type == SDL_JOYBUTTONDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick button B pressed\n");
            #endif

            setButtonPressed(BUTTON_B);
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick button B released\n");
            #endif

            setButtonReleased(BUTTON_B);
        }
    }

    if( joy_button == 9)
    {
        if( input_event->type == SDL_JOYBUTTONDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Start pressed\n");
            #endif

            setButtonPressed(BUTTON_START);
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Start released\n");
            #endif

            setButtonReleased(BUTTON_START);
        }
    }

    if( joy_button == 8)
    {
        if( input_event->type == SDL_JOYBUTTONDOWN )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Select pressed\n");
            #endif

            setButtonPressed(BUTTON_SELECT);
        }
        else
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Select released\n");
            #endif

            setButtonReleased(BUTTON_SELECT);
        }
    }

    if(input_event->jaxis.axis == 0)
    {
        if(input_event->jaxis.value < -DEAD_ZONE)
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick direction Left pressed\n");
            #endif

            setButtonPressed(BUTTON_LEFT);
        }
        else if( input_event->jaxis.value > DEAD_ZONE )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Button Right pressed\n");
            #endif

            setButtonPressed(BUTTON_RIGHT);
        }
        else
        {
            setButtonReleased(BUTTON_LEFT);
            setButtonReleased(BUTTON_RIGHT);

            #ifdef DEBUG_VGAMEPAD
                printf("Joystick horizontal directions released\n");
            #endif
        }
    }

    /* Vertical movement */

    if( input_event->jaxis.axis == 1 )
    {
        if( input_event->jaxis.value < -DEAD_ZONE) 
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick direction Up pressed\n");
            #endif

            setButtonPressed(BUTTON_UP);
        }
        else if( input_event->jaxis.value > DEAD_ZONE )
        {
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick direction Down pressed\n");
            #endif

            setButtonPressed(BUTTON_DOWN);
        }
        else
        {
            setButtonReleased(BUTTON_UP);
            setButtonReleased(BUTTON_DOWN);
            
            #ifdef DEBUG_VGAMEPAD
                printf("Joystick vertical directions released\n");
            #endif
        }
    }
}

/**
 * Handles mouse events enhancing the virtual mouse
 *
 * @param SDL_Event *input_event
 *      Pointer to the SDL_Event structure where events are stored
 **/
void handle_mouse(SDL_Event *input_event)
{
    if(input_event->type == SDL_MOUSEMOTION)
    {
        // Update mouse position
        Mouse.x = input_event->motion.x;
        Mouse.y = input_event->motion.y;

        #ifdef DEBUG_VMOUSE
            printf("Mouse coordinates %d,%d\n", Mouse.x, Mouse.y);
        #endif
    }

    if(input_event->type == SDL_MOUSEBUTTONDOWN)
    {
        if (input_event->button.button == 1)
        {
            Mouse.leftButton = 1;

            #ifdef DEBUG_VMOUSE
                printf("Mouse left button pressed\n");
            #endif
        }

        if (input_event->button.button == 3)
        {
            Mouse.rightButton = 1;

            #ifdef DEBUG_VMOUSE
                printf("Mouse right button pressed\n");
            #endif
        }
    }
    
    if(input_event->type == SDL_MOUSEBUTTONUP)
    {
        // update button down state if left-clicking
        if (input_event->button.button == 1)
        {
            Mouse.leftButton = 0;

            #ifdef DEBUG_VGAMEPAD
                printf("Mouse left button pressed\n");
            #endif
        }

        if (input_event->button.button == 3)
        {
            Mouse.rightButton = 0;

            #ifdef DEBUG_VGAMEPAD
                printf("Mouse right button pressed\n");
            #endif
        }
    }
}

int konamiCode()
{
    //Flag che impedisce l'utilizzo del Konami Code
    if(!hasFlag(Game.flags, konami_code))
    {
        return -1;
    }
    
    //Konami code!
    int konami[] = {BUTTON_UP, BUTTON_UP, 
                    BUTTON_DOWN, BUTTON_DOWN,
                    BUTTON_LEFT, BUTTON_RIGHT,
                    BUTTON_LEFT, BUTTON_RIGHT,
                    BUTTON_B, BUTTON_A };

    int size = sizeof(konami)/sizeof(int);
    
    static int index = 0;
    if(konami[index]==1)
    {
        index++;
    }
    
    //Se il giocatore ha inserito la giusta sequenza
    if(index==size)
    {
        printf("Konami code!!!\n");
        index = 0;
        return 1;
    }
    
    return 0;
}

void cleanInput()
{
    SDL_JoystickClose(joystick_ptr);
}
