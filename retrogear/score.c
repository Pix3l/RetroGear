/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   score.c - Score handling functions               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <math.h>

#include "score.h"
#include "tile.h"
#include "entity.h"
#include "game.h"
#include "player.h"
#include "gfx.h"
#include "sprite.h"

static int points[] = {100, 200, 400, 500, 800, 1000, 2000, 4000, 5000, 8000};
int points_index;

/**
 * Initialize internal score system
 **/
void initScore()
{
	Game.score = 0;
	points_index = pts100;
	score_sheet=loadImage("data/score_sheet.bmp", 0x00FF00);
}

void createScore(int x, int y, float speed, int sprite_index)
{
	//calloc allocate and initialize the memory
	scoreType *pnew = (scoreType*) calloc(1, sizeof(scoreType));

	if(scoreHead == NULL)
	{
	    scoreHead = pnew;
	    scoreTail = scoreHead;
	}
	else
	{
	    scoreTail->next = pnew;
	    scoreTail = pnew;
	}
	
    scoreTail->next = NULL;

	scoreTail->sprite.x = floorf(x);
	scoreTail->sprite.y = floorf(y);

	scoreTail->xstart = x;
	scoreTail->ystart = y;

    //NOTE: For now, width and height are fixed at TILESIZE
    setSprite(&scoreTail->sprite, scoreTail->sprite.x, scoreTail->sprite.y, TILESIZE, TILESIZE, speed, score_sheet);

    scoreTail->sprite.index = sprite_index;  //Set the sprite to the correct point index
}

void doScore()
{
	scoreType *current = scoreHead, *previous=NULL;
	while(current!=NULL)
	{
		if(current->status==DESTROY)
		{			
			if(current==scoreHead && current==scoreTail)
			{
				free(current);
				scoreHead=scoreTail=NULL;
				return;
			}

			if(scoreHead==current)
			{
				scoreHead = current->next;
				free(current);
				current = scoreHead;
			}
			else if(scoreTail==current)
			{
				previous->next=NULL;
				scoreTail=previous;
				free(current);
				current = scoreTail;
			}
			else
			{
				previous->next = current->next;
				free(current);
				current = previous->next;
			}
		}
		else
		{
            //If the score is far two times its height we destroy it
			if( (current->ystart - current->sprite.y) >= TILESIZE*2)
			{
				current->status=DESTROY;
			}
			else
			{
				current->sprite.y-=current->sprite.animation_speed;
			}
		}
		
		previous = current;
		current=current->next;
	}

}

/**
 * Preleva e ritorna un punteggio dall'array dei punti
 * 
 * @param int index
 * 		L'indice a cui cercare nell'array
 * 
 * @return int
 * 		Il punteggio, 0 in caso di indice non valido
 **/
int getScore()
{
    if(points_index>=pts1UP)
    {
        if(points_index>=pts5UP)
        {
            points_index = pts5UP;
        }

	    // 1up...
	    playerExtraLife(points_index);
    }
    else if(points_index<=pts8000)
    {
        //If is a normal score, return the points
	    return points[points_index++];
    }
	
    return 0;
}

/**
 * Add an amount of points to the global game score
 * 
 * @param int score
 * 		The amount of points to add
 * @todo int *pscore
 * 		Pointer to a game score holder (TODO: One score holder for each player)
 **/
void addScore(int score)//, int *pscore)
{
	Game.score +=score;
}

/**
 * Disegna tutti gli oggetti scoreType esistenti sullo schermo
 **/
void drawScore()
{
	//~ drawString(screen, pobj->x+camera.offsetX, pobj->y+camera.offsetY-pobj->h, (char*)points[index], white, 0);
	
	scoreType *current = scoreHead;
	while(current!=NULL)
	{
        drawSprite(&current->sprite, current->sprite.x, current->sprite.y);
		current = current->next;
	}
}

/**
 * Ripulisce la lista degli oggetti scoreType nel caso ve ne siano
 * (Utile per eventuali chiusure improvvise della finestra da parte
 * dell'utente)
 **/
void clearScoreList()
{
	#ifdef DESTROY_DBG
		printf("Clear scoreType ");
	#endif

	scoreType *current = scoreHead, *next=NULL;
	while(current!=NULL)
	{
		next=current->next;
        //~ destroySprite(current->sprite); //Free internal sprite structure
		free(current);
		current=next;
		
		#ifdef DESTROY_DBG
			printf(".");
		#endif
	}
	
	#ifdef DESTROY_DBG
		printf("Done!\n");
	#endif
}
