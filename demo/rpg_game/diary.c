/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 	 diary.c - RPG quest save object                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "diary.h"
#include "font.h"
#include "gfx.h"
#include "dialogue.h"
#include "controls.h"
#include "typewriter.h"
#include "bit.h"
#include "quest.h"
#include "menu_classic.h"

void closeDiary()
{
    closeDialogue(&defaultTypewriter);
    deactiveMenues(&quest_menu);
    deactiveMenues(&question_menu);
    quest_menu.curr_item = 0;
    curr_menu = &quest_menu;
}

void initDiary()
{
    //This menu has parent the main menu, as return point for dialogue end
    initClassicMenu(&question_menu, question_items, 208, 136, "Save", 2, white, &quest_menu, NULL);
    initStaticMenuItems(question_items, 2);
    addStaticMenuItem(&question_menu, "Yes", 1, NULL, white, &saveGame);
    addStaticMenuItem(&question_menu, "No" , 0, NULL, white, &closeDiary);
    unsetFlag(&question_menu.flags, menu_active);
    setFlag(&question_menu.flags, menu_persistent|menu_show_border);
}

int noise()
{
    setFlag(&question_menu.flags, menu_active);
    doMenu(&question_menu);

    if(isButtonPressed(BUTTON_A))
    {
        waitButtonRelease(BUTTON_A);

        if(question_menu.items[question_menu.curr_item].func || question_menu.items[question_menu.curr_item].value!=0)
        {
            question_menu.items[question_menu.curr_item].func();
        }
        //~ else
        //~ {
            //~ resetTypewriter(&typewriter);
            //~ unsetFlag(&question_menu.flags, menu_active);
        //~ }
    }

    return 0;
}

char save[] = "Game cannot be save because\nthis is only a demo!";
void saveGame()
{
    initDialogue(&defaultTypewriter, save, NULL);
    deactiveMenues(quest_menu.next);
    deactiveMenues(&question_menu);
    quest_menu.curr_item = 0;
    curr_menu = &quest_menu;
}

char diarytext[] = "You can save your progress\nwith this.\fDo you want to save your game\nnow?";
void useDiary()
{
    setFlag(&defaultDialogue.flags, dialogue_waitConfirm);
    initDialogue(&defaultTypewriter, diarytext, &noise);
    setFlag(&defaultTypewriter.flags, typewriter_waitCallback);
}
