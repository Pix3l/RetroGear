/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                villager.c - npc based entity functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "villager.h"
#include "npc.h"
#include "gfx.h"

#define TYPE 3

/**
 * villager entity constructor
 * Help programmers on setup the initial values for a villager entity
 **/
void villager_create(int id, int x, int y)
{
	int lives = 3;
	float animation_speed = 0.09f;
	float speed = 1.0f;

	createEntity(TYPE, x, y, NPC_W, NPC_H, lives, speed, 0, &updateNpc);

	if( !EntityList_Tail->sprite.surface )
	{
		initSprite(&EntityList_Tail->sprite, x, y, 16, 16, animation_speed, "data/npc.bmp");
	}

    EntityList_Tail->direction_y = 1;
    EntityList_Tail->direction_x = 0;
}
