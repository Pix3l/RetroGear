/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   game.h - Game handling function                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _GAME_H
#define _GAME_H

typedef enum _Game_flags {
    can_continue = 1 << 0,  // 000001  1
    konami_code  = 1 << 1,  // 000010  2
    on_pause     = 1 << 2,  // 000100  4
    on_dialogue  = 1 << 3,  // 001000  8
} Game_flags;

struct _Game
{
    int hiscore;
    int status; //Stato di gioco
    int score;
    unsigned char flags;

    //~ void (*doStatus)(void);		//Puntatore al gestore di stato
    //~ void (*drawStatus)(void);	//Puntatore al disegnatore di stato
} Game;

//Possibili stati di gioco
enum States
{
	GAMEOVER=0, MENU=1, PREGAME=2, GAME=3, LOST=4, WIN=5, EDITOR=6
};

void doTitleScreen(void);
void doPreGame(void);
void doGame(void);
void doWin(void);
void doGameOver(void);
void doLogic(void);
void setGameState(int status);
void mainLoop(void);
void pauseGame();

#endif
