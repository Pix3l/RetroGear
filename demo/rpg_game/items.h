/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  items.c - RPG items handling functions            *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _ITEMS_H
#define _ITEMS_H

#include "menu.h"

#define MAX_ITEMS 20

int item_index;

Menu battle_menu;
Menu fight_menu;
Menu items_menu;
Menu magic_menu;

//Player items
Item player_items[MAX_ITEMS];

void initItems();
void showItems();
void closeItemMenu();
int addItem(char *item_name, SDL_Color color, void (*call)());
void deleteItem(int pos);
void drawItemsList();

void usePotion();

#endif
