/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               chest.c - RPG generic chest object wrapper           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "chest.h"
#include "tile.h"
#include "event.h"

/**
 * Actions to be performed in case of open the chest
 * 
 * @param Entity *pobj
 * 		Pointer to an entity object
 **/
void onChestOpen(Entity *pobj)
{
	pobj->status=KILL;
	
	//Save the object state in the event list and to save state file
	char param[20];
	sprintf(param, "%d", pobj->status);
	
	int col = pixelsToTiles(pobj->x);
	int row = pixelsToTiles(pobj->y);
	
	addEvent(&eventsGeneric, curr_level->name, row, col, param, 1);
}

/**
 * Show the current state of the chest (Open/Closed)
 * 
 * @param Entity *pobj
 * 		Pointer to an entity object
 **/
void animateChest(Entity *pobj)
{
	if(pobj->status==KILL)
	{
		/**
		 * Since drawEntity() expect a frame index, we should use it
		 * instead of sprite_index
		 **/
		pobj->sprite.animation_timer=15;	//Open chest tile index
		return;
	}
	
	pobj->sprite.animation_timer=14;	//Closed chest tile index
}
