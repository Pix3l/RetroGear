/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               chest.c - RPG generic chest object wrapper           *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _CHEST_H
#define _CHEST_H

#include <SDL/SDL.h>
#include "entity.h"

#define CLOSED_CHEST_TILE 23
#define OPEN_CHEST_TILE 24

SDL_Surface *chest_spr;

void onChestOpen(Entity *pobj);
void animateChest(Entity *pobj);

#endif
