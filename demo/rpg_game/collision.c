/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               collision.c - Collision related functions            *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "tile.h"
#include "entity.h"
#include "rgtypes.h"

/**
 * Check intersection between two rectangular areas
 * Return 0 if there's a collision, 1 otherwise.
 * 
 * @param int x1, y1, w1, h1
 * 		Coordinates and size of the first area
 * 
 * @param int x2, y2, w2, h2
 * 		TCoordinates and size of the second area
 * 
 * @return 0
 * 		No collision
 * 		   1
 * 		Collision detected
 **/
int rectCollision(int x1, int y1, int w1, int h1, 
                  int x2, int y2, int w2, int h2)
{
    if (y1+h1 <= y2) return 0;
    if (y1 >= y2+h2) return 0;
    if (x1+w1 <= x2) return 0;
    if (x1 >= x2+w2) return 0;

    return 1;
}

/**
 * Check for collision between an entity and a tile of the solid layer
 * of the map.
 * Return an RG_Point reference contains the pixel coordinates of the collision,
 * otherwise NULL.
 *
 **/
RG_Point tileCollision(Entity *pobj, int new_x, int new_y)
{
    int col=0, row=0,
        minx=0, miny=0,
        maxx=0, maxy=0;

    //Resetting the RG_Point structure for good
    RG_Point point;
    point.x = -1;
    point.y = -1;

    //~ //Collision if we try to go out the map boundaries
    //~ if (new_x < 0 || (new_x + pobj->w) > TILESIZE * curr_level->cols ||
	//~ new_y < 0 || (new_y + pobj->h) > TILESIZE * curr_level->rows)
    //~ {
        //~ point.x = point.x + TILESIZE;   //Place the object nearest the top tile
        //~ point.y = point.y + TILESIZE;   //Place the object nearest the top tile
    //~ }
    
    // From pixels to tiles
    minx = pixelsToTiles( (pobj->x < new_x)? pobj->x : new_x );
    miny = pixelsToTiles( (pobj->y < new_y)? pobj->y : new_y );

    //Left : Right
    maxx = pixelsToTiles( (pobj->x > new_x)? (pobj->x + pobj->w -1) : (new_x + pobj->w-1) );
    //Up : Down
    maxy = pixelsToTiles( (pobj->y > new_y)? (pobj->y + pobj->h - 1) : (new_y + pobj->h - 1) );

    // We return the coordinates of the tile with which the entity collides
    for (col = minx; col <= maxx ; col++)
    {
        for (row = miny ; row <= maxy ; row++)
        {
            if (curr_level->map[SOLID_LAYER][row][col]==SOLID)
            {
                point.x = tilesToPixels(col);
                point.y = tilesToPixels(row);
                break;
            }
        }
    }

    return point;
}
