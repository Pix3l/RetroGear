/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 	   potion.c - RPG generic potion                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <stdlib.h>

#include "potion.h"
#include "level.h"
#include "player.h"
#include "gfx.h"
#include "tile.h"
#include "util.h"
//~ #include "chest.h"
#include "dialogue.h"
#include "event.h"
#include "quest.h"
#include "dialogue.h"
#include "controls.h"

static char *object_name = "Potion";

/**
 * potion entity constructor
 * Help programmers on setup the initial values for a potion entity
 **/
void potion_create(int id, int x, int y)
{
	int lives = 1;
	float animation_speed = 0;
	float speed = 0;

	createEntity(COLLECTABLE, x, y, TILESIZE, TILESIZE, lives, speed, 0, &updatePotion);

    //TODO: disegnare tile
	//~ if( !EntityList_Tail->sprite.surface )
	//~ {
		//~ initSprite(&EntityList_Tail->sprite, x, y, 16, 16, animation_speed, "data/npc.bmp");
	//~ }
    
    int col = pixelsToTiles(x),
        row = pixelsToTiles(y);
		
    //Set the coordinates as solid
    curr_level->map[SOLID_LAYER][row][col] = 1;
	
	//~ if(curr_level->flag_persistent)
	//~ {
		//~ Event *evt = getEvent(x, y);
		//~ setStatus(evt->status);
	//~ }

    Event *event = getEvent(eventsGeneric, curr_level->name, row, col);
	if(event)
	{
		EntityList_Tail->status = atoi(event->parameters);
	}

    curr_level->map[BACKG_LAYER][row][col] = 14;
}

/**
 * Update entity lifecycle
 * 
 * @param Entity *pobj
 * 		Pointer to the entity object
 **/
char potionText[] = "You have found a potion!";
void updatePotion(Entity *pobj)
{
    //Since the background layer are set after the solid layer, we need to update it's tile after level loading
    int col = pixelsToTiles(pobj->x),
        row = pixelsToTiles(pobj->y);
    
    if(pobj->status!=KILL)
    {
        curr_level->map[BACKG_LAYER][row][col] = 14;
    }
    else
    {
        curr_level->map[BACKG_LAYER][row][col] = 15;
    }
    
	if(rectCollision(curr_player->x+curr_player->direction_x, curr_player->y+curr_player->direction_y,
                     curr_player->w+curr_player->direction_x, curr_player->h+curr_player->direction_y, 
					 pobj->x, pobj->y, pobj->w, pobj->h))
    {
		//~ if(player_action==LOOK)
		//~ {
			//~ if(pobj->status!=KILL)
			//~ {
				//~ getPotion();
				//~ initDialog("You found a potion!");
				//~ player_action=-1;
				//~ onChestOpen(pobj);
			//~ }
		//~ }

        if(isButtonPressed(BUTTON_A))
        {
            if(pobj->status!=KILL)
            {
                pobj->status=KILL;
                addEvent(&eventsGeneric, curr_level->name, col, row, "open", 1);

                getPotion();
                initDialogue(&defaultTypewriter, potionText, NULL);
				//~ player_action=-1;
				//~ onChestOpen(pobj);
            }
        }
	}
	
	animateChest(pobj);
}

/**
 * Add a potion object to Player's items list
 **/
void getPotion()
{
    addStaticMenuItem(&quest_menu_items, "potion", 0, NULL, white, &usePotion);
    setMenuPaginator(&quest_menu_items, 5);
    unsetFlag(&quest_menu_items.flags, menu_active);
}

/**
 * Use the generic item Potion
 **/
void usePotion()
{
	player_energy+=10;
	
	//Adjust Player current energy
	if(player_energy>100)
	{
		player_energy=100;
	}
	
    removeStaticMenuItem(&quest_menu_items, quest_menu_items.curr_item);

    setMenuPaginator(&quest_menu_items, 5);
    deactiveMenues(&quest_menu);

    snprintf(dlg_holder, MAX_PARAM_LENGTH, "You used the potion.");
    initDialogue(&defaultTypewriter, dlg_holder, NULL);
}
