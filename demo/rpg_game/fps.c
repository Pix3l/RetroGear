/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    fps.c - FPS handling functions                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "fps.h"

void render_time (void)
{
	static unsigned int fc = 0;
	static Uint32 then = 0;
	Uint32 now = SDL_GetTicks();
	++fc;
	if (now - then >= 1000)
	{
		#ifdef DEBUG_FPS
			fprintf(stderr, "fps: %f \r", 1000.0*(double)fc/(double)(now-then));
		#endif

		then = now;
		fc = 0;
	}
}

int linear_interpolation (int a, int b, int int_factor)
{
	return a + (b - a) * int_factor;
}
