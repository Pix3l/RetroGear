/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  ncp.c - generic npc entity functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _NPC_H
#define _NPC_H

#define NPC_W 16
#define NPC_H 16

#include <SDL/SDL.h>
#include "entity.h"

void npc_create(int id, int x, int y, char *sprite);
void updateNpc(Entity *pobj);

#endif
