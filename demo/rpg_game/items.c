/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  items.c - RPG items handling functions            *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "items.h"
#include "player.h"
#include "player_menu.h"
#include "font.h"
#include "diary.h"
#include "menu.h"
#include "controls.h"
#include "gfx.h"

void initItems()
{

	int i;
	for(i=0; i<MAX_ITEMS; i++)
	{
		player_items[i].id = -1;
		player_items[i].func = NULL;
	}

	initMenu(&items_menu, FONT_W, 48, "Items", white, &player_menu, NULL);
	items_menu.items = player_items;
	items_menu.num_items=1;
	items_menu.w = 48;
	items_menu.h = 48;
	items_menu.flag_active=0;

	//Add some default item
	addDiary();
	addItem("Sword", green, NULL);
	
	item_index=0;
	
	//~ debugItems(&items_menu);
	//~ printf("%s\n", items_menu.items[0].name);
}

/**
 * Show the items menu by setting the next reference
 **/
void showItems()
{
	//~ battle_menu.child = &items_menu;
	items_menu.flag_active = 1;
	curr_menu = &items_menu;
	player_action = ITEM;
}

void closeItemMenu()
{
	curr_menu = items_menu.parent;
	items_menu.flag_active = 0;
	items_menu.curr_item=0;
	player_action = NONE;
}

/**
 * Add an item to the menu
 * 
 * @param char *item_name
 * 		The name of the item
 * @param SDL_Color color
 * 		The color of the voice
 * @param void *call
 * 		Call to the function to execute
 * 
 * @return 1
 * 		Item placed correctly
 * 		   -1
 * 		No rooms avaible
 **/
int addItem(char *item_name, SDL_Color color, void (*call)())
{
	//~ if(items_menu.num_items<MAX_ITEMS)
	//~ {
		//~ addMenuItem(&items_menu, createItem(1,item_name, color, call));
		//~ return 1;

		int i;
		//Start from one because 0 is the diary
		for(i=1; i<MAX_ITEMS; i++)
		{
			if(player_items[i].id==-1)
			{
				player_items[i].id = 1;
				//player_items[i].name = item_name;
				strcpy(player_items[i].name, item_name);
				player_items[i].color = color;
				player_items[i].func = call;
				player_items[i].x = items_menu.x+FONT_W;
				player_items[i].y = items_menu.y+(i*FONT_H);
				
				items_menu.num_items++;

				return 1;
			}
		}
	
	//~ }
	
	return -1;
}

void deleteItem(int pos)
{
	int i;
	//Start from one because 0 is the diary
	for(i=1; i<MAX_ITEMS; i++)
	{
		if(i==pos)
		{
			items_menu.items[i].id = -1;
			//items_menu.items[i].name = NULL;
			items_menu.items[i].func = NULL;
			items_menu.num_items--;
		}
	}

	//~ deleteLastItem(&items_menu);
}

/**
 * Draw items menu on the screen if flag_active is 1
 **/
void drawItemsList()
{
	if(items_menu.flag_active)
	{
		drawMenu(&items_menu);
	}
}

