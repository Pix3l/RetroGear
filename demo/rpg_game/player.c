/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  player.c - Generic game main player               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <math.h>

#include "player.h"
#include "controls.h"
#include "tile.h"
#include "camera.h"
#include "gfx.h"
#include "sfx.h"
#include "game.h"
#include "font.h"
#include "fps.h"
#include "timer.h"
#include "entity.h"
#include "rgtypes.h"
#include "score.h"
#include "collision.h"
#include "transition.h"
#include "bit.h"
#include "quest.h"
#include "dialogue.h"
#include "event.h"
#include "level.h"
#include "util.h"
#include "party.h"

//Private functions
void playerAction();
void movePlayerStatic();
void movePlayerDynamicX();
void movePlayerDynamicY();
void playerThink();

int think;
float speed_limit;

/**
 * Reset the player's default values
 * 
 * @param Entity *player
 * 		Pointer to a Player objects
 **/
void initPlayer(Entity *player)
{
    player->type = PLAYER;
    player->visible = 1;
    player->flag_active = 1;
	player->w= PLAYER_W;
	player->h= PLAYER_H;
	player->lives = 3;
	player->speed = 1.0f;

    player->previous_x = (int)player->x;
    player->previous_y = (int)player->y;

    //Difference between sprite and player size
    int sprite_diff_w = (PLAYER_SPRITE_W - PLAYER_W) / 2;
    int sprite_diff_h = (PLAYER_SPRITE_H - PLAYER_H) -1;

	if( !player->sprite.surface )
	{
		initSprite(&player->sprite, player->x-sprite_diff_w, player->y-sprite_diff_h, PLAYER_SPRITE_W, PLAYER_SPRITE_H, 0.09f, "data/player.bmp");
	}

	player->hspeed = 0;
	player->vspeed = 0;

    player->gravity=0.1f;
    
	player->direction_x = 1;
	player->direction_y = 0;
    
	player->status = IDLE;

    speed_limit = MAX_H_SPEED;

	//if(!player_die_snd)
	//{
	//	player_die_snd = loadSound("snd/player_dies.wav");
	//}

    //Custom variables
	player_energy = 100;
	player_magic = 50;
	player_level = 1;

    //~ party_member_create(PLAYER_ID, player->x, player->y);
}

void setCurrentPlayer(Entity *player)
{
    curr_player = player;
}

void setPlayerPosition(int x, int y)
{
	curr_player->x = x;
	curr_player->y = y;
	curr_player->xstart = x;
	curr_player->ystart = y;
    curr_player->previous_x = curr_player->x;
    curr_player->previous_y = curr_player->y;

    initParty();

    //~ party_member_create(PLAYER_ID, curr_player->x, curr_player->y);
}

void playerExtraLife()
{
	curr_player->lives++;

    //Limit player maximum lives to 99
    if(curr_player->lives > 99)
    {
        curr_player->lives = 99;
    }
    
	playSound(extralife_snd);
}

void playerAction()
{
	curr_player->sprite.index = 0;	//Action sprite index
	//Action time
	//Action function	
}

/**
 * Handle keyboard keys and update player moves and status 
 **/
void updatePlayer()
{
	//Prevent any player interaction during dialogs
	//player_action==TALK TODO;
    if(hasFlag(defaultTypewriter.flags, typewriter_active))
    {
        return;
    }

	//Open menu
	if(curr_player->status == IDLE && (isButtonPressed(BUTTON_B) || isButtonPressed(BUTTON_START)) && curr_menu==&quest_menu)
	{
        printf("open menu\n");
		waitButtonRelease(BUTTON_B);
		waitButtonRelease(BUTTON_START);
		
        toggleFlag(&quest_menu.flags, menu_active);
		quest_menu.curr_item=0;
	}
	
	if(hasFlag(quest_menu.flags, menu_active))
	{
		menuInput(curr_menu);
		return;
	}

    movePlayerStatic();


	scrollCameraX(&camera, curr_player->x);
	scrollCameraY(&camera, curr_player->y);

    updateParty();

    animateEntity(curr_player, 1);
}

void movePlayerStatic()
{
    if(curr_player->status == MOVE)
    {
        if(curr_player->direction_x!=0)
        {
            curr_player->x += 1*curr_player->direction_x;
        }

        if(curr_player->direction_y!=0)
        {
            curr_player->y += 1*curr_player->direction_y;
        }

        if(isInTile(curr_player->x, curr_player->y))
        {
            if (!isButtonPressed(BUTTON_LEFT) || !isButtonPressed(BUTTON_RIGHT))
            {
                curr_player->status = IDLE;
                curr_player->previous_x = (int)curr_player->xstart;
                //~ curr_player->hspeed = 0;
            }

            if (!isButtonPressed(BUTTON_UP) || !isButtonPressed(BUTTON_DOWN))
            {
                curr_player->status = IDLE;
                curr_player->previous_y = (int)curr_player->ystart;
                //~ curr_player->vspeed = 0;
            }
        }
    }

    if(curr_player->status == IDLE)
    {
        curr_player->xstart = (int)curr_player->x;
        curr_player->ystart = (int)curr_player->y;
        
        playerThink();

        RG_Point point = {0, 0};

        if (isButtonPressed(BUTTON_LEFT))
        {
            curr_player->direction_x = -1;
            curr_player->direction_y = 0;
            //~ curr_player->hspeed = -1.0f;
            
            point = tileCollision(&Player, floorf(curr_player->x+curr_player->direction_x), floorf(curr_player->y));
        }
        
        if (isButtonPressed(BUTTON_RIGHT))
        {
            curr_player->direction_x = 1;
            curr_player->direction_y = 0;
            //~ curr_player->hspeed = 1.0f;
            
            point = tileCollision(&Player, floorf(curr_player->x+curr_player->direction_x), floorf(curr_player->y));
        }
        
        if (isButtonPressed(BUTTON_UP))
        {
            curr_player->direction_x = 0;
            curr_player->direction_y = -1;
            //~ curr_player->vspeed = -1.0f;
            
            point = tileCollision(&Player, floorf(curr_player->x), floorf(curr_player->y+curr_player->direction_y));
        }
        
        if (isButtonPressed(BUTTON_DOWN))
        {
            curr_player->direction_x = 0;
            curr_player->direction_y = 1;
            //~ curr_player->vspeed = 1.0f;
            
            point = tileCollision(&Player, floorf(curr_player->x), floorf(curr_player->y+curr_player->direction_y));
        }

        if( point.x == -1 || point.y == -1)
        {
            think = 0;
            curr_player->status = MOVE;
            curr_player->previous_x = (int)curr_player->xstart;
            curr_player->previous_y = (int)curr_player->ystart;
        }
    }
}

/**
 * Move the player with dynamic speed - Horizontal movement
 **/
void movePlayerDynamicX()
{
	if (isButtonPressed(BUTTON_LEFT))
	{
		curr_player->direction_x = -1;
        curr_player->status = MOVE;

        curr_player->hspeed += curr_player->speed * curr_player->direction_x;
	}
	
	if (isButtonPressed(BUTTON_RIGHT))
	{
		curr_player->direction_x = +1;
        curr_player->status = MOVE;

        curr_player->hspeed += curr_player->speed * curr_player->direction_x;
	}

    //Decrease velocity only when touching the floor
    if ( isEntityOnFloor(&Player) )
    {
        points_index = 0;   //Reset points indexer
        
        if (!isButtonPressed(BUTTON_LEFT) && !isButtonPressed(BUTTON_RIGHT))
        {
            float friction = curr_player->speed/2;
            if(curr_player->hspeed > 0.0f)
            {
                curr_player->hspeed -= friction;

                if(curr_player->hspeed < 0.0f)
                {
                    curr_player->hspeed = 0.0f;
                    waitButtonRelease(BUTTON_LEFT);
                }
            }
            else if(curr_player->hspeed < 0.0f)
            {
                curr_player->hspeed += friction;

                if(curr_player->hspeed > 0.0f)
                {
                    curr_player->hspeed = 0.0f;
                    waitButtonRelease(BUTTON_RIGHT);
                }
            }
        }

        //Set the maximum speed for walk and run
        speed_limit = ( isButtonPressed(BUTTON_B) )? MAX_RUN_SPEED : MAX_H_SPEED;
    }

    //Limit maximum speed
    if (curr_player->hspeed > speed_limit) curr_player->hspeed = speed_limit;
    if (curr_player->hspeed < -speed_limit) curr_player->hspeed = -speed_limit;

    RG_Point point = {-1, -1};

    //Checking collision on the left
    if(curr_player->hspeed<0)
    {
        //Has collide on left?
        point = tileCollision(&Player, floorf(curr_player->x+curr_player->hspeed), curr_player->y);
        if( point.x != -1 )
        {
            curr_player->x= point.x+TILESIZE;
            curr_player->hspeed = 0;  //No need to slide if we touch an obstacle
        }
        else
        {
            curr_player->x += curr_player->hspeed;         //Update its position
        }
    }
    else if(curr_player->hspeed>0)
    {
        point = tileCollision(&Player, ceilf(curr_player->x+curr_player->hspeed), curr_player->y);
        if( point.x != -1 )
        {
            curr_player->x= point.x-curr_player->w;
            curr_player->hspeed = 0;  //No need to slide if we touch an obstacle
        }
        else
        {
            curr_player->x += curr_player->hspeed;         //Update its position
        }
    }
}

/**
 * Move the player with dynamic speed - Vertical movement
 **/
void movePlayerDynamicY()
{
	if (isButtonPressed(BUTTON_A))
	{
        waitButtonRelease(BUTTON_A);
        if(!isEntityOnFloor(&Player))
        {
            return;
        }

        playSound(jump_snd);

		//if the player isn't jumping already
		curr_player->vspeed = -5.0f;		//jump!
	}

	if (!isButtonPressed(BUTTON_A))
	{	
		//if the player isn't jumping already
		curr_player->vspeed+=curr_player->gravity;
	}

    if(curr_player->vspeed>0)
    {
        //Has collide on bottom?
        if(isEntityOnFloor(&Player))
        {
            if(!isButtonPressed(BUTTON_A))	//player may only jump again if the jump key is released while on ground
                waitButtonRelease(BUTTON_A);
        }
    }

    doEntityGravity(&Player);
}

void playerThink()
{
    int col, row;
    //Clean previous position
    col = pixelsToTiles( Player.x );
    row = pixelsToTiles( Player.y );

    //Check for stairs
    if(curr_level->map[SOLID_LAYER][row][col] == WARP)
    {
        doWarp(curr_level->name, col, row);
    }

    think = 1;
}

void playerDie()
{
	curr_player->sprite.index = DIE;

	//Stop any music from the game
	if(isMusicPlaying())
	{
		pauseMusic();
	}

	//Player has not been touched by enemies
	if(curr_player->status!=KILL)
	{
		//~ playSound(player_die_snd);
		curr_player->status=KILL;
	}

	if(curr_player->y>SCREEN_HEIGHT)
	{	
		if(curr_player->timer[0]==0)
		{
			curr_player->timer[0]=fps.t;
			return;
		}

		if(get_time_elapsed(curr_player->timer[0]) >= 3)
		{
			//~ printf("fps.t %d lastTick %d seconds %d\n", fps.t, curr_player->timer[0], getSeconds(curr_player->timer[0]));
			curr_player->lives--;
			curr_player->status=IDLE;
			curr_player->hspeed=curr_player->vspeed=0;
			setPlayerPosition(curr_player->xstart, curr_player->ystart);
			timer.start_time = fps.t;
			setGameState(PREGAME);
			curr_player->timer[0] = 0;	//Reset internal timer
			return;
		}

		return;
	}

	curr_player->y += curr_player->vspeed;
	curr_player->vspeed += curr_player->gravity;

	if(curr_player->vspeed >= TILESIZE/2)	//if the speed is higher than this we might fall through a tile
		curr_player->vspeed = TILESIZE/2;
}

/**
 * Draw player related stuff
 **/
void drawPlayer()
{
    if(Game.status<GAME) return;

	// Destinazione del tile
	SDL_Rect dest;

	dest.x = (int)Player.x - camera.x;
	dest.y = (int)Player.y - camera.y;
	dest.w = dest.x+Player.w;
	dest.h = dest.y+Player.h;

    //~ drawFillRect(Player.x, Player.y, TILESIZE, TILESIZE, 0xff0000);
    //~ getSprite(Player.sprite, Player.sprite_index, TILESIZE, TILESIZE, dest);
    
	//~ drawPlayerMenu();
	//~ drawPlayerStatus();
}
