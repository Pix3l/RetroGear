/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             		  party.c - RPG party handling                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "party.h"
#include "player.h"
#include "tile.h"

void initParty()
{
    int lives = 3;
    float animation_speed = 0.09f;
    float speed = 1.0f;

    char *file[MAX_PARTY] = {"data/player2.bmp", "data/player3.bmp"};
    
    int i = 0;
    for(i=0; i<MAX_PARTY; i++)
    {
        initEntity(&party[i], FRIEND, curr_player->x, curr_player->y, PLAYER_W, PLAYER_H, lives, speed, 0, &updateParty);

        if( !party[i].sprite.surface )
        {
            initSprite(&party[i].sprite, curr_player->x, curr_player->y, PLAYER_W, PLAYER_H, animation_speed, file[i]);
        }

        party[i].direction_x = curr_player->direction_x;
        party[i].previous_x = curr_player->x;

        party[i].direction_y = 0;
        party[i].previous_y = curr_player->y;

        party[i].status = IDLE;
    }
}

void updateParty()
{
    int next_x = curr_player->previous_x,
        next_y = curr_player->previous_y;

    int i = 0;
    for(i=0; i<MAX_PARTY; i++)
    {
        if(party[i].status == MOVE)
        {
            if(abs(party[i].previous_x-party[i].x)>=16)
            {
                party[i].previous_x = party[i].xstart;
                party[i].status = IDLE;
            }

            if(abs(party[i].previous_y-party[i].y)>=16)
            {
                party[i].previous_y = party[i].ystart;
                party[i].status = IDLE;
            }
            
            if(party[i].x > next_x)
            {
                party[i].x--;
                party[i].direction_x = -1;
                party[i].direction_y = 0;
                
            }
            if(party[i].x < next_x)
            {
                party[i].x++;
                party[i].direction_x = 1;
                party[i].direction_y = 0;
            }

            if(party[i].x == next_x)
            {
                party[i].status = IDLE;
            }

            if(party[i].y > next_y)
            {
                party[i].y--;
                party[i].direction_y = -1;
                party[i].direction_x = 0;
            }
            
            if(party[i].y < next_y)
            {
                party[i].y++;
                party[i].direction_y = 1;
                party[i].direction_x = 0;
            }
            
            if(party[i].y == next_y)
            {
                party[i].status = IDLE;
            }
        }

        if(party[i].status == IDLE)
        {
            party[i].xstart = party[i].x;
            party[i].ystart = party[i].y;
            
            if( party[i].x != next_x || party[i].y != next_y )
            {
                party[i].status = MOVE;
            }
        }
        
        next_x = party[i].previous_x;
        next_y = party[i].previous_y;
        
        animateEntity(&party[i], 1);
    }
}

void drawParty()
{
    int i = MAX_PARTY;
    for(i=MAX_PARTY; i>=0; i--)
    {
        drawEntity(&party[i]);
    }
}

void drawPartyHud()
{
    //~ if(Game.status<GAME) return;
    
    //~ if(player_menu.flag_active)
    //~ {
    	//~ drawMenu(&player_menu);
    	
    	//~ drawMenu(&party_menu);

		//~ if(player_action!=STAT)
		//~ {
			//~ //Gold box
			//~ drawGui(195, FONT_H, 45, 15, 0x000000);
			//~ drawString(screen, 200, 1, "Gold", white, 0);
			//~ drawString(screen, 190, 11, "1000", white, 0);

			//~ drawGui(0, (SCREEN_HEIGHT/2)+56, SCREEN_WIDTH-(FONT_W*3), 48, 0x000000);
			//~ drawString(screen, FONT_W*2, SCREEN_HEIGHT-(FONT_H*9), "Pix ", white, 0);
			
			//~ sprintf(message,"HP: %d", player_energy);
			//~ drawString(screen, FONT_W*2, SCREEN_HEIGHT-(FONT_H*7), message, white, 0);

			//~ sprintf(message,"MP: %d", player_magic);
			//~ drawString(screen, FONT_W*2, SCREEN_HEIGHT-(FONT_H*5), message, white, 0);

			//~ sprintf(message,"LV: %d", player_level);
			//~ drawString(screen, FONT_W*2, SCREEN_HEIGHT-(FONT_H*3), message, white, 0);
		//~ }

    //~ }

    //~ if(player_action==STAT)
    //~ {
    	//~ drawGui((SCREEN_WIDTH/2)+FONT_W, FONT_H, 110, 200, 0x000000);
    	
    	//~ sprintf(message,"LV: %d", player_level);
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*2, message, white, 0);

    	//~ sprintf(message,"HP: %d / 100", player_energy);
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*4, message, white, 0);

    	//~ sprintf(message,"MP: %d / 100", player_magic);
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*6, message, white, 0);

    	//~ sprintf(message,"Exp: 325235");
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*8, message, white, 0);

    	//~ sprintf(message,"Next: 325235");
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*10, message, white, 0);

    //~ }
}
