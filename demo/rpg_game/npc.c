/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  ncp.c - generic npc entity functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "npc.h"
#include "gfx.h"
#include "sfx.h"
#include "util.h"
#include "timer.h"
#include "player.h"
#include "camera.h"
#include "fps.h"
#include "game.h"
#include "tile.h"
#include "level.h"
#include "controls.h"
#include "dialogue.h"
#include "quest.h"
#include "collision.h"
#include "bit.h"

static void onCollision();
static void onDestroy();
static void onAnimate();

/**
 * npc entity constructor
 * Help programmers on setup the initial values for an npc entity
 **/
//~ void npc_create(int id, int x, int y, char *sprite)
//~ {
    //~ int solid = 1;
	//~ int lives = 3;
	//~ float animation_speed = 0.09f;
	//~ float speed = 1.0f;

    //~ createEntity(id, x, y, NPC_W, NPC_H, lives, speed, 0, &updateNpc);

	//~ if( !EntityList_Tail->sprite.surface )
	//~ {
		//~ initSprite(&EntityList_Tail->sprite, x, y, 16, 16, animation_speed, sprite);
	//~ }
//~ }

/**
 * Azioni da eseguire in caso di collisione della entity
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
static void onCollision(Entity *pobj)
{
	//~ Entity *current = headList;
	//~ while(current!=NULL)
	//~ {
		//~ if(current!=pobj && pobj->active && pobj->status!=KILL)
		//~ {
			//~ if(rectCollision(current->x, current->y, current->w, current->h, 
							//~ (int)(pobj->x+pobj->direction_x), (int)(pobj->y+pobj->direction_y), pobj->w, pobj->h))
			//~ {
				//~ pobj->hspeed =0;
				//~ pobj->vspeed =0;
			//~ }
		//~ }
		//~ current=current->next;
	//~ }

	/**
	 * Interact with the player
	 * 
	 * Preventing the Player walk on us
	 * Handle talking request
	 **/
	if(rectCollision(curr_player->x+curr_player->direction_x, curr_player->y+curr_player->direction_y,
                     curr_player->w+curr_player->direction_x, curr_player->h+curr_player->direction_y, 
					 pobj->x, pobj->y, pobj->w, pobj->h))
	{
        if(!hasFlag(defaultTypewriter.flags, typewriter_active) && isButtonPressed(BUTTON_A))
        {
            #ifdef DEBUG_DLG
                printf("[Dialogue] NPC x: %d y: %d:\n", (int)pobj->x, (int)pobj->y);
            #endif
            
            //~ initDialog(dialogues[0], NULL);
            loadDialogueFromFile(pobj->x, pobj->y, curr_level->name);
            //~ pobj->status=KILL;
        }
	}
}

/**
 * Azioni da eseguire alla distruzione della entity
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
static void onDestroy(Entity *pobj)
{
	
}

static void onAnimate(Entity *pobj)
{
    if(pobj->sprite.surface==NULL)
    {
        return;
    }

    /**
     * L'indice dei frame dell'animazione è incrementato in base 
     * alla velocità d'animazione scelta
     **/
    pobj->sprite.animation_timer += pobj->sprite.animation_speed;

    ///Di default impostiamo 4 fotogrammi d'animazione per la camminata
    if (pobj->sprite.animation_timer >= 2)
    {
        pobj->sprite.animation_timer = 0;
    }
    
    if(pobj->direction_x != 0) 
    {
        pobj->sprite.index = (pobj->direction_x < 0 ? WALKLEFT1 : WALKRIGHT1)+ abs(pobj->sprite.animation_timer);
    }

    //TODO: Se direction_y fosse diverso da 0 durante il movimento sull'asse X? Caduta ad esempio.

    if(pobj->direction_y != 0)// && pobj->status == MOVE)
    {
        pobj->sprite.index = (pobj->direction_y < 0 ? WALKUP1 : WALKDOWN1)+ abs(pobj->sprite.animation_timer);
    }
}

/**
 * Gestisce la vita della entity npc nel gioco
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 * 
 * @return Entity *ptr
 * 		Puntatore all'oggetto successivo nel caso l'attuale venga 
 * 		distrutto
 **/
void updateNpc(Entity *pobj)
{
	pobj->previous_x = pobj->x;
	pobj->previous_y = pobj->y;

	//Make solid the actual position
	if(isInTile(pobj->x, pobj->y))
	{
		int col, row;
		//Clean previous position
		col = pobj->previous_x / TILESIZE;
		row = pobj->previous_y / TILESIZE;
		curr_level->map[SOLID_LAYER][row][col] = 0;
		
		//Set new position
		col = pobj->x / TILESIZE;
		row = pobj->y / TILESIZE;
		
		curr_level->map[SOLID_LAYER][row][col] = 1;
	}

    onAnimate(pobj);
	onCollision(pobj);
}
