/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   quest.c - RPG like example menu                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                      Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <string.h>

#include "quest.h"
#include "controls.h"
#include "gfx.h"
#include "font.h"
#include "dialogue.h"
#include "game.h"
#include "menu.h"
#include "menu_classic.h"
#include "menu_paginator.h"
#include "bit.h"
#include "potion.h"
#include "diary.h"
#include "player.h"
#include "tile.h"

void generic()
{
    setFlag(&quest_menu.flags, menu_active);
    
    snprintf(dlg_holder, MAX_PARAM_LENGTH, "element %s", curr_menu->items[curr_menu->curr_item].name);
    initDialogue(&defaultTypewriter, dlg_holder, NULL);
}

/**
 * Players actions
 **/
char swordtext[] = "This is your sword.";
void dummy2()
{
    //If the length of src is less than n, strncpy() writes additional null bytes to dest to ensure that a total of n bytes are written. 
    //~ strncpy(&dlg_holder, "This is your sword.", MAX_PARAM_LENGTH);
    initDialogue(&defaultTypewriter, swordtext, NULL);
}

void showItemsMenu()
{
    setFlag(&quest_menu_items.flags, menu_show_border|menu_active);
    curr_menu = &quest_menu_items;
}

char talkText[] = "There's nothing to say...";
void talk()
{
    #ifdef DEBUG_DLG
        printf("[Dialogue] Talk action x: %d y: %d:\n", (int)curr_player->x, (int)curr_player->y);
    #endif

    loadDialogueFromFile(curr_player->x+curr_player->direction_x, curr_player->y+curr_player->direction_y, curr_level->name);

    initDialogue(&defaultTypewriter, (dlg_holder[0]!='\0')? dlg_holder : talkText, NULL);

    unsetFlag(&quest_menu.flags, menu_active);	//Hide menu (Close)
}

void look()
{
    
}

void init_quest_menu()
{
    initClassicMenu(&quest_menu, NULL, 0, 0, "Command", 6, white, NULL, &quest_menu_items);

    addMenuItem(&quest_menu, "Talk", NULL, white, talk);
    addMenuItem(&quest_menu, "Mag ", NULL, white, generic);
    addMenuItem(&quest_menu, "Stat", NULL, white, generic);
    addMenuItem(&quest_menu, "Look", NULL, white, generic);
    addMenuItem(&quest_menu, "Item", NULL, white, showItemsMenu);
    addMenuItem(&quest_menu, "????", NULL, white, generic);

    setMenuTable(&quest_menu, 3, 2, 4*FONT_W, 1);
    setFlag(&quest_menu.flags, menu_persistent|menu_show_border);
    unsetFlag(&quest_menu.flags, menu_active);

    //Init items and menu
    initClassicMenu(&quest_menu_items, quest_items, 0, 40, "itemz", QUEST_ITEMS, white, &quest_menu, NULL);
    initStaticMenuItems(quest_items, QUEST_ITEMS);	//Clean the structures before use

    addStaticMenuItem(&quest_menu_items, "Diary", 0, NULL, green, &useDiary);
    addStaticMenuItem(&quest_menu_items, "Sword", 0, NULL, red, &dummy2);
    addStaticMenuItem(&quest_menu_items, "potion", 0, NULL, white, &usePotion);
    addStaticMenuItem(&quest_menu_items, "elixir", 0, NULL, white, &generic);
    addStaticMenuItem(&quest_menu_items, "bomb", 0, NULL, white, &generic);
    //5
    addStaticMenuItem(&quest_menu_items, "armor", 0, NULL, red, &generic);
    addStaticMenuItem(&quest_menu_items, "shield", 0, NULL, red, &generic);

    setMenuPaginator(&quest_menu_items, 5);
    unsetFlag(&quest_menu_items.flags, menu_active);

    initDiary();
}

/**
	player_names[0].id = 1;
	strcpy(player_names[0].name, "Pix ");
	player_names[0].color = white;
	player_names[0].func = showPlayerStat;
	player_names[0].x = party_menu.x+FONT_W;
	player_names[0].y = party_menu.y;

	player_names[1].id = 2;
	strcpy(player_names[1].name, "Valy ");
	player_names[1].color = white;
	player_names[1].func = showPlayerStat;
	player_names[1].x = party_menu.x+FONT_W;
	player_names[1].y = party_menu.y+(FONT_H);

	player_names[2].id = 3;
	strcpy(player_names[2].name, "Elf ");
	player_names[2].color = white;
	player_names[2].func = showPlayerStat;
	player_names[2].x = party_menu.x+FONT_W;
	player_names[2].y = party_menu.y+(2*FONT_H);
**/
//~ }

void drawQuestHUD()
{
    //~ if(Game.status<GAME) return;
    
    //~ if(player_menu.flag_active)
    //~ {
    	//~ drawMenu(&player_menu);
    	
    	//~ drawMenu(&party_menu);

		//~ if(player_action!=STAT)
		//~ {
			//~ //Gold box
			//~ drawGui(195, FONT_H, 45, 15, 0x000000);
			//~ drawString(screen, 200, 1, "Gold", white, 0);
			//~ drawString(screen, 190, 11, "1000", white, 0);

			//~ drawGui(0, (SCREEN_HEIGHT/2)+56, SCREEN_WIDTH-(FONT_W*3), 48, 0x000000);
			//~ drawString(screen, FONT_W*2, SCREEN_HEIGHT-(FONT_H*9), "Pix ", white, 0);
			
			//~ sprintf(message,"HP: %d", player_energy);
			//~ drawString(screen, FONT_W*2, SCREEN_HEIGHT-(FONT_H*7), message, white, 0);

			//~ sprintf(message,"MP: %d", player_magic);
			//~ drawString(screen, FONT_W*2, SCREEN_HEIGHT-(FONT_H*5), message, white, 0);

			//~ sprintf(message,"LV: %d", player_level);
			//~ drawString(screen, FONT_W*2, SCREEN_HEIGHT-(FONT_H*3), message, white, 0);
		//~ }

    //~ }

    //~ if(player_action==STAT)
    //~ {
    	//~ drawGui((SCREEN_WIDTH/2)+FONT_W, FONT_H, 110, 200, 0x000000);
    	
    	//~ sprintf(message,"LV: %d", player_level);
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*2, message, white, 0);

    	//~ sprintf(message,"HP: %d / 100", player_energy);
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*4, message, white, 0);

    	//~ sprintf(message,"MP: %d / 100", player_magic);
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*6, message, white, 0);

    	//~ sprintf(message,"Exp: 325235");
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*8, message, white, 0);

    	//~ sprintf(message,"Next: 325235");
    	//~ drawString(screen, (SCREEN_WIDTH/2)+FONT_W, FONT_H*10, message, white, 0);

    //~ }
}
