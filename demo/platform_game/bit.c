/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  bit.c - Bitwise operation functions               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "bit.h"

/**
 * Check if specific flag has been set
 *
 * @param int flag_store
 *      Variable containing the flags
 *
 * @param unsigned char flags
 *      The flags to check
 **/
int hasFlag(unsigned char flag_store, unsigned char flags)
{
    return (flag_store & flags);
}

/**
 * Set a specific flag
 *
 * @param int *flag_store
 *      Pointer to a flag store variable
 *
 * @param unsigned char flags
 *      The flags to set
 **/
void setFlag(unsigned char *flag_store, unsigned char flags)
{
    (*flag_store) |= flags;
}

/**
 * Unset a specific flag
 *
 * @param int *flag_store
 *      Pointer to a flag store variable
 *
 * @param unsigned char flags
 *      The flag to unset
 **/
void unsetFlag(unsigned char *flag_store, unsigned char flags)
{
    (*flag_store) &= ~flags;
}

/**
 * Toggle a specific flag
 *
 * @param int *flag_store
 *      Pointer to a flag store variable
 *
 * @param Menu_flags flag
 *      The flag to toggle
 **/
void toggleFlag(unsigned char *flag_store, unsigned char flags)
{
    (*flag_store) ^= flags;
}
