/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  ichigo.h - Ichigo entity functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _ICHIGO_H
#define _ICHIGO_H

#define ICHIGO_W 14
#define ICHIGO_H 14

#include <SDL/SDL.h>
#include "entity.h"

SDL_Surface *ichigo_spr;

void ichigo_create(int id, int x, int y);
void updateIchigo(Entity *pobj);

#endif
