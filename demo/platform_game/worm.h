/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    worm.h - Worm entity functions                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _WORM_H
#define _WORM_H

#define WORM_W 14
#define WORM_H 8

#include <SDL/SDL.h>
#include "entity.h"

void worm_create(int id, int x, int y);
void updateWorm(Entity *pobj);
void worm_clean();

#endif
