/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  score.h - Score handling functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _SCORE_H
#define _SCORE_H

#include <SDL/SDL.h>
#include "sprite.h"

#define SCORE_SPEED 0.04f   //Standard score speed

typedef struct _scoreType
{
  int xstart, ystart;   //Start coordinates

  Sprite sprite;        //The sprite sheet structure
  short int status;

  struct _scoreType *next;
} scoreType;

scoreType *scoreHead, *scoreTail;

//This help taking track of the correct score index
enum { pts100, pts200, pts400, pts500, pts800, pts1000, pts2000, pts4000, pts5000, pts8000, pts1UP, pts2UP, pts3UP, pts4UP, pts5UP} SCORES;

int high_score; //Global high score counter
//~ int player_score[MAX_PLAYERS];  //Personal score for each player

int points_index;
SDL_Surface *score_sheet;

void createScore(int x, int y, float speed, int sprite_index);
void initScore();
void doScore();
int getScore();
void addScore(int score); //, int *pscore);
void drawScore();
void clearScoreList();

#endif
