/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   gfx.h - Generic drawing functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _GFX_H
#define _GFX_H

#include <SDL/SDL.h>

#define SCREEN_WIDTH 256
#define SCREEN_HEIGHT 240

SDL_Surface *screen, *double_screen;

/** Colors **/
static const SDL_Color white = {255, 255, 255};
static const SDL_Color black = {0, 0, 0};
static const SDL_Color cyan  = {0, 255, 255};
static const SDL_Color blue  = {0, 0, 255};
static const SDL_Color yellow  = {255, 255, 0};
static const SDL_Color purple = {255, 0, 255};
static const SDL_Color red  = {255, 0, 0};
static const SDL_Color green  = {0, 255, 0};
static const SDL_Color gray  = {192, 192, 192};

#define RED 0xFF0000
#define BLUE 0x0000FF
#define GREY 0xC0C0C0
#define WHITE 0xFFFFFF
#define BLACK 0x000000
#define GREEN 0x008000
#define ORANGE 0xFF9D2E
#define PURPLE 0xFF00FF
#define YELLOW 0xFFFF00
#define COLORKEY 0x00FF00

#define SKYBLUE 0x8080FF

Uint32 get_pixel(SDL_Surface *surface, int x, int y);
void put_pixel(SDL_Surface *_ima, int x, int y, Uint32 pixel);
void replaceColor (SDL_Surface * src, Uint32 target, Uint32 replacement);
void drawFillRect(int x, int y, int w, int h, int color);
void drawRect(int x, int y, unsigned int w, unsigned int h, int color);
void drawGui(int x, int y, unsigned int w, unsigned int h, int bg, SDL_Color color);
SDL_Surface * loadImage(char *file, Uint32 key);
void putImage(SDL_Surface *src, SDL_Surface *dest, int x_orig, int y_orig, int w, int h, int x, int y);
void getSprite(SDL_Surface *sprite_sheet, int index, int width, int height, SDL_Rect dest);

#endif
