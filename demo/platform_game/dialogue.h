/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *               dialogue.h - Dialogue handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 			Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _DIALOGUE_H
#define _DIALOGUE_H

#include "typewriter.h"
#include "event.h"

typedef enum _Dialogue_flags {
    dialogue_active         = 1 << 0,  // 000001  1
    dialogue_waitConfirm    = 1 << 1,  // 000010  2
} Dialogue_flags;

typedef struct _Dialogue
{
    Typewriter *typewriter;
	int (*callback)();
    unsigned char flags;
} Dialogue;

Dialogue defaultDialogue;

extern char dlg_holder[MAX_PARAM_LENGTH];

void initDialogue(Typewriter *pTypewriter, char text[], int(*callback)());
int loadDialogueFromFile(int x, int y, char *filename);
void doDialogue(Typewriter *pTypewriter);
//~ int doDialogueCallback(Dialogue *pDialogue);
void closeDialogue(Typewriter *pTypewriter);
void drawDialogue(Typewriter *pTypewriter);

#endif
