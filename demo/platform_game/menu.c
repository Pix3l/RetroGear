/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 menu.h - Game menu handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <string.h>

#include "menu.h"
#include "font.h"
#include "util.h"
#include "controls.h"
#include "timer.h"
#include "fps.h"
#include "gfx.h"
#include "bit.h"

//Private functions
void adjustMenuWidth(Menu* pmenu, int strlen);
void setMenuHeight(Menu* pmenu, int height);
void alignMenuItems(Menu* pmenu);
void switchCursorSelection(Menu *pmenu, int increment);

/**
 * Initialize a Menu object
 * 
 * @param Menu *pmenu
 * 		Pointer to the Menu object
 * 
 * @param int x,y,w,h
 * 		Menu coordinate and dimensions (Used for drawing borders)
 * 
 * @param char name
 * 		Menu name
 * 
 * @param int max_items
 * 		Maximum number of collectable items
 * 		A -1 value mean infinite, 0 value mean "no items allowed"
 * 
 * @param int num_items
 * 		Number of items stored in the menu
 * 
 * @param _Menu *previous, *child
 * 		previous Menu pointer, Child Menu Pointer.
 **/
void initMenu(Menu* pmenu, Item *pitem, int x, int y, char *name, int max_items, SDL_Color color, Menu *previous, Menu *next, void(*update)(), void(*draw)())
{
    setFlag(&pmenu->flags, 0);  //Init flags

    pmenu->x = x;
    pmenu->y = y;

    pmenu->curr_item = 0;
    pmenu->page_start = 0;
    pmenu->max_items = max_items;

    pmenu->items = NULL;

    //If pitem is a valid pointer structure, this will be a static menu
    if(pitem!=NULL)
    {
        //Static menu can't have an infinite number of items or zero
        if(max_items < 1)
        {
            quit=1;
            printf("Static menu can't have %d number of items, define a correct max number of items!\n", max_items);
            return;
        }

        //Statically link the item array structure
        pmenu->items = pitem;
    }

    setFlag(&pmenu->flags, menu_active);    //Set active flag

    strcpy(pmenu->name, name);
    pmenu->cursor = '>';

    pmenu->color = color;
    pmenu->previous = previous;
    pmenu->next = next;

    pmenu->update = update;
    pmenu->draw = draw;

    pmenu->cols = 1;
}

/**
 * Crea un oggetto di tipo Item da poter aggiungere ad un menu'
 * 
 * @param int id
 * 		L'id della voce di menu
 * @param char *text
 * 		Testo della voce di menu
 * 
 * @param int x, y
 * 		Coordinate della voce all'interno del menu
 * 
 * @param SDL_Color color
 * 		Colore del testo
 * 
 * @param void (*func)()
 * 		Funzione associata alla voce di menu
 * 
 * @param Menu *pmenu
 * 		Menu in cui creare la nuova voce?????
 **/
Item* createItem(char *name, char* desc, SDL_Color color, void (*func)())
{
	if(func==NULL)
	{
		quit=1;
		printf("Function pointer for menu \"%s\" not initialized!\n", name);
	}
	
	Item *pnew = (Item*) malloc(sizeof(Item));

	strcpy(pnew->name, name);
	
	if(desc!=NULL)
	{
		pnew->description = (char*) malloc(sizeof(char) * (strlen(desc) + 1));
		strcpy(pnew->description, desc);
	}

	pnew->color = color;
	pnew->func = (*func);

	return pnew;
}

int addMenuItem(Menu *pmenu, char *name, char* desc, SDL_Color color, void (*func)())
{
	//If the number of collectible items is limited
	if(pmenu->max_items > 0)
	{
		//We checks if we have free rooms
		if(pmenu->num_items > pmenu->max_items)
		{
			//There's no room...
			return -1;
		}
	}
	
	//The menu doesn't accept any items
	if(pmenu->max_items==0)
	{
		return -1;
	}

	++pmenu->num_items;
	
	//Reallocatin Item array
	pmenu->items = (Item*) realloc(pmenu->items, sizeof(Item) * pmenu->num_items);

	//Allocate a new Item object
	Item *new_item = createItem(name, desc, color, func);
	memcpy(&pmenu->items[pmenu->num_items-1], new_item, sizeof(Item));

	adjustMenuWidth(pmenu, strlen(new_item->name));
	
	free(new_item);
	
	pmenu->rows++;
	
	alignMenuItems(pmenu);
	
	return 1;
}
/**
 * Initialize an Item object to default values, this can also be used 
 * for deleting unused items
 *
 * @param Item *pitem
 * 		The Item object
 **/
void initStaticMenuItem(Item pitem)
{
    pitem.name[0] = '\0';
    pitem.value = 0;
    pitem.color = white;
    pitem.func = NULL;
    pitem.x = 0;
    pitem.y = 0;
    pitem.flags = 0;
}

/**
 * Initialize an array of Item object to default values, this can also be 
 * used for delete unused items
 * 
 * @param Item *pitem
 * 		The Item object
 * 
 * @param int num_items
 * 		Number of Item elements to iterate
 **/
void initStaticMenuItems(Item pitem[], int max_items)
{
    register int i;
    for(i=0; i< max_items; i++)
    {
        initStaticMenuItem(pitem[i]);
    }
}

int findFreeMenuItem(Item *pitem, int max_elements)
{
  register int i;

  for(i=0; pitem[i].name[0] && i < max_elements; ++i) ;

  if(i==max_elements)
  {
	  return -1; /* no slots free */
  }

  return i;
}

/**
 * Add an item to the a static Item object
 * 
 * @param Menu *pmenu
 * 		Menu object pointer
 * 
 * @param Item *pitem
 * 		Item object pointer
 * 
 * @param char *name
 * 		Item name
 * 
 * @param SDL_Color color
 * 		Item name color
 * 
 * @param void *call
 * 		Item associated function
 * 
 * @param int num_items
 * 		Number of Item objects to iterate
 * 
 * @return int
 * 		1 for Item setted, -1 no rooms avaible for new Items
 **/
int addStaticMenuItem(Menu *pmenu, char *name, int value, char *description, SDL_Color color, void (*call)())
{
	if(!pmenu->items)
	{
		printf("Menu %s not initialized, no items found!\n", pmenu->name);
		return 0;
	}

    //TODO: limitare il numero di parametri in entrata a pmenu e prendere quelli di item da li
    int slot = findFreeMenuItem(pmenu->items, pmenu->max_items);

    if(slot>=0)
    {
        //~ printf("free slot at index %d\n", slot);

        pmenu->items[slot].flags = 0;
        pmenu->items[slot].x = pmenu->x+FONT_W;
        pmenu->items[slot].y = pmenu->y+(slot*FONT_H);
        strcpy(pmenu->items[slot].name, name);
        pmenu->items[slot].value = value;
        //TODO: Attualmente è un puntatore
        if(description)
        {
            //~ strcpy(pmenu->items[slot].description, description);
        }
        pmenu->items[slot].color = color;
        pmenu->items[slot].func = (*call);

        //~ printf("index %d %s pmenu.x %d pmenu.y %d\n", slot, pmenu->items[slot].name, pmenu->items[slot].x, pmenu->items[slot].y);

        pmenu->num_items++;
        pmenu->rows++;		//TODO: Forse andrebbe gestito altrove

        //Adjust menu width and height
        adjustMenuWidth(pmenu, strlen(pmenu->items[slot].name));
        setMenuHeight(pmenu, pmenu->num_items);		//TODO: Andrebbe gestito altrove!

        return 1;
    }
    else
    {
        printf("No free slot found\n");
        return -1;
    }
}

void removeStaticMenuItem(Menu *pmenu, int element_index)
{
    if(element_index > pmenu->max_items)
    {
        return;
    }

    int i=element_index;
    while(i < pmenu->num_items)
    {
        //~ printf("next item %s\n", pmenu->items[i+1].name);
        pmenu->items[i] = pmenu->items[i+1];
        i++;
    }

    pmenu->num_items--;
}

void doMenu(Menu *pmenu)
{
    while(pmenu!=NULL)
    {
        if(pmenu->update!=NULL)
        {
            pmenu->update(pmenu);
        }

        pmenu = pmenu->next;
    }
}

void drawMenu(Menu *pmenu)
{
    while(pmenu!=NULL)
    {
        if(pmenu->draw!=NULL)
        {
            pmenu->draw(pmenu);
        }

        //TODO: Può esserci la situazione in cui in una lista di menù, uno sia inattivo e quello successivo no?
        // Aggiungere un argomento flag alla funzione per ignorare l'attivazione dei menù?
        //~ if(pmenu->next->flag_active)
        //~ {
                pmenu = pmenu->next;
        //~ }
    }
}

/**
 * Check if a Menu object is persistent or not.
 * A persistent menu will not disappear if inactive
 * 
 * @param Menu *pmenu
 * 		Menu object pointer
 * 
 * @return int
 * 		Return 1 if menu is persistent, else 0
 **/
int isMenuPersistent(Menu *pmenu)
{
	if(hasFlag(pmenu->flags, menu_persistent))
	{
		return 1;
	}
	
	return 0;
}

void destroyMenu(Menu *pmenu)
{
	if(pmenu->num_items != 0 && pmenu->items != NULL)
	{
		free(pmenu->items);
	}
}

/**
 * Set the current menu to the previous menu, if available
 * 
 **/
void previousMenu(Menu *pmenu)
{
    if(pmenu->previous)
    {
        if(!isMenuPersistent(pmenu))
        {
            unsetFlag(&pmenu->flags, menu_active);
        }

        curr_menu=pmenu->previous;

        if(!isMenuPersistent(curr_menu))
        {
            setFlag(&curr_menu->flags, menu_active);
        }
    }
}

/**
 * Set the current menu to the next menu, if available
 * 
 **/
void nextMenu(Menu *pmenu)
{
	if(pmenu->next)
	{
        unsetFlag(&pmenu->flags, menu_active);    //Deactive old menu
	    curr_menu=pmenu->next;
	    setFlag(&curr_menu->flags, menu_active);
	}
}

/**
 * Funzione privata di callback che allinea gli elementi di un menu con 
 * le nuove coordinate di esso
 * 
 * @param Menu *pmenu
 * 		Il menu di cui allineare gli items
 **/
void alignMenuItems(Menu* pmenu)
{
	int i;
	for(i = 0; i < pmenu->num_items; ++i)
	{
		pmenu->items[i].x = pmenu->x+FONT_W;
		pmenu->items[i].y = pmenu->y+(i*FONT_H);
	}
	
	pmenu->h = pmenu->num_items*FONT_H;
}

/**
 * Allinea al centro dello schermo il menu specificato
 * 
 * @param Menu *pmenu
 * 		Il menu da allineare
 **/
void alignMenuCenter(Menu* pmenu)
{
	pmenu->x = (SCREEN_WIDTH/2)- ((pmenu->w)/2);
	alignMenuItems(pmenu);
}

/**
 * Allinea al bordo inferiore dello schermo il menu specificato
 * 
 * @param Menu *pmenu
 * 		Il menu da allineare
 **/
void alignMenuBottom(Menu* pmenu)
{
	//*altezza carattere x4
	pmenu->y = SCREEN_HEIGHT-(pmenu->h)-32;
	alignMenuItems(pmenu);
}

void alignItemRow(Menu* pmenu, int rows, int flag_packed)
{
	int i, j=0, x, y;
	x = pmenu->x+FONT_W;
	
	int str_len_max = 0;
	
	for(i = 0; i < pmenu->num_items; ++i)
	{
		//The longest string
		if(strlen(pmenu->items[i].name)>str_len_max)
		{
			str_len_max = strlen(pmenu->items[i].name);
		}
				
		y = pmenu->y+(j*FONT_H);
		
		if(j % rows ==0 && j!=0)
		{
			//Next column at (string lenght*font width)+character width
			x += (str_len_max*FONT_W)+(FONT_W);
			y = pmenu->y;
			j=0;
		}

		pmenu->items[i].x = x;
		pmenu->items[i].y = y;
		
		j++;
	}

	int cols = (pmenu->num_items / rows);
	
	if(flag_packed)
	{
		pmenu->w=((str_len_max*cols)+1)*FONT_W;
		pmenu->h = j*FONT_H;
		pmenu->rows = rows;
	}

	//Debug
	//~ printf("x %d y %d w %d h %d\n", x, y, pmenu->w, pmenu->h);
}

/**
 * Funzione di callback che imposta la larghezza del menu in base alla 
 * lunghezza della voce piu' lunga di uno dei suoi items.
 * La larghezza convertita in pixel calcolando la lunghezza della 
 * stringa per la larghezza del singolo carattere.
 * 
 * @param Collection *pmenu
 * 		Il menu di cui impostare la larghezza
 * 
 * @param int strlen
 * 		Lunghezza della voce di menu
 **/
void adjustMenuWidth(Menu* pmenu, int strlen)
{
	if(strlen*FONT_W > pmenu->w)	//Lunghezza del testo*larghezza lettera
	  pmenu->w = strlen*FONT_W;
}

void setMenuHeight(Menu* pmenu, int height)
{
	pmenu->h = height*FONT_H;
}

/**
 * Set a cursor for the given menu
 **/
void setMenuCursor(Menu* pmenu, char cursor)
{
	pmenu->cursor = cursor;
}

void setItemPosition(Item* pitem, int x, int y)
{
	pitem->x = x;
	pitem->y = y;
	
	//~ printf("[DEBUG] setItemPos: %s %d %d\n", pitem->name, pitem->x, pitem->y);
}

/**
 * Turn off activation flag on all linked menues
 *
 * @param Menu *pmenu
 *      The start menu
 **/
void deactiveMenues(Menu *pmenu)
{
    curr_menu = pmenu;
    
    while(pmenu!=NULL)
    {
        unsetFlag(&pmenu->flags, menu_active);
        pmenu = pmenu->next;
    }
}

/**
 * Switch cursor position inside standard menues between items
 * 
 * @param Menu *pmenu
 * 		Pointer to Menu object to be handle
 * 
 * @param int increment
 * 		Number of entries to skip forward or backward through the menu 
 * 		items
 **/
void switchCursorSelection(Menu *pmenu, int increment)
{
    //Increment/decrement by 1

    //Up and down only if the current item is not in the last row
    if(abs(increment)!=pmenu->rows)
    {
        //Up
        if(increment<0)
        {
            if(pmenu->curr_item+increment>=0)
            {
                pmenu->curr_item+=increment;
            }
        }

        //Down
        if(increment>0)
        {
            if(pmenu->curr_item+increment<pmenu->num_items)
            {
                pmenu->curr_item+=increment;
            }
            else
            {
                pmenu->curr_item=pmenu->num_items-1; //Wrap the selection to last item
            }
        }
    }
    else 	//Switch between columns
    {
        //Left
        if(increment < 0)
        {
            if(pmenu->curr_item >0)
            {
                //~ pmenu->curr_item--;

                pmenu->curr_item -= pmenu->rows;

                if(pmenu->curr_item < 0)
                {
                        pmenu->curr_item = 0;
                }
            }
            #ifdef MENU_H_WRAPPING
                else
                {
                        pmenu->curr_item=pmenu->num_items-1; //Wrap the selection to last item
                }
            #endif
        }

        //Right
        if(increment > 0)
        {
            //Paginated menu
            if(pmenu->cols < 0)
            {
                pmenu->curr_item += pmenu->rows;

                if(pmenu->curr_item > pmenu->num_items-1)
                {
                    pmenu->curr_item = pmenu->num_items-1;
                }

                //TODO: Bisogna gestire la paginazione con page_start!!!
            }
            else if(pmenu->curr_item < pmenu->num_items-1)
            {
                    pmenu->curr_item++;
            }
            #ifdef MENU_H_WRAPPING
                else
                {
                    pmenu->curr_item=0; //Wrap the selection to first item
                }
            #endif
        }
    }
}

/**
 * Default Menu handler
 * 
 * @param Menu *pmenu
 * 		The Menu object to handle 
 **/
void menuInput(Menu *pmenu)
{
    if(pmenu==NULL)
    {
        #ifdef DEBUG_MENU
            printf("[menuInput] Empty pointer, skipping.\n");
        #endif
        return;
    }

    if( !hasFlag(pmenu->flags, menu_active) )
    {
        return;
    }

    if(isButtonPressed(BUTTON_UP))
    {
        waitButtonRelease(BUTTON_UP);

        if(pmenu->rows>1)
        {
            if(pmenu->cols > 0)
            {
                    switchCursorSelection(pmenu, -pmenu->cols);
            }
            else 	//Paginated menu
            {
                    switchCursorSelection(pmenu, -1);
            }
        }
        else
        {
            previousMenu(pmenu);
        }
    }

    if(isButtonPressed(BUTTON_DOWN) || isButtonPressed(BUTTON_SELECT)==button_pressed)
    {
        waitButtonRelease(BUTTON_DOWN);
        waitButtonRelease(BUTTON_SELECT);

        if(pmenu->rows>1)
        {
            if(pmenu->cols > 0)
            {
                switchCursorSelection(pmenu, pmenu->cols);
            }
            else  	//Paginated menu
            {
                switchCursorSelection(pmenu, 1);
            }
        }
        else
        {
            nextMenu(pmenu);
        }
    }

    if(isButtonPressed(BUTTON_LEFT))
    {
        waitButtonRelease(BUTTON_LEFT);

        if(pmenu->cols > 1)
        {
            switchCursorSelection(pmenu, -1);
        }
        else if(pmenu->cols < 0)	//Possibly a paginated menu, no columns
        {
            switchCursorSelection(pmenu, -pmenu->rows);
        }
        else
        {
            previousMenu(pmenu);
        }
    }

    if(isButtonPressed(BUTTON_RIGHT))
    {
        waitButtonRelease(BUTTON_RIGHT);

        if(pmenu->cols > 1)
        {
            switchCursorSelection(pmenu, 1);
        }
        else if(pmenu->cols < 0)	//Possibly a paginated menu, no columns
        {
            switchCursorSelection(pmenu, pmenu->rows);
        }
        else
        {
            nextMenu(pmenu);
        }
    }

    //We can ovverride this part of logic with custom one
    if(pmenu->update == &menuInput)
    {
        if ( (isButtonPressed(BUTTON_START) || isButtonPressed(BUTTON_A) ) )
        {
            waitButtonRelease(BUTTON_START);
            waitButtonRelease(BUTTON_A);
            
            timer.start_time = fps.t;

            //~ printf("%p %d\n", pmenu->items[pmenu->curr_item].func, pmenu->curr_item);

            if(pmenu->items[pmenu->curr_item].func!=NULL)
            {
                pmenu->items[pmenu->curr_item].func();
                return;
            }
            #ifdef DEBUG_MENU
            else
            {
                printf("No action set\n");
            }
            #endif
        }

        //Close current menu or get back to previous
        if(isButtonPressed(BUTTON_B))
        {
            waitButtonRelease(BUTTON_B);

            if(!isMenuPersistent(pmenu))
            {
                unsetFlag(&pmenu->flags, menu_active);	//Hide menu (Close)
                pmenu->curr_item=0;
            }

            if(pmenu->previous)
            {
                previousMenu(pmenu);
            }
        }
    }

}
