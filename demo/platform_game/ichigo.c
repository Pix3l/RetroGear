/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  ichigo.c - Ichigo entity functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <math.h>

#include "ichigo.h"
#include "gfx.h"
#include "sfx.h"
#include "util.h"
#include "timer.h"
#include "player.h"
#include "score.h"
#include "camera.h"
#include "fps.h"
#include "game.h"
#include "collision.h"
#include "timer.h"
#include "tile.h"

#define SCORE 100
#define TYPE 4

static void onCollision();
static void onDestroy();
static void onAnimate();

/**
 * Ichigo entity constructor
 * Help programmers on setup the initial values for an ichigo entity
 **/
void ichigo_create(int id, int x, int y)
{
	int lives = 3;
	float animation_speed = 0.05f;
	float speed = 0.8f;
	float gravity = 0.05f;

	createEntity(ENEMY, x, y, ICHIGO_W, ICHIGO_H, lives, speed, gravity, &updateIchigo);

	if( !EntityList_Tail->sprite.surface )
	{
		initSprite(&EntityList_Tail->sprite, x, y, 16, 16, animation_speed, "data/ichigo.bmp");
	}
}

/**
 * Azioni da eseguire in caso di collisione della entity
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
static void onCollision(Entity *pobj)
{
	Entity *current = EntityList_Head;
	while(current!=NULL)
	{
		if(current!=pobj && pobj->flag_active && pobj->status!=KILL)
		{
			if(rectCollision(current->x, current->y, current->w, current->h, 
							(int)(pobj->x+pobj->direction_x), pobj->y, pobj->w, pobj->h))
			{
				pobj->direction_x *=-1;
			}
		}
		current=current->next;
	}
}

/**
 * Azioni da eseguire alla distruzione della entity
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
static void onDestroy(Entity *pobj)
{
	if(get_time_elapsed(pobj->timer[0]) >= 1)
	{
		pobj->status=DESTROY;
		pobj->visible=0;
	}
}

static void onAnimate(Entity *pobj)
{
    pobj->sprite.animation_timer += pobj->sprite.animation_speed;
    if (pobj->sprite.animation_timer > 2)
    {
        pobj->sprite.animation_timer = 0;
    }
    
    pobj->sprite.index = IDLELEFT + abs(pobj->sprite.animation_timer);
}

/**
 * Gestisce la vita della entity Ichigo nel gioco
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 * 
 * @return Entity *ptr
 * 		Puntatore all'oggetto successivo nel caso l'attuale venga 
 * 		distrutto
 **/
void updateIchigo(Entity *pobj)
{
    //Exit on the bottom of the screen
    if(pobj->y > SCREEN_HEIGHT)
    {
        pobj->status=DESTROY;
        return;
    }

	//~ if(pobj->direction==0)
	if(pobj->status==KILL)
	{
        pobj->sprite.index = 2;		//La entity schiacciata nello sprite sheet
		onDestroy(pobj);
		return;
	}

	//Stomp
	if(Player.vspeed > 1 && pobj->status!=KILL) 
	{
		if(rectCollision(Player.x, Player.y, Player.w, Player.h+1, 
						 pobj->x, pobj->y, pobj->w, pobj->h))
		{
			Player.vspeed = -3.4f;		//Make the player jump
			Player.y--;
			pobj->ystart=pobj->y;
			pobj->status=KILL;		//Set the entity to stomp
			pobj->direction_x=0;

			if(points_index<10)
			{
				playSound(stomp_snd);
			}

			createScore(((int)pobj->x-camera.x), ((int)pobj->y-camera.y), SCORE_SPEED, points_index);
			addScore(getScore(points_index));
			points_index++;
			
			pobj->timer[0]=fps.t;	//The moment the entity has been touched
			onDestroy(pobj);
			return;
		}
	}
	else 	//Bouncing
	{
		if(rectCollision((int)(curr_player->x+curr_player->direction_x), curr_player->y, curr_player->w, curr_player->h, 
						 pobj->x, pobj->y, pobj->w, pobj->h))
		{
			curr_player->vspeed = -3.4f;
			curr_player->status = KILL;
			
			//Stop any music from the game
			if(isMusicPlaying())
			{
				pauseMusic();
			}
			
			playSound(player_die_snd);
			
			setGameState(LOST);
		}
	}

	//Keep ichigo on platform, don't let it fall!
    if(getTile((pobj->direction_x>0)? pobj->x+TILESIZE : pobj->x, pobj->y+TILESIZE, 0)==0)
    {
        pobj->direction_x*=-1;
    }

	moveEntity_X(pobj);
	doEntityGravity(pobj);
	//~ animateEntity(pobj, 1);
    onAnimate(pobj);
	onCollision(pobj);
}
