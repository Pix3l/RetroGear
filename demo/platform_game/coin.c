/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    coin.c - Coin entity functions                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "coin.h"
#include "gfx.h"
#include "sfx.h"
#include "util.h"
#include "score.h"
#include "player.h"
#include "camera.h"
#include "collision.h"

#define SCORE 200
#define TYPE 5

static void onCollision();
static void onDestroy();
static void onAnimate();

/**
 * Ichigo entity constructor
 * Help programmers on setup the initial values for an ichigo entity
 **/
void coin_create(int id, int x, int y)
{
    float animation_speed = 0.08f;
    
	createEntity(COLLECTABLE, x, y, COIN_W, COIN_H, 1, 0, 0, &updateCoin);

	if( !EntityList_Tail->sprite.surface )
	{
		initSprite(&EntityList_Tail->sprite, x, y, 16, 16, animation_speed, "data/coin.bmp");
	}
}

/**
 * Azioni da eseguire in caso di collisione della entity
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
static void onCollision(Entity *pobj)
{
	if(rectCollision(curr_player->x, curr_player->y, curr_player->w, curr_player->h, 
					 pobj->x, pobj->y, pobj->w, pobj->h))
	{
		createScore(((int)pobj->x-camera.x), ((int)pobj->y-camera.y), 0.4f, 1);
		addScore(SCORE);
		
		playSound(collectable_snd);
		
		onDestroy(pobj);
	}
}

/**
 * Azioni da eseguire alla distruzione della entity
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
static void onDestroy(Entity *pobj)
{
	//~ int count = countTypeEntity(pobj->type);
	//~ printf("%d\n", count);
	
	if(countTypeEntity(pobj->type)==0)
	{
		//~ playSound(goal_snd);
		addScore(10000);
	}

	pobj->status=DESTROY;
}

static void onAnimate(Entity *pobj)
{
    pobj->sprite.animation_timer += pobj->sprite.animation_speed;
    if (pobj->sprite.animation_timer > 3)
    {
        pobj->sprite.animation_timer = 0;
    }
    
    pobj->sprite.index = IDLELEFT + abs(pobj->sprite.animation_timer);
}

/**
 * Gestisce la vita della entity Coin nel gioco
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 * 
 * @return Entity *ptr
 * 		Puntatore all'oggetto successivo nel caso l'attuale venga 
 * 		distrutto
 **/
void updateCoin(Entity *pobj)
{
	if(pobj->direction_x==0)
	{
		onDestroy(pobj);
		return;
	}
	
	onAnimate(pobj);
	onCollision(pobj);
}
