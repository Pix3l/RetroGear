/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             menu_paginator.c - Classic paginated game menu         *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "menu_paginator.h"
#include "controls.h"
#include "gfx.h"
#include "font.h"
#include "menu.h"
#include "timer.h"
#include "bit.h"

/**
 * Formats the menu overlapping items and dividing by the number of rows specified
 * 
 * @param Menu *pmenu
 * 		The menu to be paginated
 */
void paginateItems(Menu *pmenu)
{
	int i = 0, y = 0;
	for(i=0; i < pmenu->num_items; i++)
	{
		if( (i>0) && (i % pmenu->rows) == 0)
		{
			y = 0;
		}
		
		pmenu->items[i].x = pmenu->x+FONT_W;
		pmenu->items[i].y = pmenu->y+(y*FONT_H);
		y++;
	}
}

void setMenuPaginator(Menu* pmenu, int rows)
{
	pmenu->rows = rows;
	pmenu->cols = -1;
	
	pmenu->draw = &drawMenuPaginator;
	
	pmenu->h = pmenu->rows*FONT_H;
	
	paginateItems(pmenu);
}

void drawMenuPaginator(Menu *pmenu)
{
    if( !hasFlag(pmenu->flags, menu_active) )
    {
        return;
    }

    if( hasFlag(pmenu->flags, menu_show_border) )
    {
        //0x000000 is the background color
        drawGui(pmenu->x+FONT_W, pmenu->y+FONT_H, pmenu->w, pmenu->h, 0x000000, white);
    }

    if( hasFlag(pmenu->flags, menu_show_title) )
    {
	    drawString(pmenu->x+(FONT_W*2), pmenu->y-10, pmenu->name, pmenu->color, 0);
    }	

    //Check where we are
    int lenght = (pmenu->curr_item / pmenu->max_items) +1;

    //Calculate the starting item for the current page
    pmenu->page_start = lenght*pmenu->rows;

    //We are at the beginning of the menu or we are scrolling it back
    if( pmenu->curr_item < pmenu->page_start )
    {
	    pmenu->page_start-=pmenu->rows;
    }

    //If the number of elements in the page exceeds the number of elements present
    int rows = pmenu->rows;
    if( (pmenu->page_start + rows) > pmenu->num_items )
    {
	    //We calculate how many items remain to be drawn for that page
	    rows = pmenu->num_items - pmenu->page_start;
    }

    int i=0;
    for(i = pmenu->page_start; i < pmenu->page_start+rows; ++i)
    {
	    drawString(pmenu->items[i].x+FONT_W, pmenu->items[i].y+FONT_H, pmenu->items[i].name, pmenu->items[i].color, 0);
    }

    drawChar(pmenu->items[pmenu->curr_item].x, pmenu->items[pmenu->curr_item].y+FONT_H, pmenu->cursor, pmenu->color, 0);

    //If the initial item of the page + the items to show +1, exceeds the number of total elements, it means that we are on the last page
    //Return and stop showing the blinking arrow
    if(pmenu->page_start+rows+1 > pmenu->num_items)
    {
	    return;
    }

    //If there are other pages, we draw the flashing arrow to the right
    if(get_time_elapsed(5)%2)
    {
	    return;
    }
    drawChar(pmenu->x+pmenu->w+FONT_W, pmenu->y+pmenu->h+FONT_H, 26, pmenu->color, 0);
}
