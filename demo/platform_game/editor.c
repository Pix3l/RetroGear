/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   editor.c - Simple level editor					  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "editor.h"

void initEditor()
{
	camera.offsetX = 0;
	camera.shot.w = 15;		//Max columns displayed
	pick = 1;	//Il tile attualmente scelto

	//Colore di default: VERDE 0x00FF00
	Uint32 key = SDL_MapRGB( screen->format, 0, 255, 0 ); 
	obj_sheet=loadImage("data/obj_sheet.bmp", key);
	tile_sheet=loadImage("data/tile_sheet.bmp", key);
	
	curr_level->layer = 1;
	curr_sheet = tile_sheet;
}

/**
 * Gestisce gli input per l'editor di livello
 **/
void editorInput()
{
	getMousePosition();
	setMouseOnGrid();

	//~ printf("X %d\n", (-camera.offsetX+Mouse.x)/TILESIZE);

	while(SDL_PollEvent(&event))
	{
		if(event.type == SDL_KEYDOWN)
		{
			if(Mouse.x < (SCREEN_WIDTH-TILESIZE*5))
			{
				if(event.key.keysym.sym == SDLK_LEFT)
				{
					//~ printf("%d\n", camera.shot.x);
					if (camera.shot.w > 15)
					 updateCamera(&camera, TILESIZE, 0);
				}

				if(event.key.keysym.sym == SDLK_RIGHT)
				{
					//~ printf("%d %d\n",camera.shot.w, camera.maxOffsetX/TILESIZE);
					//~ if (camera.shot.w < camera.maxOffsetX/TILESIZE)
					//~ printf("%d %d\n",camera.shot.w, curr_level->cols);
					if (camera.shot.w < curr_level->cols)
					 updateCamera(&camera, -TILESIZE, 0);
				}

				if(event.key.keysym.sym == SDLK_UP)
				{
					if (camera.shot.y > 0)
					 updateCamera(&camera, 0, -TILESIZE);
				}
	
				if(event.key.keysym.sym == SDLK_DOWN)
				{
					if (camera.shot.h < camera.maxOffsetY/TILESIZE)
					 updateCamera(&camera, 0, TILESIZE);
				}
				
				if(event.key.keysym.sym == SDLK_1)	//Background layer 1
				{
					curr_level->layer = 1;
					curr_sheet = tile_sheet;
					setNotification(fps.t, "Background layer");
				}
				
				if(event.key.keysym.sym == SDLK_2)	//Solid layer 0
				{
					curr_level->layer = 0;
					curr_sheet = obj_sheet;
					setNotification(fps.t, "Entities layer");
				}

				if(event.key.keysym.sym == SDLK_c)	//Create map
				{
					initEditor();
					emptyLevel(&level);
					setNotification(fps.t, "New level map");
				}

				if(event.key.keysym.sym == SDLK_n)	//Next map
				{
					if(loadLevel(&level, curr_level->number+1))
					 initEditor();
					
					sprintf(sys_message,"Map %d loaded", curr_level->number);
					setNotification(fps.t, sys_message);
					
					//TODO: impostare lo scrolling ad inizio mappa!!!!
				}
				
				if(event.key.keysym.sym == SDLK_p)	//Previous map
				{
					if(curr_level->number>1)
					 if(loadLevel(&level, curr_level->number-1))
					  initEditor();
					  
					sprintf(sys_message,"Map %d loaded", curr_level->number);
					setNotification(fps.t, sys_message);
				}

				if(event.key.keysym.sym == SDLK_s)	//Save map
				{
					saveMap();
					setNotification(fps.t, "Map saved");
				}
			}
		}

		if(Mouse.x < (SCREEN_WIDTH-TILESIZE*5))
		{
			if(event.type == SDL_MOUSEBUTTONDOWN)
			{
				//Assegna un tile
				if(event.button.button == SDL_BUTTON_LEFT)
				{
					curr_level->map[curr_level->layer][(-camera.offsetY+Mouse.y)/TILESIZE]  
								   [(-camera.offsetX+Mouse.x)/TILESIZE] = pick;
				}
				
				//Cancella un tile
				if(event.button.button == SDL_BUTTON_RIGHT)
				{
					curr_level->map[curr_level->layer][(-camera.offsetY+Mouse.y)/TILESIZE]  
								   [(-camera.offsetX+Mouse.x)/TILESIZE] = 0;
				}
			}
		}
		else
		{
			if(event.type == SDL_MOUSEBUTTONDOWN)
			{
				//Preleva il valore di un tile dalla barra laterale
				if(event.button.button == SDL_BUTTON_LEFT || event.button.button == SDL_BUTTON_RIGHT)
				{
					int x = (Mouse.x-240)%5;
					int y = (Mouse.y /TILESIZE);
					
					if(y>0)
					 x+=5*y;
					
					pick = x;
					
					printf("Mouse %d\n", pick);
				}
			}
		}
	}


	if (camera.shot.x < 0)
	{
		camera.shot.x = 0;
	}
	else if (camera.shot.w > camera.maxOffsetX)
	{
		camera.shot.x--;
	}

}

void drawLevelEditor()
{
	
	//~ drawString(screen, 10, 40, message, cyan); //draw string with bmp font
	
	drawEntityMap(camera.shot, curr_level, curr_sheet);
	
	drawFillRect(SCREEN_WIDTH-TILESIZE*5, 0, 2, SCREEN_HEIGHT, WHITE);
	
	putImage(curr_sheet, screen, 0, 0, tiles->w, tiles->h, SCREEN_WIDTH-TILESIZE*5, 0);
	
	drawMousePointer();
	//~ printf("%d %d %d %d\n", camera.shot.x, camera.shot.y, camera.shot.w, camera.shot.h);
	//~ drawRect(camera.shot.x, camera.shot.y, camera.shot.w-1, camera.shot.h-1, ORANGE);
}
