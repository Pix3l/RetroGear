/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             draw.c - Game drawing events callback functions        *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "draw.h"
#include "gfx.h"
#include "tile.h"
#include "menu.h"
#include "font.h"
#include "player.h"
#include "game.h"
#include "score.h"
#include "bit.h"
#include "transition.h"
#include "timer.h"
#include "util.h"
#include "config.h"

/**
 * Funzione privata di callback per le notifiche di sistema sotto forma
 * di messaggi video.
 * 
 * Assegnando un contenuto alla variabile sys_message, ne viene mostrato
 * il contenuto a video in una posizione di default.
 **/
void callback_DrawSystemMessages()
{
    if(sys_message != NULL)
    {
        drawString(8, SCREEN_HEIGHT-16, sys_message, red, 0);

        if(get_time_elapsed(sys_timer.start_time) > 3)
        {
	        strcpy(sys_message, "");
	        sys_timer.start_time = 0;
        }
    }
}

/**
 * Disegna la schermata del titolo con relativo menu'
 **/
void drawTitle(void)
{
    drawSprite(&title, 16, 25);

	drawMenu(&main_menu);
	drawString(65, SCREEN_HEIGHT-16, "* PIX3LWORKSHOP *", white, 1);
}

/**
 * Draw pre-game screen
 **/
void drawPreGame(void)
{
    drawString(90, 50, curr_level->name, white, 1);

    curr_player->sprite.index = JUMPRIGHT;
    drawSprite(&curr_player->sprite, 93, 115);
    sprintf(message,"x %d", Player.lives);
	drawString(115, 117, message, white, 1);
}

/**
 * Draw main game
 **/
void drawGame(void)
{
    drawTileMap(curr_level);

    //Score
    drawString(160, 8, "Score", white, 1);
	sprintf(message,"%05d", Game.score);
	drawString(160, 16, message, white, 1);

    sprintf(message,"%s", curr_level->name);
	drawString(32, 8, message, white, 1);

    //Draw in-game entities
    Entity *current = EntityList_Head;
    while(current!=NULL)
    {
        drawEntity(current);
        current=current->next;
    }

    drawEntity(curr_player);
    drawScore();

    if(hasFlag(Game.flags, on_pause))
	{
		drawString(115, 117, "PAUSE", white, 0);
		return;
	}
}

/**
 * Draw victory screen
 **/
void drawWin(void)
{
	drawString(90, 117, "YOU WIN!", white, 1);
	drawString(60, 125, "THANKS FOR PLAYING!", white, 1);
	drawString(65, SCREEN_HEIGHT-16, "* PIX3LWORKSHOP *", white, 1);
}

/**
 * Draw game over screen
 **/
void drawGameOver(void)
{
	drawString(90, 117, "GAME OVER", white, 1);
}

void clearScreen()
{
	//If we have the level structure correctly initialized, we use the rgb background values stored in it
	if(Game.status>=GAME && Game.status<=LOST && curr_level!=NULL)
	{
		int r = curr_level->bkgd_red;
		int g = curr_level->bkgd_green;
		int b = curr_level->bkgd_blue;

		SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, r, g, b));
	}
	else
	{
        //Default background color
		SDL_FillRect(screen, NULL, BLACK); 
	}
}

void draw()
{
	if(transition.flag_active)
	{
		doTransition();
	}
	else
	{
		clearScreen();
	
		switch(Game.status)
		{
			case MENU:
				drawTitle();
				break;
			case PREGAME:
				drawPreGame();
				break;
			case GAME:
				drawGame();
				break;
			case LOST:
				drawGame();
				break;
			case WIN:
				drawWin();
				break;
			case GAMEOVER:
				drawGameOver();
				break;
		}
	}
	
	callback_DrawSystemMessages();

	//Update the screen
	#ifdef DOUBLE_SCREEN
		SDL_SoftStretch(screen, NULL, double_screen, NULL);
		//~ scaleScreen2X(screen, double_screen);
		SDL_Flip(double_screen);
	#else
		SDL_Flip(screen);
	#endif
}
