/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   camera.c - Scrolling functions                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "camera.h"
#include "tile.h"
#include "gfx.h"
#include "util.h"
#include "collision.h"
#include "config.h"

void initCamera(RG_rect *pcam)
{
	//Initialize camera at Player position
	pcam->x = (int)0;
	pcam->y = (int)0;
    
	//(Lunghezza della mappa - Dimensione della finestra)
	pcam->w = (int)(tilesToPixels(curr_level->cols) - SCREEN_WIDTH);
	pcam->h = (int)(tilesToPixels(curr_level->rows) - SCREEN_HEIGHT);
}

/**
 * void scrollCameraX()
 * 
 * Aggiorna lo scrolling sull'asse Y in base ad una coordinata X 
 * specificata in argomento.
 * 
 * @param int x
 * 		La coordinata X sulla quale basare lo scrolling della mappa
 * @param Camera *pcam
 * 		Puntatore alla struttura camera da usare per lo scrolling
 * 
 **/
void scrollCameraX(RG_rect *pcam, int x)
{
	//Update camera position
    #ifdef LAZY_SCROLL
        float offset = (x - pcam->x - CENTER_X) /20;
	    pcam->x += offset;
    #else
        pcam->x += (x - pcam->x - CENTER_X);
    #endif

	#ifndef NO_SCROLL_BOUND
		//Limit camera's movement inside the map width
		if (pcam->x < 0)
			pcam->x = 0;
		
		if (pcam->x > pcam->w)
			pcam->x = pcam->w;
	#endif
}

/**
 * Aggiorna lo scrolling sull'asse Y in base ad una coordinata Y 
 * specificata in argomento.
 * 
 * @param int y
 * 		La coordinata Y sulla quale basare lo scrolling della mappa
 * @param Camera *pcam
 * 		Puntatore alla struttura camera da usare per lo scrolling
 * 
 **/
void scrollCameraY(RG_rect *pcam, int y)
{
	//Update camera position
    #ifdef LAZY_SCROLL
        float offset = (y - pcam->y - CENTER_Y) /20;
	    pcam->y += offset;
    #else
        pcam->y += (y - pcam->y - CENTER_Y);
    #endif
	
	#ifndef NO_SCROLL_BOUND
		//Limit camera's movement inside the map heigth
		if (pcam->y < 0)
			pcam->y = 0;
		
		if (pcam->y > pcam->h)
			pcam->y = pcam->h;
	#endif
}

/**
 * Controlla che un determinato oggetto si trovi in un'area della mappa
 * attualmente visualizzata da camera, quindi disegnata e visible.
 * 
 * @param int x, int y, int w, int h
 *      Coordinate e dimensioni dell'oggetto da controllare
 * 
 * @return 0
 *      In caso l'oggetto in questione non sia inquadrato
 * 
 * @return 1
 *      In caso l'oggetto in questione sia inquadrato
 **/
int is_onCamera(RG_rect *pcam, RG_rect area)
{
	//Camera's rectangle
	if(rectCollision(pcam->x, pcam->y, SCREEN_WIDTH-1, SCREEN_HEIGHT-1,
					 area.x, area.y, area.w, area.h))
	{
		return 1;
	}
	
	return 0;
}
