/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    worm.c - Worm entity functions                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "worm.h"
#include "gfx.h"
#include "sfx.h"
#include "util.h"
#include "timer.h"
#include "player.h"
#include "score.h"
#include "camera.h"
#include "fps.h"
#include "game.h"
#include "collision.h"
#include "timer.h"
#include "tile.h"

#define SCORE 100
#define TYPE 3

static void onCollision();
static void onDestroy();

/**
 * Worm entity constructor
 * Help programmers on setup the initial values for a worm entity
 **/
void worm_create(int id, int x, int y)
{
	int lives = 3;
	float animation_speed = 0.05f;
	float speed = 1.0f;
	float gravity = 0.05f;

	createEntity(ENEMY, x, y+WORM_H, WORM_W, WORM_H, lives, speed, gravity, &updateWorm);

	if( !EntityList_Tail->sprite.surface )
	{
		initSprite(&EntityList_Tail->sprite, x, y, 16, 16, animation_speed, "data/worm.bmp");
        //~ EntityList_Tail->draw = drawWorm;   //custom drawing
	}
}

/**
 * Azioni da eseguire in caso di collisione della entity
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
static void onCollision(Entity *pobj)
{
	Entity *current = EntityList_Head;
	while(current!=NULL)
	{
		if(current!=pobj && current->flag_active && current->status!=KILL && current->type!=COLLECTABLE)
		{
			//~ //Evitiamo che i nemici si incastrino tra di loro
			//~ if(!rectCollision(current->x, current->y, current->w, current->h, 
							  //~ pobj->x, pobj->y, pobj->w, pobj->h))
			//~ {
				if(rectCollision(current->x, current->y, current->w, current->h, 
								(int)(pobj->x+pobj->direction_x), pobj->y, pobj->w, pobj->h))
				{
					pobj->direction_x *=-1;
				}
			//~ }
		}
		current=current->next;
	}
}

/**
 * Azioni da eseguire alla distruzione della entity
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
static void onDestroy(Entity *pobj)
{
	if(get_time_elapsed(pobj->timer[0]) >= 1)
	{
		pobj->status=DESTROY;
		pobj->visible=0;
	}
}

static void onAnimate(Entity *pobj)
{
    pobj->sprite.animation_timer += pobj->sprite.animation_speed;
    if (pobj->sprite.animation_timer > 2)
    {
        pobj->sprite.animation_timer = 0;
    }

    pobj->sprite.index = (pobj->direction_x < 0 ? WALKLEFT1 : WALKRIGHT1)+ abs(pobj->sprite.animation_timer);
}

/**
 * Gestisce la vita della entity Worm nel gioco
 * 
 * @param Entity *pobj
 * 		Puntatore all'oggetto da gestire
 **/
void updateWorm(Entity *pobj)
{
	if(pobj->status==KILL)
	{
		pobj->sprite.index = 12;		//La entity schiacciata nello sprite sheet
		onDestroy(pobj);
		return;
	}

	//Stomp
	if(Player.vspeed > 1) 
	{
		if(rectCollision(Player.x, Player.y, Player.w, Player.h+1, 
						 pobj->x, pobj->y, pobj->w, pobj->h))
		{
			Player.vspeed = -3.4f;		//Make the player jump
			Player.y--;
			pobj->ystart=pobj->y;
			pobj->status=KILL;		//Set the entity to stomp
			pobj->direction_x=0;

			if(points_index<10)
			{
				playSound(stomp_snd);
			}

			createScore(((int)pobj->x-camera.x), ((int)pobj->y-camera.y), SCORE_SPEED, points_index);
			addScore(getScore(points_index));
			points_index++;
			
			pobj->timer[0]=fps.t;	//Salviamo il momento in cui la entity viene schiacciata
			return;
		}
	}
	else 	//Bouncing
	{
		if(rectCollision((int)(Player.x+Player.direction_x), Player.y, Player.w, Player.h, 
						 pobj->x, pobj->y, pobj->w, pobj->h))
		{
			Player.vspeed = -3.4f;
			Player.status = KILL;
			
			//Stop any music from the game
			if(isMusicPlaying())
			{
				pauseMusic();
			}
			
			playSound(player_die_snd);
			
			setGameState(LOST);
			
			//Make the player bounce
			//~ if(Player.hspeed >= 0.1f)
			//~ {
				//~ Player.hspeed -= 1.0f;
			//~ }
			//~ else if(Player.hspeed <= 0.1f)
			//~ {
				//~ Player.hspeed += 1.0f;
			//~ }
			
			return;
		}
	}

	moveEntity_X(pobj);
	doEntityGravity(pobj);
	onAnimate(pobj);
	onCollision(pobj);
}

/**
 * Worm entity cleaner
 * Free resources allocated for the entity
 **/
void worm_clean()
{
	//~ if(worm_spr)
	//~ {
		//~ SDL_FreeSurface(worm_spr);
	//~ }
}
