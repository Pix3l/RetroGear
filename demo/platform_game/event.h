/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *              event.h - Level events handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _EVENT_H
#define _EVENT_H

#define MAX_PARAM_LENGTH 600

typedef enum
{
    WARP_EVT, DIALOGUE_EVT, OBJECT_EVT, GENERIC_EVT
} EVENT_TYPE;

typedef struct _Event {
	char mapname[20];
	unsigned int evt_x, evt_y;
	char parameters[MAX_PARAM_LENGTH];
	int flag_save;
	struct _Event *next;	//Next event in list
} Event;

Event *eventsGeneric,   //List for generic type events
      *eventsWarp,      //List for warp type events
      *eventsDialog;    //List for warp type events

void loadEvents(char *event_file, EVENT_TYPE evt_type);
int addEvent(Event **pEvent, char *mapname, unsigned int evt_x, unsigned int evt_y, char *parameters, int flag_save);
Event *getEvent(Event *pList, char *mapname, unsigned int evt_x, unsigned int evt_y);
void deleteEvent(Event **pList, char *mapname, unsigned int evt_x, unsigned int evt_y);
void debugEvents(Event *pointer);
void cleanEvents(Event **pList, int purge);

#endif
