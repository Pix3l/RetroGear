/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  player.h - Generic game main player               *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _PLAYER_H
#define _PLAYER_H

#define PLAYER_ID 2     //id 2 is reaserved for player entities in maps

#define PLAYER_SPRITE_W 16
#define PLAYER_SPRITE_H 16

#define PLAYER_W 12
#define PLAYER_H 16

#define MIN_H_SPEED -1.0f
#define MAX_H_SPEED 1.0f

#define MIN_V_SPEED -1.0f
#define MAX_V_SPEED 1.0f
#define MAX_RUN_SPEED 1.2f

#include "entity.h"

Entity Player;//[MAX_PLAYERS];
Entity *curr_player;

void initPlayer(Entity *player);
void setCurrentPlayer(Entity *player);
void setPlayerPosition(int x, int y);
void playerExtraLife();
void updatePlayer();
void drawPlayer();
void playerClimb();
void movePlayerX();
void movePlayerY();
void doPlayerGravity();
void doPlayerJump();
void playerDie();

#include "sfx.h"
//Extra variables
Mix_Chunk *player_snd, *player_die_snd;

#endif
