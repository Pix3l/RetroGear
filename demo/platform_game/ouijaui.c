/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             		ouija.c - Ingame virtual keyboard 	              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "ouijaui.h"
#include "util.h"
#include "font.h"
#include "controls.h"
#include "gfx.h"
#include "sfx.h"
#include "tile.h"
#include "menu.h"
#include "menu_classic.h"
#include "bit.h"

char ouija_str_holder[OUIJA_STR_MAX_LENGTH];
static char *ouija_alphabet = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

void initOuija()
{
    int max_items = strlen(ouija_alphabet)+2;

    initClassicMenu(&ouija, ouija_items, 16, 64, "Test", max_items, white, NULL, NULL);
    initStaticMenuItems(ouija_items, max_items);	//Clean ouija_items structure before use

    //Left outside DEL and DONE items from loop
    int i=0;
    for(i=0; i< (max_items-2); i++)
    {
        addStaticMenuItem(&ouija, "", 0, NULL, white, &ouija_appendCharacter);
        strncpy(ouija_items[i].name, ouija_alphabet+i, 1);
    }

    addStaticMenuItem(&ouija, "Del ", 0, NULL, white, &ouija_deleteCharacter);
    addStaticMenuItem(&ouija, "Done", 0, NULL, white, &ouija_done);

    setMenuTable(&ouija, 7, 10, 1*FONT_W, FONT_H);
    ouija.update = &doOuija;
    ouija.draw = &drawOuija;

    setFlag(&ouija.flags, menu_active|menu_show_border);

    setItemPosition(&ouija_items[63], ouija_items[63].x+(FONT_W*2), ouija_items[63].y);
}

/**
 * Assign a character to the string holder
 **/
void ouija_appendCharacter()
{
    int lenght = strlen(ouija_str_holder);
    if( lenght>=5 )
    {
	    return;
    }

    ouija_str_holder[lenght] = ouija.items[ouija.curr_item].name[0];

    //If character name is full, switch to the "Done" button
    if(strlen(ouija_str_holder)==5)
    {
	    ouija.curr_item = ouija.num_items-1;
    }
}

void ouija_deleteCharacter()
{
    int lenght = strlen(ouija_str_holder);
	if( lenght<=0 )
    {
	    return;
    }

    ouija_str_holder[lenght-1] = 0;
}

/**
 * Terminate ouija and switch to another game status
 **/
void ouija_done(void)
{
    unsetFlag(&ouija.flags, menu_active);
}

/**
 * Handles events for a character table style "Ouija"
 * 
 * @return int 0
 * 		The player hasn't finished yet
 * 
 * @return int 1
 * 		The player has finished yet
 **/
void doOuija()
{
	//~ if(curr_gamepad->button_Right && (ouija.curr_item>0 && ouija.curr_item%8==0))
	//~ {
		//~ printf("last column %d\n", ouija.curr_item);
		//~ nextMenu(&ouija);
		//~ setMenuActive(&ouija, 1);
//~ 
		//~ curr_gamepad->button_Right=0;
	//~ }
	
	menuInput(&ouija);

	if (!isButtonHeld(BUTTON_A) && isButtonPressed(BUTTON_A))
	{
        waitButtonRelease(BUTTON_A);
		
		if(ouija.items[ouija.curr_item].func)
		{
			ouija.items[ouija.curr_item].func();
		}
	}

	//Close menu or get back to previous
	if (!isButtonHeld(BUTTON_B) && isButtonPressed(BUTTON_B))
	{
        waitButtonRelease(BUTTON_B);
		
		if(ouija.items[ouija.curr_item].func && ouija.curr_item==63)
		{
			ouija.items[ouija.curr_item].func();
            return;
		}
		
		//~ playSound(hurt_snd);
	}
	
	if (!isButtonHeld(BUTTON_START) && isButtonPressed(BUTTON_START))
	{
		if(strlen(ouija_str_holder)<1)
		return;
		
        waitButtonRelease(BUTTON_START);
        unsetFlag(&ouija.flags, menu_active);
	}
}

void drawOuija(Menu *pmenu)
{
	//String holder label
	char *label = "Write player name";
	
	if( hasFlag(pmenu->flags, menu_show_border) )
	{
		//Characters table
		drawGui(pmenu->x, pmenu->y, pmenu->w, pmenu->h, 0x000000, white);
		
		//String holder
		drawGui(8, 8, (strlen(label)*FONT_W) + FONT_W, FONT_H*4, 0x000000, white);
	}
	
	drawString(16, 16, label, white, 0);
	drawString(32, 32, ouija_str_holder, white, 0);
	
	int i=0;
	for(i = 0; i < pmenu->num_items; ++i)
	{
	    //~ drawChar(pmenu->items[i].x+pmenu->x, pmenu->items[i].y+pmenu->y, pmenu->items[i].name[0], white, 0);
	    drawString(pmenu->items[i].x, pmenu->items[i].y, pmenu->items[i].name, white, 0);
	}
	
	if( hasFlag(pmenu->flags, menu_active) )
	{
	    drawChar(pmenu->items[pmenu->curr_item].x-FONT_W, pmenu->items[pmenu->curr_item].y, pmenu->cursor, pmenu->color, 0);
	}
}
