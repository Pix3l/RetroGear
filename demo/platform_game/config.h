/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                config.h - User defined configurations              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _CONFIG_H
#define _CONFIG_H

#include <SDL/SDL.h>

///This will enable SDL_Softstrech
//~ #define DOUBLE_SCREEN

///Enable Camera scrolling bounded to map limits
//~ #define NO_SCROLL_BOUND

///Enable Camera lazy scrolling
//~ #define LAZY_SCROLL

///Enable in-game events debugging
//~ #define DEBUG_EVENT

///Enable map debugging on load
//~ #define DEBUG_MAP

//User defined entities goes here

#endif
