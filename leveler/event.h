/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *              event.h - Level events handling functions             *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _EVT_H
#define _EVT_H

typedef struct _Event {
	char mapname[20];
	unsigned int evt_x, evt_y;
	char parameters[26];
	int flag_save;
	struct _Event *next;	//Next event in list
} Event;

Event *evt_headList, *evt_tailList;
Event evt_holder;	//Keep event data for the editor

void loadEvents(char *event_file);
void saveEvents();
void addEvent(char *mapname, unsigned int evt_x, unsigned int evt_y, char *parameters, int flag_save);
Event *getEvent(char *mapname, unsigned int evt_x, unsigned int evt_y);
char *getEventParameters(char *mapname, unsigned int evt_x, unsigned int evt_y);
void deleteEvent(unsigned int evt_x, unsigned int evt_y);
void cleanEvents();

#endif
