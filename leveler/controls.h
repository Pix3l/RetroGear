/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   controls.h - User input handling                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/
#ifndef _CONTROLS_H
#define _CONTROLS_H

#include <SDL/SDL.h>

#define DEAD_ZONE 3200  

#define BUTTON_A SDLK_x
#define BUTTON_B SDLK_z

#define BUTTON_START SDLK_RETURN
#define BUTTON_SELECT SDLK_LSHIFT

typedef struct _Gamepad {
	int button_A, button_B;
	int button_Start, button_Select;
	int button_Left, button_Right, button_Up, button_Down;	
} Gamepad;

//Gamepad structure (Keyboard / Gamepad)
Gamepad gamepad; //[2];

//Current gamepad (For multiplayer games)
Gamepad *curr_gamepad;

Uint8 *keystate; // keyboard state
SDL_Joystick *joystick_ptr; // Joystick device pointer

SDL_Event event;
Uint8 *keystate; // keyboard state

void initController();
void keyboardInput();

#endif
