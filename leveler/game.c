/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   game.c - Game handling function                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "game.h"
#include "fps.h"
#include "draw.h"
#include "controls.h"
#include "util.h"
#include "gui.h"
#include "rg_gui/rg_widget.h"

/**
 * void doTitleScreen()
 * 
 * Questa funzione gestisce la schermata dei titoli e relativo menu.
 * Permette al giocatore di scegliere varie opzioni di gioco.
 **/ 
void doTitleScreen()
{
	/**
	 * Since is possibile to have multiple menu on title screen we use
	 * a generic pointer
	 **/
}

/**
 * void doPreGame()
 * 
 * Questa funzione gestisce uno screen di pre-game.
 * Permette al giocatore di prendersi una manciata di secondi per
 * prepararsi prima di iniziare il gioco vero e proprio.
 **/ 
void doPreGame()
{
	//~ if(Game.status!=PREGAME)
	//~ {
		//~ setGameState(PREGAME);
	//~ }

    setGameState(GAME);
}

void doGame()
{
	//~ winz->callback(&winz);
	RG_updateWidgets();
}

/**
 * Gestisce gli eventi dello stato di vittoria
 **/
void doWin()
{

}

/**
 * Gestisce lo stato di game over.
 * A pressione del tasto INVIO reimposta i parametri di gioco allo stato
 * di default.
 **/
void doGameOver()
{

}

void doLogic()
{
	
}

void setGameState(int state)
{
	Game.status = state;
}

void mainLoop()
{
	static Uint32 then = 0;
	static unsigned int dtime = 0;

	//main game loop
	while(!quit)
	{
		keyboardInput();
		
		unsigned int maxl = 256;
		Uint32 now = SDL_GetTicks();
		dtime += now - then;
		fps.t = now;
		then = now;

		while (--maxl && dtime >= LOGICMS)
		{
			switch(Game.status)
			{
				case MENU:
					doTitleScreen();
					break;
				case PREGAME:
					doPreGame();
					break;
				case GAME:
					doGame();
					break;
				case LOST:
					doLogic();
					break;
				case WIN:
					doWin();
					break;
				case GAMEOVER:
					doGameOver();
					break;
			}
			
			now = SDL_GetTicks();
			dtime += now - then - LOGICMS;
			then = now;
		}

		render_time();
		
		draw();
		
		SDL_Delay(1);
	}
}
