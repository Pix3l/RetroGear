/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    editor.c - Level editor core                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "editor.h"
#include "level.h"
#include "gfx.h"
#include "tile.h"
#include "gui.h"
#include "level.h"
#include "fps.h"
#include "util.h"
#include "timer.h"
#include "event.h"
#include "rg_gui/rg_widget.h"
#include "rg_gui/rg_editfield.h"
#include "rg_gui/rg_listbox.h"
#include "rg_gui/rg_canvas.h"
#include "rg_gui/rg_messagebox.h"
//~ #include "events.h"

void initEditor()
{
	//~ initEventMenu();
//~ 
	//~ camera.offsetX = 0;
	//~ camera.shot.w = 15;		//Max columns displayed
	//~ camera.shot.h = 15;		//Max rows displayed
	//~ 
	//~ pick = 1;	//Il tile attualmente scelto
	//~ 
	//~ 
	//~ doEditor();
}

void createMap()
{
	curr_level = &level;
	emptyLevel(curr_level);
	
	RG_editfield *name_field = (RG_editfield*)mapname_field->data;
	RG_editfield *width_field = (RG_editfield*)mapwidth_field->data;
	RG_editfield *height_field = (RG_editfield*)mapheight_field->data;
	
	//~ printf("create map\n");

	int cols = atoi(width_field->store);
	int rows = atoi(height_field->store);

	if(cols == 0 && rows == 0)
	{
		curr_level->cols = COLS;
		curr_level->rows = ROWS;
	}

	//Check submitted values
	else if(cols < COLS && rows < ROWS)
	{
		curr_level->cols = cols;
		curr_level->rows = rows;
	}

	if(name_field->store != '\0')
	{
		//Save our map filename
		strcpy(curr_level->name, name_field->store);
	}
	else
	{
		strcpy(curr_level->name, "map");
	}
	
	//~ printf("%s %d %d\n", name_field->store, cols, rows);

	///Graphic set
	RG_listbox *sets = (RG_listbox*)sets_box->data;

	//Colore di default: VERDE 0x00FF00
	Uint32 key = SDL_MapRGB( screen->format, 0, 255, 0 );
	
	char path[80];
	//Layer 1
	snprintf(path, 80, "sets/%s/obj_sheet.bmp", sets->items[sets->curr_item].text);
	obj_sheet=loadImage(path, key);

	//Layer 2
	snprintf(path, 80, "sets/%s/tile_sheet.bmp", sets->items[sets->curr_item].text);
	tile_sheet=loadImage(path, key);
	
	curr_level->num_layers = 2;

	//Layer 3
	snprintf(path, 80, "sets/%s/alpha_sheet.bmp", sets->items[sets->curr_item].text);
	alpha_sheet=loadImage(path, key);

	if(alpha_sheet>0)
	 curr_level->num_layers = 3;

	//~ printf("%s\n",sets->items[sets->curr_item].text);
	
	curr_level->curr_layer = 1;
	strcpy(curr_level->theme, sets->items[sets->curr_item].text);
	curr_sheet = tile_sheet;
	pick = 1;
	flag_mode = BG;
	
	//Destroy new dialog now...
	mapname_field->flags=CLOSE;
	mapwidth_field->flags=CLOSE;
	mapheight_field->flags=CLOSE;
	sets_box->flags=CLOSE;
	mapnew_btn->flags=CLOSE;
	new_dlg->flags=CLOSE;
	new_dlg = NULL;
	
	//Setup canvas
	RG_canvas *data = (RG_canvas*)map_canvas->data;
	map_canvas->draw = &drawTileMap;
	map_canvas->update = &doMap;
	data->field_w = curr_level->cols;
	data->field_h = curr_level->rows;
	
	tiles_canvas->update = &doTilepicker;
	tiles_canvas->draw = &drawTilePicker;

	printf("create map...\n %d %d\n", data->field_w, data->field_h);
}

void loadMap()
{
	RG_listbox *file = (RG_listbox*)files_box->data;
	
	if(!file->items[file->curr_item].text)
	{
		RG_popup(50, 50, "Load Error", "No map available!", -1);

		//Destroy load dialog now...
		files_box->flags=CLOSE;
		mapload_btn->flags=CLOSE;
		load_dlg->flags=CLOSE;
		load_dlg = NULL;
		
		return;
	}
		
	curr_level = &level;
	emptyLevel(&level);
	loadLevel(curr_level, file->items[file->curr_item].text);

	///Graphics set///
	//Colore di default: VERDE 0x00FF00
	Uint32 key = SDL_MapRGB( screen->format, 0, 255, 0 );
	
	char path[80];
	//Layer 1
	snprintf(path, 80, "sets/%s/obj_sheet.bmp", curr_level->theme);
	obj_sheet=loadImage(path, key);

	//Layer 2
	snprintf(path, 80, "sets/%s/tile_sheet.bmp", curr_level->theme);
	tile_sheet=loadImage(path, key);
	
	//Layer 3
	if(curr_level->num_layers>=3)
	{
		snprintf(path, 80, "sets/%s/alpha_sheet.bmp", curr_level->theme);
		alpha_sheet=loadImage(path, key);
	}

	curr_level->curr_layer = 1;
	curr_sheet = tile_sheet;
	pick = 1;
	flag_mode = BG;

	//Destroy load dialog now...
	files_box->flags=CLOSE;
	mapload_btn->flags=CLOSE;
	load_dlg->flags=CLOSE;
	load_dlg = NULL;
	
	//Setup canvas
	RG_canvas *data = (RG_canvas*)map_canvas->data;
	map_canvas->draw = &drawTileMap;
	map_canvas->update = &doMap;
	data->field_w = curr_level->cols;
	data->field_h = curr_level->rows;
	data->offsetX=0;
	data->offsetY=0;
	
	tiles_canvas->update = &doTilepicker;
	tiles_canvas->draw = &drawTilePicker;
}

/**
 * Save the map file and show a in information popup
 **/
void saveMap()
{
	if(curr_level)
	{
		flag_mode = BG;
		saveLevel();
		saveEvents();
		char msg[80];
		snprintf(msg, 80, "Map %s has been saved!", curr_level->name);
		RG_popup(50, 50, "Map saved", msg, -1);
	}
	else
	{
		RG_popup(50, 50, "Error", "No map has been setup", -1);
	}
}

/**
 * Add a new event to the event list from the new event dialog
 **/
void saveEvent()
{
	RG_editfield *coord = (RG_editfield*)eventcoord_field->data;
	unsigned int evt_x, evt_y;
	sscanf(coord->store, "%u,%u", &evt_x, &evt_y);

	//Load parameters
	RG_editfield *param = (RG_editfield*)eventparam_field->data;
	char parameters[26];
	strcpy(parameters, param->store);

	addEvent(curr_level->name, evt_x, evt_y, parameters, 1);
	
	//Destroy the dialog
	eventcoord_field->flags=CLOSE;
	eventparam_field->flags=CLOSE;
	event_dlg->flags=CLOSE;
	event_dlg = NULL;
}

void switchGrid()
{
	//Disable the button if the map is not ready
	if(!curr_level)
	return;
	
	RG_canvas *data = (RG_canvas*)map_canvas->data;
	data->flag_showgrid = (data->flag_showgrid==1)? 0 : 1;
}

void setBackgroundLayer()
{
	//Disable the button if the map is not ready
	if(!curr_level)
	return;
	
	flag_mode = BG;
	
	curr_level->curr_layer = 1;
	curr_sheet = tile_sheet;
	setNotification(fps.t, "Background layer");
}

void setEntitiesLayer()
{
	//Disable the button if the map is not ready
	if(!curr_level)
	return;
	
	flag_mode = ENT;

	curr_level->curr_layer = 0;
	curr_sheet = obj_sheet;
	setNotification(fps.t, "Entities layer");
}

void setAlphaLayer()
{
	//Disable the button if the map is not ready
	if(!curr_level)
	return;
	
	flag_mode = ALPHA;
	
	if(alpha_sheet>0)
	{
		curr_level->curr_layer = 2;
		curr_sheet = alpha_sheet;
		setNotification(fps.t, "Alpha layer");
	}
	else
	{
		setNotification(fps.t, "No Alpha layer available");
	}
}

/**
 * Set editing mode to EVENT MODE
 **/
void setEventMode()
{
	//Disable the button if the map is not ready
	if(!curr_level)
	return;
	
	flag_mode = EVENT;
	setNotification(fps.t, "Event editor");
}

void doMap(RG_widget *pwig)
{
	RG_canvas *data = (RG_canvas*)pwig->data;

	//Editor shortcuts
	if(keystate[SDLK_1])	//Background layer 1
	{
		if(flag_mode==EVENT)
		return;

		setBackgroundLayer();
		keystate[SDLK_1] = 0;
	}
	
	if(keystate[SDLK_2])	//Solid layer 0
	{
		if(flag_mode==EVENT)
		return;

		setEntitiesLayer();
		//~ printf("Layer: %d\n", curr_level->curr_layer);

		keystate[SDLK_2] = 0;
	}

	if(keystate[SDLK_3])	//Alpha layer 2
	{
		if(flag_mode==EVENT)
		return;

		//~ if(2>getLayerSize(curr_level))
		if(curr_level->num_layers==3)
		{
			setAlphaLayer();
		}
		else
		{
			setNotification(fps.t, "Alpha layer not available!");
		}
		
		keystate[SDLK_3] = 0;
	}

	if(keystate[SDLK_e])	//Event editor
	{
		keystate[SDLK_e] = 0;
		setEventMode();
	}

	//End of editor shortcuts

	if(keystate[SDLK_LEFT])
	{
		data->offsetX-=data->cell_size;
		keystate[SDLK_LEFT] = 0;

		if(data->offsetX<0)
		{
			data->offsetX=0;
		}
	}
	
	int firstTileX = (data->offsetX / data->cell_size);
	int lastTileX = firstTileX + (pwig->w/data->cell_size);

	if(lastTileX<data->field_w)
	{
		if(keystate[SDLK_RIGHT])
		{
			keystate[SDLK_RIGHT] = 0;
			data->offsetX+=data->cell_size;
		}
	}

	//Up and down scroll
	if(keystate[SDLK_UP])
	{
		data->offsetY-=data->cell_size;
		keystate[SDLK_UP] = 0;

		if(data->offsetY<0)
		{
			data->offsetY=0;
		}
	}

	int firstTileY = (data->offsetY / data->cell_size);
	int lastTileY = firstTileY + (pwig->h/data->cell_size);
	
	if(lastTileY<data->field_h)
	{
		if(keystate[SDLK_DOWN])
		{
			keystate[SDLK_DOWN] = 0;
			data->offsetY+=data->cell_size;
		}
	}

	//Handling mouse clicks events
	if(rectCollision(Mouse.x, Mouse.y, 1, 1, pwig->x, pwig->y, pwig->w, pwig->h) && !Mouse.flag_hook)
	{
		int mouse_x = ((Mouse.x-pwig->x+data->offsetX) / data->cell_size );
		int mouse_y = (Mouse.y-pwig->y+data->offsetY) / data->cell_size;
		
		if(flag_mode==EVENT)
		{
			//Add an event
			if(Mouse.leftButton)
			{
				//Save new event position
				//~ evt_holder.evt_x = mouse_y;
				//~ evt_holder.evt_y = mouse_x;
				createDialog_event(mouse_x, mouse_y);
			} 
			
			//Delete an event
			if(Mouse.rightButton)
			{
				//~ deleteEvent(mouse_x, mouse_y);
			}
			
			return;
		}

		if(Mouse.leftButton)
		{
			curr_level->map[curr_level->curr_layer]
						   [mouse_y]
						   [mouse_x] = pick;
		} 
		
		if(Mouse.rightButton)
		{
			curr_level->map[curr_level->curr_layer]
						   [mouse_y]
						   [mouse_x] = 0;
		}
	}
}

//Preleva il valore di un tile dalla barra laterale
void doTilepicker(RG_widget *pwig)
{
	RG_canvas *data = (RG_canvas*)pwig->data;

	//Handling mouse clicks events
	if(rectCollision(Mouse.x, Mouse.y, 1, 1, pwig->x, pwig->y, pwig->w, pwig->h) && !Mouse.flag_hook)
	{
		int mouse_x = (Mouse.x-pwig->x+data->offsetX) / data->cell_size;
		int mouse_y = (Mouse.y-pwig->y+data->offsetY) / data->cell_size;

		if(mouse_y>0)
		{
			mouse_x+=TILESPERCOL*mouse_y;
	 	}
		
		if(Mouse.leftButton)
		{
			//~ printf("mouse pressed %d %d\n", mouse_x, mouse_y);
			pick = mouse_x;
		}
	}
}

void drawTileMap(RG_widget *pwig)
{
	//Retrive object data
	RG_canvas *data = (RG_canvas*)pwig->data;
	
	SDL_Rect rect;

	//Canvas body
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = pwig->w;
	rect.h = pwig->h;

    SDL_FillRect(screen, &rect, 0x000000);

	//White borders
	//Horizontal
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = pwig->w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Vertical
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = 1;
	rect.h = pwig->h-1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Gray borders
	//Horizontal
	rect.x = pwig->x+1;
	rect.y = pwig->y+pwig->h-1;
	rect.w = pwig->w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, WHITE);

	//Vertical
	rect.x = pwig->x+pwig->w-1;
	rect.y = pwig->y;
	rect.w = 1;
	rect.h = pwig->h-1;
	SDL_FillRect(screen, &rect, WHITE);

    //Check if we need to enable scrolling
    int canvas_w = pwig->w;
    if(data->cell_size*data->field_w < pwig->w)
    {
		canvas_w = data->cell_size*data->field_w;
	}

    int canvas_h = pwig->h;
    if(data->cell_size*data->field_h < pwig->h)
    {
		canvas_h = data->cell_size*data->field_h;
	} 

    int firstTileX = (data->offsetX / data->cell_size);
    int lastTileX = firstTileX + (canvas_w/data->cell_size);
    int firstTileY = (data->offsetY / data->cell_size);
    int lastTileY = firstTileY + (canvas_h/data->cell_size);
    
    SDL_Rect dest;

	int x, y, layer;
	for (y = firstTileY; y < lastTileY; y++)
	{
		for (x = firstTileX; x < lastTileX; x++)
		{
			int cell_x = (pwig->x+1)+(x * data->cell_size) - (firstTileX*data->cell_size);
			int cell_y = (pwig->y+1)+(y * data->cell_size) - (firstTileY*data->cell_size);
			
			//~ if(data->field[y][x]!=4)
				//~ drawFillRect(cell_x, cell_y , data->cell_size, data->cell_size, data->field[y][x]);
				
			dest.x = cell_x;
			dest.y = cell_y;
			dest.w = dest.h = data->cell_size;

			drawTile(tile_sheet, curr_level->map[1][y][x], &dest);
			drawTile(obj_sheet, curr_level->map[0][y][x], &dest);
			drawTile(alpha_sheet, curr_level->map[2][y][x], &dest);
		}
	}

	//Show the grid
	if(data->flag_showgrid && data->cell_size>2)
	{
		SDL_Rect line;

		int i;
		for(i = 0; i < (pwig->w-data->cell_size); i+=data->cell_size)
		{
			
			line.x = (pwig->x+data->cell_size)+i;
			line.y = pwig->y+1;
			line.w = 1;
			line.h = pwig->h-2;
			
			SDL_FillRect(screen, &line, 0x6d6d6d);
		}
		
		for(i = 0; i < (pwig->h-data->cell_size); i+=data->cell_size)
		{
			
			line.x = pwig->x+1;
			line.y = (pwig->y+data->cell_size)+i;
			line.w = pwig->w-2;
			line.h = 1;

			SDL_FillRect(screen, &line, 0x6d6d6d);
		}
	}
}

void drawTilePicker(RG_widget *pwig)
{
	//Retrive object data
	RG_canvas *data = (RG_canvas*)pwig->data;
	
	SDL_Rect rect;

	//Canvas body
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = pwig->w;
	rect.h = pwig->h;

    SDL_FillRect(screen, &rect, 0xAAAAAA);

	//White borders
	//Horizontal
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = pwig->w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Vertical
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = 1;
	rect.h = pwig->h-1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Gray borders
	//Horizontal
	rect.x = pwig->x+1;
	rect.y = pwig->y+pwig->h-1;
	rect.w = pwig->w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, WHITE);

	//Vertical
	rect.x = pwig->x+pwig->w-1;
	rect.y = pwig->y;
	rect.w = 1;
	rect.h = pwig->h-1;
	SDL_FillRect(screen, &rect, WHITE);

	putImage(curr_sheet, screen, 0, 0, curr_sheet->w, curr_sheet->h, pwig->x, pwig->y);

	//Show the grid
	if(data->flag_showgrid && data->cell_size>2)
	{
		SDL_Rect line;

		int i;
		for(i = 0; i < (pwig->w-data->cell_size); i+=data->cell_size)
		{
			
			line.x = (pwig->x+data->cell_size)+i;
			line.y = pwig->y+1;
			line.w = 1;
			line.h = pwig->h-2;
			
			SDL_FillRect(screen, &line, 0x6d6d6d);
		}
		
		for(i = 0; i < (pwig->h-data->cell_size); i+=data->cell_size)
		{
			
			line.x = pwig->x+1;
			line.y = (pwig->y+data->cell_size)+i;
			line.w = pwig->w-2;
			line.h = 1;

			SDL_FillRect(screen, &line, 0x6d6d6d);
		}
	}
	
	//Wrap the selected tile
	//~ drawRect(pwig->x+(1*pick*data->cell_size), pwig->y+(1*pick), data->cell_size, data->cell_size, 0xff0000);
}
