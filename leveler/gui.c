/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                gui.c - Simple SDL gui base on RetroGear			  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <ctype.h>
#include <dirent.h>

#include "gui.h"
#include "gfx.h"
#include "level.h"
#include "fps.h"
#include "editor.h"
#include "rg_gui/rg_widget.h"
#include "rg_gui/rg_window.h"
#include "rg_gui/rg_button.h"
#include "rg_gui/rg_editfield.h"
#include "rg_gui/rg_txtutils.h"
#include "rg_gui/rg_canvas.h"
#include "rg_gui/rg_listbox.h"
#include "editor.h"

void createDialog_new();
void createDialog_load();
void mainDialog();

void initGui()
{
	Uint32 key = SDL_MapRGB( screen->format, 255, 0, 255 );
	RG_icons = loadImage("data/rg_icons.bmp", key);
	
	mainDialog();
	curr_level = NULL;	//Override init() set, and wait the user create a new valid map
	
	RG_font_init();
}

void mainDialog()
{
	RG_createImageButton(5, 5, createDialog_new, NULL, 0);
	RG_createImageButton(25, 5, createDialog_load, NULL, 1);
	RG_createImageButton(45, 5, saveMap, NULL, 2);

	RG_createImageButton(70, 5, setBackgroundLayer, NULL, 3);
	RG_createImageButton(90, 5, setEntitiesLayer, NULL, 4);
	RG_createImageButton(110, 5, setAlphaLayer, NULL, 5);
	RG_createImageButton(130, 5, setEventMode, NULL, 6);
	//~ RG_createImageButton(150, 5, NULL, NULL, 7);
	RG_createImageButton(190, 5, switchGrid, NULL, 9);
	
	map_canvas = RG_createCanvas(5, 30, 240, 225, 16, 0, 0, NULL, NULL);
	tiles_canvas = RG_createCanvas(254, 30, 96, 225, 16, 0, 0, NULL, NULL);
}

void createDialog_new()
{
	if(new_dlg && new_dlg->flags==CLOSE)
	{
		new_dlg = NULL;
	}

	//if window not already exist
	if(!new_dlg)
	{
		new_dlg = RG_createWindow(50, 50, 180, 185, NULL, NULL, "New map");

		mapname_field = RG_createEditField(5, 25, NULL, new_dlg, "Map name", TEXFIELD);
		mapwidth_field = RG_createEditField(5, 55, NULL, new_dlg, "width", TEXFIELD);
		mapheight_field = RG_createEditField(5, 85, NULL, new_dlg, "height", TEXFIELD);
		
		sets_box = RG_createListBox (5, 115, new_dlg, "Graphic set");

		//Load graphics sets names
		DIR *d;
		struct dirent *dir;
		const char *d_name;

		d = opendir("./sets");
		if (d)
		{
			while ((dir = readdir(d)) != NULL)
			{
				d_name = dir->d_name;
				
				if (strcmp(d_name, "..") != 0 &&
					strcmp(d_name, ".") != 0)
				{
					RG_addListBoxItem(sets_box, dir->d_name);
				}
			}

			closedir(d);
		}

		mapnew_btn = RG_createButton(110, 160, createMap, new_dlg, "Create");
	}
}

void createDialog_load()
{
	//if window not already exist
	if(!load_dlg)
	{
		load_dlg = RG_createWindow(50, 50, 180, 185, NULL, NULL, "Load map");

		files_box = RG_createListBox (5, 25, load_dlg, "Map files");

		//Load graphics sets names
		DIR *d;
		struct dirent *dir;
		const char *d_name;
		char* ext;

		d = opendir("./maps");
		if (d)
		{
			while ((dir = readdir(d)) != NULL)
			{
				d_name = dir->d_name;

				ext = strchr(d_name,'.');

				if (strcmp(d_name, "..") != 0 &&
					strcmp(d_name, ".") != 0)
				{
					//List only .map files
					if(strcmp(ext+1, "map") == 0)
					{
						RG_addListBoxItem(files_box, d_name);
					}
				}
			}

			closedir(d);
		}

		mapload_btn = RG_createButton(110, 160, loadMap, load_dlg, "Open");
	}
}

void createDialog_event(int evt_x, int evt_y)
{
	//if window not already exist
	if(!event_dlg)
	{
		event_dlg = RG_createWindow(50, 50, 180, 185, NULL, NULL, "New event");

		eventparam_field = RG_createEditField(5, 25, NULL, event_dlg, "Params", TEXFIELD);

		eventcoord_field = RG_createEditField(5, 55, NULL, event_dlg, "Start coordinates", TEXFIELD);
		//Set coordinate into the field
		char coord[8];	//xxx,yyy
		snprintf(coord, 8, "%d,%d", evt_x, evt_y);
		RG_editfield *coord_field = (RG_editfield*)eventcoord_field->data;
		strcpy(coord_field->store, coord);
		coord_field->flag_active=0;

		//Generate e dialogue event if not empty
		event_dialog = RG_createEditField(5, 85, NULL, event_dlg, "Dialogue (Optional)", TEXFIELD);

		//~ newevent_btn = RG_createButton(110, 160, createEvent, event_dlg, "Create");
		newevent_btn = RG_createButton(110, 160, saveEvent, event_dlg, "Create");
	}
}
