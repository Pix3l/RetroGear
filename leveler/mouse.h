/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  mouse.c - Mouse handling functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _MOUSE_H
#define _MOUSE_H

#include <SDL/SDL.h>

struct _Mouse
{
	int x, y;
	int motion_x, motion_y;
	int leftButton, rightButton;
	int flag_hook;
} Mouse;

void getMousePosition();
void setMouseOnGrid(int grid_w, int grid_h);
void drawMousePointer();

#endif
