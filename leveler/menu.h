/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 menu.h - Game menu handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _MENU_H
#define _MENU_H

#include <SDL/SDL.h>

/**
 * Generic game menu structure
 * 
 * @param int flag_active
 * 		Active/deactive menu. Default 1.
 * 
 * @param int flag_title
 * 		Show/Hide menu title. Default 0.
 * 
 * @param int flag_border
 * 		Show/Hide menu border. Default 1.
 *
 * @param int x,y
 * 		Menu coordinate
 * 
 * @param unsigned int w,h
 * 		Menu dimensions (Used for drawing borders)
 * 
 * @param char name
 * 		Menu name
 * 
 * @param char cursor
 * 		Menu cursors character. Default '>'.
 *
 * @param int num_items
 * 		Number of items stored in the menu
 * 
 * @param int curr_item
 * 		Item actually selected
 * 
 * @param SDL_Color color
 * 		Menu color, affect border and cursor
 * 
 * @param _Item *items
 * 		Pointer to Item structure containing menu contents
 * 
 * @param _Menu *parent, *child
 * 		Parent Menu pointer, Child Menu Pointer.
 **/
typedef struct _Menu {
	int flag_active, flag_title, flag_border;
	int x, y;
	unsigned int w, h;
	char name[4];
	char cursor;
	int rows;
	int num_items;
	int curr_item;
	SDL_Color color;
	struct _Item *items;
	struct _Menu *parent, *child;
} Menu;

/**
 * Menu items structure
 * 
 * @param int id
 * 		Item ID
 * 
 * @param int x, y
 * 		Item coordinate
 * 
 * @param char *name
 * 		Item name	//TODO: Limit to 8 chars?
 * 
 * @param int value
 * 		Item value (Ex: price or quantity)
 * 
 * @param SDL_Color color
 * 		Item's text color
 * 
 * @param void (*func)()
 * 		Function to be called on item selected
 **/
typedef struct _Item
{
	int id;
	int x, y;
	char *name;
	int	value;
	SDL_Color color;
	void (*func)();
} Item;

Menu main_menu, *curr_menu;

void initMenu(Menu* pmenu, int x, int y, char *name, SDL_Color color, Menu *pprev, Menu *pnext);
Item* createItem(int id, char *texts, SDL_Color color, void (*func)()); //, Menu *pmenu)
void alignMenuCenter(Menu* pmenu);
void alignMenuBottom(Menu* pmenu);
void alignItemRow(Menu* pmenu, int rows, int flag_packed);
void setMenuActive(Menu* pmenu, int flag_active);
void setMenuBorder(Menu* pmenu, int flag_border);
void setMenuCursor(Menu* pmenu, char cursor);
void addMenuItem(Menu *pmenu, Item* pitem);
void deleteLastItem(Menu *pmenu);
void menuInput(Menu *pmenu);
void debugItems(Menu *pmenu);
void drawMenu(Menu *pmenu);
void destroyMenu(Menu *pmenu);

#endif
