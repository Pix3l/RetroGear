/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    level.c - Level file handling                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "level.h"
#include "tile.h"
#include "event.h"
#include "util.h"

/**
 * Crea una struttura di livello vuota, idonea per la creazione di un
 * nuovo livello di gioco
 *
 * @param struct _Level* plevel
 *      Puntatore ad una struttura di livello
**/
//TODO: rendere interattivo il processo richiedendo i dati di livello
int emptyLevel(Level* plevel)
{
	int i,j;

	plevel->number = 0;		//I nuovi livelli di default avranno 1

	//Load header info
	//~ strcpy(plevel->name, "New level");
	memset(plevel->name,0,sizeof(plevel->name));
	//Theme
	memset(plevel->name,0,sizeof(plevel->theme));

	/* (Song file for this level) */
	//~ strcpy(plevel->song_title, "song file name here");
	memset(plevel->song_title,0,sizeof(plevel->song_title));
	
	plevel->bkgd_red = 0;
	plevel->bkgd_green = 0;
	plevel->bkgd_blue = 0;
	
	plevel->num_layers= 2;

	//New level will have default cols and rows size
	plevel->cols = COLS;
	plevel->rows = ROWS;

	int layer;
	for(layer=0; layer < 3; layer++)
	{
		for(i=0; i < plevel->rows; i++)
		{
			for(j=0; j < plevel->cols; j++)
			{
				plevel->map[layer][i][j]=0;

				//~ printf ("%d,", plevel->map[layer][i][j]);
			}

			//~ printf("\n");
		}
		
		//~ printf("\n");
	}

  return 0;
}

/**
 * Load a level along with its parameters
 * 
 * @param struct _Level* plevel
 *      Pointer to the level holding structure
 * 
 * @param int level
 * 		Actual level number (Part of the level name)
 * 
 * @return
 *      -1 In case of fail
 * 
 **/
int loadLevel(Level* plevel, char *filename)
{
	int i,j;
	FILE * fi;
	char str[80];

	/* Load data file: */
	sprintf(str, "maps/%s", filename);

	fi = fopen(str, "r");
	if (fi == NULL)
	{
		#ifdef DEBUG_FILE_READ
			perror(str);
		#endif

		return -1;
	}

	//~ plevel->number = level;

	//Load header info
	/* (Level title) */
	fgets(str, 20, fi);
	strcpy(plevel->name, str);
	plevel->name[strlen(plevel->name)-1] = '\0';
	
	fgets(str, 20, fi);
	strcpy(plevel->theme, str);
	plevel->theme[strlen(plevel->theme)-1] = '\0';

	/* (Song file for this level) */
	fgets(str, sizeof(plevel->song_title), fi);
	strcpy(plevel->song_title, str);
	plevel->song_title[strlen(plevel->song_title)-1] = '\0';
	
	//Background color
	//~ fscanf (fi, "%6x", &plevel->background_color);
	fscanf (fi, "%d%*c%d%*c%d", &plevel->bkgd_red, &plevel->bkgd_green, &plevel->bkgd_blue);

	//~ printf("%d\n", plevel->bkgd_red);
	//~ printf("%d\n", plevel->bkgd_green);
	//~ printf("%d\n", plevel->bkgd_blue);

	//Layers
	fscanf (fi, "%d", &plevel->num_layers);
	
	//Level rows and cols
	fscanf (fi, "%02d%*c%02d", &plevel->cols, &plevel->rows);

	//~ printf("%d,%d\n",plevel->cols, plevel->rows);

	if(plevel->cols > COLS || plevel->rows > ROWS)
	{
		printf("Error: Map %s too big! Max allowed size is %d,%d\n", plevel->name, COLS, ROWS);
		quit=1;
		return -1;
	}

	int layer;
	for(layer=0; layer < curr_level->num_layers; layer++)
	{
		for(i=0; i < curr_level->rows; i++)
		{
			for(j=0; j < curr_level->cols; j++)
			{
				fscanf(fi, "%02d%*c", &curr_level->map[layer][i][j]);

				//~ printf ("%d,", plevel->map[layer][i][j]);
			}

			//~ printf("\n");
		}
		
		//~ printf("\n");
	}

	plevel->flag_complete=0;

	fclose(fi);
	
	loadEvents(filename);
	
	return 0;
}

void saveLevel()
{
	printf("Save map\n");

	int i,j;
	FILE * fp;

	char map_file[25];	//maps/file.map
	snprintf(map_file, 25, "maps/%s.map", curr_level->name);

	fp = fopen(map_file, "wb");
	if (fp == NULL)
	{
	  return;
	}

	/* (Level title) */
	fprintf(fp, "%s\n", curr_level->name);

	/* (Level theme) */
	fprintf(fp, "%s\n", curr_level->theme);
	
	/* (Song file for this level) */
	//~ fprintf(fp, "%s\n", curr_level->song_title);
	fprintf(fp, "dummy.wav\n");

	/* (Song file for this level) */
	//~ fprintf(fp, "%d,%d,%d\n", curr_level->bkgd_red, curr_level->bkgd_green, curr_level->bkgd_blue);
	fprintf(fp, "0,0,0\n");

	/* Layers */
	fprintf(fp, "%d\n", curr_level->num_layers);

	/* (Level width) & (Level height) */
	fprintf(fp, "%02d,%02d", curr_level->cols, curr_level->rows);

	fprintf(fp, "\n");

	int layer;
	for(layer=0; layer < curr_level->num_layers; layer++)
	{
		for(i=0; i < curr_level->rows; i++)
		{
			for(j=0; j < curr_level->cols; j++)
			{
				fprintf(fp, "%d,", curr_level->map[layer][i][j]);
			}
			fprintf(fp, "\n");
		}

		//Give an empty line between layers
		fprintf(fp, "\n");
	}

	fclose(fp);
	
	saveEvents();
}

void debugLevel(Level* plevel)
{
	int i,j;
	
	printf("[Debug] Level\n");
	printf("Name: %s\n", plevel->name);
	printf("Name: %s\n", plevel->theme);
	printf("song_title: %s\n", plevel->song_title);
	
	printf("Map:\n");
	for(i=0; i< plevel->rows; i++)
	{
		for(j=0; j< plevel->cols; j++)
		{
			printf ("%d,", plevel->map[1][i][j]);
		}
		printf("\n");
	}

	printf("Solid:\n");
	for(i=0; i< plevel->rows; i++)
	{
		for(j=0; j< plevel->cols; j++)
		{
			printf ("%d,", plevel->map[0][i][j]);
		}
		printf("\n");
	}
	
	printf("rows: %d\n", plevel->rows);
	printf("cols: %d\n", plevel->cols);
}
