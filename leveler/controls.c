/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   controls.c - User input handling                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <SDL/SDL.h>

#include "controls.h"
#include "util.h"

#ifdef HAVE_MOUSE
	#include "mouse.h"
#endif

//~ #define DEBUG_INPUT

void initController()
{
	//Initialize current gamepad pointer
	curr_gamepad = &gamepad; //[0];

	#ifdef HAVE_JOYSTICK
		//Debug informations
		//~ int joystick_count = SDL_NumJoysticks();
		//~ int joystick_buttons = SDL_JoystickNumButtons(joystick_ptr);
		//~ printf("Joystick has %d buttons\n", joystick_buttons);  
		//~ printf("Joystick has %d axes\n", SDL_JoystickNumAxes(joystick_ptr)); 
		
		//Initialize and setup the joysticks
		joystick_ptr = SDL_JoystickOpen(0);

	#endif
}

void keyboardInput()
{
	while (SDL_PollEvent(&event))
	{
		keystate = SDL_GetKeyState( NULL );
		
		switch (event.type)
		{

			/***********************************************************
			 * Keyboard input handling
			 **********************************************************/

			case SDL_KEYDOWN:

				if (event.key.keysym.sym == SDLK_ESCAPE)
					quit = 1;
				
				#ifdef USE_GAMEPAD

					//Generic gamepad buttons

					if(keystate[BUTTON_A])
					{
						#ifdef DEBUG_INPUT
							printf("Button A prressed\n");
						#endif
						curr_gamepad->button_A = 1;
					}

					if(keystate[BUTTON_B])
					{
						#ifdef DEBUG_INPUT
							printf("Button B prressed\n");
						#endif
						curr_gamepad->button_B = 1;
					}

					if(keystate[BUTTON_START])
					{
						#ifdef DEBUG_INPUT
							printf("Button Start prressed\n");
						#endif
						curr_gamepad->button_Start = 1;
					}

					if(keystate[BUTTON_SELECT])
					{
						#ifdef DEBUG_INPUT
							printf("Button Select prressed\n");
						#endif
						curr_gamepad->button_Select = 1;
					}
					
					/*******************************************************
					 * Direction and axis
					 ******************************************************/

					if(keystate[SDLK_LEFT]) //|| joystick axis...
					{
						#ifdef DEBUG_INPUT
							printf("Button Left prressed\n");
						#endif
						curr_gamepad->button_Left = 1;
					}

					if(keystate[SDLK_RIGHT]) //|| joystick axis...
					{
						#ifdef DEBUG_INPUT
							printf("Button Right prressed\n");
						#endif
						curr_gamepad->button_Right = 1;
					}

					if(keystate[SDLK_UP]) //|| joystick axis...
					{
						#ifdef DEBUG_INPUT
							printf("Button Up prressed\n");
						#endif
						curr_gamepad->button_Up = 1;
					}

					if(keystate[SDLK_DOWN]) //|| joystick axis...
					{
						#ifdef DEBUG_INPUT
							printf("Button Down prressed\n");
						#endif
						curr_gamepad->button_Down = 1;
					}

				break;

				case SDL_KEYUP:

					if(!keystate[BUTTON_A])
					{
						#ifdef DEBUG_INPUT
							printf("Button A released\n");
						#endif
						curr_gamepad->button_A = 0;
					}

					if(!keystate[BUTTON_B])
					{
						#ifdef DEBUG_INPUT
							printf("Button B released\n");
						#endif
						curr_gamepad->button_B = 0;
					}

					if(!keystate[BUTTON_START])
					{
						#ifdef DEBUG_INPUT
							printf("Button Start released\n");
						#endif
						curr_gamepad->button_Start = 0;
					}

					if(!keystate[BUTTON_SELECT])
					{
						#ifdef DEBUG_INPUT
							printf("Button Select released\n");
						#endif
						curr_gamepad->button_Select = 0;
					}

					/*******************************************************
					 * Direction and axis
					 ******************************************************/

					if(!keystate[SDLK_LEFT]) //|| joystick axis...
					{
						#ifdef DEBUG_INPUT
							printf("Button Left released\n");
						#endif
						curr_gamepad->button_Left = 0;
					}

					if(!keystate[SDLK_RIGHT]) //|| joystick axis...
					{
						#ifdef DEBUG_INPUT
							printf("Button Right released\n");
						#endif
						curr_gamepad->button_Right = 0;
					}

					if(!keystate[SDLK_UP]) //|| joystick axis...
					{
						#ifdef DEBUG_INPUT
							printf("Button Up released\n");
						#endif
						curr_gamepad->button_Up = 0;
					}

					if(!keystate[SDLK_DOWN]) //|| joystick axis...
					{
						#ifdef DEBUG_INPUT
							printf("Button Donw released\n");
						#endif
						curr_gamepad->button_Down = 0;
					}

				#endif

			break;
			
			/***********************************************************
			 * Joystick input handling
			 **********************************************************/

			#ifdef HAVE_JOYSTICK

				case SDL_JOYBUTTONDOWN:

					switch (event.jbutton.button)
					{
						case 1:
							#ifdef DEBUG_INPUT
								printf("Joystic A pressed\n");
							#endif
							
							curr_gamepad->button_A = 1;
						break;

						case 2:
							#ifdef DEBUG_INPUT
								printf("Joystic B pressed\n");
							#endif
							
							curr_gamepad->button_B = 1;
						break;

						case 8:
							#ifdef DEBUG_INPUT
								printf("Joystic Select pressed\n");
							#endif
							
							curr_gamepad->button_Select = 1;
						break;

						case 9:
							#ifdef DEBUG_INPUT
								printf("Joystic Start pressed\n");
							#endif
							
							curr_gamepad->button_Start = 1;
						break;

					}

				break;

				case SDL_JOYBUTTONUP:

					switch (event.jbutton.button)
					{
						case 1:
							#ifdef DEBUG_INPUT
								printf("Joystic A released\n");
							#endif

						curr_gamepad->button_A = 0;
						break;

						case 2:
							#ifdef DEBUG_INPUT
								printf("Joystic B released\n");
							#endif
							
							curr_gamepad->button_B = 0;
						break;

						case 8:
							#ifdef DEBUG_INPUT
								printf("Joystic Select released\n");
							#endif
							
							curr_gamepad->button_Select = 0;
						break;

						case 9:
							#ifdef DEBUG_INPUT
								printf("Joystic Start released\n");
							#endif
							
							curr_gamepad->button_Start = 0;
						break;
					}

				break;

				case SDL_JOYAXISMOTION:

					/* Horizontal movement */
					if (event.jaxis.axis == 0)
					{
						if (event.jaxis.value < -DEAD_ZONE)
						{
							#ifdef DEBUG_INPUT
								printf("Joystic Left pressed\n");
							#endif

							curr_gamepad->button_Left = 1;
						}

						else if (event.jaxis.value > DEAD_ZONE)
						{
							#ifdef DEBUG_INPUT
								printf("Joystic Right pressed\n");
							#endif
							curr_gamepad->button_Right = 1;
						}

						else
						{
							#ifdef DEBUG_INPUT
								printf("Joystic Left and Right released\n");
							#endif

							curr_gamepad->button_Left = 0;
							curr_gamepad->button_Right = 0;
						}
					}

					/* Vertical movement */
					if (event.jaxis.axis == 1)
					{
						if (event.jaxis.value < -DEAD_ZONE)
						{
							#ifdef DEBUG_INPUT
								printf("Joystic Up pressed\n");
							#endif

							curr_gamepad->button_Up = 1;
						}

						else if (event.jaxis.value > DEAD_ZONE)
						{
							#ifdef DEBUG_INPUT
								printf("Joystic Down pressed\n");
							#endif

							curr_gamepad->button_Down = 1;
						}
						else
						{
							#ifdef DEBUG_INPUT
								printf("Joystic Up and Down released\n");
							#endif

							curr_gamepad->button_Up = 0;
							curr_gamepad->button_Down = 0;
						}
					}

				break;

			#endif

			case SDL_QUIT:
				quit = 1;
			break;
			
			//Mouse support if required
			#ifdef HAVE_MOUSE
			
				case SDL_MOUSEMOTION:
					// update mouse position
					Mouse.x = event.motion.x;
					Mouse.y = event.motion.y;
				break;
				case SDL_MOUSEBUTTONDOWN:
					// update button down state if left-clicking
					if (event.button.button == SDL_BUTTON_LEFT)
					  Mouse.leftButton = 1;

					if (event.button.button == SDL_BUTTON_RIGHT)
					  Mouse.rightButton = 1;
				break;
				
				case SDL_MOUSEBUTTONUP:
					// update button down state if left-clicking
					if (event.button.button == SDL_BUTTON_LEFT)
					  Mouse.leftButton = 0;

					if (event.button.button == SDL_BUTTON_RIGHT)
					  Mouse.rightButton = 0;
				break;
			
			#endif
			
		}
	}
}
