/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                gui.h - Simple SDL gui base on RetroGear			  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _GUI_H
#define _GUI_H

#include "rg_gui/rg_widget.h"

RG_widget *event_dlg, *sets_box, *map_canvas, *tiles_canvas;

//New map
RG_widget *new_dlg, *mapname_field, *mapwidth_field, *mapheight_field, *mapnew_btn;

//Load map
RG_widget *load_dlg, *files_box, *mapload_btn;

//New event
RG_widget *eventparam_field, *eventcoord_field, *event_dialog, *newevent_btn;

void initGui();
void createDialog_event(int evt_x, int evt_y);
void drawUI();

#endif
