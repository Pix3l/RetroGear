#ifndef _RG_BUTTON_H
#define _RG_BUTTON_H

#include <SDL/SDL.h>
#include "rg_widget.h"

#define BUTTON_BG 0x525573
#define DEFAULT_BUTTON_H 20

#define TITLE_SIZE 12

typedef struct RG_button
{
	int x, y;
	char flag_clicked;
	char text[20];
	int icon_index;
	//~ SDL_Surface *image;
} RG_button;

RG_widget *RG_createButton (short x, short y, void(*callback) (), RG_widget *par, char *text);
RG_widget *RG_createImageButton (short x, short y, void(*callback) (), RG_widget *par, int icon_index);
void RG_doButton(RG_widget *pwig);
void RG_drawButton(RG_widget *pwig);

#endif
