#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "rg.h"
#include "rg_widget.h"
#include "rg_window.h"
#include "rg_button.h"
#include "rg_messagebox.h"
#include "rg_txtutils.h"
#include "gfx.h"
#include "tile.h"
#include "util.h"
#include "controls.h"

void onClose(RG_widget *pwig);

RG_widget *RG_createMessagebox(short x, short y, void(*callback)(), RG_widget *par, char *name, char *msg, int type)
{
    RG_widget *widget, *msgbox, *test;
    RG_messagebox *data = (RG_messagebox*)calloc(1, sizeof(RG_messagebox));

	//Store button coordinates from parent Widget
	data->x = 0;
	data->y = TITLE_SIZE*2;
    data->msg_type = type;
    if(msg)
    {
		strcpy(data->message, msg);
    }
    
    int w = strlen(msg)*RG_CHAR_W+(RG_CHAR_W*2);
    
    if(data->msg_type>0)
    {
		w+=16;	//Lift the text on the left for the icon
	}
    
    widget = RG_createWindow (x, y, w, DEFAULT_MESSAGEBOX_H, onClose, NULL, name);
    msgbox = RG_createChildWidget(x, y, w, 80, data, onClose, RG_drawMessageBox, widget);
    msgbox->update = RG_doMessageBox;
    test = RG_createButton((w-16)/2, y-10, callback, widget, "Ok");
    
    return widget;
}

void RG_doMessageBox(RG_widget *pwig)
{
	RG_messagebox *data = (RG_messagebox*)pwig->data;

	if(pwig->parent)
	{
		pwig->x = pwig->parent->x + data->x;
		pwig->y = pwig->parent->y + data->y;
	}
}

void onClose(RG_widget *pwig)
{
	//~ printf("Try to close: %p\n", pwig);
	RG.ptr = NULL;

	if(pwig->parent)
	{
		//~ printf("pwig->parent %p\n", pwig->parent);
		pwig->parent->flags=CLOSE;
		pwig->flags=CLOSE;
		//~ printf("%s\n", data->name);
	}
}

/**
 * This create a message box with onClose as default callback function.
 * Usefull for just show some warning or info messages.
 **/
void RG_popup(short x, short y, char *name, char *msg, int type)
{
	if(RG.ptr==NULL)
	{
		RG.ptr = RG_createMessagebox(x, y, onClose, NULL, name, msg, type);
	}
}

void RG_drawMessageBox(RG_widget *pwig)
{
    RG_messagebox *data = (RG_messagebox*)pwig->data;
    
    int x = pwig->x+RG_CHAR_W;

	//Show icon if requested and shift the text
    if(data->msg_type>=0)
    {
		SDL_Rect rect;
		rect.x = pwig->x+2;
		rect.y = pwig->y;
		rect.w = pwig->w-2;
		rect.h = pwig->h-2;

		getSprite(RG_icons, data->msg_type, 16, 16, &rect);
		
		x = pwig->x+RG_CHAR_W+TILESIZE;
	}

    RG_putString(screen, x, pwig->y, data->message, BLACK);
}
