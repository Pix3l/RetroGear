#ifndef _RG_EDITFIELD_H
#define _RG_EDITFIELD_H

#include "rg_widget.h"

#define EDITFIELD_BG 0x525573
#define DEFAULT_EDITFIELD_H 20

typedef struct RG_editfield
{
	int x, y;
	char flag_active;
	char label[20];
	char store[30]; 	//input data storer
} RG_editfield;

RG_widget *RG_createEditField (short x, short y, void(*callback) (), RG_widget *par, char *label, int type);
void RG_doEditField(RG_widget *pwig);
void RG_drawEditField(RG_widget *pwig);

#endif
