#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "rg.h"
#include "rg_widget.h"
#include "rg_window.h"
#include "rg_button.h"
#include "rg_txtutils.h"
#include "gfx.h"
#include "font.h"
#include "mouse.h"
#include "util.h"
#include "controls.h"

RG_widget *RG_createWindow (short x, short y, short w, short h, void(*callback) (), RG_widget *par, char *name)
{
    RG_widget *widget, *close_btn;
    RG_window *data = (RG_window*)calloc(1, sizeof(RG_window));

    data->_x = x;
    data->_y = y;
    data->_w = w;
    data->_h = h;
    data->flag_maximized = 0;

    if (name)
    {
		strcpy(data->name, name);
    }

    //If has parent, create as a child of
    if(par)
    {
		widget = RG_createChildWidget (x, y, w, h, data , callback, RG_drawWindow, par);
		//~ widget->parent = par;
	}
	else
	{
		widget = RG_createWidget(x, y, w, h, data , callback, RG_drawWindow);
		widget->parent = NULL;
	}

	widget->type = WINDOW;
	widget->update = RG_doWindow;

	//The close button
    //~ close_btn = RG_createButton(x+w-30, widget->y-50, NULL, widget, "x");
	//~ close_btn->x = x+(w-12);
	//~ close_btn->y = widget->y-2;
	//~ close_btn->w = 10;
	//~ close_btn->h = 10;

    return widget;
}

void RG_doWindow(RG_widget *pwig)
{
	RG_window *data = (RG_window*)pwig->data;
	
	if(Mouse.leftButton)
	{
		if(rectCollision(Mouse.x, Mouse.y, 1, 1, pwig->x, pwig->y, pwig->w-13, TITLE_SIZE) && !Mouse.flag_hook)
		{
			if(pwig->flags!=DRAG)
			{
				data->diff_x = Mouse.x - pwig->x;
				data->diff_y = Mouse.y - pwig->y;

				//Swap current widget to the head of the list for make it appear on top
				if(pwig != tailWidget)
				{
					swapWidget(pwig);
				}
			}

			pwig->flags = DRAG;
			Mouse.flag_hook = 1;
			
			RG.ptr = pwig;
			
			//~ printf("%d %d\n", offsetX, offsetY);
		}
		//~ return;

		//Window close
		if(rectCollision(Mouse.x, Mouse.y, 1, 1, pwig->x+(pwig->w-12), pwig->y+2, 10, 10) && !Mouse.flag_hook)
		{
			pwig->flags=CLOSE;
		}
	}
	else
	{
		data->diff_x = 0;
		data->diff_y = 0;
		pwig->flags = 0;
		Mouse.flag_hook = 0;
	}

	if(pwig->flags==DRAG)
	{
		//~ printf("pwig %d %d - diff %d %d - Mouse %d %d\n", pwig->x, pwig->y, diff_x, diff_y, Mouse.x, Mouse.y);
		
		pwig->x = Mouse.x - data->diff_x;
		pwig->y = Mouse.y - data->diff_y;
	}

	//Keep window inside main game window border	
	if(pwig->x < 0)
	{
		pwig->x = 1;
	}

	if(pwig->x+pwig->w > SCREEN_WIDTH)
	{
		pwig->x = SCREEN_WIDTH - pwig->w;
	}

	if(pwig->y < 0)
	{
		pwig->y = 1;
	}

	if(pwig->y+pwig->h > SCREEN_HEIGHT)
	{
		pwig->y = SCREEN_HEIGHT - pwig->h;
	}

	//~ RG_drawWindow();
}

static void onClose(RG_widget *pwig)
{
	pwig->flags=CLOSE;
}

void RG_drawWindow(RG_widget *pwig)
{
	//Retrive object data
	RG_window *data = (RG_window*)pwig->data;
	
	SDL_Rect rect;
	//~ 
	//~ //Draw window body
	//~ rect.x = pwig->x;
	//~ rect.y = pwig->y;
	//~ rect.w = pwig->w;
	//~ rect.h = pwig->h;
    //~ SDL_FillRect(screen, &rect, WINDOW_BG);
//~ 

	RG_drawWidget(pwig->x, pwig->y, pwig->w, pwig->h);

	///Draw window title
	rect.x = pwig->x+FONT_W;
	rect.y = pwig->y+RG_CHAR_W+2;
	rect.w = pwig->w-FONT_W*3;
	rect.h = 1;
    SDL_FillRect(screen, &rect, 0x6d6d6d);

    rect.y = pwig->y+RG_CHAR_W+3;
    SDL_FillRect(screen, &rect, WHITE);
    
    RG_putString(screen, pwig->x+RG_CHAR_W, pwig->y+2, data->name, BLACK);
    
	///Draw close button
	rect.x = pwig->x+(pwig->w-12);
	rect.y = pwig->y+2;
	rect.w = 10;
	rect.h = 10;
    RG_drawWidget(rect.x, rect.y, rect.w, rect.h);
    RG_putChar(screen, rect.x, rect.y, 'x', BLACK);
}
