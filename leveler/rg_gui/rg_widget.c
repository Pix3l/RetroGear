#include <stdlib.h>

#include "rg_widget.h"
#include "rg_listbox.h"
#include "gfx.h"
#include "util.h"
#include "mouse.h"

RG_widget *RG_createWidget (short x, short y, short w, short h, void *data, void(*callback)(), void(*draw)())
{
	//calloc alloc and initialize the memory
	RG_widget *pnew = (RG_widget*) calloc(1, sizeof(RG_widget));

	if(headWidget == NULL)
	{
		pnew->id = 1;
		headWidget = pnew;
		tailWidget = headWidget;
		//~ printf("New RG_widget HEAD\n");
	}
	else
	{
		pnew->id = tailWidget->id+1;
		tailWidget->next = pnew;
		tailWidget = pnew;
		//~ printf("New RG_widget TAIL\n");
	}
	
	tailWidget->next = NULL;
	tailWidget->children = NULL;
	
	tailWidget->x = x;
	tailWidget->y = y;
	tailWidget->w = w;
	tailWidget->h = h;
	//~ tailWidget->depth = 0;
	
	tailWidget->data = data;
	
	tailWidget->callback = callback;
	tailWidget->draw = draw;
	
	return tailWidget;
}

/**
 * Create an RG_widget object as a child of a given parent object and
 * append it to the parent internal list (*children)
 * 
 * @param short x, y, w, h
 * 		Coordinates and dimensions of the new widget
 * 
 * @param void *data
 * 		Reference to extra data structure
 * 
 * @param void(*callback)()
 * 		Reference to the update function
 * 
 * @param void(*draw)()
 * 		Reference to the drawing function
 * 
 * @param RG_widget *parent
 * 		Reference to the parent object
 * 
 * @return *RG_wiget
 * 		Return the last created child
 **/
RG_widget *RG_createChildWidget (short x, short y, short w, short h, void *data, void(*callback)(), void(*draw)(), RG_widget *parent)
{
	//calloc alloc and initialize the memory
	RG_widget *pnew = (RG_widget*) calloc(1, sizeof(RG_widget));
	RG_widget *previous = NULL;
	RG_widget *child = parent->children;

	#ifdef WIDGET_CHILDREN_DBG
		printf("Child new is: %p\n", pnew);
	#endif
	
	if(!child)
	{
		child = pnew;
		parent->children = child;
	}
	else
	{
		while(child != NULL)
		{
			previous = child;
			child = child->next;
		}
		
		child = pnew;
		previous->next = child;
	}

	child->next = NULL;
	
	child->x = x;
	child->y = y;
	child->w = w;
	child->h = h;
	
	child->data = data;
	
	child->callback = callback;
	child->draw = draw;
	child->parent = parent;

	#ifdef WIDGET_CHILDREN_DBG
		printf("This child parent is: %p\n", parent);
	#endif

	return child;
}

void RG_updateWidgets()
{
	RG_widget *widget = headWidget, *previous=NULL;
	while(widget!=NULL)
	{
		//Free memory from closed widgets
		if(widget->flags==CLOSE)
		{
			//Free children
			RG_widget *child = widget->children, *next_child=NULL;
			while(child!=NULL)
			{
				#ifdef DESTROY_DBG
					printf("Free a child\n");
				#endif
				
				next_child = child->next;
				free(child);
				child = next_child;
			}
			
			if(headWidget==widget && tailWidget==widget)
			{
				free(widget);
				headWidget=tailWidget=NULL;
				return;
			}

			if(headWidget==widget)
			{
				#ifdef DESTROY_DBG
					printf("head!\n");
				#endif

				headWidget = widget->next;
				free(widget);
				widget = headWidget;
			}
			else if(tailWidget==widget)
			{
				#ifdef DESTROY_DBG
					printf("tail!\n");
				#endif

				previous->next=NULL;
				tailWidget=previous;
				free(widget);
				widget = tailWidget;
			}
			else
			{
				previous->next = widget->next;
				free(widget);
				widget = previous->next;
			}
		}
		else	//Just update their lifecycle
		{
			if(widget->update!=NULL)
			{
				widget->update(widget);
			}

			//Update children lifecycle
			RG_widget *child = widget->children;
			while(child!=NULL)
			{
				if(child->update!=NULL)
				{
					child->update(child);
				}
				child = child->next;
			}
		}
//~ 
		previous = widget;
		previousWidget = widget;
		widget = widget->next;
	}
}

void swapWidget(RG_widget *pwig)
{
	RG_widget *tmp_tail = tailWidget;
	RG_widget *tmp_head = headWidget;
	RG_widget *my_next = NULL;
	
	//Widget is already first
	if(headWidget==pwig)
	{
		return;
	}

	//Only one widget, no need to swap
	if(headWidget==tailWidget)
	{
		return;
	}
	
	//NOTA: Il primo ad essere disegnato in cima è l'ultimo elemento
	if(headWidget==pwig)
	{
		tailWidget->next = pwig->next;
		headWidget->next = NULL;
		
		headWidget = tailWidget;
		tailWidget = tmp_head;
	}
	else
	{
		previousWidget->next = headWidget; //widget->next;

		my_next = headWidget->next;	//Save head next
		headWidget->next = pwig->next;
		
		pwig->next = my_next;
		headWidget = pwig;
	}

	//~ headWidget = pwig;
}

void cleanWidgets()
{
	printf("Clear widgets ");
	RG_widget *current = headWidget, *next=NULL, *child=NULL, *next_child=NULL;
	while(current!=NULL)
	{
		next=current->next;
		
		//Clean list elements
		if(current->type==LIST)
		{
			RG_listbox *data = (RG_listbox*)current->data;
			free(data->items);
			printf(".");
		}
		
		//Clean children
		child = current->children;
		while(child!=NULL)
		{
			next_child = child->next;
			free(child);
			child = next_child;
			printf(".");
		}
		
		free(current);
		current=next;
		printf(".");
	}
	
	printf("\n");

    headWidget=NULL;
    tailWidget=NULL;
    
    SDL_FreeSurface(RG_icons);
}

/**
 * Draw a generic widget graphic
 **/
void RG_drawWidget(int x, int y, int w, int h)
{
	SDL_Rect rect;

	///Widget body
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;

    SDL_FillRect(screen, &rect, 0xaaaaaa);

	///White borders
	//Horizontal
	rect.x = x;
	rect.y = y;
	rect.w = w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, WHITE);

	//Vertical
	rect.x = x;
	rect.y = y;
	rect.w = 1;
	rect.h = h-1;
	SDL_FillRect(screen, &rect, WHITE);

	///Gray borders
	//Horizontal
	rect.x = x+1;
	rect.y = y+h-1;
	rect.w = w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Vertical
	rect.x = x+w-1;
	rect.y = y;
	rect.w = 1;
	rect.h = h-1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);
}

void RG_drawField(int x, int y, int w, int h)
{
	SDL_Rect rect;

	///Widget body
	rect.x = x;
	rect.y = y;
	rect.w = w;
	rect.h = h;

    SDL_FillRect(screen, &rect, WHITE);

	///White borders
	//Horizontal
	rect.x = x;
	rect.y = y;
	rect.w = w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, BLACK);

	//Vertical
	rect.x = x;
	rect.y = y;
	rect.w = 1;
	rect.h = h-1;
	SDL_FillRect(screen, &rect, BLACK);

	///Gray borders
	//Horizontal
	rect.x = x;
	rect.y = y+h-1;
	rect.w = w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Vertical
	rect.x = x+w-1;
	rect.y = y;
	rect.w = 1;
	rect.h = h;
	SDL_FillRect(screen, &rect, 0x6d6d6d);
}

void drawUI()
{
	RG_widget *tmp = headWidget, *child;
	while(tmp!=NULL)
	{
		if(tmp->draw!=NULL)
		{
			tmp->draw(tmp);
		}

		child = tmp->children;
		while(child!=NULL)
		{
			if(child->draw!=NULL)
			{
				child->draw(child);
			}
			child = child->next;
		}

		tmp = tmp->next;
	}

	//~ putImage(RG_font, screen, 0, 0, SCREEN_WIDTH, SCREEN_HEIGHT, 0, 0);
	
	//~ RG_putChar(screen, 10, 10, 'a', 0xff0000);
	//~ RG_putString(screen, 10, 10, "hello world!", 0xff0000);
}
