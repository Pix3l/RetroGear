#include <SDL/SDL.h>
#include <stdlib.h>
#include <string.h>

#include "rg_widget.h"
#include "rg_button.h"
#include "rg_txtutils.h"
#include "gfx.h"
#include "mouse.h"
#include "util.h"
#include "controls.h"

/**
 * Create a simple button
 * 
 * @param int x, int y
 * 		The button coordinate
 * 
 * @param void(*callback) ()
 * 		Callback function to call on click
 * 
 * @param RG_widget *par
 * 		The parent widget
 * 
 * @param char *text
 * 		String to put over the button.
 * 		Set NULL if you want to use an image
 **/
RG_widget *RG_createButton (short x, short y, void(*callback) (), RG_widget *par, char *text)
{
    RG_widget *widget;
    RG_button *data = (RG_button*)calloc(1, sizeof(RG_button));

	//Store button coordinates from parent Widget
	data->x = x;
	data->y = y;

    if(text)
    {
		strcpy(data->text, text);
    }
    
    data->icon_index = -1;


    //If has parent, create as a child of
    if(par)
    {
		//~ printf("button parent is: %p\n", par);
		widget = RG_createChildWidget(x, y, strlen(text)*RG_CHAR_W+(RG_CHAR_W*2), DEFAULT_BUTTON_H, data, callback, RG_drawButton, par);
		//~ RG_createChildWidget(x, y, strlen(text)*RG_CHAR_W+(RG_CHAR_W*2), DEFAULT_BUTTON_H, data, callback, RG_drawButton, par);
		//~ widget->parent = par;
		//~ printf("button is: %p\n", widget);
	}
	else
	{
		widget = RG_createWidget(x, y, strlen(text)*RG_CHAR_W+(RG_CHAR_W*2), DEFAULT_BUTTON_H, data, callback, RG_drawButton);
		widget->parent = NULL;
	}
	
	widget->update = RG_doButton;
	widget->callback = callback;
	
	///////////////////////////////////////////////////////////////////////////

	//~ widget->type = BUTTON;
	//~ widget->update = RG_doButton;


	//~ widget = RG_createWidget(x, y, strlen(text)*RG_CHAR_W+(RG_CHAR_W*2), DEFAULT_BUTTON_H, data, callback, RG_drawButton);
	//~ widget->parent = par;
	//~ widget->type = BUTTON;
	//~ widget->update = RG_doButton;

    return widget;
}

RG_widget *RG_createImageButton (short x, short y, void(*callback) (), RG_widget *par, int icon_index)
{
    RG_widget *widget;
    RG_button *data = (RG_button*)calloc(1, sizeof(RG_button));

	//Store button coordinates from parent Widget
	data->x = x;
	data->y = y;

    data->icon_index = icon_index;


    //If has parent, create as a child of
    if(par)
    {
		widget = RG_createChildWidget(x, y, 20, 20, data, callback, RG_drawButton, par);
		widget->parent = par;
	}
	else
	{
		widget = RG_createWidget(x, y, 20, 20, data, callback, RG_drawButton);
		widget->parent = NULL;
	}
	
	//~ printf("callback %p\n", callback);
	
	widget->update = RG_doButton;
	widget->callback = callback;

	///////////////////////////////////////////////////////////////////////////

	//~ widget = RG_createWidget(x, y, 20, 20, data , callback, RG_drawButton);
	//~ widget->parent = NULL;
//~ 
    //~ widget->type = BUTTON;
    //~ widget->update = RG_doButton;

    return widget;
}

void RG_doButton(RG_widget *pwig)
{
	RG_button *data = (RG_button*)pwig->data;

	if(Mouse.leftButton)
	{
		if(rectCollision(Mouse.x, Mouse.y, 1, 1, pwig->x, pwig->y, pwig->w, pwig->h) && !Mouse.flag_hook)
		{
			//~ printf("region hit\n");
			if(Mouse.leftButton)
			{
				data->flag_clicked=1;
				Mouse.flag_hook = 1;
			}
		}
	}

	if(!Mouse.leftButton && data->flag_clicked)
	{
		data->flag_clicked=0;
		Mouse.flag_hook = 0;
		//~ printf("button clicked!\n");

		if(pwig->callback)
		{
			pwig->callback(pwig);
		}
	}

	if(pwig->parent)
	{
		pwig->x = pwig->parent->x + data->x;
		pwig->y = pwig->parent->y + data->y;
	}
}

void RG_drawButton(RG_widget *pwig)
{
	//Retrive object data
	RG_button *data = (RG_button*)pwig->data;
	
	SDL_Rect rect;

	//Button body
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = pwig->w;
	rect.h = pwig->h;

	int bgcolor = (!data->flag_clicked)? 0xAAAAAA : 0x6d6d6d;
    SDL_FillRect(screen, &rect, bgcolor);

	//White borders
	//Horizontal
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = pwig->w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, WHITE);

	//Vertical
	rect.x = pwig->x;
	rect.y = pwig->y;
	rect.w = 1;
	rect.h = pwig->h-1;
	SDL_FillRect(screen, &rect, WHITE);

	//Gray borders
	//Horizontal
	rect.x = pwig->x+1;
	rect.y = pwig->y+pwig->h-1;
	rect.w = pwig->w-1;
	rect.h = 1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);

	//Vertical
	rect.x = pwig->x+pwig->w-1;
	rect.y = pwig->y;
	rect.w = 1;
	rect.h = pwig->h-1;
	SDL_FillRect(screen, &rect, 0x6d6d6d);
	
	if(data->text)
	{
    	RG_putString(screen, pwig->x+RG_CHAR_W, pwig->y+4, data->text, 0x000000);
    }
    
    //TODO: gestire meglio l'allineamento dell'immagine
	rect.x = pwig->x+2;
	rect.y = pwig->y+2;
	rect.w = pwig->w-2;
	rect.h = pwig->h-2;
    if(data->icon_index>=0)
    {
		getSprite(RG_icons, data->icon_index, 16, 16, &rect);
	}
}
