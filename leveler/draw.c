/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             draw.c - Game drawing events callback functions        *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "draw.h"
#include "gfx.h"
#include "font.h"
#include "tile.h"
#include "game.h"
#include "util.h"
#include "timer.h"
#include "rg_gui/rg_widget.h"

/**
 * Funzione privata di callback per le notifiche di sistema sotto forma
 * di messaggi video.
 * 
 * Assegnando un contenuto alla variabile sys_message, ne viene mostrato
 * il contenuto a video in una posizione di default.
 **/
void callback_DrawSystemMessages()
{
	if(sys_message != NULL)
	{
		drawString(screen, 8, SCREEN_HEIGHT-16, sys_message, red, 0);

		if(getSeconds(sys_timer.start_time) > 3)
		{
			strcpy(sys_message, "");
			sys_timer.start_time = 0;
		}
	}
}

/**
 * Disegna la schermata del titolo con relativo menu'
 **/
void drawTitle()
{

}

/**
 * Disegna la schermata di pre-gioco
 **/
void drawPreGame()
{

}

/**
 * Disegna la schermata di pre-gioco
 **/
void drawGame()
{
	drawUI();
}

/**
 * Disegna la schermata di vittoria
 **/
void drawWin()
{

}

/**
 * Disegna la schermata di game over
 **/
void drawGameOver()
{

}

void clearScreen()
{
	SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 170, 170, 170)); 
}

void draw()
{
	clearScreen();

	switch(Game.status)
	{
		case MENU:
			drawTitle();
			break;
		case PREGAME:
			drawPreGame();
			break;
		case GAME:
			drawGame();
			break;
		case LOST:
			drawGame();
			break;
		case WIN:
			drawWin();
			break;
		case GAMEOVER:
			drawGameOver();
			break;
	}
	
	callback_DrawSystemMessages();

	//Update the screen
	#ifdef DOUBLE_SCREEN
		SDL_SoftStretch(screen, NULL, double_screen, NULL);
		//~ scaleScreen2X(screen, double_screen);
		SDL_Flip(double_screen);
	#else
		SDL_Flip(screen);
	#endif
}
