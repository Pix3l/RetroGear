/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   editor.c - Main level editor file				  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _EDITOR_H
#define _EDITOR_H

#include <SDL/SDL.h>
#include "util.h"
#include "menu.h"
#include "mouse.h"
#include "controls.h"
#include "rg_gui/rg_widget.h"

#define TILESPERCOL 6

//Possible editing modes
typedef enum
{
	BG, ENT, ALPHA, EVENT
} EDITING_MODE;

int flag_mode;	//Editing mode flag

int pick;	//Tile index value from sidebar

//Objects, background, transparent
SDL_Surface *obj_sheet, *tile_sheet, *alpha_sheet;

SDL_Surface *curr_sheet;	//Puntatore allo sprite sheet in uso

void initEditor();
void loadGraphicSets();

void createMap();
void loadMap();
void saveMap();
void saveEvent();
void switchGrid();
void setBackgroundLayer();
void setEntitiesLayer();
void setAlphaLayer();

void editorInput();
void doMap(RG_widget *pwig);
void doTilepicker(RG_widget *pwig);

void setEventMode();

void drawLevelEditor();
void drawTileMap(RG_widget *pwig);
void drawTilePicker(RG_widget *pwig);

void cleanEditor();

#endif
