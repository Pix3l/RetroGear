/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                font.c - Image font handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _FONT_H
#define _FONT_H

#define FONT_W 8
#define FONT_H 8

#include <SDL/SDL.h>

char message[35];	//Testo generico

void initFont();
void drawChar(SDL_Surface *screen, SDL_Surface *bmpfont, int X, int Y, int w, int h, int asciicode, SDL_Color color, int alpha);
void drawString(SDL_Surface *screen, int X, int Y, char *text, SDL_Color color, int alpha);

#endif
