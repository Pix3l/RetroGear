/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 menu.h - Game menu handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "menu.h"
#include "font.h"
#include "gfx.h"
#include "controls.h"
#include "timer.h"
#include "fps.h"

/**
 * Inizializza un oggetto Menu, permettendo di associarlo ad altri
 * 
 * @param Menu *pmenu
 * 		Puntatore all'oggetto Menu ad inizializzare
 * 
 * @param int active
 * 		Flag menu attivo/disattivo
 * 		Attivo significa attualmente in uso, TODO: controllare altri menu!!!!!
 *
 * @param int x, y
 * 		Coordinate del menu'
 * 
 * @param int w, h
 * 		Larghezza ed altezza del contenitore del menu'
 * 
 * @param char *name
 * 		Nome del menu'
 * 
 * @param Menu *pprev
 * 		Menu' precedente
 *
 * @param Menu *pnext
 * 		Menu' successivo
 **/
void initMenu(Menu* pmenu, int active, int x, int y, char *name, Menu *pprev, Menu *pnext)
{
	pmenu->active = active;
	pmenu->x = x;
	pmenu->y = y;
	strcpy(pmenu->name, name);
	pmenu->previous = pprev;
	pmenu->next = pnext;
}

/**
 * Crea un oggetto di tipo Item da poter aggiungere ad un menu'
 * 
 * @param int id
 * 		L'id della voce di menu
 *
 * @param char *text
 * 		Testo della voce di menu
 *
 * @param unsigned int field_size
 * 		Dimensione del campo di testo associato alla voce di menu
 * 
 * @param int x, y
 * 		Coordinate della voce all'interno del menu
 * 
 * @param SDL_Color color
 * 		Colore del testo
 * 
 * @param void (*func)()
 * 		Funzione associata alla voce di menu
 **/
Item* createItem(int id, char *text, unsigned int field_size,
				 int field_type, SDL_Color color, void (*func)())
{
	#ifdef DEBUG
		if(func==NULL)
		{
			printf("Function pointer not initialized!\n");
		}
	#endif

	Item *pnew = (Item*) malloc(sizeof(Item));
	pnew->id = id;

	pnew->name = (char*) malloc(sizeof(char) * (strlen(text) + 1));
	strcpy(pnew->name,text);
	
	pnew->type = ITEM;
	
	//Allocating text field if necessary
	if(field_size > 0)
	{
		pnew->text_field = (char*) calloc(1, sizeof(char) * (field_size + 1));
		pnew->type = field_type;
		pnew->text_field_size = field_size;
	}
	
	pnew->color = color;
	pnew->func = (*func);

	return pnew;
}

/**
 * Funzione di callback che allinea gli elementi di un Menu con le nuove
 * coordinate di allineamento di esso
 * 
 * @parama Menu *pmenu
 * 		Il menu di cui allineare gli items
 **/
void alignMenuItems(Menu* pmenu)
{
	int i;
	for(i = 0; i < pmenu->num_items; ++i)
	{
		pmenu->items[i].x = pmenu->x+FONT_W;
		pmenu->items[i].y = pmenu->y+(i*FONT_H);
	}
	
	pmenu->h = pmenu->num_items;
}

void alignMenuCenter(Menu* pmenu)
{
	pmenu->x = (SCREEN_WIDTH/2)- ((pmenu->w*FONT_W)/2);
	alignMenuItems(pmenu);
}

void alignMenuBottom(Menu* pmenu)
{
	pmenu->y = SCREEN_HEIGHT-(pmenu->num_items*FONT_H)-32;	//*altezza carattere x4
	alignMenuItems(pmenu);
}

/**
 * Funzione di callback che imposta la larghezza del menu in base alla 
 * lunghezza della voce piu' lunga di uno dei suoi items
 * 
 * @param Collection *pmenu
 * 		Il menu di cui impostare la larghezza
 * 
 * @param int strlen
 * 		Lunghezza della voce di menu
 **/
void setMenuWidth(Menu* pmenu, int strlen)
{
	//~ printf("len %d\n", strlen);
	if(strlen > pmenu->w)	//Lunghezza del testo*larghezza lettera
	  pmenu->w = strlen;
}

/**
 * Aggiunge una voce ad un menu specifico
 * 
 * @param Menu *pmenu
 * 		Menu a cui aggiungere la nuova voce
 * @param Item *pitem
 * 		Elemento da aggiungere al menu
 **/
void addMenuItem(Menu *pmenu, Item* pitem)
{
	++pmenu->num_items;
	pmenu->items = (Item*) realloc(pmenu->items, sizeof(Item) * pmenu->num_items);
	memcpy(&pmenu->items[pmenu->num_items-1],pitem,sizeof(Item));
	setMenuWidth(pmenu, strlen(pitem->name));
	//~ printf("w %d\n", strlen(pitem->name));
	free(pitem);
}

void switchCursorSelection(Menu *pmenu, int increment)
{
	//TODO: Puliamo la posizione attuale del cursore
	//....................
	
	//Move cursor down
	if(increment>0)
	{
		if(pmenu->curr_item+increment < pmenu->num_items)
		{
			pmenu->curr_item++;
		}
		else
			pmenu->curr_item=0; //Wrap the selection to first item
	}
	
	if(increment<0)
	{
		if(pmenu->curr_item>0)
		{
			pmenu->curr_item--;
		}
		else
		{
			pmenu->curr_item=pmenu->num_items-1; //Wrap the selection to last item
		}
	}	
	
	//~ //Disegnamo il cursore
	//~ drawFillRect(pmenu->items[pmenu->curr_item].x-15, pmenu->items[pmenu->curr_item].y+2, 10, 10, 0xff0000);
}

/**
 * Gestisce gli input del giocatore nel menu
 **/
void MenuInput(Menu *pmenu)
{
	if(keystate[SDLK_UP])
	{
		keystate[SDLK_UP] = 0;
		switchCursorSelection(pmenu, -1);
	}
		
	if(keystate[SDLK_DOWN])
	{
		keystate[SDLK_DOWN] = 0;
		switchCursorSelection(pmenu, 1);
	}

	if(keystate[SDLK_RETURN])
	{
		if(pmenu->items[pmenu->curr_item].func>0)
		{
			timer.start_time = fps.t;
			pmenu->items[pmenu->curr_item].func();
		}
		
		keystate[SDLK_RETURN] = 0;
	}
	
	if(pmenu->items[pmenu->curr_item].type==TEXT)
	{
		textInput(pmenu->items[pmenu->curr_item].text_field, pmenu->items[pmenu->curr_item].text_field_size);
		
	}

	if(pmenu->items[pmenu->curr_item].type==NUMERIC)
	{
		numericInput(pmenu->items[pmenu->curr_item].text_field, pmenu->items[pmenu->curr_item].text_field_size);
		
	}

	if(pmenu->items[pmenu->curr_item].type==MISC)
	{
		miscInput(pmenu->items[pmenu->curr_item].text_field, pmenu->items[pmenu->curr_item].text_field_size);
	}

	if(keystate[SDLK_TAB])
	{
		switchCursorSelection(pmenu, 1);
		keystate[SDLK_TAB] = 0;
	}
}

void drawMenu(Menu *pmenu, int title)
{
	if(title)
	{
		drawString(screen, pmenu->x+FONT_W, pmenu->y-FONT_H*2, pmenu->name, dark_gray, 1);
	}
	
	int i;
	for(i = 0; i < pmenu->num_items; ++i)
	{
		drawString(screen, pmenu->items[i].x, pmenu->items[i].y, pmenu->items[i].name, pmenu->items[i].color, 1);
		
		//Draw text fields
		if(pmenu->items[i].type==TEXT || pmenu->items[i].type==NUMERIC || pmenu->items[i].type==MISC)
		{
			int input_x = pmenu->items[i].x + (strlen(pmenu->items[i].name)*FONT_W+FONT_W);
			//~ int input_size = sizeof(pmenu->items[i].text_field) / sizeof(pmenu->items[i].text_field[0]);
			int input_size = (pmenu->items[i].text_field_size)*FONT_W+FONT_W;

			drawFillRect(input_x, pmenu->items[i].y, input_size, FONT_H, DARK_GRAY);
			
			drawString(screen, input_x, pmenu->items[i].y, pmenu->items[i].text_field, white, 1);
			
			if(i == pmenu->curr_item)
			{
				drawFillRect(input_x+(strlen(pmenu->items[i].text_field)*FONT_W+1), pmenu->items[i].y+1, 5, 8, WHITE);
			}
		} 
	}
}

void drawMenuBorder(Menu *pmenu)
{
	//Up-left corner
	drawChar(screen, bmpfont, pmenu->x-FONT_W, pmenu->y-FONT_H, FONT_W, FONT_H, 0, cyan, 1);
	//Up-right corner
	drawChar(screen, bmpfont, pmenu->x+pmenu->w*FONT_W+FONT_W, pmenu->y-FONT_H, FONT_W, FONT_H, 6, cyan, 1);
	
	//Linee orizzontali
	int i;
	for(i=-1; i < pmenu->w; i++)
	{
		drawChar(screen, bmpfont, (pmenu->x+8)+8*i, pmenu->y-8, 8, 8, 2, cyan, 1);
		drawChar(screen, bmpfont, (pmenu->x+8)+8*i, pmenu->items[pmenu->num_items-1].y+8, 8, 8, 3, cyan, 1);
	}

	//Linee verticali
	for(i=0; i < pmenu->h; i++)
	{
		drawChar(screen, bmpfont, pmenu->x-8, pmenu->y+8*i, 8, 8, 4, cyan, 1);
		drawChar(screen, bmpfont, pmenu->x+pmenu->w*8+8, pmenu->y+8*i, 8, 8, 5, cyan, 1);
	}

	//Down-left corner
	drawChar(screen, bmpfont, pmenu->x-8, pmenu->items[pmenu->num_items-1].y+8, 8, 8, 1, cyan, 1);

	//Down-right corner
	drawChar(screen, bmpfont, pmenu->x+pmenu->w*8+8, pmenu->items[pmenu->num_items-1].y+8, 8, 8, 7, cyan, 1);
}

void destroyMenu(Menu *pmenu)
{
	if(pmenu->num_items != 0 && pmenu->items != NULL)
	{
		free(pmenu->items);
	}
	printf("Free menu\n");
}
