/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    editor.c - Level editor core                    *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <stdio.h>
#include <ctype.h>
#include <dirent.h>


#include "editor.h"
#include "camera.h"
#include "level.h"
#include "tile.h"
#include "gfx.h"
#include "game.h"
#include "mouse.h"
#include "controls.h"
#include "fps.h"
#include "font.h"
#include "event.h"

void loadGraphicSets();

int flag_tileN;
int flag_event;

int new_evt_x, new_evt_y;

static int start_x = SCREEN_WIDTH-(TILESIZE*TILESPERCOL);	//Drawing starts from this point

void showEventDialog()
{
	curr_level->curr_layer = 0;		//The events reside on the SOLID layer
	curr_sheet = obj_sheet;
	flag_tileN = 0;		//Disable tile numbering
	
	addEvent();
	
	//Clean event holder and reset event dialog
	evt_holder.evt_x = 0;
	evt_holder.evt_y = 0;
	evt_holder.parameters[0] = '\0';
	
	event_menu.items[event_menu.curr_item].text_field[0] = '\0';
	event_menu.curr_item = 0;

	menuptr=NULL;
}

void initEventMenu()
{
	initMenu(&event_menu, 1, 30, 10, "New event", NULL, NULL);
	addMenuItem(&event_menu, createItem(1,"Param", 25, MISC, white, NULL));
	addMenuItem(&event_menu, createItem(4,"Done", 0, 0, white, showEventDialog));
	//~ alignMenuCenter(&event_menu);
	alignMenuBottom(&event_menu);
}

void initEditor()
{
	initEventMenu();

	camera.offsetX = 0;
	camera.shot.w = 15;		//Max columns displayed
	camera.shot.h = 15;		//Max rows displayed
	
	pick = 1;	//Il tile attualmente scelto
	
	int cols = atoi(editor_menu.items[1].text_field);
	int rows = atoi(editor_menu.items[2].text_field);

	if(cols == 0 && rows == 0)
	{
		curr_level->cols = COLS;
		curr_level->rows = ROWS;
	}
	//Check submitted values
	else if(cols < COLS && rows < ROWS)
	{
		curr_level->cols = cols;
		curr_level->rows = rows;
	}

	if(editor_menu.items[0].text_field[0] != '\0')
	{
		//Save our map filename
		strcpy(curr_level->name, editor_menu.items[0].text_field);
	}
	else
	{
		strcpy(curr_level->name, "map");
	}

	//Colore di default: VERDE 0x00FF00
	Uint32 key = SDL_MapRGB( screen->format, 0, 255, 0 );
	
	char path[80];

	snprintf(path, 80, "sets/%s/obj_sheet.bmp", tileset_menu.items[tileset_menu.curr_item].name);
	obj_sheet=loadImage(path, key);

	snprintf(path, 80, "sets/%s/tile_sheet.bmp", tileset_menu.items[tileset_menu.curr_item].name);
	tile_sheet=loadImage(path, key);
	
	curr_level->num_layers = 2;
	
	snprintf(path, 80, "sets/%s/alpha_sheet.bmp", tileset_menu.items[tileset_menu.curr_item].name);
	alpha_sheet=loadImage(path, key);
	
	if(alpha_sheet>0)
	 curr_level->num_layers = 3;
	
	curr_level->curr_layer = 1;
	curr_sheet = tile_sheet;
	
	//~ debugLevel(curr_level);
	
	doEditor();
}

void setMap()
{
	initEventMenu();

	camera.offsetX = 0;
	camera.shot.w = 15;		//Max columns displayed
	camera.shot.h = 15;		//Max rows displayed
	pick = 1;	//Il tile attualmente scelto

	char file[80];
	snprintf(file, 80, "%s", map_menu.items[map_menu.curr_item].name);

	loadLevel(curr_level, map_menu.items[map_menu.curr_item].name);

	//Colore di default: VERDE 0x00FF00
	Uint32 key = SDL_MapRGB( screen->format, 0, 255, 0 );
	
	char path[80];

	snprintf(path, 80, "sets/%s/obj_sheet.bmp", curr_level->gset);
	obj_sheet=loadImage(path, key);

	snprintf(path, 80, "sets/%s/tile_sheet.bmp", curr_level->gset);
	tile_sheet=loadImage(path, key);
	
	curr_level->num_layers = 2;
	
	snprintf(path, 80, "sets/%s/alpha_sheet.bmp", curr_level->gset);
	alpha_sheet=loadImage(path, key);
	
	if(alpha_sheet>0)
	 curr_level->num_layers = 3;
	
	curr_level->curr_layer = 1;
	curr_sheet = tile_sheet;
	
	//~ debugLevel(curr_level);

	doEditor();
}

/**
 * Inizializza alcune impostazioni della mappa di livello richiedendole
 * all'utente
 * 
 * @return 
 * 		Ritorna 0 in caso di impostazioni non accettabili, 1 in caso di
 * 		impostazioni accettabili e corrette 
 **/
void loadMap()
{
	//Main menu
    menuptr = &map_menu;
	initMenu(&map_menu, 1, 30, 10, "Maps", NULL, NULL);

	DIR *d;
	struct dirent *dir;
	const char * d_name;

	d = opendir("./maps");
	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			d_name = dir->d_name;
			
			if (strcmp(d_name, "..") != 0 &&
                strcmp(d_name, ".") != 0)
            {
				addMenuItem(&map_menu, createItem(1, dir->d_name, 0, 0, white, setMap));
			}
		}

		printf("\n");
		closedir(d);
	}

	alignMenuCenter(&map_menu);
	alignMenuBottom(&map_menu);
}

void loadGraphicSets()
{

  //~ char fname[1024];
//~ 
  //~ snprintf(fname, 1024, "%s/themes/%s/%s", st_dir, theme, file);
  //~ if(!faccessible(fname))
    //~ snprintf(fname, 1024, "%s/images/themes/%s/%s", DATA_PREFIX, theme, file);
//~ 
  //~ texture_load(ptexture, fname, use_alpha);


	//Main menu
    menuptr = &tileset_menu;
	initMenu(&tileset_menu, 1, 30, 10, "Tileset", NULL, NULL);

	DIR *d;
	struct dirent *dir;
	const char * d_name;

	d = opendir("./sets");
	if (d)
	{
		while ((dir = readdir(d)) != NULL)
		{
			d_name = dir->d_name;
			
			if (strcmp(d_name, "..") != 0 &&
                strcmp(d_name, ".") != 0)
            {
				addMenuItem(&tileset_menu, createItem(1, dir->d_name, 0, 0, white, initEditor));
				//~ printf("%s\n", d_name);
			}
		}

		printf("\n");
		closedir(d);
	}

	alignMenuCenter(&tileset_menu);
	alignMenuBottom(&tileset_menu);

}

/**
 * Gestisce gli input per l'editor di livello
 **/
void editorInput()
{
	//~ getMousePosition();
	setMouseOnGrid();

	#ifdef DEBUG_EDITOR
		printf("X %d\n", (-camera.offsetX+Mouse.x)/TILESIZE);
	#endif

	if(Mouse.x < (SCREEN_WIDTH-TILESIZE*TILESPERCOL))
	{
		if(keystate[SDLK_LEFT] || keystate[SDLK_a])
		{
			if (camera.shot.w > 15)
			 updateCamera(&camera, TILESIZE, 0);
			 
			keystate[SDLK_a] = 0;

			#ifdef DEBUG_EDITOR
				printf("Camera x %d\n", camera.shot.x);
			#endif
		}

		if(keystate[SDLK_RIGHT] || keystate[SDLK_d])
		{
			//~ printf("%d %d\n",camera.shot.w, camera.maxOffsetX/TILESIZE);
			//~ if (camera.shot.w < camera.maxOffsetX/TILESIZE)
			//~ printf("%d %d\n",camera.shot.w, curr_level->cols);
			if (camera.shot.w < curr_level->cols)
			 updateCamera(&camera, -TILESIZE, 0);
			 
			keystate[SDLK_d] = 0;
		}

		if(keystate[SDLK_UP] || keystate[SDLK_w])
		{
			if (camera.shot.y > 0)
			 updateCamera(&camera, 0, TILESIZE);
			
			keystate[SDLK_w] = 0;
		}

		if(keystate[SDLK_DOWN] || keystate[SDLK_s])
		{
			if (camera.shot.h < curr_level->rows)
			 updateCamera(&camera, 0, -TILESIZE);
			
			keystate[SDLK_s] = 0;
		}
	}

	if(keystate[SDLK_1])	//Background layer 1
	{
		curr_level->curr_layer = 1;
		curr_sheet = tile_sheet;
		setNotification(fps.t, "Background layer");
		
		keystate[SDLK_1] = 0;
	}
	
	if(keystate[SDLK_2])	//Solid layer 0
	{
		curr_level->curr_layer = 0;
		curr_sheet = obj_sheet;
		setNotification(fps.t, "Entities layer");
		
		//~ printf("Layer: %d\n", curr_level->curr_layer);

		keystate[SDLK_2] = 0;
	}

	if(keystate[SDLK_3])	//Alpha layer 2
	{
		//~ if(2>getLayerSize(curr_level))
		if(curr_level->num_layers==3)
		{
			curr_level->curr_layer = 2;
			curr_sheet = alpha_sheet;
			setNotification(fps.t, "Alpha layer");
		}
		else
		{
			setNotification(fps.t, "Alpha layer not available!");
		}
		
		keystate[SDLK_3] = 0;
	}

	if(keystate[SDLK_c])	//Create map
	{
		initEditor();
		//~ emptyLevel(curr_level);
		setNotification(fps.t, "New level map");
		
		keystate[SDLK_c] = 0;
	}

	//~ if(keystate[SDLK_n])	//Next map
	//~ {
		//~ if(loadLevel(curr_level, curr_level->number+1))
		 //~ initEditor();
		//~ 
		//~ sprintf(sys_message,"Map %d loaded", curr_level->number);
		//~ setNotification(fps.t, sys_message);
		//~ 
		//~ //TODO: impostare lo scrolling ad inizio mappa!!!!
		//~ keystate[SDLK_n] = 0;
	//~ }
	//~ 
	//~ if(keystate[SDLK_p])	//Previous map
	//~ {
		//~ if(curr_level->number>1)
		 //~ if(loadLevel(curr_level, curr_level->number-1))
		  //~ initEditor();
		  //~ 
		//~ sprintf(sys_message,"Map %d loaded", curr_level->number);
		//~ setNotification(fps.t, sys_message);
		//~ 
		//~ keystate[SDLK_p] = 0;
	//~ }

	if(keystate[SDLK_m])	//Save map
	{
		saveMap();
		sprintf(sys_message, "Map %s.map has been saved!", curr_level->name);
		setNotification(fps.t, sys_message);
		
		keystate[SDLK_m] = 0;
	}

	if(keystate[SDLK_t])	//Show tiles number
	{
		keystate[SDLK_t] = 0;
		flag_tileN = (flag_tileN==1)? 0 : 1;
	}

	if(keystate[SDLK_e])	//Event editor
	{
		keystate[SDLK_e] = 0;
		flag_event = (flag_event==1)? 0 : 1;
		
		setNotification(fps.t, "Event editor");
	}

	if(Mouse.x < (SCREEN_WIDTH-(TILESIZE*TILESPERCOL)))
	{
		if(flag_event)
		{
			//Add an event
			if(mousestate & SDL_BUTTON(SDL_BUTTON_LEFT))
			{
				//Save new event position
				evt_holder.evt_x = (-camera.offsetY+Mouse.y)/TILESIZE;
				evt_holder.evt_y = (-camera.offsetX+Mouse.x)/TILESIZE;
				
				menuptr = &event_menu;
			}

			//Delete an event
			else if(mousestate & SDL_BUTTON(SDL_BUTTON_RIGHT))
			{
				deleteEvent((-camera.offsetX+Mouse.x)/TILESIZE, (-camera.offsetY+Mouse.y)/TILESIZE);
			}
			return;
		}		
		
		//(Senza event.type e' continuo, ma all'uscita del puntatore dalla finestra rilascia un tile...)
		//Assegna un tile
		if(mousestate & SDL_BUTTON(SDL_BUTTON_LEFT))
		{
			curr_level->map[curr_level->curr_layer]
						   [(-camera.offsetY+Mouse.y)/TILESIZE]  
						   [(-camera.offsetX+Mouse.x)/TILESIZE] = pick;
			
			//Warp event tile!   
			//TODO: assegnare valore 3 al tile nel layer 0 per i warp
			if(pick==3 && curr_level->curr_layer == 0)
			{
				flag_event = 1;
			}
		}

		//Cancella un tile
		else if(mousestate & SDL_BUTTON(SDL_BUTTON_RIGHT))
		{
			curr_level->map[curr_level->curr_layer][(-camera.offsetY+Mouse.y)/TILESIZE]  
						   [(-camera.offsetX+Mouse.x)/TILESIZE] = 0;
		}
	}
	else
	{
		if(!flag_event)
		{
			//Preleva il valore di un tile dalla barra laterale
			if(event.button.button == SDL_BUTTON_LEFT || event.button.button == SDL_BUTTON_RIGHT)
			{
				int x = (Mouse.x-start_x)/TILESIZE;
				int y = (Mouse.y /TILESIZE);
				
				if(y>0)
				 x+=TILESPERCOL*y;
				
				pick = x;
				
				event.button.button=0;
				
				printf("Mouse pick tile: %d\n", pick);
			}
		}
	}

	if (camera.shot.x < 0)
	{
		camera.shot.x = 0;
	}
	else if (camera.shot.w > camera.maxOffsetX)
	{
		camera.shot.x--;
	}
}

void drawLevelEditor()
{
	//~ drawString(screen, 10, 40, message, cyan); //draw string with bmp font
	
	drawEntityMap(camera.shot, curr_level, curr_sheet);
	
	drawFillRect(SCREEN_WIDTH-TILESIZE*TILESPERCOL, 0, 2, SCREEN_HEIGHT, WHITE);
	
	//Draw tiles picker along with tile numbers
	putImage(curr_sheet, screen, 0, 0, curr_sheet->w, curr_sheet->h, start_x, 0);

	if(flag_tileN>0)
	{
		char index[10];		//Tile number index
		int row=0, col = 0, i=0;
		while(i<=75)
		{
			snprintf(index, 10, "%d", i);
			drawString(screen, start_x+(col*TILESIZE), 0+(row*TILESIZE)+(TILESIZE/2), index, white, 0);
			col++;
			i++;
			
			if(i%TILESPERCOL==0)
			{
				row++;
				col=0;
			}
		}
	}

	//Draw event dialog
	if(menuptr==&event_menu)
	{
		drawFillRect(0, menuptr->y-TILESIZE*2, SCREEN_WIDTH, SCREEN_HEIGHT/2, BLACK);
		drawMenu(menuptr, 1);
		drawChar(screen, bmpfont, menuptr->items[menuptr->curr_item].x-10, menuptr->items[menuptr->curr_item].y, 8, 8, '*', white, 1);
		
		//White border
		//~ drawRect(0, menuptr->y-TILESIZE*2, SCREEN_WIDTH, TILESIZE*TILESPERCOL,0xffffff);
		return;
	}

	//Draw events locations
	if(flag_event==1)
	{
		Event *current = evt_headList, *next=NULL;
		while(current!=NULL)
		{
			next=current->next;
			
			SDL_Rect dest;
			dest.x = (current->evt_x*TILESIZE) + camera.offsetX;
			dest.y = (current->evt_y*TILESIZE) + camera.offsetY;
			dest.w = TILESIZE;
			dest.h = TILESIZE;
			
			if(curr_level->map[0][current->evt_x][current->evt_y] == 3)
			{
				drawTile(sys, 3, &dest);
			}
			else
			{
				drawTile(sys, 4, &dest);
			}

			current=next;
		}
	}
	
	if(curr_level->curr_layer == 0)
	{
		int i;
		for(i=0; i<=3; i++)
		{
			SDL_Rect dest;
			dest.x = start_x+(i*TILESIZE);
			dest.y = 0;
			dest.w = TILESIZE;
			dest.h = TILESIZE;
			
			drawTile(sys, i, &dest);
		}
	}

	drawMousePointer();
	//~ printf("%d %d %d %d\n", camera.shot.x, camera.shot.y, camera.shot.w, camera.shot.h);
	//~ drawRect(camera.shot.x, camera.shot.y, camera.shot.w-1, camera.shot.h-1, ORANGE);


}

void cleanEditor()
{
    SDL_FreeSurface(tile_sheet);
    SDL_FreeSurface(obj_sheet);
    SDL_FreeSurface(alpha_sheet);

    destroyMenu(&editor_menu);
    destroyMenu(&tileset_menu);
    destroyMenu(&event_menu);
    
    cleanEvents();
    
	SDL_Quit();
}
