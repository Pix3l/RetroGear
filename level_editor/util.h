#ifndef _UTIL_H
#define _UTIL_H

//Global variables
int repeat, quit;

char sys_message[50];	//Messaggi di sistema

void setNotification(int curr_time, char *notification);
void appendChar(char *str, char ch, int length);
void eraseLastChar(char *string);
char *substr(char *string, int position, int length);

void init();
void cleanUp();

#endif
