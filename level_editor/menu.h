/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                 menu.h - Game menu handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _MENU_H
#define _MENU_H

#include <SDL/SDL.h>

//Gui element types
typedef enum
{
    ITEM, TEXT, NUMERIC, MISC
} guiType;

/**
 * Struttura del menu' con relative caratteristiche
 * 
 * int active - Menu' attivo/disattivo
 * int x,y,w,h - Coordinate e dimensione del contenitore di menu'
 * char name[32] - Nome del menu' visualizzato
 * int num_items - Numero di voci selezionabili del menu'
 * int curr_item - Voce del menu' attualmente selezionata
 * _Item *items - Voci del menu'
 * _Menu *previous, *next - Puntatore a menu' successivi o precedenti
 **/
typedef struct _Menu {
	int active;
	int x, y;
	int w, h;
	char name[32];
	int num_items;
	int curr_item;	//Item actually selected
	struct _Item *items;
	struct _Menu *previous, *next;
} Menu;

/**
 * Struttura per le voci di menu'
 * 
 * @param int id
 * 		ID dell'elemento di menu'
 * 
 * @param int x, y
 * 		Coordinate della voce nel contenitore del menu'
 * 
 * @param char *name
 * 		Nome della voce visualizzato
 * 
 * @param char *text_field
 * 		Campo di testo editabile, da allocare o associare ad una 
 * 		variabile stringa
 * 
 * @param SDL_Color color
 * 		Colore del testo
 * 
 * @removed int value
 * 		Valore ritornato alla selezione della voce (ES: stato di gioco)
 * 
 * @param void (*func)()
 * 		Funzione associata alla scelta della voce di menu'
 **/
typedef struct _Item
{
	int id;
	int x, y;
	char *name;
	char *text_field;
	int text_field_size;
	int type;
	SDL_Color color;
	void (*func)();	//Funzione di callback
} Item;

Menu main_menu, *menuptr;

void initMenu(Menu* pmenu, int active, int x, int y, char *name, Menu *pprev, Menu *pnext);
Item* createItem(int id, char *text, unsigned int field_size, int field_type, SDL_Color color, void (*func)());
void alignMenuItems(Menu* pmenu);
void alignMenuCenter(Menu* pmenu);
void alignMenuBottom(Menu* pmenu);
void setMenuBox(Menu* pmenu, Item* pitem);
void addMenuItem(Menu *pmenu, Item* pitem);
void switchCursorSelection(Menu *pmenu, int increment);
void MenuInput(Menu *pmenu);
void drawMenu(Menu *pmenu, int title);
void drawMenuBorder(Menu *pmenu);
void destroyMenu(Menu *pmenu);

#endif
