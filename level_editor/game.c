/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   game.c - Game handling function                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "game.h"
#include "menu.h"
#include "editor.h"
#include "fps.h"
#include "controls.h"
#include "gfx.h"
#include "draw.h"

/**
 * void doTitleScreen()
 * 
 * Questa funzione gestisce la schermata dei titoli e relativo menu.
 * Permette al giocatore di scegliere varie opzioni di gioco.
 **/ 
void doTitleScreen()
{
	MenuInput(menuptr);
}

/**
 * Gestisce gli eventi dello stato di LevelEditing
 **/
void doEditor()
{
	if(Game.status!=EDITOR)
	{
		setGameState(EDITOR);
	}
	
	//Wait user input event data
	if(menuptr==&event_menu)
	{
		MenuInput(menuptr);
	}
	else
	{
		editorInput();
	}
}

void setGameState(int state)
{
	Game.status = state;
}

void mainLoop()
{
	//main game loop
	while(!quit)
	{
		repeat = fps_sync();
		int i;
		for (i = 0; i < repeat; i ++)
		{
			keyboardInput();
				
			switch(Game.status)
			{
				case MENU:
					doTitleScreen();
					break;
				case EDITOR:
					doEditor();
					break;
			}
			
			// Cancella tutto lo schermo di gioco
			SDL_FillRect(screen, NULL, SDL_MapRGB(screen->format, 0, 0, 0)); //Lo schermo viene disegnato per intero ogni volta...
			// Gestisce il disegno dello schermo
			draw();
			// Aggiorna lo schermo di gioco
			SDL_Flip(screen);
		 }
	}
}
