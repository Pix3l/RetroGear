/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             draw.c - Game drawing events callback functions        *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "draw.h"
#include "font.h"
#include "util.h"
#include "gfx.h"
#include "timer.h"
#include "menu.h"
#include "editor.h"
#include "game.h"

/**
 * Funzione privata di callback per le notifiche di sistema sotto forma
 * di messaggi video.
 * 
 * Assegnando un contenuto alla variabile sys_message, ne viene mostrato
 * il contenuto a video in una posizione di default.
 **/
void callback_DrawSystemMessages()
{
	if(sys_message != NULL)
	{
		drawString(screen, 8, SCREEN_HEIGHT-16, sys_message, red, 0);

		if(getSeconds(sys_timer.start_time) > 3)
		{
			strcpy(sys_message, "");
			sys_timer.start_time = 0;
		}
	}
}

/**
 * Disegna la schermata del titolo con relativo menu'
 **/
void drawTitle()
{
	putImage(title, screen, 0, 0, 220, 28, 50, 25);
	drawString(screen, 180, 60, "Version 1.0", white, 1);

	drawMenu(menuptr, 1);
	drawChar(screen, bmpfont, menuptr->items[menuptr->curr_item].x-10, menuptr->items[menuptr->curr_item].y, 8, 8, '*', white, 1);

	drawString(screen, 95, SCREEN_HEIGHT-25, "* PIX3LWORKSHOP *", white, 1);
	drawString(screen, 70, SCREEN_HEIGHT-16, "* Powered by RetroGear *", white, 1);
}

/**
 * Disegna la schermata di Level Editing
 **/
void drawEditor()
{
	drawLevelEditor();
}

void draw()
{
	switch(Game.status)
	{
		case MENU:
			drawTitle();
			break;
		case EDITOR:
			drawEditor();
			break;
	}

	callback_DrawSystemMessages();
}
