/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   game.h - Game handling function                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _GAME_H
#define _GAME_H

struct _Game
{
	int status; //Stato di gioco
} Game;

//Possibili stati di gioco
enum States
{
	GAMEOVER, MENU, PREGAME, GAME, LOST, WIN, EDITOR
};

void doTitleScreen();
void doEditor();
void setGameState(int status);
void mainLoop();

#endif
