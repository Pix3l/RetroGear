/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                  util.c - Generic purpose functions                *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 * 23/05/2012                                                         *
 *********************************************************************/

#include "util.h"
#include "fps.h"
#include "level.h"
#include "font.h"
#include "camera.h"
#include "timer.h"
#include "gfx.h"

/**
 * Inizializza i parametri interni del motore di gioco
 **/
void init()
{
    fps.t=0;
    fps.tl=0;
    fps.frequency=1000 / 100;

    quit = 0;

	/** Di default assegnamo il livello attuale alla struttura level,
	 *  presente di default nel programma
	 **/
	curr_level = &level;
	//~ curr_level->layer=1;
	
	//Init game engine subsystems
	initFont();
	initCamera(&camera);
}

void setNotification(int curr_time, char *notification)
{
	sys_timer.start_time = curr_time;
	sprintf(sys_message,notification);
}

/**
 * Appende un singolo char ad una stringa e la termina correttamente
 *
 * @param char *str
 * 		La stringa a cui appendee il char
 * @param char ch
 * 		Il carattere da appendere alla stringa
 **/
//~ void appendChar(char* str, char ch)
//~ {
	//~ int len = strlen(str);
	//~ str[len] = ch;
	//~ str[len+1] = '\0';
	//~ 
	//~ #define DEBUG
	//~ #ifdef DEBUG
		//~ printf("debug %s\n", str);
	//~ #endif
//~ }

void appendChar(char *str, char ch, int length)
{
    size_t len = strlen(str);
    
    if((len+1) <= length)
    {
        str[len] = ch;
        str[len+1] = '\0';
    }

	#ifdef DEBUG
		printf("debug %s\n", str);
	#endif

}

/**
 * Erase last character from a string
 * 
 * @param char *string
 * 		The string to handle
 **/
void eraseLastChar(char *string)
{
	//If backspace was pressed and the string isn't blank
	if(strlen(string) != 0 )
	{
		char *tmp;
		
		tmp = substr(string, 0, strlen(string)-1);
		strcpy(string, tmp);
	}
}

char *substr(char *string, int position, int length) 
{
   char *pointer;
   int c;
 
   pointer = malloc(length+1);
 
   if (pointer == NULL)
   {
      printf("Unable to allocate memory.\n");
      exit(EXIT_FAILURE);
   }
 
   for (c = 0 ; c < position -1 ; c++) 
      string++; 
 
   for (c = 0 ; c < length ; c++)
   {
      *(pointer+c) = *string;      
      string++;   
   }
 
   *(pointer+c) = '\0';
 
   return pointer;
}

/**
 * void cleanUp()
 * 
 * Libera le risorse precedentemente allocate in memoria e termina
 * correttamente le librerie.
 *
 **/
void cleanUp()
{
    SDL_FreeSurface(screen);
    SDL_FreeSurface(bmpfont);
    SDL_FreeSurface(tmpfont);
    
    quit=1;
    
    //~ freeLevel(curr_level);

	SDL_Quit();
}
