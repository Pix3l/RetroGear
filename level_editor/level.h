/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    level.c - Level file handling                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#ifndef _LEVEL_H
#define _LEVEL_H

#define COLS 500
#define ROWS 500
#define LAYERS 3

/**
 * Current map layer
 * 0 - Entities / Solid blocks
 * 1 - Background
 * 2 - Alpha
**/
int layer;	//Current map layer

typedef struct _Level
{
	unsigned int number;		//Numero di livello
	char name[20];
	char gset[20];				//Graphic set
	//~ char theme[100];
	char song_title[100];
	//~ char bkgd_image[100];
	int bkgd_red, bkgd_green, bkgd_blue;
	int map[LAYERS][ROWS][COLS];
	//~ int*** map;
	//~ int solid[ROWS][COLS];
	//~ int time_left;
	//~ int bkgd_red;
	//~ int bkgd_green;
	//~ int bkgd_blue;
	unsigned int cols, rows;
	//~ float gravity;
	unsigned int curr_layer;
	unsigned int num_layers;
} Level;

//TODO: eliminare il puntatore? Una struttura sola puo' andare bene
//		in alternativa un array di struttura
Level level, *curr_level;

int loadLevel(Level* plevel, char* file);
int emptyLevel(Level* plevel);
int*** allocateMap(unsigned int layers, unsigned int cols,
				   unsigned int rows);
void debugLevel(Level* plevel);
void saveMap();
int getLayerSize(Level *plevel);
void freeLevel(Level *plevel);

#endif
