/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    fps.c - FPG handling functions                  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "fps.h"

int fps_sync (void)
{
	fps.t = SDL_GetTicks ();

	if (fps.t - fps.tl >= fps.frequency)
	{
		fps.temp = (fps.t - fps.tl) / fps.frequency;
		fps.tl += fps.temp * fps.frequency;
		return fps.temp;
	}
	else
	{
		SDL_Delay (fps.frequency - (fps.t - fps.tl));
		fps.tl += fps.frequency;
		return 1;
	}
}
