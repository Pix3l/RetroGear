/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   editor.c - Simple level editor					  *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _EDITOR_H
#define _EDITOR_H

#include <SDL/SDL.h>
#include "util.h"
#include "menu.h"

#define TILESPERCOL 6

int pick;	//Tile index value from sidebar

Menu editor_menu, map_menu, tileset_menu, event_menu; //Level editor specific menues

//Objects, background, transparent
SDL_Surface *obj_sheet, *tile_sheet, *alpha_sheet;

SDL_Surface *curr_sheet;	//Puntatore allo sprite sheet in uso

void initEditor();
void loadGraphicSets();
void loadMap();
void editorInput();
void saveLevelFile();
void drawLevelEditor();
void cleanEditor();

#endif
