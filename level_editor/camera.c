/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   camera.c - Scrolling functions                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "camera.h"
#include "tile.h"
#include "gfx.h"

void initCamera(Camera *pcam)
{
	pcam->shot.x = pcam->shot.y = 0;
    pcam->shot.w = SCREEN_WIDTH / TILESIZE;
    pcam->shot.h = (SCREEN_HEIGHT / TILESIZE)+2;
    
	//Lunghezza della mappa - Dimensione della finestra
	pcam->maxOffsetX = -(TILESIZE*curr_level->cols - SCREEN_WIDTH);
	pcam->maxOffsetY = -(TILESIZE*curr_level->rows - SCREEN_HEIGHT);
}

/**
 * void scrollCameraX()
 * 
 * Aggiorna lo scrolling sull'asse Y in base ad una coordinata X 
 * specificata in argomento.
 * 
 * @param int x
 * 		La coordinata X sulla quale basare lo scrolling della mappa
 * @param Camera *pcam
 * 		Puntatore alla struttura camera da usare per lo scrolling
 * 
 **/
void scrollCameraX(int x, Camera *pcam)
{
	//Dimensione di meta' schermo meno la dimensione di un tile

	//Dove si trova attualmente il giocatore meno meta' dello schermo
	//Tutta la distanza prima di questo punto e' offsetX
	//L'offset torna utile per disegnare la mappa sullo schermo (drawTileMap)
	pcam->offsetX = -(x - CENTER_X);
	
	if (pcam->maxOffsetX > 0)
		pcam->maxOffsetX = 0; // screen > image
	
	//Left Case
	if (pcam->offsetX > 0 || x < CENTER_X)
	{
		pcam->offsetX = 0;
	} //Right Case
	else if (pcam->offsetX < pcam->maxOffsetX || x > CENTER_X - pcam->maxOffsetX)
	{
		pcam->offsetX = pcam->maxOffsetX;
	}

	//Area di mappa da visualizzare
    pcam->shot.x = -(pcam->offsetX / TILESIZE);
    pcam->shot.w = pcam->shot.x + (SCREEN_WIDTH / TILESIZE)+1;
}

/**
 * Aggiorna lo scrolling sull'asse Y in base ad una coordinata Y 
 * specificata in argomento.
 * 
 * @param int y
 * 		La coordinata Y sulla quale basare lo scrolling della mappa
 * @param Camera *pcam
 * 		Puntatore alla struttura camera da usare per lo scrolling
 * 
 **/
void scrollCameraY(int y, Camera *pcam)
{
	pcam->offsetY = -(y - CENTER_Y);
	
	if (pcam->maxOffsetY > 0)
		pcam->maxOffsetY = 0; // screen > image
	
	if (pcam->offsetY > 0 || y < CENTER_Y)
	{
		pcam->offsetY = 0;
	}
	else if (pcam->offsetY < pcam->maxOffsetY || y > CENTER_Y - pcam->maxOffsetY)
	{
		pcam->offsetY = pcam->maxOffsetY;
	}
	
	//Area di mappa da visualizzare
    pcam->shot.y = -(pcam->offsetY / TILESIZE);
    pcam->shot.h = pcam->shot.y + (SCREEN_HEIGHT / TILESIZE)+2;
}

/**
 * void updateCamera()
 * 
 * Aggiorna la posizione attuale della sezione di mappa da visualizzare 
 * nel campo di gioco rimanendo nei limiti della mappa stessa.
 * 
 **/
void updateCamera(Camera *pcam, int new_x, int new_y)
{
	pcam->offsetX += new_x;
	pcam->shot.x = -(pcam->offsetX / TILESIZE);
	pcam->shot.w = pcam->shot.x + 15;		//Max columns displayed
	
	pcam->offsetY += new_y;
	pcam->shot.y = -(pcam->offsetY / TILESIZE);
	pcam->shot.h = pcam->shot.y + 15;		//Max columns displayed
}
