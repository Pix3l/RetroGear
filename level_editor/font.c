/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                font.c - Image font handling functions              *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "font.h"
#include "gfx.h"

/**
 * Inizializza la superficie bmpfont caricando l'immagine con i font
 * e realizzando una superficie temporanea per la manipolazione grafica
 * dei singoli caratteri
 **/
void initFont()
{
	// Load bmp font on a surface
	Uint32 key = SDL_MapRGB( screen->format, 255, 0, 255 ); 
    bmpfont=loadImage("data/nesfont.bmp", key);

    // Create a temporary surface for single bmp font character
    tmpfont=SDL_CreateRGBSurface(SDL_SWSURFACE,FONT_W,FONT_H,32,0,0,0,0);
    if(tmpfont == NULL) {
        fprintf(stderr, "CreateRGBSurface failed: %s\n", SDL_GetError());
        exit(1);
    }
}

/**
 * Copia una porzione dalla superficie bmpfont e la copia sulla superficie screen
 * 
 * @param SDL_Surface *screen
 *      Puntatore alla superficie di destinazione
 * @param SDL_Surface *bmpfont
 *      Puntatore alla superficie di origine
 * @param int X, int Y
 *      Destinazione in cui si andra' a copiare il contenuto di origine
 * @param int w, int h
 *      Rispettivamente, larghezza e grandezza della superficie da copiare
 * @param int asciicode
 *      Singolo carattere convertito in un equivalente decimale
 * @param SDL_Color color
 *      Struttura contenente i valori RGB di colore da usare per la superficie
**/
void drawChar(SDL_Surface *screen, SDL_Surface *bmpfont, int X, int Y, int w, int h, int asciicode, SDL_Color color, int alpha)
{
     SDL_Rect area, pick, tmpfontrect;
     
     //Cordinate nella superficie con le lettere
     pick.x=(asciicode % 32)*w;
     pick.y=(asciicode / 32)*h;
     pick.w=w;
     pick.h=h;
     area.x=X;
     area.y=Y;
     area.w=w;
     area.h=h;
     
     tmpfontrect.x=0;
     tmpfontrect.y=0;
     tmpfontrect.w=FONT_W;
     tmpfontrect.h=FONT_H;
     
     SDL_BlitSurface(bmpfont,&pick,tmpfont,NULL);
     
     //~ if(color.r!=255 && color.g!=255 && color.b!=255)
		 replaceColor (tmpfont, SDL_MapRGB (tmpfont->format, 255, 255, 255), 
					   SDL_MapRGB (tmpfont->format, color.r, color.g, color.b));
     
     //Set background transaparency
     if(alpha>0)
      SDL_SetColorKey(tmpfont, SDL_SRCCOLORKEY, BLACK);
     else //Disable any background transaparency
      SDL_SetColorKey(tmpfont, 0, 0);
     
     //int SDL_BlitSurface(SDL_Surface *src, SDL_Rect *srcrect, SDL_Surface *dst, SDL_Rect *dstrect);
     SDL_BlitSurface(tmpfont,NULL,screen,&area);
}

void drawString(SDL_Surface *screen, int x, int y, char *text, SDL_Color color, int alpha)
{
	
	if(bmpfont==NULL)
	{
		printf("Error: Font surface not initialized!\n");
		quit=1;
		return;
	}
	
	int i=0;
	int asciicode;
  
	SDL_Rect area = {x, y, FONT_W, FONT_H};

	//Per la lunghezza della stringa
	for (i=0;i<35;i++)
	{
		asciicode=text[i]; //Valori decimali del carattere
		//~ printf("%d ascii\n", asciicode);
		if (asciicode == 0 )
		 break;

		// 8x16 dimensioni della lettera
		drawChar(screen, bmpfont, area.x, area.y, area.w, area.h, asciicode, color, alpha);    
		area.x=area.x+FONT_W;
	}
}
