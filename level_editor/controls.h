/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   controls.h - User input handling                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/
#ifndef _CONTROLS_H
#define _CONTROLS_H

#include <SDL/SDL.h>

Uint8 *keystate; // keyboard state
SDL_Event event;

void keyboardInput();
void textInput(char *string, int length);
void numericInput(char *string, int length);
void miscInput(char *string, int length);

#endif
