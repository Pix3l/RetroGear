/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                     main.c - Simple level editor                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "util.h"
#include "menu.h"
#include "editor.h"
#include "gfx.h"
#include "game.h"
#include "controls.h"

/**
 * Prima di liberare le risorse dal sistema bisogna terminare il ciclo 
 * principale del programma
 **/
void quitGame()
{
	quit = 1;
}

void initMap()
{
	menuptr = &editor_menu;
}

void mainInit()
{
	init();
	
	SDL_WM_SetCaption("Leveler", "RetroGear Level Editor");

	//Setting TitleScreen
	title=loadImage("data/title.bmp", 0x000000);
	sys=loadImage("data/system.bmp", 0x000000);

	//Main menu
    menuptr = &main_menu;
	initMenu(&main_menu, 1, 30, 10, "main", NULL, NULL);
	addMenuItem(&main_menu, createItem(1,"New Map", 0, 0, white, initMap));
	addMenuItem(&main_menu, createItem(3,"Load Map", 0, 0, white, loadMap));
	addMenuItem(&main_menu, createItem(5,"Quit", 0, 0, white, quitGame));
	alignMenuCenter(&main_menu);
	alignMenuBottom(&main_menu);

	//Editor menu
	initMenu(&editor_menu, 1, 30, 10, "setup", NULL, NULL);
	addMenuItem(&editor_menu, createItem(1,"Name  ", 5, TEXT, white, NULL));
	addMenuItem(&editor_menu, createItem(2,"Width ", 5, NUMERIC, white, NULL));
	addMenuItem(&editor_menu, createItem(3,"Height", 5, NUMERIC, white, NULL));
	//~ addMenuItem(&editor_menu, createItem(3,"Layers", 2, white, NULL));
	addMenuItem(&editor_menu, createItem(4,"Done", 0, 0, white, loadGraphicSets));
	alignMenuCenter(&editor_menu);
	alignMenuBottom(&editor_menu);
	
	setGameState(MENU);
}

int main(int argc, char *argv[])
{
	// init video stuff
	if(SDL_Init(SDL_INIT_VIDEO | SDL_INIT_AUDIO) != 0)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}
	atexit(SDL_Quit);

	// init screen
	screen = SDL_SetVideoMode(SCREEN_WIDTH, SCREEN_HEIGHT, 0, SDL_HWSURFACE);
	if(screen == NULL)
	{
		fprintf(stderr, "Can't initialize SDL: %s\n", SDL_GetError());
		exit(-1);
	}
	
	mainInit();
	
	mainLoop();
	
	//Liberiamo la memoria dalle risorse allocate
	cleanUp();
	cleanEditor();

	return 0;
}
