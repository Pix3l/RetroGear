/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   tile.h - Tiles related functions                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#ifndef _TILE_H
#define _TILE_H

#define TILESIZE 16
#define SOLID 1		//1 is reserved for solid blocks!

#include <SDL/SDL.h>
#include "level.h"

void initTiles();
int isInTile(int x, int y);
int tileCollision(int x, int y, int w, int h, int type, unsigned int layer);
void drawTile(SDL_Surface *tiles, int tile, SDL_Rect *dest);
void drawTileMap(SDL_Rect camera_rect, Level *plevel);
void drawEntityMap(SDL_Rect camera_rect, Level *plevel, SDL_Surface *sheet);

#endif
