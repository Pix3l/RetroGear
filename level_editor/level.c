/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                    level.c - Level file handling                   *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "level.h"
#include "editor.h"
#include "event.h"

/**
 * Crea una struttura di livello vuota, idonea per la creazione di un
 * nuovo livello di gioco
 *
 * @param struct _Level* plevel
 *      Puntatore ad una struttura di livello
**/
//TODO: rendere interattivo il processo richiedendo i dati di livello
int emptyLevel(Level* plevel)
{
	int i,j;

	plevel->number = 0;		//I nuovi livelli di default avranno 1

	//Load header info
	strcpy(plevel->name, "New level");

	/* (Song file for this level) */
	strcpy(plevel->song_title, "song file name here");

	//New level will have default cols and rows size
	plevel->cols = COLS;
	plevel->rows = ROWS;

	//Layer 1: Solid tiles
	for(i=0; i<plevel->rows; i++)// && !feof (fi))
		for(j=0; j<plevel->cols; j++)
			plevel->map[0][i][j]=0;

	//Layer 2: Tiles
	for(i=0; i<plevel->rows; i++)// && !feof (fi))
		for(j=0; j<plevel->cols; j++)
			plevel->map[1][i][j]=0;

  return 0;
}

/**
 * Alloca dinamicamente in memoria la mappa di livello
 * 
 * @param unsigned int layers
 * 		Il numero di strati per la mappa
 * 
 * @param unsigned int cols
 * 		Il numero di colonne per il livello
 * 
 * @param unsigned int rows
 * 		Il numero di righe per il livello
 * 
 * @return int[][][]
 * 		Ritorna indietro una matrice 3D della dimensione specificata
 **/
int*** allocateMap(unsigned int layers, unsigned int cols, unsigned int rows)
{
	int i;
	int j;
	int*** array = (int***) calloc(1,  (layers * sizeof(int*)) + (layers*cols * sizeof(int**)) + (layers*cols*rows * sizeof(int)) );
	for(i = 0; i < layers; ++i) {
		array[i] = (int**)(array + layers) + i * cols;
		for(j = 0; j < cols; ++j) {
		  array[i][j] = (int*)(array + layers + layers*cols) + i*cols*rows + j*rows;
		}
	}
	  
	return array;
}

/**
 * Carica un livello e relative impostazioni da file, salvandole in una
 * apposita struttura
 * 
 * @param struct _Level* plevel
 *      Puntatore alla struttura contenente i dati del livello caricato
 * 
 * @param int level
 * 		Numero del livello attuale (Parte del nome del file)
 * 
 * @return -1
 *      In caso il caricamento del file fallisca
 * 
 **/
int loadLevel(Level* plevel, char* file)
{
	int i,j, layer;
	FILE * fi;
	char str[80];

	/* Load data file: */
	char filename[1024];
	snprintf(filename, 1024, "maps/%s", file);

	fi = fopen(filename, "r");
	if (fi == NULL)
	{
		perror(filename);
		return -1;
	}

	//~ plevel->number = level;

	//Load header info
	/* (Level title) */
	fgets(str, 20, fi);
	strcpy(plevel->name, str);
	plevel->name[strlen(plevel->name)-1] = '\0';

	/* (Level graphic set) */
	fgets(str, 20, fi);
	strcpy(plevel->gset, str);
	plevel->gset[strlen(plevel->gset)-1] = '\0';

	/* (Song file for this level) */
	fgets(str, sizeof(plevel->song_title), fi);
	strcpy(plevel->song_title, str);
	plevel->song_title[strlen(plevel->song_title)-1] = '\0';

	//Background color
	//~ fscanf (fi, "%6x", &plevel->background_color);
	fscanf (fi, "%d%*c%d%*c%d", &plevel->bkgd_red, &plevel->bkgd_green, &plevel->bkgd_blue);

	//Layers
	fscanf (fi, "%d", &plevel->num_layers);

	//Level rows and cols
	fscanf (fi, "%02d%*c%02d", &plevel->cols, &plevel->rows);

	//~ //Layer 1: Solid tiles
	//~ for(i=0; i<plevel->rows; i++)// && !feof (fi))
		//~ for(j=0; j<plevel->cols; j++)
		//~ {
			//~ fscanf (fi, "%02d%*c", &plevel->map[0][i][j]);
		//~ }
//~ 
	//~ //Layer 2: Tiles
	//~ for(i=0; i<plevel->rows; i++)// && !feof (fi))
		//~ for(j=0; j<plevel->cols; j++)
			//~ fscanf (fi, "%02d%*c", &plevel->map[1][i][j]);

	for(layer=0; layer < curr_level->num_layers; layer++)
	{
		for(i=0; i < curr_level->rows; i++)
		{
			for(j=0; j < curr_level->cols; j++)
			{
				fscanf(fi, "%02d%*c", &curr_level->map[layer][i][j]);
			}
		}
	}

  fclose(fi);
  return 0;
}

void saveMap()
{
	int i,j, layer;
	FILE * fp;
	
	char filename[100];
	sprintf(filename, "maps/%s.map", curr_level->name);

	fp = fopen(filename, "wb");
	if (fp == NULL)
	{
		printf("error\n");
	  return;
	}

	/* (Level title) */
	fprintf(fp, "%s\n", curr_level->name);

	/* (Level title) */
	fprintf(fp, "%s\n", curr_level->gset);

	/* (Song file for this level) */
	//~ fprintf(fp, "%s\n", curr_level->song_title);
	fprintf(fp, "dummy.wav\n");

	//Background color
	//~ fscanf (fi, "%6x", &plevel->background_color);
	fprintf(fp, "0,0,0\n");

	//Layers
	fprintf(fp, "%d\n", curr_level->num_layers);

	/* (Level width) & (Level height) */
	fprintf(fp, "%02d,%02d\n\n", curr_level->cols, curr_level->rows);

	for(layer=0; layer < curr_level->num_layers; layer++)
	{
		printf("layer: %d\n", layer);
		for(i=0; i < curr_level->rows; i++)
		{
			for(j=0; j < curr_level->cols; j++)
			{
				fprintf(fp, "%d,", curr_level->map[layer][i][j]);
			}
			fprintf(fp, "\n");
		}

		//Give an empty line between layers
		fprintf(fp, "\n");
	}

	fclose(fp);
	
	saveEvents();
}

void debugLevel(Level* plevel)
{
	int i,j,layer;
	
	printf("[Debug] Level:\n");
	printf("Name: %s\n", plevel->name);
	printf("song_title: %s\n", plevel->song_title);
	
	for(layer=0; layer < plevel->num_layers; layer++)
	{
		printf("Layer: %d\n", layer);
		for(i=0; i < plevel->rows; i++)
		{
			for(j=0; j < plevel->cols; j++)
			{
				printf ("%d,", plevel->map[layer][i][j]);
			}
			printf("\n");
		}
		printf("\n\n");
	}

	printf("rows: %d\n", plevel->rows);
	printf("cols: %d\n", plevel->cols);
}

int getLayerSize(Level *plevel)
{
	int size = sizeof(plevel->curr_layer)/sizeof(int);
	printf("%d\n",size);
	
	return size;
}

void freeLevel(Level *plevel)
{
	printf("Free level.\n");
	if(plevel->map>0)
	{
		free(plevel->map);
	}
}
