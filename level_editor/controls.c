/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   controls.c - User input handling                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 * 						Written by Cerullo Davide                     *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/

#include "controls.h"
#include "util.h"
#include "mouse.h"

void keyboardInput()
{
	while (SDL_PollEvent(&event))
	{
		keystate = SDL_GetKeyState( NULL );
		mousestate = SDL_GetMouseState(&Mouse.x, &Mouse.y);
		
		switch (event.type)
		{
			case SDL_KEYDOWN:

			if (event.key.keysym.sym == SDLK_ESCAPE)
				quit = 1;

				//~ if (keystate[SDLK_LEFT])
				//~ 
				//~ if (keystate[SDLK_RIGHT])
				//~ 
				//~ if (keystate[SDLK_UP])
				//~ 
				//~ if (keystate[SDLK_DOWN])

			break;

			case SDL_KEYUP:
				
				#ifdef DEBUG
					printf("keyup\n");
				#endif

				//~ if (keystate[SDLK_LEFT])
				//~ 
				//~ if (keystate[SDLK_RIGHT])
				//~ 
				//~ if (keystate[SDLK_UP])
				//~ 
				//~ if (keystate[SDLK_DOWN])

			break;

			case SDL_MOUSEBUTTONDOWN:
				#ifdef DEBUG
					printf("mouse button down\n");
				#endif
			break;

			case SDL_QUIT:
				quit = 1;
			break;
		}
	}
}

/**
 * Funzione privata per la cancellazione del testo digitato dall'utente
 **/
void eraseInput(char *string)
{
	//If backspace was pressed and the string isn't blank
	if( ( keystate[SDLK_BACKSPACE] ) && ( strlen(string) != 0 ) )
	{
		//Remove a character from the end
		//~ str.erase( str.length() - 1 );
		char *tmp;
		
		tmp = substr(string, 0, strlen(string)-1);
		strcpy(string, tmp);
		
		keystate[SDLK_BACKSPACE] = 0;
	}
}

/**
 * Gestisce l'input da tastiera per la scrittura di testo
 * 
 * @param char *string
 * 		Stringa alla quale appendere l'input dell'utente
 **/
void textInput(char *string, int length)
{
	SDL_EnableUNICODE( SDL_ENABLE );

	//If the key is a space
	if( event.key.keysym.unicode == (Uint16)' ' )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}

	//If the key is a uppercase letter
	else if( ( event.key.keysym.unicode >= (Uint16)'A' ) && ( event.key.keysym.unicode <= (Uint16)'Z' ) )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}
	//If the key is a lowercase letter
	else if( ( event.key.keysym.unicode >= (Uint16)'a' ) && ( event.key.keysym.unicode <= (Uint16)'z' ) )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}

	eraseInput(string);
	
	//Stop key repeating!
	event.key.keysym.unicode = 0;
}

void numericInput(char *string, int length)
{
	SDL_EnableUNICODE( SDL_ENABLE );

	//If the key is a number
	if( ( event.key.keysym.unicode >= (Uint16)'0' ) && ( event.key.keysym.unicode <= (Uint16)'9' ) )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}

	eraseInput(string);

	//Stop key repeating!
	event.key.keysym.unicode = 0;
}

void miscInput(char *string, int length)
{
	SDL_EnableUNICODE( SDL_ENABLE );

	//If the key is a number
	if( ( event.key.keysym.unicode >= (Uint16)'0' ) && ( event.key.keysym.unicode <= (Uint16)'9' ) )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}

	//If the key is a space
	if( event.key.keysym.unicode == (Uint16)' ' )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}

	//If the key is a comma
	if( event.key.keysym.unicode == (Uint16)',' )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}

	//If the key is a uppercase letter
	else if( ( event.key.keysym.unicode >= (Uint16)'A' ) && ( event.key.keysym.unicode <= (Uint16)'Z' ) )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}
	//If the key is a lowercase letter
	else if( ( event.key.keysym.unicode >= (Uint16)'a' ) && ( event.key.keysym.unicode <= (Uint16)'z' ) )
	{
		//Append the character
		appendChar(string, (char)event.key.keysym.unicode, length);
	}

	eraseInput(string);
	
	//Stop key repeating!
	event.key.keysym.unicode = 0;
}
