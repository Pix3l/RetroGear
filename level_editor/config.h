/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *             conifg.h - Common game functions and settings          *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 * 02/06/2012                                                         *
 *********************************************************************/ 

#ifndef _CONFIG_H
#define _CONFIG_H

#define SCREEN_WIDTH 336
#define SCREEN_HEIGHT 256

#include <time.h>
#include <dirent.h>
#include <SDL/SDL.h>

#include "util.h"

#endif
