/**********************************************************************
 *                                                                    *
 *                              RetroGear                             *
 *                                                                    *
 *                   SDL based generic 2D game engine                 *
 *                   tile.c - Tiles related functions                 *
 *                                                                    *
 *       This game is released as open source under GPL license       *
 *        http://creativecommons.org/licenses/GPL/2.0/deed.it         *
 *             http://www.gnu.org/licenses/gpl-2.0.txt                *
 *   This game is provided without warranty, use it at your own risk! *
 *                                                                    *
 *                     pix3lworkshop.altervista.org                   *
 *                                                                    *
 *********************************************************************/ 

#include "tile.h"
#include "gfx.h"
#include "camera.h"
#include "editor.h"

/**
 * Inizializza la superficie tiles con uno sprite sheet contenente le 
 * immagini.
 * 
 **/
void initTiles()
{
	if(curr_level==NULL)
	{
		printf("Errore: Struttura di livello non inizializzata correttamente\n");
		return;	
	}
	
	//Colore di default: VERDE 0x00FF00
	Uint32 key = SDL_MapRGB( screen->format, 0, 255, 0 ); 
	tiles=loadImage("data/tile_sheet.bmp", key);
}

/**
 * Controlla se una coordinata rientra o meno in un possibile tile del
 * livello in base alla grandezza di esso definita globalmente.
 * 
 * Utile per controllare l'allineamento di una Entity su un'ipotetica
 * griglia di gioco
 * 
 * @param int x, int y
 * 		Coordinate da controllare sul campo di gioco
 **/
int isInTile(int x, int y)
{
	int cols, rows;
	// Se non c'e' resto siamo all'interno del tile
	cols = !(x%TILESIZE); //Asse x
	rows = !(y%TILESIZE); //Asse y
	// Ritorniamo 0 se siamo nel tile
	if(cols==1 && rows==1)
	 return 1;

	return 0; // Non e' perfettamente nel tile
}

/**
 * Controlla eventuali collisioni alle coordinate specificate
 * 
 * @param int x, int y
 * 		Coordinate da controllare sul campo di gioco
 * @param int w, int h
 * 		Larghezza e altezza della entity
 * @param int type
 * 		Valore del tile nel file di livello (Tipologia)
 * @param unsigned int layer
 * 		Il layer su cui controllare la collisione del tile
 **/
int tileCollision(int x, int y, int w, int h, int type, unsigned int layer)
{
    int i, j;
    int minx, miny, maxx, maxy;
    // Ritorna una collisione se si esce dal campo di gioco
    if (x < 0 || (x + w) > TILESIZE * curr_level->cols ||
        y < 0 || (y + h) > TILESIZE * curr_level->rows)
        return 1;

    // Convertiamo le coordinate da pixel a tiles
    minx = x / TILESIZE;
    miny = y / TILESIZE;
    
    maxx = (x + w - 1) / TILESIZE;
    maxy = (y + h - 1) / TILESIZE;

    // Ritorniamo una collisione all'intersezione del tile
    for (i = minx; i <= maxx ; i++)
    {
        for (j = miny ; j <= maxy ; j++)
        {
            if (curr_level->map[layer][j][i]==type)
            	return 1;
        }
    }
    // Nessuna collisione
    return 0;
}

/**
 * void drawTile(SDL_Surface *tiles, int tile, SDL_Rect *dest)
 * 
 * Disegna un tile specifico in base al valore di indice passato in
 * argomento
 * 
 * @param SDL_Surface *tiles
 * 		Superficie contenente i vari tiles
 * @param int tile
 * 		Valore indicativo del tile da prelevare
 * @param SDL_Rect *dest
 * 		Destinazione del tile sulla superficie principale
 *
 **/
void drawTile(SDL_Surface *tiles, int tile, SDL_Rect *dest)
{
	SDL_Rect pick;

	//Di default ci si aspetta 5 colonne
	pick.x=(tile % 6)*TILESIZE;
	pick.y=(tile / 6)*TILESIZE;
	pick.w=TILESIZE;
	pick.h=TILESIZE;

	if(tile!=0)	//No need to draw empty tile
	 SDL_BlitSurface(tiles,&pick,screen, dest);
}

/**
 * void drawTileMap(SDL_Rect)
 * 
 * Disegna la mappa di gioco, con relativa grafica in tiles.
 * La porzione disegnata e' solamente quella attualmente visibile, 
 * rappresentata dall'apposita struttura SDL_Rect.
 * 
 * @param SDL_Rect rect
 *      Rettangolo rappresentativo della porzione di matrice attualmente
 * 		letta.
 * 
 **/
void drawTileMap(SDL_Rect camera_rect, Level *plevel)
{
	int x, y;

	// Destinazione del tile
	SDL_Rect dest;
	
    // Limitiamo il disegno solo ai tiles visibili
    for (y = camera_rect.y; y < camera_rect.h; y++)
    {
        for (x = camera_rect.x; x < camera_rect.w; x++)
        {
			dest.x = (x * TILESIZE) + camera.offsetX;
			dest.y = (y * TILESIZE) + camera.offsetY;
			dest.w = dest.h = TILESIZE;

        	//~plevel->curr_layer
        	drawTile(tiles, plevel->map[1][y][x], &dest);
		}
    }
}

/**
 * Funzione specifica per l'editor di livello.
 * Disegna il layer 1 della mappa, contenente le entity del livello e i
 * tile solidi.
 * 
 * @param SDL_Rect rect
 *      Rettangolo indicante la porzione di matrice attualmente letta.
 * 
 * @param Level *plevel
 * 		Puntatore ad una struttura di livello specifica
 * 
 * @param SDL_Surface *sheet
 * 		Puntatore allo sprite sheet di riferimento del layer
 **/
void drawEntityMap(SDL_Rect camera_rect, Level *plevel, SDL_Surface *sheet)
{
	int x, y;

	// Destinazione del tile
	SDL_Rect dest;

	int layer;
	for(layer=0; layer < curr_level->num_layers; layer++)
	{
		// Limitiamo il disegno solo ai tiles visibili
		for (y = camera_rect.y; y < camera_rect.h; y++)
		{
			for (x = camera_rect.x; x < camera_rect.w; x++)
			{
				dest.x = (x * TILESIZE) + camera.offsetX;
				dest.y = (y * TILESIZE) + camera.offsetY;
				dest.w = dest.h = TILESIZE;

				//~ drawTile(sheet, plevel->map[curr_level->layer][y][x], &dest);
				
				drawTile(tile_sheet, plevel->map[1][y][x], &dest);
				drawTile(obj_sheet, plevel->map[0][y][x], &dest);
				drawTile(alpha_sheet, plevel->map[2][y][x], &dest);
			}
		}
	}

}
